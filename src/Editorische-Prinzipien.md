---
title: Editorische Prinzipien
lang: de
layout: Layout
---

<h1 class="h1-in-tab-layout">Editorische Prinzipien</h1>

<div class="EUPT-EdPrinz-tabs">

<input id="tab1" type="radio" name="tabs" checked="checked">
<label class="EUPT-EdPrinz-tabs-label" for="tab1"><p>Transliteration</p></label>
<div class="EUPT-EdPrinz-tabs-content">

Die ugaritischen poetischen Texte werden in EUPT zunächst transliteriert. Die keilalphabetischen Texte werden also in lateinische Schrift übertragen. Sämtliche Alphabetzeichen werden kursiv wiedergegeben. Anders als z. B. in CTA oder KTU<sup>3</sup> spiegelt die Schriftlage nicht den Erhaltungszustand der Zeichen wider. Beschädigte Zeichen werden durch halbe eckige Klammern (⸢<i>a</i>⸣) ausgewiesen.

Die Aleph-Zeichen werden mit den Vokalbuchstaben {a}, {i} und {u} wiedergegeben (statt mit {ả}, {ỉ} und {ủ}, wie z. B. in DUL<sup>3</sup> oder <bibl>Bordreuil / Pardee, 2009<VuepressApiPlayground url="/api/eupt/biblio/HY4FG9D7" method="get" :data="[]"/></bibl>). Präpositionen, Konjunktionen und Partikeln, auf die kein Worttrenner folgt, werden in der Transliteration nicht vom folgenden Wort getrennt.

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Sigel</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td><i>a</i><sup>?</sup></td>
        <td>Nicht sicher identifizierbares Zeichen</td></tr>
    <tr>
        <td>⸢<i>a</i>⸣</td>
        <td>Beschädigtes Zeichen</td></tr>
    <tr>
        <td>[<i>a</i>]</td>
        <td>Abgebrochenes, ergänztes Zeichen</td></tr>
    <tr>
        <td>[<i>a</i><sup>?</sup>]</td>
        <td>Unsichere Ergänzung</td></tr>
    <tr>
        <td>[[<i>a</i>]]</td>
        <td>Getilgtes oder überschriebenes Zeichen</td></tr>
    <tr>
        <td>&lt;<i>a</i>&gt;</td>
        <td>Ausgelassenes, ergänztes Zeichen</td></tr>
    <tr>
        <td>{<i>a</i>}</td>
        <td>Zu tilgendes Zeichen</td></tr>
    <tr>
        <td><i>a</i><sup>!</sup>(Text:&#8239;<i>b</i>)</td>
        <td>Emendiertes Zeichen</td></tr>
    <tr>
        <td>x</td>
        <td>Nicht identifizierbare Reste eines Zeichens</td></tr>
    <tr>
        <td>[x&#8239;x&#8239;x]</td>
        <td>Geschätzte Anzahl abgebrochener Zeichen (Bsp.: [x&#8239;x&#8239;x&#8239;(x)] = „3–4 Zeichen abgebrochen“)</td></tr>
    <tr>
        <td>[…]</td>
        <td>Unbestimmte Anzahl abgebrochener Zeichen</td></tr>
</table>
</details>

</div>


<input id="tab2" type="radio" name="tabs">
<label class="EUPT-EdPrinz-tabs-label" for="tab2"><p>Vokalisation</p></label>
<div class="EUPT-EdPrinz-tabs-content">

In EUPT werden die ugaritischen Texte nicht nur transliteriert, sondern auch vokalisiert. Wenngleich die Vokalisation der primär konsonantischen Texte bis zu einem gewissen Grad hypothetisch bleibt, ist sie unseres Erachtens ein unverzichtbares Mittel, um die philologische Analyse transparent zu machen. Ziel der Vokalisation ist weniger, die exakte phonetische Form zu rekonstruieren, in der die antiken Gelehrten die Texte vortrugen, als vielmehr die editorischen Entscheidungen offenzulegen und unmittelbar nachvollziehbar zu machen.

Die Vokalisation folgt grundsätzlich <bibl>UG&#178;<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl> und <bibl>KWU<VuepressApiPlayground url="/api/eupt/biblio/PJH8HIUP" method="get" :data="[]"/></bibl>. Einige Formen werden jedoch anders vokalisiert als in <bibl>UG&#178;<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl> vorgeschlagen. Außerdem werden bestimmte Formen in „vereinfachter“ Form vokalisiert, d.h. es werden nicht alle möglichen Vokalisationen abgeführt, sondern lediglich <i>eine</i> mögliche (zugunsten der besseren Lesbarkeit). Im Folgenden findet sich ein Überblick über die wichtigsten Vorentscheidungen (work in progress):

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Zur Grammatik</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td><i>Pron.Suff.</i></td>
        <td>Die Pronominalsuffixe der 2. und 3.m./f.Sg. werden mit kurzem Auslautvokal vokalisiert (alt. Langvokal; vgl. <bibl>Tropper / Vita, 2020: 73<VuepressApiPlayground url="/api/eupt/biblio/B7RDHFVP" method="get" :data="[]"/></bibl>; <i>-ka</i> / <i>-ki</i> / <i>-hu</i> / <i>-ha</i> statt <i>-kā</i> / <i>-kī</i> / <i>-hū</i> / <i>-hā</i>).</td></tr>
    <tr>
        <td><i>Term.</i></td>
        <td>Die Terminativ-Endung <i>-H</i> wird /<i>-ah</i>/ vokalisiert (alt. /<i>-āh</i>/, /<i>-ah</i>V/ oder /<i>-āh</i>V/; <bibl>UG&#178; 320<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>).</td></tr>
    <tr>
        <td><i>Lok.</i></td>
        <td>Die Lokativ-Endung wird /<i>-u</i>/ vokalisiert (alt. /<i>-ū</i>/ [zumindest im St.cs. vor Possessivsuffix]; <bibl>UG&#178;<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl> 326–327). Die Suffixkette <i>Lokativ</i> + <i>enklitisches -M</i> wird /<i>-uma</i>/ vokalisiert (Lok. + EP <i>-M</i>; alt. /<i>-um</i>/ [Lok. + Mimation] oder /<i>-umma</i>/ [Lok. + Mimation + EP <i>-M</i>]; <bibl>UG&#178; 327<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>).</td></tr>
    <tr>
        <td><i>Energ. II</i></td><td>Das Energikussuffix <i>-NN</i> (Energikus II + OS 3.m./f.Sg.) wird /<i>-ninnu</i>/ bzw. /<i>-ninna</i>/ vokalisiert (die Qualität des Vokals der ersten Silbe ist unsicher; vgl. <bibl>UG&#178; 502<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>).</td></tr>
    <tr>
        <td><i>G-Imp.</i></td>
        <td>Für den (anaptyktischen) Vokal der ersten Silbe in <i>{qVtVl}</i>-Imperativformen (G-Stamm) wird dieselbe Qualität vorausgesetzt wie für den Themavokal (bei Themavokal /<i>a</i>/ ist statt <i>{qatal}</i> auch <i>{qital}</i> denkbar; <bibl>UG&#178; 426<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>). Der anaptyktische Vokal wird nicht hochgestellt (vgl. auch <bibl>Sivan, 1997: 120–121<VuepressApiPlayground url="/api/eupt/biblio/2TVWV7UX" method="get" :data="[]"/></bibl>).</td></tr>
    <tr>
        <td><i>D- / Š-PK</i></td><td>Als Präfixvokal der D- und Š-PK-Formen (aktive Variante) wird in der 2. und 3.P. /<i>u</i>/ angesetzt (zum Problem vgl. <bibl>UG&#178; 545–546 / 587–588<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl> und <bibl>Pardee, 2003–2004: 4–5 / 273<VuepressApiPlayground url="/api/eupt/biblio/T7H9G2S4" method="get" :data="[]"/></bibl>).</td></tr>
    <tr>
        <td><i>Verba I-y/w</i></td><td>Der Vokal der ersten Silbe in den G-PK-Formen der I-<i>y</i>/<i>w</i>-Verben wird als kontraktionslang angegeben (zur Diskussion vgl. <bibl>UG&#178; 630–632<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>).</td></tr>
    <tr>
        <td><i>Verba II-gem.</i></td><td>Die endungslosen PK<sup>K</sup>- und Imp.-Formen der II-<i>gem.</i>-Verben werden mit einfachem Konsonanten (reduzierte Geminatengruppe) und ohne Hilfsvokal im Wortauslaut vokalisiert (z. B. PK<sup>K</sup> 3.m.Sg. <i>YSB</i> /<i>yasub</i>/ statt /<i>yasubb</i>V/; vgl. <bibl>UG&#178; 672<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>).</td></tr>
</table>
</details>

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Zu einzelnen Lexemen und Formen</summary>
<table class="edPrinzAbkuerzungenTable">
<tr>
    <td><i>AL</i></td>
    <td>Affirmationspart. „fürwahr, gewiss“; Vok. EUPT: /<i>ˀalla</i>/ (&#60; &#42;/<i>ˀan</i>/ [&#60; <i>AN</i> /<i>ˀanna</i>/ „doch, bitte“] + <i>L</i>, Affirmationspart.; <bibl>UG&#178; 805–806<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>).</td></tr>
<tr>
    <td><i>IŠD-</i></td>
    <td>Subst.m. „Bein“; belegt als Du.cs. in den Formen <i>IŠDK</i> „deine Beine“ und <i>IŠDH</i> „seine Beine“; Vok. EUPT: /<i>ˀišdu</i>/ (Nom.m.Sg.) / /<i>ˀišdā-</i>/ (Nom.m.Du. St.cs.); <bibl>KWU 11 s.v. <i>išd-</i><VuepressApiPlayground url="/api/eupt/biblio/PJH8HIUP" method="get" :data="[]"/></bibl>: <span style="white-space: nowrap;">/<i>ˀišdâ-</i>/</span> (Nom.m.Du. St.cs.); <bibl>Bordreuil / Pardee, 2009: 296<VuepressApiPlayground url="/api/eupt/biblio/HY4FG9D7" method="get" :data="[]"/></bibl>: /<i>ˀišdu</i>/ (Nom.m.Sg.).</td></tr>
<tr>
    <td><i>BN</i></td>
    <td>Subst.m. „Sohn“; Vok. EUPT: /<i>binu</i>/ (Nom.m.Sg.) / /<i>banū-</i>/ (Nom.m.Pl. St.cs.); vgl. auch <bibl>Huehnergard, 2012: 144 s.v. <i>bn</i>&#185;<VuepressApiPlayground url="/api/eupt/biblio/K5V632Z4" method="get" :data="[]"/></bibl>.</td></tr>
<tr>
    <td><i>BT</i></td>
    <td>Subst.f. „Tochter“; Vok. EUPT: /<i>bittu</i>/ (Nom.f.Sg.) / /<i>banātu</i>/ (Nom.f.Pl.); vgl. auch <bibl>Huehnergard, 2012: 145 s.v. <i>bt</i>&#185;<VuepressApiPlayground url="/api/eupt/biblio/K5V632Z4" method="get" :data="[]"/></bibl>.</td></tr>
<tr>
    <td><i>L</i></td>
    <td>Affirmationspart. „fürwahr, gewiss“; Vok. EUPT: /<i>la</i>/; <bibl>UG&#178; 810<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>: /<i>la</i>/ od. /<i>lu</i>/ od. /<i>lū</i>/; <bibl>KWU 62 s.v. <i>l</i>&#8324;<VuepressApiPlayground url="/api/eupt/biblio/PJH8HIUP" method="get" :data="[]"/></bibl>: /<i>la</i>/ od. /<i>lū</i>/.</td></tr>
<tr>
    <td><i>L</i></td>
    <td>Vokativpart. „o“ (nach <bibl>UG&#178; 804<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl> etymologisch identisch mit der Affirmationspart. <i>L</i>); Vok. EUPT: /<i>la</i>/; <bibl>UG&#178; 804<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>: /<i>la</i>/<sup>?</sup>; <bibl>KWU 62 s.v. <i>l</i>&#8323;<VuepressApiPlayground url="/api/eupt/biblio/PJH8HIUP" method="get" :data="[]"/></bibl>: /<i>lV</i>/.</td></tr>
<tr>
    <td><i>TK</i></td>
    <td>Präp. „inmitten hinein“; &#60; <i>TK</i> /<i>tôku</i>/ „Mitte“, Akk.; Vok. EUPT: /<i>tôka</i>/; <bibl>UG&#178; 772<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>: /<i>tôkâ</i>/; <bibl>Bordreuil / Pardee, 2009: 354<VuepressApiPlayground url="/api/eupt/biblio/HY4FG9D7" method="get" :data="[]"/></bibl>: /<i>tôka</i>/.</td></tr>
</table>
</details>
</div>


<input id="tab3" type="radio" name="tabs">
<label class="EUPT-EdPrinz-tabs-label" for="tab3"><p>Kommentar</p></label>
<div class="EUPT-EdPrinz-tabs-content">
<p>In den Kommentaren zur <a href="/edition.html" target="_self">Edition</a> finden sich Erläuterungen zu Epigraphie, Grammatik, Lexik und Interpretation der Texte. Unvokalisierter ugaritischer Text wird in Großbuchstaben zitiert, vokalisierter Text in Kleinbuchstaben. Die vokalisierte Form einzelner Lexeme ist zwischen zwei Schrägstriche gestellt (Bsp.: <i>IL</i> /<i>ˀilu</i>/ „Gott“). Akkadische, arabische, aramäische und hebräische (etc.) Wörter werden transliteriert in Kleinbuchstaben zitiert. Sumerogramme in akkadischen Texten erscheinen in geraden Kapitälchen, Determinative und phonetische Komplemente in hochgestellten Kleinbuchstaben. Akkadogramme in hethitischen Texten werden in kursiven Kapitälchen gesetzt, Sumerogramme in geraden Kapitälchen.</p>

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Kommentarrubriken</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td style="font-variant: small-caps;">epi</td>
        <td>epigraphischer Kommentar</td></tr>
    <tr>
        <td style="font-variant: small-caps;">gr</td>
        <td>Kommentar zur Grammatik</td></tr>
    <tr>
        <td style="font-variant: small-caps;">inh</td>
        <td>Kommentar zum Inhalt / zur Interpretation</td></tr>    
    <tr>
        <td style="font-variant: small-caps;">lx</td>
        <td>Kommentar zur Lexik</td></tr>
    <tr>
        <td style="font-variant: small-caps;">poet</td>
        <td>poetologischer Kommentar</td></tr>
    <tr>
        <td style="font-variant: small-caps;">rek</td>
        <td>Kommentar zur Rekonstruktion</td></tr>
</table>
</details>

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Allgemeine Abkürzungen und Sigel</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td>ägypt.</td><td>ägyptisch</td></tr>
    <tr>
        <td>akk.</td><td>akkadisch</td></tr>
    <tr>
        <td>alt.</td><td>alternativ / alternative Deutungsmöglichkeit</td></tr>
    <tr>
        <td>altakk.</td><td>altakkadisch</td></tr>
    <tr>
        <td>ar.</td><td>arabisch</td></tr>
    <tr>
        <td>aram.</td><td>aramäisch</td></tr>
    <tr>
        <td>ass.</td><td>assyrisch</td></tr>
    <tr>
        <td>ed. / Ed.</td><td>ediert bei / Edition</td></tr>
    <tr>
        <td>epigraph.</td><td>epigraphisch</td></tr>
    <tr>
        <td>hebr.</td><td>hebräisch</td></tr>
    <tr>
        <td>K</td><td>Konsonant</td></tr>
    <tr>
        <td>Kol.</td><td>Kolumne</td></tr>
    <tr>
        <td>Komm.</td><td>Kommentar</td></tr>
    <tr>
        <td>Lit.</td><td>Forschungsliteratur (in „mit Lit.“)</td></tr>
    <tr>
        <td>Par.</td><td>Parallelstelle</td></tr>
    <tr>
        <td>phil.</td><td>philologisch</td></tr>
    <tr>
        <td>rek.</td><td>rekonstruiert</td></tr>
    <tr>
        <td>Rs.</td><td>Rückseite</td></tr>
    <tr>
        <td>syr.</td><td>syrisch</td></tr>
    <tr>
        <td>Translit.</td><td>Transliteration</td></tr>    
    <tr>
        <td>u. ö.</td><td>und öfter</td></tr>
    <tr>
        <td>ÜS</td><td>Übersetzung</td></tr>
    <tr>
        <td>V</td><td>Vokal</td></tr>
    <tr>
        <td>Var.</td><td>Deutungsvariante</td></tr>
    <tr>
        <td>Vok.</td><td>a) Vokativ; b) Vokalisation</td></tr>
    <tr>
        <td>Vs.</td><td>Vorderseite</td></tr>
    <tr>
        <td>wörtl.</td><td>wörtlich</td></tr>
    <tr>
        <td>z. St.</td><td>zur (genannten Beleg-)Stelle</td></tr>
    <tr>
        <td>{k}</td><td>Zeichen <i>k</i></td></tr>
    <tr>
        <td>/<i>k</i>/</td><td>Phonem <i>k</i></td></tr>
    <tr>
        <td><i>{qātil}</i></td><td>Morphemtyp <i>{qātil}</i></td></tr>
    <tr>
        <td>\</td><td>Zeilenumbruch</td></tr>
    <tr>
        <td>i, ii, iii (etc.)</td><td>Kolumnenzahl</td></tr>
    <tr>
        <td>I, I, III (etc.)</td><td>Tafelzahl</td></tr>
    <tr>
        <td><sup>?</sup></td><td>unsicher</td></tr>
</table>
</details>

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Abkürzungen und Sigel zur morphologischen Analyse</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td>Adj.</td><td>Adjektiv</td></tr>
    <tr>
        <td>Adv.</td><td>Adverb</td></tr>
    <tr>
        <td>adv.Akk.</td><td>adverbialer Akkusativ</td></tr>
    <tr>
        <td>Affirmationspart.</td><td>Affirmationspartikel</td></tr>
    <tr>
        <td>Akk.</td><td>Akkusativ</td></tr>
    <tr>
        <td>akt.</td><td>aktiv (Diathese)</td></tr>
    <tr>
        <td>D-</td><td>D-Stamm = Verbalstamm mit Gemination des zweiten Radikals</td></tr>
    <tr>
        <td>Dem.Pron.</td><td>Demonstrativpronomen</td></tr>
    <tr>
        <td>Dp-</td><td>Dp-Stamm = passive Variante des D-Stamms des Verbs</td></tr>
    <tr>
        <td>Energ.</td><td>Energikussuffix</td></tr>
    <tr>
        <td>EP</td><td>enklitische Partikel</td></tr>
    <tr>
        <td>Existenzpart.</td><td>„Existenzpartikel“ (<bibl>UG&#178; 819–822<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>)</td></tr>
    <tr>
        <td>f.</td><td>feminin</td></tr>
    <tr>
        <td>G-</td><td>G-Stamm = Grundstamm des Verbs</td></tr>
    <tr>
        <td>Gen.</td><td>Genitiv</td></tr>
    <tr>
        <td>GN</td><td>Göttername</td></tr>
    <tr>
        <td>Gp-</td><td>Gp-Stamm = passive Variante des Grundstamms des Verbs</td></tr>
    <tr>
        <td>Gt-</td><td>Gt-Stamm = Variante des Grundstamms des Verbs mit <i>t</i>-Infix</td></tr>
    <tr>
        <td>Imp.</td><td>Imperativ</td></tr>
    <tr>
        <td>Inf.</td><td>Infinitiv</td></tr>
    <tr>
        <td>Interj.</td><td>Interjektion</td></tr>
    <tr>
        <td>Interr.Adv.</td><td>Interrogativadverb</td></tr>
    <tr>
        <td>Interr.Pron.</td><td>Interrogativpronomen</td></tr>
    <tr>
        <td>Konj.</td><td>Konjunktion</td></tr>    
    <tr>
        <td>L-</td><td>L-Stamm = Verbalstamm mit Längung des ersten Stammvokals</td></tr>
    <tr>
        <td>Lok.</td><td>Lokativ</td></tr>
    <tr>
        <td>Lp-</td><td>Lp-Stamm = passive Variante des L-Stamms des Verbs</td></tr>
    <tr>
        <td>m.</td><td>maskulin</td></tr>
    <tr>
        <td>N-</td><td>N-Stamm = Verbalstamm mit <i>n</i>-Präfix</td></tr>
    <tr>
        <td>Neg.</td><td>Negator / Negationspartikel</td></tr>
    <tr>
        <td>Nom.</td><td>Nominativ</td></tr>
    <tr>
        <td>Nom.prop.</td><td>Nomen proprium</td></tr>
    <tr>
        <td>Num.</td><td>Numerale / Zahlwort</td></tr>
    <tr>
        <td>Obl.</td><td>Obliquus</td></tr>
    <tr>
        <td>ON</td><td>Ortsname</td></tr>
    <tr>
        <td>OS</td><td>Objektsuffix</td></tr>
    <tr>
        <td>pass.</td><td>passiv (Diathese)</td></tr>
    <tr>
        <td>Pers.Pron.</td><td>Personalpronomen</td></tr>
    <tr>
        <td>PK</td><td>Präfixkonjugation</td></tr>
    <tr>
        <td>PKK</td><td>Präfixkonjugation, Kurzform</td></tr>
    <tr>
        <td>PKKe</td><td>Präfixkonjugation, erweiterte Kurzform</td></tr>
    <tr>
        <td>PKL</td><td>Präfixkonjugation, Langform</td></tr>
    <tr>
        <td>Pl.</td><td>Plural</td></tr>
    <tr>
        <td>PN</td><td>Personenname</td></tr>
    <tr>
        <td>Poss.Suff.</td><td>Possessivsuffix</td></tr>
    <tr>
        <td>Präp.</td><td>Präposition</td></tr>
    <tr>
        <td>Präp.Phrase</td><td>Präpositionalphrase (Adverbial, Präpositionalattribut oder Präpositionalobjekt)</td></tr>
    <tr>
        <td>Pron.</td><td>Pronomen</td></tr>
    <tr>
        <td>Pron.Suff.</td><td>Pronominalsuffix (u. a. an Präpositionen)</td></tr>
    <tr>
        <td>Ptz.</td><td>Partizip</td></tr>
    <tr>
        <td>Rel.Pron.</td><td>Relativpronomen</td></tr>
    <tr>
        <td>Sg.</td><td>Singular</td></tr>
    <tr>
        <td>SK</td><td>Suffixkonjugation</td></tr>
    <tr>
        <td>St.cs.</td><td>Status constructus (und Status pronominalis)</td></tr>
    <tr>
        <td>Subj.</td><td>Subjunktion</td></tr>
    <tr>
        <td>Š</td><td>Š-Stamm = Verbalstamm mit <i>š</i>-Präfix</td></tr>
    <tr>
        <td>Šp</td><td>Šp-Stamm = passive Variante des Š-Stamms des Verbs</td></tr>
    <tr>
        <td>Št</td><td>Št-Stamm = Variante des Š-Stamms des Verbs mit <i>t</i>-Infix</td></tr>
    <tr>
        <td>tD-</td><td>tD-Stamm = Variante des D-Stamms des Verbs mit <i>t</i>-Präfix</td></tr>
    <tr>
        <td>tL-</td><td>tL-Stamm = Variante des L-Stamms des Verbs mit <i>t</i>-Präfix</td></tr>
    <tr>
        <td>Term.</td><td>Terminativ</td></tr>
    <tr>
        <td>Verbalsubst.</td><td>Verbalsubstantiv</td></tr>
    <tr>
        <td>Vok.</td><td>Vokativ</td></tr>
    <tr>
        <td>Vokativpart.</td><td>Vokativpartikel</td></tr>
    <tr>
        <td>1., 2., 3.</td><td>1., 2., 3. Person</td></tr>
    <tr>
        <td>I-y-Verb (o. Ä.)</td><td>Verb, dessen erster Radikal /<i>y</i>/ ist</td></tr>
</table>
</details>

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Abkürzungen und Sigel zur syntaktischen Analyse</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td>A</td><td>Adverbial (Adverb, Präpositionalphrase oder subordinierter Adverbialsatz)</td></tr>
    <tr>
        <td>Adv.Satz</td><td>Adverbialsatz</td></tr>
    <tr>
        <td><i>Anr.</i></td><td>Anrede</td></tr>
    <tr>
        <td>App.</td><td>Apposition</td></tr>
    <tr>
        <td>App.<sup>O4</sup> (u. Ä.)</td><td>Apposition, bezogen auf das Akkusativobjekt</td></tr>
    <tr>
        <td>Attr.</td><td>Attribut (Genitivattribut, Adjektivattribut, Präpositionalattribut oder Attributsatz)</td></tr>
    <tr>
        <td>Attr.<sup>O4</sup> (u.Ä.)</td><td>Attribut, bezogen auf das Akkusativobjekt</td></tr>
    <tr>
        <td>Attr.Satz</td><td>Attributsatz</td></tr>
    <tr>
        <td><i>Interj.</i></td><td>Interjektion</td></tr>
    <tr>
        <td>O<sub>4</sub></td><td>Akkusativobjekt</td></tr>
    <tr>
        <td>O<sub>4-1</sub></td><td>Akkusativobjekt (eines trivalenten Verbs; es tritt im Satz vor dem O<sub>4-2</sub> auf)</td></tr>
    <tr>
        <td>S</td><td>Subjekt</td></tr>
    <tr>
        <td>SG</td><td>selbständiges Satzglied (Prädikat, Subjekt, Akkusativobjekt, Adverbial)</td></tr>
    <tr>
        <td>SG<sup>N</sup></td><td>selbständiges Satzglied, das nicht das Prädikat des Satzes markiert (i. e. Subjekt, Akkusativobjekt oder Adverbial)</td></tr>
    <tr>
        <td>P</td><td>Prädikat (verbal oder nicht-verbal)</td></tr>
    <tr>
        <td>P<sup>V</sup></td><td>Verbales Prädikat</td></tr>
    <tr>
        <td>P<sup>N</sup></td><td>Nicht-verbales Prädikat in Form eines Nominativs</td></tr>
    <tr>
        <td>P<sup>A</sup></td><td>Nicht-verbales Prädikat in Form eines Adverbs oder einer Präpositionalphrase</td></tr>
    <tr>
        <td>P<sup>+O</sup></td><td>Verbales Prädikat mit Objektsuffix</td></tr>
    <tr>
        <td><i>Präp.Phrase</i></td><td>Präpositionalphrase (Adverbial, Präpositionalattribut oder Präpositionalobjekt)</td></tr>
    <tr>
        <td><i>Rel.Satz</i></td><td>Relativsatz</td></tr>
</table>
</details>

<details class="edPrinzAbkuerzungen">
<summary class="edPrinzAbkuerzungenSummary">Sigel zur Analyse der Versstruktur</summary>
<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td>a, b, c (etc.)</td><td>Element des Kolons, das im parallel gestellten Kolon als selbständiges Segment aufgegriffen (und ggf. umgestellt) wird (entspricht meist einem selbständigen Satzglied, i.e. Prädikat, Subjekt, Akkusativobjekt oder Adverbial)</td></tr>
    <tr>
        <td><u>a</u>, <u>b</u>, <u>c</u> (etc.)</td><td>Syntaktisch selbständige Phrase im Kolon (i.e. ein selbständiger Satz; Teile des Satzes können ausgelassen sein)</td></tr>
    <tr>
        <td>A, B, C (etc.)</td><td>Kolon</td></tr>
    <tr>
        <td><u>A</u>, <u>B</u>, <u>C</u> (etc.)</td><td>Vers</td></tr>
    <tr>
        <td><u><i>A</i></u>, <u><i>B</i></u>, <u><i>C</i></u> (etc.)</td><td>Strophe</td></tr>
    <tr>
        <td>;</td><td>Grenze zwischen zwei syntaktisch selbständigen Phrasen (<u>a</u>, <u>b</u>, <u>c</u> etc.), die innerhalb eines Kolons aufeinanderfolgen</td></tr>
    <tr>
        <td>/</td><td>Grenze zwischen zwei semantisch / grammatisch parallelen Phrasen, die sich innerhalb eines Kolons gegenüberstehen</td></tr>
    <tr>
        <td>//</td><td>a) Grenze zwischen zwei Kola; b) Trenner zwischen zwei Lexemen oder Phrasen, die parallel zueinander auftreten</td></tr>
    <tr>
        <td>|</td><td>Grenze zwischen zwei Versen</td></tr>
    <tr>
        <td>||</td><td>Grenze zwischen zwei Strophen</td></tr>
    <tr>
        <td>a // a</td><td>Das Element a des ersten Kolons entspricht dem Element a des folgenden Kolons wörtlich.</td></tr>
    <tr>
        <td>a // a'</td><td>Das Element a des ersten Kolons korrespondiert semantisch und / oder grammatisch mit dem Element a' des folgenden Kolons (a und a' entsprechen sich jedoch nicht wörtlich).</td></tr>
    <tr>
        <td>a<sub>1</sub> // a<sub>1</sub>' // a<sub>2</sub></td><td>Das Element a<sub>1</sub> des ersten Kolons korrespondiert semantisch und / oder grammatisch mit dem Element a<sub>1</sub>' des folgenden Kolons und gleichzeitig mit dem Element a<sub>2</sub> des dritten Kolons.</td></tr>
    <tr>
        <td>a // b</td><td>Das Element a des ersten Kolons korrespondiert (semantisch / grammatisch) nicht mit dem Element b des folgenden Kolons.</td></tr>
    <tr>
        <td>◌</td><td>Leerstelle infolge der Ellipse eines Kolonteils.</td></tr>
    </table>
    </details>
</div>

<input id="tab4" type="radio" name="tabs">
<label class="EUPT-EdPrinz-tabs-label EUPT-EdPrinz-tabs-last-label" for="tab4"><p>Zitation</p></label>
<div class="EUPT-EdPrinz-tabs-content">

<p>Die keilalphabetischen Tafeln werden in EUPT entsprechend ihrer KTU<sup>(3)</sup>-Nummer zitiert (<bibl>Dietrich / Loretz / Sanmartín, 2013<VuepressApiPlayground url="/api/eupt/biblio/Z4J97DSD" method="get" :data="[]"/></bibl>), die nicht-keilalphabetischen Tafeln gemäß ihrer Fundnummer (RS-Nummer). Eine Konkordanz der verschiedenen Tafelzählungen findet sich <a href="/Korpus.html">hier</a>.</p>

<p>Auf Forschungsliteratur wird in Kurzzitaten verwiesen ([Autor], [Jahr]: [Seite]). Per Klick auf das Kurzzitat wird jeweils das Vollzitat angezeigt. Die Projekt-Bibliographie ist <a href="/Bibliographie.html">hier</a> abrufbar; die bibliographischen Kürzel sind <a href="/Abkuerzungen.html">hier</a> zusammengestellt.</p>

<p>Die Fotos aus der Sammlung des ehemaligen <i>West Semitic Research Project</i> / <i>InscriptiFact</i> (zugänglich über die Website der <a href="https://digitallibrary.usc.edu/CS.aspx?VP3=SearchResult&VBID=2A3BXZS3K8SER&PN=1&WS=SearchResults">USC Libraries</a>) werden nach dem <i>Unique identifier</i> (UC-Nummer) zitiert.</p>

</div>




</div>
