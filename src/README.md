---
title: Edition des ugaritischen poetischen Textkorpus (EUPT)
lang: de
layout: Layout
---

<h1>Edition des ugaritischen poetischen Textkorpus (EUPT)</h1>

<div class="allTextStartseite">

<div class="newsEUPT">
    <div class="newsEUPT-sub-section" id="newsEUPT-sub-section-mobile-on-bottom">
        <div class="newsEUPT-sub-section-h">Schnellzugriff:</div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-link-Edition" class="button-style-link" href="/edition.html"></a></div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-link-Lab" href="/EUPT-Lab/Einfuehrung.html"></a></div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-link-GMU" href="/GMU/Einfuehrung.html"></a></div></div>
    <div class="newsEUPT-sub-section">
        <div class="newsEUPT-sub-section-h">Links:</div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-link-Ugarit-Portal" href="https://uni-goettingen.de/de/431176.html" target="_blank"></a></div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-link-RSTI" href="https://ochre.lib.uchicago.edu/RSTI/" target="_blank"></a></div></div>
    <div class="newsEUPT-sub-section newsEUPT-sub-section-last">
        <div class="newsEUPT-sub-section-h">News: <a href="/News.html" target="_self" style="float: right;">mehr</a></div>
        <div class="newsEUPTEntry"><i>2025-03-05:</i></div><!-- Requires update on release -->
            <div>Update des EUPT-Webportals</div>
        <div class="newsEUPTEntry"><hr class="ruling-news"/></div>
        <div class="newsEUPTEntry"><i>2024-05-07:</i></div>
            <div>Launch des EUPT-Webportals</div>
        <div class="newsEUPTEntry"><i>2023-11-02:</i></div>
            <div><a href="/assets/images-content/EUPT_Kick-Off_Plakat.pdf" target="_blank">EUPT-Auftaktveranstaltung</a></div></div>
</div>

<div class="mainTextStartseite">

<div class="newsEUPT-sub-section" id="newsEUPT-sub-section-mobile-on-top">
        <div class="newsEUPT-sub-section-h">Schnellzugriff:</div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-mobile-on-top-link-Edition" class="button-style-link" href="/edition.html"></a></div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-mobile-on-top-link-Lab" href="/EUPT-Lab/Einfuehrung.html"></a></div>
        <div class="newsEUPTEntry"><a id="newsEUPTEntry-mobile-on-top-link-GMU" href="/GMU/Einfuehrung.html"></a></div></div>

<p>Das EUPT-Webportal bietet eine digitale <a href="/edition.html">Edition</a> der poetischen Texte in ugaritischer Sprache. Die Bearbeitungen werden sukzessive online gestellt. Neben der Edition werden auf dem Webportal Ergebnisse begleitender philologischer und poetologischer Untersuchungen veröffentlicht (im <a href="/EUPT-Lab/Einfuehrung.html">EUPT-<i>Lab</i></a>). Im Bereich <a href="/GMU/Einfuehrung.html">GMU</a> ist die Open-Access-Zeitschrift <i>Göttinger Miszellen zur Ugaritistik</i> zugänglich.</p>

<p>Das Forschungsvorhaben „Edition des ugaritischen poetischen Textkorpus (EUPT). Webportal zur philologischen und poetologischen Analyse“ wird von der <a href="https://www.dfg.de/" target="_blank">Deutschen Forschungsgemeinschaft</a> gefördert (Sachbeihilfe / Langfristvorhaben; Projektnummer: <a href="https://gepris.dfg.de/gepris/projekt/506107876" target="_blank">506107876</a>; Projektstart: 01.08.2023). EUPT wird von einem internationalen Advisory Board wissenschaftlich begleitet und arbeitet eng mit dem Chicagoer Ugarit-Projekt <a href="https://ochre.lib.uchicago.edu/RSTI/" target="_blank">Ras Shamra Tablet Inventory (RSTI)</a> zusammen.</p>

<p><b>Edition:</b> Sämtliche Tafeln werden autographiert, transliteriert, vokalisiert, übersetzt, morphosyntaktisch analysiert und kommentiert. Die Tafeln, die im Musée du Louvre und im British Museum verwahrt sind, werden kollationiert. Die Tafeln, die sich in Syrien befinden und z. Zt. nicht zugänglich sind, werden auf Grundlage der verfügbaren Fotos, Faksimiles und Abgüsse bearbeitet.</p>

<p>Das Vorhaben ist in drei Phasen gegliedert (s. <a href="/Arbeitsplan.html">Arbeitsplan</a>): In den ersten drei Jahren werden die Gedichte über Kirtu (KTU 1.14–16), ˀAqhatu (KTU 1.17–19) und die Rāpiˀūma (KTU 1.20–22) bearbeitet. Die zweite Phase gilt dem Baˁlu-Zyklus (KTU 1.1–6). In der dritten Phase werden die kürzeren mythologischen Texte, Gebete, Beschwörungen und die Ritualtexte bearbeitet, die poetische Formen enthalten. Das Korpus umfasst 68 Tafeln mit fast 4.000 Zeilen.</p>

<p>Die Texteditionen werden fortlaufend auf dem Webportal publiziert und nach der Veröffentlichung stetig überarbeitet. Die deutschen Übersetzungen von <i>ˀAqhatu</i>, <i>Kirtu</i> und <i>Baˁlu</i> werden zudem in der Ausgabe „Das Baal-Epos und andere Erzählungen aus dem alten Syrien“ bei C.H.Beck veröffentlicht (Erscheinungsjahr: 2027).</p>

<p><b>Poetologische Analyse:</b> Die Edition wird mit einer poetologischen Analyse verzahnt: Die Versstruktur wird analysiert, parallele Versmuster werden rekonstruiert und stilistische Merkmale beleuchtet; daneben wird das Motivnetzwerk, das die ugaritische Poesie inhaltlich prägt, untersucht. Wiederkehrende Charakteristika werden mit den Literaturen der Nachbarkulturen verglichen. Der Fokus liegt auf der sumerisch-akkadischen Überlieferung aus der Spätbronzezeit sowie auf alttestamentlichen Texten.</p>

<p><b>Datenbank und Webportal:</b> Das Webportal und die Editionsansicht befinden sich derzeit im Alpha-Status. Die Editionen werden z. Zt. im xml-Format erstellt (angelehnt an die Standards der <a href="https://tei-c.org/" target="_blank">TEI</a>). Gleichzeitig entwickelt das Team der SUB Göttingen eine Graphdatenbank, in der die Texte künftig bearbeitet werden. Im Laufe der Weiterentwicklung wird das EUPT-Webportal sukzessive um neue Funktionen ergänzt.</p>

<p>Die links im Hintergrund sichtbare „Stillende Göttin mit zwei Knaben“ steht im Zentrum eines Bettpaneels aus Elfenbein, das 1952 in Ugarit gefunden wurde. Weitere Motive des nahezu vollständig erhaltenen Paneels zieren die verschiedenen Bereiche der EUPT-Webpage. Die Abbildungen basieren auf Umzeichnungen von Anna Lanaro (<bibl>2015<VuepressApiPlayground url="/api/eupt/biblio/CCUK545G" method="get" :data="[]"/></bibl>). Das EUPT-Team dankt Frau Lanaro für die freundliche Erlaubnis, die Umzeichnungen verwenden zu dürfen.</p>

</div><!--End of mainText-->
</div><!--End of allText-->
