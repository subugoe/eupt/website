---
title: Ugarit und Altes Testament
lang: de
layout: Layout
---

<h1>Ugarit und Altes Testament</h1>

<div class="allTextPagesIncludingFigures">

<figure class="float-right">
    <img src="/assets/images-content/Canaanite_City_States_In_The_Bronze_Age.jpg" alt="Städte und Königtümer in der Levante im 2. und 1. Jts. v. Chr."/>
    <figcaption>Städte und Königtümer in der Levante im 2. und 1. Jts. v. Chr. Bild: <a href="https://commons.wikimedia.org/wiki/File:Canaanite_City_States_In_The_Bronze_Age.svg" target="_blank">Wikimedia Commons</a> (this file is licensed under a <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en" target="_blank">Creative Commons CC BY-SA 4.0 License</a>).</figcaption>
<br/>
    <img src="/assets/images-content/Dussaud_Decouvertes_de_Ras_Shamra_et_l_Ancien_Testament_1937.jpg" alt="Dussaud_1937."/>
    <figcaption>Haupttitel von <bibl>Dussaud, 1937<VuepressApiPlayground url="/api/eupt/biblio/RV4DB98K" method="get" :data="[]"/></bibl>.</figcaption>
</figure>

<div class="mainTextPagesIncludingFigures">
<p style="margin-top: 0;">Nach dem Untergang Ugarits (ca. 1200 v. Chr.) bestand der Traditionsstrom, zu dem die ugaritische Kultur gehörte, an anderen Orten fort. Das beweist vor allem das Alte Testament, das eng mit der poetischen Tradition Ugarits verwandt ist. Die ugaritischen Texte sind von entscheidender Bedeutung, um die alttestamentliche Literatur form- und religionsgeschichtlich zu kontextualisieren.</p>

<p>Die poetische Sprache des Alten Testaments ist eng mit der ugaritischen Poesie verwandt: Der poetische Parallelismus ist das wichtigste Ordnungsprinzip ugaritischer wie hebräischer poetischer Texte. Außerdem tauchen im Alten Testament zahlreiche Wortpaare wieder auf, die schon in der ugaritischen Poesie bezeugt sind. Wie die ugaritische Poesie ist auch die alttestamentliche meist in Bikola (zweigliedrige Verse) gegliedert; an hervorgehobenen Stellen treten aber auch Trikola auf, deren Struktur sich im Licht der ugaritischen Texte viel besser verstehen lässt. Zudem gibt es zahlreiche Vers- und Strophenformen, die in beiden Korpora zu finden sind, z. B. elliptische Konstruktionen, Stufen- oder Terrassenparallelismen.</p>

<p>Die formgeschichtlichen Zusammenhänge zwischen der ugaritischen und hebräischen Poesie sind bislang weder synchron noch diachron hinreichend erforscht. Um die hebräische Versstruktur im Licht der ugaritischen zu objektivieren, wurden verschiedene Zugänge verfolgt: Oswald Loretz und Ingo Kottsieper entwickelten ein System („Kolometrie“), das die Konsonantenzahl zu einem wesentlichen Maßstab macht (<bibl>Loretz / Kottsieper, 1987<VuepressApiPlayground url="/api/eupt/biblio/ID2WKKGK" method="get" :data="[]"/></bibl>). Andere richteten den Blick auf syntaktische Konstruktionen und parallele Strukturen, die sich nicht nur in der hebräischen, sondern auch in der ugaritischen Poesie finden 
(<bibl>Watson, 1975<VuepressApiPlayground url="/api/eupt/biblio/VKZE5CHH" method="get" :data="[]"/></bibl>; <bibl>1976<VuepressApiPlayground url="/api/eupt/biblio/PT8893ED" method="get" :data="[]"/></bibl>; <bibl>1986<VuepressApiPlayground url="/api/eupt/biblio/AAJ26GVN" method="get" :data="[]"/></bibl>; <bibl>1994<VuepressApiPlayground url="/api/eupt/biblio/F67X4D69" method="get" :data="[]"/></bibl>; 
<bibl>Korpel / de Moor, 1986<VuepressApiPlayground url="/api/eupt/biblio/9FAA9WXS" method="get" :data="[]"/></bibl>; <bibl>Pardee, 1988<VuepressApiPlayground url="/api/eupt/biblio/5CCQQMMM" method="get" :data="[]"/></bibl>).</p>

<p>In religionsgeschichtlicher Perspektive tragen die ugaritischen Texte maßgeblich zum Verständnis der alttestamentlichen Darstellungen von Gott, Welt und Mensch bei. Ein Beispiel ist der scharfe Kontrast zwischen Jahwe und Baˁal (ug. Baˁlu), der in den biblischen Texten gezeichnet wird. Er verbildlicht den Gegensatz zwischen dem erwählten Gottesvolk der Israeliten und den Kanaanäern. Im Licht der ugaritischen Texte hat sich dieser Gegensatz und die alttestamentliche Rede über den kanaanäischen Gott Baˁal als polemisches Zerrbild erwiesen. Überraschenderweise zeigt nämlich ein Vergleich ugaritischer und biblischer Zeugnisse, dass der alttestamentliche Jahwe ganz ähnlich wie der ugaritische Baˁlu vorgestellt wurde: Beide Götter wurden als Wettergötter verehrt, die im Gewitter erscheinen (Ps 18,8–16; 29,3–9; 77,17–19; 97,2–5; KTU 1.4 vii 27b–32) und den herbstlich-winterlichen Regen spenden (Ps 65,10–14; KTU 1.16 iii 4b–11). Wie von Baˁlu erzählte man auch von Jahwe, dass er die Ungeheuer des Meeres bezwungen habe; eines dieser Ungeheuer ist der berühmte Leviathan (Ps 74,14; Hi 3,8; Jes 27,1), der aus dem ugaritischen Baˁlu-Zyklus als <i>ltn</i> bekannt ist (KTU 1.5 i 1–2). Und in Psalm 48 wird Jahwes Residenz, der Zion, sogar mit dem nordsyrischen Berg Zaphon gleichgesetzt, der nach den ugaritischen Texten als Wohnstatt Baˁlus galt (KTU 1.101).</p>

<p>Die beträchtlichen Ähnlichkeiten, die sich zwischen der ugaritischen und der hebräischen Dichtung zeigen, haben schon bald nach der Entdeckung Ugarits für großes Aufsehen gesorgt. Erste einschlägige Studien hierzu wurden von René Dussaud (<bibl>1937<VuepressApiPlayground url="/api/eupt/biblio/RV4DB98K" method="get" :data="[]"/></bibl>), Harold Ginsberg (<bibl>1938<VuepressApiPlayground url="/api/eupt/biblio/64MTGZHV" method="get" :data="[]"/></bibl>) und Robert de Langhe (<bibl>1945<VuepressApiPlayground url="/api/eupt/biblio/ZUF8KPI6" method="get" :data="[]"/></bibl>) vorgelegt. Einen regelrechten Boom erlebte dieser komparatistische Forschungsansatz zwischen 1960 und 1990 (vgl. <bibl>Craigie, 1981<VuepressApiPlayground url="/api/eupt/biblio/RUGSC5M5" method="get" :data="[]"/></bibl>; <bibl>1983<VuepressApiPlayground url="/api/eupt/biblio/ZN4CC2JA" method="get" :data="[]"/></bibl>). In zahlreichen Publikationen widmeten sich v.a. William F. Albright und Mitchell Dahood den Gemeinsamkeiten ugaritischer und hebräischer Poesie und postulierten ein Korpus von „Early Hebrew Poetry“, das im Alten Testament enthalten und zeitgleich mit den ugaritischen Texten entstanden sei (<bibl>Dahood, 1965 / 1968 / 1970<VuepressApiPlayground url="/api/eupt/biblio/J2ZGZ38K" method="get" :data="[]"/></bibl>; <bibl>Albright, 1968<VuepressApiPlayground url="/api/eupt/biblio/VR45NVR8" method="get" :data="[]"/></bibl>). Ergebnisse dieser komparatistischen Arbeit sind in den von Loren Fisher und Stan Rummel publizierten „Ras Shamra Parallels“ gesammelt (<bibl>Fisher, 1972<VuepressApiPlayground url="/api/eupt/biblio/XDF8QVZF" method="get" :data="[]"/></bibl>; <bibl>1975<VuepressApiPlayground url="/api/eupt/biblio/VIJIXJ9I" method="get" :data="[]"/></bibl>; <bibl>Rummel, 1981<VuepressApiPlayground url="/api/eupt/biblio/M6GDBXN4" method="get" :data="[]"/></bibl>). Dagegen wurde der berechtigte Vorwurf laut, die Bedeutung der ugaritischen Texte für die alttestamentliche Exegese werde weit überschätzt („Pan-Ugaritismus“) und die Eigenprägung der Traditionen nicht gebührend berücksichtigt. Zudem lässt sich der Eindruck nicht vermeiden, dass die ugaritischen Texte von der Albright-Schule für ein bestimmtes Bild der Geschichte Israels instrumentalisiert wurden.</p>

<p>Demgegenüber entstanden seit den 1960ern zahlreiche motiv- und religionsgeschichtliche Einzelstudien, die den Vergleich zwischen alttestamentlicher und ugaritischer Überlieferung historisch umsichtiger und differenzierter durchgeführt haben (u. a. 
<bibl>Kaiser, 1959<VuepressApiPlayground url="/api/eupt/biblio/7UW7DDND" method="get" :data="[]"/></bibl>; 
<bibl>Schmidt, 1961<VuepressApiPlayground url="/api/eupt/biblio/HRHPS5T6" method="get" :data="[]"/></bibl>; 
<bibl>Jeremias, 1965<VuepressApiPlayground url="/api/eupt/biblio/XSRP2WUE" method="get" :data="[]"/></bibl>; 
<bibl>1987<VuepressApiPlayground url="/api/eupt/biblio/FS3PXH8P" method="get" :data="[]"/></bibl>; 
<bibl>Lipiński, 1965<VuepressApiPlayground url="/api/eupt/biblio/JXCQX9X7" method="get" :data="[]"/></bibl>; 
<bibl>Day, 1985<VuepressApiPlayground url="/api/eupt/biblio/M6JAGS25" method="get" :data="[]"/></bibl>; 
<bibl>Kloos, 1986<VuepressApiPlayground url="/api/eupt/biblio/MUXF2S9M" method="get" :data="[]"/></bibl>; 
<bibl>Niehr, 1986<VuepressApiPlayground url="/api/eupt/biblio/BD9PRRNK" method="get" :data="[]"/></bibl>; 
<bibl>1998a<VuepressApiPlayground url="/api/eupt/biblio/ECPT9UJV" method="get" :data="[]"/></bibl>; 
<bibl>1998b<VuepressApiPlayground url="/api/eupt/biblio/8W8KE9SW" method="get" :data="[]"/></bibl>; 
<bibl>2003<VuepressApiPlayground url="/api/eupt/biblio/BMWX7H2U" method="get" :data="[]"/></bibl>; 
<bibl>Tropper, 1989<VuepressApiPlayground url="/api/eupt/biblio/PABCEZQG" method="get" :data="[]"/></bibl>; 
<bibl>Parker, 1989<VuepressApiPlayground url="/api/eupt/biblio/IPQMHK79" method="get" :data="[]"/></bibl>; 
<bibl>Lewis, 1989<VuepressApiPlayground url="/api/eupt/biblio/3KP9QU9T" method="get" :data="[]"/></bibl>; 
<bibl>2020<VuepressApiPlayground url="/api/eupt/biblio/4NPUCDGI" method="get" :data="[]"/></bibl>; 
<bibl>Smith, 1990<VuepressApiPlayground url="/api/eupt/biblio/GE39IHJV" method="get" :data="[]"/></bibl>; 
<bibl>1994<VuepressApiPlayground url="/api/eupt/biblio/KK77USAN" method="get" :data="[]"/></bibl>; 
<bibl>2001<VuepressApiPlayground url="/api/eupt/biblio/A5GPKEB2" method="get" :data="[]"/></bibl>; 
<bibl>2007a<VuepressApiPlayground url="/api/eupt/biblio/EBQPUEWW" method="get" :data="[]"/></bibl>; 
<bibl>2007b<VuepressApiPlayground url="/api/eupt/biblio/PCEGT7IU" method="get" :data="[]"/></bibl>; 
<bibl>Smith / Pitard, 2009<VuepressApiPlayground url="/api/eupt/biblio/K6V5DA4V" method="get" :data="[]"/></bibl>; 
<bibl>Theuer, 2000<VuepressApiPlayground url="/api/eupt/biblio/29M9C28H" method="get" :data="[]"/></bibl>; 
<bibl>Kratz, 2003<VuepressApiPlayground url="/api/eupt/biblio/T8SRZGGJ" method="get" :data="[]"/></bibl>; 
<bibl>2004<VuepressApiPlayground url="/api/eupt/biblio/NR324A79" method="get" :data="[]"/></bibl>; 
<bibl>Gulde, 2007<VuepressApiPlayground url="/api/eupt/biblio/5TZ5V2DW" method="get" :data="[]"/></bibl>; 
<bibl>Müller, 2008<VuepressApiPlayground url="/api/eupt/biblio/H9UAJW5M" method="get" :data="[]"/></bibl>; 
<bibl>Wikander, 2014<VuepressApiPlayground url="/api/eupt/biblio/7KP5D5BS" method="get" :data="[]"/></bibl>; 
<bibl>Salo, 2017<VuepressApiPlayground url="/api/eupt/biblio/FMABGI97" method="get" :data="[]"/></bibl>; 
<bibl>Kühn, 2018<VuepressApiPlayground url="/api/eupt/biblio/B8G4T2I7" method="get" :data="[]"/></bibl>; 
<bibl>Töyräänvuori, 2018<VuepressApiPlayground url="/api/eupt/biblio/P2GTV32V" method="get" :data="[]"/></bibl>; 
<bibl>Ayali-Darshan, 2020<VuepressApiPlayground url="/api/eupt/biblio/UU8NNAQV" method="get" :data="[]"/></bibl>; 
s. auch die <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/tags/Ugarit%20und%20die%20Bibel/collection" target="_blank">hier</a> versammelten Studien).</p>

<p>Nicht zuletzt Oswald Loretz, dessen Forschungsbibliothek den Grundstein der <a href="https://uni-goettingen.de/de/ugarit-bibliothek/431177.html" target="_blank">Ugarit-Bibliothek Göttingen</a> bildet, befasste sich eingehend mit motivgeschichtlichen Parallelen zwischen ugaritischen und alttestamentlichen Texten (<bibl>1990<VuepressApiPlayground url="/api/eupt/biblio/V2I4XWFD" method="get" :data="[]"/></bibl>; <bibl>2003<VuepressApiPlayground url="/api/eupt/biblio/JUDDV8RA" method="get" :data="[]"/></bibl>), vor allem zu den Psalmen (<bibl>1979<VuepressApiPlayground url="/api/eupt/biblio/E2WBD9VM" method="get" :data="[]"/></bibl>; <bibl>2002<VuepressApiPlayground url="/api/eupt/biblio/BIBHPWXT" method="get" :data="[]"/></bibl>). 1984 begründete er gemeinsam mit Manfried Dietrich die Reihe „Ugaritisch-Biblische Literatur“ (UBL), in der etwa der Band „Ugarit and the Bible“ (<bibl>Brooke / Curtis / Healey, 1994<VuepressApiPlayground url="/api/eupt/biblio/587SRFWH" method="get" :data="[]"/></bibl>) und die Gedenkschrift für John Gibson „Ugarit, Religion and Culture“ (<bibl>Wyatt / Watson / Lloyd, 1996<VuepressApiPlayground url="/api/eupt/biblio/GSK5NRQ9" method="get" :data="[]"/></bibl>) erschienen sind.</p>

</div><!--End of mainText-->
</div><!--End of allText-->