---
title: Glossar der ugaritischen poetischen Formen
lang: de
layout: Layout
---
<div id="GUPF-Version-Draft-10">
<span class="EUPT-HTML-article-versioning-warning">Version 1.0. This is an outdated version of the site (data may have been updated since this version's publication; styling and functionality are not maintained). For the current version, please return to <a href="/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html">this page</a>.</span>

# {{ $frontmatter.title }}

**Citatio:** Steinberger, Clemens, 2024: "Glossar der ugaritischen poetischen Formen". *EUPT-Laboratory*: <https://eupt.uni-goettingen.de/lab/Glossar_der_ugaritischen_poetischen_Formen.html> [DD.MM.YYYY].  

Die ugaritische Poesie ist durch sprachliche, strukturelle und stilistische Besonderheiten geprägt, die die poetischen Texte von nicht-poetischen unterscheiden. Die Gedichte gliedern sich in verschieden große Verseinheiten, die oft parallel gestaltet sind, und enthalten eine Fülle von Stilmitteln.

Das folgende Glossar gibt einen Überblick über diverse Formen, die in der ugaritischen Dichtkunst belegt sind. Um Vergleiche mit verwandten Poesien zu erleichtern, sind auch einige Stilfiguren genannt, für die sich im ugaritischen Korpus bislang keine Beispiele gefunden haben. Unter jedem Glossar-Eintrag in grüner Schrift findet sich eine knappe Definition des behandelten Phänomens mit einigen Beispielen. Die Liste wird fortlaufend erweitert und verbessert.

Stand: 03.05.2024 (Version 1.0)

<button onclick="document.querySelectorAll('details').forEach((e) => {(e.hasAttribute('open')) ? e.removeAttribute('open') : e.setAttribute('open',true)})">Alle Einträge aus-/einklappen</button>

<style>
    #GUPF-Version-Draft-10 {
.done {
 summary.doneSummary {
        font-weight: normal;
        text-decoration: underline;
        color: var(--c-text-accent);
        margin-left: 20px;
    }
}
.done[open] {
 summary.doneSummary {
        font-weight: bold;
        margin-left: 0px;
    }
    margin-top: 12px;
    margin-bottom: 12px;
 border-color: #ddd;
 border-width: 1px;
 border-style: solid;
    border-radius: 8px;
    padding: 20px;
}
.undone {
    summary {
        font-weight: normal;
        text-decoration: none;
        color: var(--c-text);
        margin-left: 20px;
    }
}
.example {
 summary {
     font-weight: normal;
        text-decoration: underline;
        color: var(--c-text-accent);
    }
}
.example[open] {
 summary {
     font-weight: bold;
    }
}
.exampleTable {
    th {
        padding: .6em 1em .3em;
        font-size: 90%;
        background-color: var(--c-bg-light);
    }
    td {
        padding: .3em 1em;
        font-size: 90%;
        background-color: var(--c-bg-navbar);
    }
    td:nth-child(1) {
        white-space: nowrap;
    }
}
.exampleFirstOrder {
    summary.exampleFirstOrderSummary {
        font-weight: normal;
     text-decoration: underline;
        color: var(--c-text-accent);
    }
}
.exampleFirstOrder[open] {
    summary.exampleFirstOrderSummary {
        font-weight: bold;
    }
}
.exampleSecondOrder {
 summary.exampleSecondOrderSummary {
     font-weight: normal;
        text-decoration: none;
        color: var(--c-text-accent);
    }
    margin-left: 40px;
}
.exampleSecondOrder[open] {
 summary {
     font-weight: bold;
    }
    margin-bottom: 12px;
}
.exampleThirdOrder {
 summary.exampleThirdOrderSummary {
     font-weight: normal;
        text-decoration: none;
        color: var(--c-text-accent);
    }
    margin-left: 40px;
}
.exampleThirdOrder[open] {
 summary.exampleThirdOrderSummary {
     font-weight: bold;
    }
    margin-bottom: 12px;
}
}
</style>

## A

<details class="undone">
<summary>Accumulatio</summary>
</details>

<details class=done>
<summary class="doneSummary">Akrostichon</summary>
<p>Im Akrostichon ergeben die Zeichen, die jeweils am Anfang aufeinanderfolgender Zeilen oder Abschnitte stehen, aneinandergereiht ein Wort, einen Namen oder einen Satz (vgl. <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/CDFUWIJB/item-list">Grimm, 2007</a>). Das Akrostichon ist im ugaritischen Korpus bislang nicht bezeugt.</p>
</details>

<details class=done>
<summary class="doneSummary">Alliteration</summary>
<p>Die Alliteration ist eine Klangfigur. Mindestens zwei Begriffe, die innerhalb eines Kolons aufeinander folgen, klingen gleich an (die alliterierenden Wörter folgen unmittelbar aufeinander). Vermutlich ist nicht jede Alliteration, die sich in den ugaritischen poetischen Texten ausmachen lässt, bewusst gesetzt (s. KTU 1.14 iii 31b-32a oder KTU 1.14 iii 35b-36a). Zuweilen gebrauchten die ugaritischen Dichter die Alliteration aber wohl bewusst, um die Aufmerksamkeit des Publikums auf bestimmte Aussagen zu lenken und diese so hervorzuheben (Alliterationen ließen die Rezipienten des Texts wohl kurz aufhorchen; außerdem haben alliterierend gestaltete Aussagen wahrscheinlich hohen Wiedererkennungswert; s. KTU 1.17 vi 32b-33a). Vielleicht wurden Alliterationen mitunter auch aus euphonischen Gründen (i. e. zugunsten des Wohlklangs) eingesetzt.</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.14 iii 31b-32a</th></tr>
    <tr>
        <td>wa ˀôšānu <sup>32</sup> <b>ˀa</b>bī <b>ˀa</b>dami</td>
        <td>ja, ein Geschenk des Vaters der Menschheit</td></tr>
<tr><th colspan="2">KTU 1.14 iii 35b-36a</th></tr>
    <tr>
        <td>wa <b>ˁa</b>bda <sup>36</sup> <b>ˁā</b>lami ṯalāṯa sus(s)uwīma</td>
        <td>und einen ewig (gebundenen) Knecht (und) drei Pferde</td></tr>
<tr><th colspan="2">KTU 1.14 iii 43a</th></tr>
    <tr>
        <td>dā ˁQ-āha <b>ˀi</b>bbā <b>ˀi</b>qnaˀi</td>
        <td>deren Pupillen (wie) zwei Lapislazuli-Steine sind</td></tr>
<tr><th colspan="2">KTU 1.14 iii 52</th></tr>
    <tr>
        <td><sup>52</sup> <b>yi</b>rtaḥaṣ wa <b>yi</b>ˀˀadim</td>
        <td>Er wusch sich und schminkte sich rot.</td></tr>
<tr><th colspan="2">KTU 1.14 iii 53</th></tr>
    <tr>
        <td><sup>53</sup> <b>y</b>irḥaṣ <b>y</b>adêhu ˀammatah</td>
        <td>Er wusch seine Hände bis zum Ellbogen.</td></tr>
<tr><th colspan="2">KTU 1.14 v 1b-2</th></tr>
    <tr>
        <td>wa bi <sup>2</sup> <b>m</b>aqâri <b>m</b>umalliˀatu</td>
        <td>ja, die an der Quelle (Schläuche) füllte</td></tr>
<tr><th colspan="2">KTU 1.17 vi 32b-33a</th></tr>
    <tr>
        <td><b>ˀa</b>ppV <b>ˀa</b>nāku <b>ˀa</b>ḥawwiya <sup>33</sup> <b>ˀa</b>qhata [ġāzi]ra</td>
        <td>(So) will auch ich beleben ˀAqhatu, [den Hel]den!</td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Alternation</summary>
</details>

<details class="undone">
<summary>Anadiplose</summary>
</details>

<details class="undone">
<summary>Anapher</summary>
</details>

<details class="undone">
<summary>Antonomasie</summary>
</details>

<details class="done">
<summary class="doneSummary">Apokoinu</summary>
<p>Das Apokoinu ist eine Konstruktion, in der sich eine Phrase, ein Satzglied oder ein Satzgliedteil (i. e. das Koinon) gleichzeitig auf die voranstehende und die nachfolgende Phrase bezieht (Sonderform des Zeugmas; vgl. <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/VMKEQ6IB/item-list">Steinhoff / Burdorf, 2007</a> und <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/ADRMUJGR/item-list">Schweikle / Schlösser, 2007</a>). In der ugaritischen Poesie ist die Figur u. a. in der Kolonkonstruktion P<sup>V</sup> &rarr; SG<sup>N</sup> &larr; P<sup>V</sup> bezeugt (zu den Sigeln s. <a href="https://eupt-dev.sub.uni-goettingen.de/Einfuehrung/Editorische_Prinzipien_Kommentar.html">Abkürzungen und Sigel zur Analyse der Versstruktur</a>): Das Kolon setzt sich aus zwei verbalen Prädikaten (P<sup>V</sup>) und einem nominalen Satzglied (SG<sup>N</sup>) zusammen. Das nominale Satzglied steht zwischen den beiden Prädikaten; die Prädikate stehen jeweils am Kolonrand. Weder auf das erste Prädikat (am Kolonanfang) noch auf das nominale Satzglied (in der Kolonmitte) folgt eine Konjunktion. Das nominale Satzglied in der Kolonmitte (i. e. das Koinon) bezieht sich gleichermaßen auf das voranstehende und das nachfolgende Prädikat. In den unten zitierten Beispielen bezeichnet das Koinon jeweils das Subjekt der beiden Prädikate. Das Apokoinu kann als Sonderform eines elliptischen Satzpaars begriffen werden: Die Konstruktion P<sup>V</sup>&rarr;S&larr;P<sup>V</sup> kann auch als P<sup>V</sup>-S / ◌-P<sup>V</sup> (&lt; P<sup>V</sup>-S / S-P<sup>V</sup>) analysiert werden (das Koinon-Element, i. e. das nominal ausgedrückte Subjekt, ist im zweiten Satz ausgelassen).</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.2 i 19b (P&rarr;S&larr;P)</th></tr>
    <tr>
        <td>tabiˁā ġalmāmi lā yaṯabā</td>
        <td>Es machten sich auf &rarr; die (beiden) Jünglinge &larr; verweilten nicht.</td></tr>
<tr><th colspan="2">KTU 1.15 iii 17 (P&rarr;S&larr;P)</th></tr>
    <tr>
        <td><sup>17</sup> tabarrikū ˀilūma taˀtiyū</td>
        <td>Es sprachen den Segen &rarr; die Götter &larr; gingen (heim).</td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Archaismus</summary>
</details>

<details class="undone">
<summary>Assonanz und Konsonanz</summary>
</details>

<details class="done">
<summary class="doneSummary">Asyndeton</summary>
<p>Im Asyndeton sind die Glieder einer Aufzählung, die sich aus zwei oder mehreren syntaktisch gleichrangigen Lexemen oder Phrasen zusammensetzt, ohne Konjunktion aneinandergereiht (<a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/QS82WPVF/item-list">Steinhoff, 2007: 51</a>); die erwartete Konjunktion vor dem letzten Glied der Aufzählung ist ausgelassen (Gegenstück zum &#x2197; Polysyndeton). Gleichgeordnete Nomina / Nominalphrasen (keine vollständigen Sätze), die innerhalb eines Kolons aufeinanderfolgen, sind in der ugaritischen Poesie nur selten asyndetisch aneinandergereiht (s. Beispiele A; vgl. <a href=https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/search/steinberger/titleCreatorYear/items/ZXWKYWAA/item-list>Steinberger, 2022: 302 Anm. 44</a> u.a. zu KTU 1.4 vi 47-54). Im weiteren Sinn ist jedoch jedes Versgefüge als asyndetisch zu betrachten, das sich in zwei oder mehrere Verseinheiten gliedert, vorausgesetzt dass a) in den einzelnen Verseinheiten je <i>ein</i> Teil desselben übergeordneten Sachverhalts beschrieben ist und b) die einzelnen Verseinheiten nicht durch eine Konjunktion miteinander verbunden sind. Solche Konstruktionen finden sich in der ugaritischen Dichtung häufiger (s. Beispiele B).</p>
<details class="example">
<summary>Beispiele A</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.4 vi 47-54</th></tr>
    <tr>
        <td><sup>47</sup> šapîqa ˀilīma karrīma <b>ø</b> yêna<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>48</sup> šapîqa ˀilahāti ḫupārāti(/ ḫapūrāti)<br/>
            <sup>49</sup> šapîqa ˀilīma ˀalapīma <b>ø</b> yê[na]<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>50</sup> šapîqa ˀilahāti ˀaraḫāti<br/>
            <sup>51</sup> šapîqa ˀilīma kaḥ(a)ṯīma<sup>?</sup> <b>ø</b> yêna<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>52</sup> šapîqa ˀilahāti kussiˀāti<br/>
            <sup>53</sup> šapîqa ˀilīma rVḥabāti yêni<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>54</sup> šapîqa ˀilahā&lt;ti&gt; DKR-ā&lt;ti&gt;</td>
        <td>Er reichte den Göttern (junge) Widder (und) Wein,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;er reichte den Göttinnen (weibliche) Lämmer,<br/>
            Er reichte den Göttern Ochsen (und) We[in],<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;er reichte den Göttinnen Kühe,<br/>
            Er reichte den Göttern Herrschersitze (und) Wein,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;er reichte den Göttinnen Throne,<br/>
            Er reichte den Göttern Krüge voll Wein,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;er reichte den Göttinnen Gefäße / (Wein-)Schläuche.</td></tr>
<tr><th colspan="2">KTU 1.14 iii 22-25</th></tr>
    <tr>
        <td><sup>22</sup> qaḥ kaspa wa yarqa<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;ḫurāṣa <sup>23</sup> yada maqâmihu<br/>
            wa ˁabda ˁālami <sup>24</sup> <b>ø</b> ṯalāṯa sus(s)uwīma<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;markabta <sup>25</sup> bi tarbaṣi <b>ø</b> bina ˀam(a)ti</td>
        <td>Nimm Silber und Gelbgold,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;Gold samt seinem Fundort,<br/>
            und einen ewig gebundenen Knecht (und) drei Pferde,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;einen Streitwagen aus (meinem) Stall (und) den Sohn einer Magd!</td></tr>
<tr><th colspan="2">KTU 1.14 iii 2-3a</th></tr>
    <tr>
        <td><sup>2</sup> lik yôma wa ṯānâ<br/>
            ṯāliṯa rābiˁa yôma<br/>
            <sup>3</sup> ḫāmiša ṯādiṯa yôma</td>
        <td>Geh einen Tag lang und einen zweiten,<br/>
            einen dritten, einen vierten Tag,<br/>
            einen fünften, einen sechsten Tag!</td></tr>
</table>
</details>
<p/>
<details class="example">
<summary>Beispiele B</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.3 iii 43–44</th></tr>
    <tr>
        <td><sup>43</sup> maḫaštu môdāda ˀilima ARŠ-a<br/>
            <b>ø</b> <sup>44</sup> ṣammittu ˁigla ˀili ˁTK-a</td>
        <td>Ich schlug nieder den Liebling des ˀIlu, ARŠ,<br/>
            (und) ich vernichtete das Kalb des ˀIlu, ˁTK!</td></tr>
</table>
</details>

</details>

## B

<details class="done">
<summary class="doneSummary">Bikolon</summary>
<p>Das Bikolon ist ein &#x2197; Vers, der sich aus zwei &#x2197; Kola zusammensetzt. Das Bikolon ist die gängigste Versform der ugaritischen Dichtung.</p>
</details>

## C

<details class="undone">
<summary>Chiasmus</summary>
</details>

<details class="undone">
<summary>Constructio ad sensum</summary>
</details>

<details class="done">
<summary class="doneSummary">Conversio</summary>
<p>&#x2197; Geminatio.</p>
</details>

<details class="undone">
<summary>Correctio</summary>
</details>

## D

<details class="done">
<summary class="doneSummary">Distichon</summary>
<p>Das Distichon ist eine &#x2197; Strophe, die sich aus zwei &#x2197; Versen zusammensetzt.</p>
</details>

## E

<details class="undone">
<summary>Ellipse</summary>
</details>

<details class="done">
<summary class="doneSummary">Enjambement</summary>
<p>Im Enjambement sind die Glieder eines zusammenhängenden Satzes (i. d. R. ein Verbalsatz) über zwei (oder mehrere) Verseinheiten verteilt. In der ugaritischen Poesie ist das Enjambement sowohl auf Versebene (i. e. das Kolon-Enjambement zwischen aufeinanderfolgenden Kola) als auch auf Strophenebene belegt (i. e. das das Vers-Enjambement zwischen aufeinanderfolgenden Versen; das Kolon-Enjambement ist öfter belegt als das Vers-Enjambement).</p>
<p>Im Kolon-Enjambement verteilen sich die Glieder eines zusammenhängenden Satzes über zwei (nur in Ausnahmefällen drei) Kola (s. Beispiele A). Die Kola gehören zu <i>einem</i> Vers (der Vers wird als Enjambement-Vers bezeichnet). Nicht selten tritt vor oder hinter die beiden durch Enjambement verbundenen Kola ein weiteres Kolon, das parallel zu einem der beiden Kola steht (so ergibt sich ein &#x2197; Trikolon). Das Kolon des Enjambement-Verses, das des Prädikats des übergeordneten Satzes entbehrt, enthält meist ein einziges selbständiges Satzglied (Subjekt, Akkusativobjekt oder Adverbial; häufig enthält das prädikatlose Kolon ein Adverbial; seltener zwei nicht-prädikativische Satzglieder); die restlichen Satzteile samt dem Prädikat des Satzes stehen im anderen Kolon. Gleichwohl sind die Kola des Enjambement-Verses meist ungefähr gleich lang: Das prädikatlose Kolon setzt sich i. d. R. aus mehreren Worteinheiten zusammen (der nominale Satzteil, der in dem prädikatlosen Kolon steht, ist um ein oder mehrere Attribute / Appositionen erweitert oder setzt sich aus mehreren Nomina zusammen, die in Form einer &#x2197; Accumulatio miteinander verbunden sind).</p>
<details class="example">
<summary>Beispiele A (Kolon-Enjambement)</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.15 ii 11–12</th></tr>
    <tr>
        <td><sup>11</sup> [ˀaḫ(ḫa)]ra maġāyi ˁidati ˀilīma<br/>
            <sup>12</sup> [wa] yaˁnî ˀalˀiyā[nu] baˁlu </td>
        <td>[Nachd]em die Götterversammlung gekommen war,<br/>
            [da] sprach der Mächti[ge], Baˁlu.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: A- // P-S; vgl. auch KTU 1.6 ii 26b–27 (A- // S-P); KTU 1.6 i 9b–10a (A<sub>1</sub>- // P-A<sub>2</sub>-O<sub>4</sub>).</p></td></tr>
<tr><th colspan="2">KTU 1.14 iii 3b–5</th></tr>
    <tr>
        <td>maka šapšuma <sup>4</sup> bi šābiˁi<br/>
            wa tamġiyu li ˀud(u)mi <sup>5</sup> rabbati<br/>
            wa li ˀud(u)mi ṮRR-(a)ti</td>
        <td>Dann, bei Sonnenaufgang, am siebten (Tag),<br/>
            da wirst du nach ˀUd(u)mu, zur großen (Stadt), kommen,<br/>
            ja, nach ˀUd(u)mu, zur starken (Stadt).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: A<sub>1</sub>- // P-A<sub>2</sub> // A<sub>2</sub> &#8594; a- // b-c // ◌-c'; vgl. auch KTU 1.14 iv 32b–36a; KTU 1.4 iv 27–28 (A- // P-O<sub>4</sub> ; P).</p></td></tr>
<tr><th colspan="2">KTU 1.4 iv 20–22</th></tr>
    <tr>
        <td><sup>20</sup> ˀid(d)āka lV tâtin(u) panîma<br/>
            <sup>21</sup> ˁimma ˀili mabbakV nah(a)rêma<br/>
            <sup>22</sup> qarba ˀapīqi<sup>?</sup> tahāmatêma</td>
        <td>Dann machte sie sich auf den Weg<br/>
            zu ˀIlu, zur Quelle der beiden Flüsse,<br/>
            ins Flussbett der beiden Urfluten.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: A<sub>1</sub>-P-O<sub>4</sub>- // A<sub>2</sub> // A<sub>2</sub> &#8594; a-b-c- // d-e // ◌-e'. Zum Enjambement-Vers, dessen zweites Kolon eine durch <i>ˁM</i> eingeleitete Präpositionalphrase enthält, vgl. auch KTU 1.15 i 3–4 (P-S- // A) und KTU 1.24 16–17a (P-S- // A).</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 27–28a</th></tr>
    <tr>
        <td><sup>27</sup> šaˀū ˀilūma raˀašātikumū<br/>
            li ẓûri bir(a)kātikumū<br/>
            lina<sup>?</sup> kaḥ(a)ṯī<sup>?</sup> <sup>28</sup> ZBL-ikumū</td>
        <td>Erhebt, Götter, eure Häupter<br/>
            von euren Knien,<br/>
            von euren fürstlichen Thronen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: P-Anr.-O<sub>4</sub>- // A // A &#8594; a-b-c- // d // d'. Zum Enjambement-Vers, dessen zweites Kolon eine durch <i>L</i> eingeleitete Präpositionalphrase enthält, vgl. auch KTU 1.2 iii 16b–d (P-S- // A // A; a-b- // c // c') und KTU 1.4 v 46b–48a (P-S ; P- // A).</p></td></tr>
<tr><th colspan="2">KTU 1.4 vii 19b–20</th></tr>
    <tr>
        <td>wa [pa]taḥ BDQ-(a)ta ˁarapāti<br/>
            <sup>20</sup> ˁalê/â hawâti kôṯari-wa-ḫasīsi</td>
        <td>Ja, [ö]ffne einen Spalt in den Wolken<br/>
            gemäß dem Wort des Kôṯaru-wa-Ḫasīsu!</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p>Anm.: P-O<sub>4</sub>- // A.</p></td></tr>
<tr><th colspan="2">KTU 1.15 iii 13–15</th></tr>
    <tr>
        <td><sup>13</sup> maˀda râma [Kirtu]<br/>
            <sup>14</sup> bi tôki rāpiˀī ˀar[ṣi]<br/>
            <sup>15</sup> bi puḫri QBṢ-i ditāni</td>
        <td>Hoch erhaben ist [Kirtu]<br/>
            inmitten der Rāpiˀūma der 'Er[de'],<br/>
            in der Zusammenkunft der Versammlung des Ditānu.</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p>Anm.: A<sub>1</sub>-P-S- // A<sub>2</sub> // A<sub>2</sub> &#8594; a-b-c- // d // d'.</p></td></tr>
<tr><th colspan="2">KTU 1.14 i 28–30</th></tr>
    <tr>
        <td><sup>28</sup> tinnatikna ˀudmaˁātuhu<br/>
            <sup>29</sup> kama ṯiqalīma ˀarṣah<br/>
            <sup>30</sup> kama ḫamušāti maṭṭâtah</td>
        <td>Seine Tränen ergossen sich<br/>
            wie Schekel zur Erde,<br/>
            wie Fünftel (eines Schekels) aufs Bett.</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p>Anm.: P-S- // A<sub>1</sub>-A<sub>2</sub> // A<sub>1</sub>-A<sub>2</sub> &#8594; a-b- // c-d // c'-d'.</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 25d–26</th></tr>
    <tr>
        <td>ˀaḥda<sup>?</sup> <sup>26</sup> ˀilūma taˁniyū<br/>
            lûḥāti malˀakê yammi<br/>
            taˁûdati ṯāpiṭi nah(a)&#9001;ri&#10217;</td>
        <td>Einstimmig mögen die Götter antworten<br/>
            den Tafeln der Boten des Yammu,<br/>
            der Gesandtschaft des Herrschers Nah(a)ru.</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p>Anm.: A-S-P- // O<sub>4</sub> // O<sub>4</sub> &#8594; a-b-c- // d<sub>x-y-z</sub> // d'<sub>◌-y'-z'.</sub></p></td></tr>
<tr><th colspan="2">KTU 1.6 i 56–57</th></tr>
    <tr>
        <td><sup>56</sup> ˀappūnaka ˁaṯtaru ˁarīẓu<sup>?</sup><br/>
            <sup>57</sup> yaˁlû/î bi ṢRR-āti ṣapāni</td>
        <td>Sodann (ist) ˁAṯtaru, der Starke,<br/>
            hinaufgestiegen auf die Höhen<sup>?</sup> des Zaphon.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: A<sub>1</sub>-S- // P-A<sub>2</sub>. Zum Enjambement-Vers, dessen erstes Kolon die Phrase <i>APNK</i> - <i>Subjekt<sub>Nomen proprium + Epitheton</sub></i> enthält, vgl. auch KTU 1.5 vi 11–14a (A<sub>1</sub>-S- // P-A<sub>2</sub> ; P-A<sub>2</sub> // A<sub>2</sub> ; P-A<sub>2</sub>; a-b- // c-d ; e-f // ◌-d' ; e'-f') und KTU 1.15 ii 8–9a (A<sub>1</sub>-S- // O<sub>4</sub>-A<sub>2</sub>-P).</p></td></tr>
</table>
</details>
<p>Im Vers-Enjambement sind die Glieder eines zusammenhängenden Satzes über zwei oder mehrere Verse verteilt, die zur selben Strophe gehören (die Strophe wird als Enjambement-Strophe bezeichnet; s. Beispiele B). Die durch Enjambement verbundenen Verse sind meist &#x2197; Bikola. Der Satzteil, der im ersten Kolon jedes Verses steht, wird im zweiten Kolon des Verses parallel aufgegriffen.</p>
<details class="example">
<summary>Beispiele B (Vers-Enjambement)</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.14 iv 40–43</th></tr>
    <tr>
        <td><sup>40</sup> himma ḥurriya bêtaya <sup>41</sup> ˀiqqaḥu // ˀašaˁribu ġalmata <sup>42</sup> ḥaẓiraya<br/>
            ṯinêha kaspima<sup>!</sup> <sup>43</sup> ˀâtina // wa ṯalāṯataha ḫurāṣima</td>
        <td>Wenn ich Ḥurriya in mein Haus nehmen kann, // das Mädchen in meine Wohnstatt führen kann,<br/>
            (dann) will ich ihr Doppeltes an Silber darbringen, // ja, ihr Dreifaches an Gold!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: A- // (…) ǀ O<sub>4</sub>-P // (…) &#8594; a<sub><i>HM</i>-x-y-z</sub>- // a'<sub>◌-z'-x'-y'</sub>- ǀ b-c // b'-◌.</p></td></tr>
<tr><th colspan="2">KTU 1.19 iii 42b–45a</th></tr>
    <tr>
        <td>kanapê našarīma <sup>43</sup> baˁlu yaṯbi/ur // baˁlu yaṯbi/ur DˀIY-ê <sup>44</sup> humūti<br/>
            himma taˁûpūna ˁalê/â qubūri biniya // <sup>45</sup> tašaḫîṭūnaninnu bi šinatihu</td>
        <td>Baˁlu möge die Flügel der Adler zerbrechen, // Baˁlu möge die Schwingen von jenen zerbrechen,<br/>
        wenn sie über das Grab meines Sohnes fliegen, // (und) ihn wecken aus seinem Schlaf!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: O<sub>4</sub>-S-P // (…) ǀ -A // (…) &#8594; a-b-c // b-c-a' ǀ -d // e.</p></td></tr>
<tr><th colspan="2">KTU 1.17 v 4b–7a (Par. KTU 1.19 i 19b–23a)</th></tr>
    <tr>
        <td>ˀappūnaka danīˀilu mutu <sup>5</sup> rāpiˀi // ˀa&#9001;ppV&#10217;hinnā ġāziru mutu harnamī[yi]<sup>?</sup><br/>
            <sup>6</sup> yittaša/iˀu yâṯib(u) bi ˀappi/ê ṯaġri // taḥta <sup>7</sup> ˀadurīma dā bi gurni</td>
        <td>Sodann (ist) Danīˀilu, der Mann des Rāpiˀu, // sodann (ist) der Held, der Mann des Harnamiten,<br/>
            aufgestanden (und) hat sich an der Vorderseite des Tores gesetzt, // unter den Noblen, die bei der Tenne (waren).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: A<sub>1</sub>-S- // (…) ǀ P ; P-A<sub>2</sub> // (…) &#8594; a-b- // a'-b'- ǀ c ; d-e // ◌ ; ◌-e'; vgl. auch KTU 1.17 i 0–3a: A<sub>1</sub>-S- // (…) ǀ A<sub>2</sub>-O<sub>4</sub>-P // (…) &#8594; a-b- // a'-b'- ǀ c-d-e // c-e'-d'.</p></td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Epipher</summary>
</details>

<details class="done">
<summary class="doneSummary">Enumeratio</summary>
<p>&#x2197; Accumulatio.</p>
</details>

<details class="undone">
<summary>Erzählzeit und erzählte Zeit</summary>
</details>

<details class="done">
<summary class="doneSummary">Euphemismus</summary>
<p>Der Euphemismus ist eine Form der &#x2197; Periphrase. Ein negativ konnotierter oder tabuisierter Begriff ist durch einen beschönigenden Ausdruck ersetzt. Oberflächlich betrachtet verschleiert der euphemistisch gebrauchte Ausdruck, worum es eigentlich geht (das Beschriebene wird nicht benannt; es wird lediglich darauf angespielt). Steht die wörtliche Bedeutung des euphemistisch gebrauchten Ausdrucks in Kontrast zum Umschriebenen, wirkt die beschönigende Umschreibung ironisierend (&#x2197; Ironie / Witz / Zweideutigkeit). Der Euphemismus benennt in dem Fall genau das, was das Beschriebene <i>nicht</i> ist.</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.4 VIII 7–9</th></tr>
    <tr>
        <td><sup>7</sup> wa ridā bêta ḫupṯati<sup>?</sup> <sup>8</sup> ˀarṣi<br/>
            tissapirā bi yā<sup>9</sup>ridī(-)ma ˀarṣi/a</td>
        <td>Und steigt hinab in das 'Haus der Freiheit' der 'Erde'!<br/>
            Möget ihr gezählt werden zu denen, die in die 'Erde' (i. e. die Unterwelt) hinabsteigen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>BT ḪPṮT</i> "Haus der Freiheit" in viii 7-8a ist wahrscheinlich eine euphemistische Umschreibung der Unterwelt (s. <i>ARṢ</i> "Erde; Unterwelt" in viii 8b-9), also für jenes "Haus", das seinen "Bewohnern" kaum Freiheiten bietet (wer einmal eingetreten ist, wird gewöhnlich nie wieder freigelassen). Die Unterwelt ist im Grunde also das glatte Gegenteil eines "Hauses der Freiheit".</p></td></tr>
</table>
</details>
</details>

## F

<details class="undone">
<summary>Figura etymologica / Paronomasie</summary>
</details>

<details class="undone">
<summary>Fremdwort / Lehnwort</summary>
</details>

## G

<details class="undone">
<summary>Genus-komplementärer Parallelismus (<i>gender-matched parallelism</i>)</summary>
</details>

<details class="done">
<summary class="doneSummary">Geminatio</summary>
<p>Als Geminatio wird die Wortdoppelung bezeichnet. Ein Wort wird innerhalb des Kolons wiederholt (&#x2197; Repetitio); die beiden Begriffe folgen unmittelbar aufeinander. Die Geminatio ist in der ugaritischen Poesie selten bezeugt. Sie diente wohl der besonderen Hervorhebung des gedoppelten Lexems. In KTU 1.3 iv 32b ist das erste Wort des Kolons verdoppelt, in KTU 1.14 iii 26-27a (und Par.) das letzte (die Geminatio am Kolonende wird als Conversio bezeichnet).</p>
<details class="example">
<summary>Beispiel</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.3 iv 32b</th></tr>
    <tr>
        <td><b>likā likā</b> ˁNN ˀilīma</td>
        <td>Geht, geht, Diener der Götter!</td></tr>
<tr><th colspan="2">KTU 1.14 iii 26-27a (und Par.)</th></tr>
    <tr>
        <td><sup>26</sup> qaḥ kirtu ŠLM-īma <sup>27</sup> ŠLM-īma</td>
        <td>Nimm, Kirtu, Friedensgeschenke, (ja,) Friedensgeschenke!</td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Grammatische Varianz</summary>
</details>

## H

<details class="undone">
<summary>Hendiadyoin</summary>
</details>

<details class="done">
<summary class="doneSummary">Hexastichon</summary>
<p>Das Hexastichon ist eine &#x2197; Strophe, die sich aus sechs &#x2197; Versen zusammensetzt.</p>
</details>

<details class="undone">
<summary>Homöarkton</summary>
</details>

<details class="undone">
<summary>Homöoprophoron</summary>
</details>

<details class="undone">
<summary>Homöoptoton</summary>
</details>

<details class="undone">
<summary>Homöoteleuton</summary>
</details>

<details class="undone">
<summary>Hyperbel</summary>
</details>

<details class="undone">
<summary>Hysteron proteron</summary>
</details>  

## I

<details class="undone">
<summary>Inclusio</summary>
</details>

<details class="undone">
<summary>Inversion</summary>
</details>

<details class="undone">
<summary>Ironie / Witz / Zweideutigkeit</summary>
</details>

## K

<details class="done">
<summary class="doneSummary">Kolon</summary>
<p>Das Kolon ist eine elementare Verseinheit (&#x2197; Versgliederung) der ugaritischen Dichtungkunst. Auf manchen Tafeln (bzw. in einzelnen Tafelabschnitten) entspricht das Kolon je einer Zeile. Dies weist darauf hin, dass die ugaritischen Schreiber das Kolon als konstitutive Verseinheit erachteten. Das Kolon enthält i. d. R. mindestens ein selbständiges Satzglied (Prädikat, Subjekt, Akkusativobjekt und / oder Adverbial; oft setzt sich das Kolon aus einem Prädikat und einem oder mehreren nominalen Satzgliedern zusammen). Es umfasst meist drei oder vier Worteinheiten (i. e. Lexemen samt Präpositionen / Partikeln). Am Ende des Kolons steht vermutlich eine &#x2197; Zäsur.</p>
<p>Das Kolon ist meist mit einem oder zwei angrenzenden Kola zu einem &#x2197; Vers verbunden (es ergibt sich ein &#x2197; Bikolon [Vers aus zwei Kola] oder ein &#x2197; Trikolon [Vers aus drei Kola]). Im Fall des Verses, der nur ein Kolon enthält (i. e. des &#x2197; Monokolons), entspricht das Kolon gleichzeitig einem Vers.</p>
</details>

<details class="undone">
<summary>Konnexion</summary>
</details>

<details class="done">
<summary class="doneSummary">Konsonanz</summary>
<p>&#x2197; Assonanz und Konsonanz.</p>
</details>

<details class="undone">
<summary>Koppelung</summary>
</details>

## L

<details class="done">
<summary class="doneSummary">Lehnwort</summary>
<p>&#x2197; Fremdwort / Lehnwort.</p>
</details>

<details class="undone">
<summary>Litanei</summary>
</details>

<details class="done">
<summary class="doneSummary">Litotes</summary>
<p>&#x2197; Periphrase.</p>
</details>

## M

<details class="done">
<summary class="doneSummary">Merismus</summary>
<p>Im Merismus sind zwei Begriffe miteinander verknüpft, die zwei gegensätzliche Teile desselben Ganzen bezeichnen ("Himmel" ↔ "Erde"; "Sohn" ↔ "Tochter"; die Teile, die die beiden Begriffe bezeichnen, sind die einzigen Teile des Ganzen oder zwei charakteristische Teile). Der Merismus dient dazu, das Ganze, zu dem die bezeichneten Teile gehören, zu veranschaulichen ("Himmel" ↔ "Erde" ~ "Kosmos"; "Sohn" ↔ "Tochter" ~ "Kinder, Nachkommen"). Die Begriffe können aneinandergereiht und durch eine Konjunktion miteinander verbunden sein (in Form einer &#x2197; Accumulatio; s. Beispiele A) oder einander in zwei (parallelen) Verseinheiten gegenübergestellt sein (s. Beispiele B).</p>
<details class="example">
<summary>Beispiele A</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.16 III 2</th></tr>
    <tr>
        <td><sup>2</sup> ˁînā tûrā ˀarṣa wa šamîma</td>
        <td>Schaut, durchstreift Erde und Himmel!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>ARṢ W ŠMM</i> "Erde und Himmel" ~ "Kosmos".</p></td></tr>
</table>
</details>
<p/>
<details class="example">
<summary>Beispiele B</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.15 iii 22-25a</th></tr>
    <tr>
        <td><sup>22</sup> maka bi šabūˁi šanāti<br/>
            <sup>23</sup> banū kirti kama(-)humū tuddarū<br/>
            <sup>24</sup> ˀappV binātu ḥurriyi <sup>25</sup> kama(-)humū</td>
        <td>Dann, im siebten Jahr,<br/>
            waren (da) die Söhne Kirtus, wie sie (ihm) versprochen worden waren,<br/>
            und ebenso die Töchter Ḥurriyas, wie sie (ihr versprochen worden waren).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>BN KRT</i> "Söhne Kirtus" // <i>BNT ḤRY</i> "Töchter Ḥurriyas" ~ "Kinder von Kirtu und Ḥurriya" (in Kolon 2-3).</p></td></tr>
<tr><th colspan="2">KTU 1.6 iii 6–7</th></tr>
    <tr>
        <td>šamûma šamna tamṭurūnna<br/>
            naḫalūma talikū nubtama</td>
        <td>Die Himmel mögen Öl regnen,<br/>
            die Wadis mögen voll Honig fließen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>ŠMM</i> "Himmel" steht parallel zu <i>NḪLM</i> "Wadis". Die beiden Begriffe bezeichnen die beiden entgegengesetzten Regionen (oben und unten), denen gewöhnlich Wasser entspringt. Die S-P-Phrasen <i>ŠMM TMṬRN</i> "die Himmel mögen regnen" und <i>NḪLM TLK</i> "die Wadis mögen (voll XY) fließen" beschreiben zwei Teilaspekte des übergeordneten Sachverhalts "die wasserführenden / -spendenden Regionen des Kosmos mögen (XY über die Erde) fließen lassen". Das Akkusativobjekt zeigt jeweils an, was sich aus Himmel und Wadi ergießen solle: Nicht etwa Wasser, sondern <i>ŠMN</i> "Öl" und <i>NBT-</i> "Honig".</p></td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Metapher / Vergleich</summary>
</details>

<details class="done">
<summary class="doneSummary">Metonymie</summary>
<p>In der Metonymie ist ein Wort durch ein anderes (metonymisch gebrauchtes) Wort ersetzt, das (im vorliegenden Kontext) die Bedeutung des ersetzten Begriffs übernimmt, obwohl die Grundbedeutung des metonymisch gebrauchten Lexems von der Bedeutung des ersetzten Begriffs abweicht. Die Grundbedeutung des metonymisch gebrauchten Lexems (bzw. deren Referent, also das, was das Lexem kontextunabhängig bezeichnet) und der ersetzte Begriff (bzw. dessen Referent) stehen aus der Sicht des Autors / der Rezipient:innen des Texts in real- oder vorstellungsweltlicher Beziehung zueinander. Meist stehen die Grundbedeutung des metonymisch gebrauchten Lexems und die im Kontext übernommene Wortbedeutung in <i>Teil-Ganzes</i>- oder <i>Ganzes-Teil</i>-Relation (bzw. einer verwandten Sinnrelation, z. B. <i>Enthaltenes-Behälter</i>; Beispiel: "das Haus des Königs war vollkommen zerstört"; "Haus des Königs" meint hier nicht den königlichen Palast, sondern - metonymisch - die Königsfamilie / Dynastie; stehen die Grundbedeutung des metonymisch gebrauchten Lexems und die im Kontext übernommene Wortbedeutung in <i>Teil-Ganzes</i>-Beziehung, wird die Figur auch als Synekdoche bezeichnet; entweder steht der <i>Teil</i> für das <i>Ganze</i> [<i>pars pro toto</i>] oder das <i>Ganze</i> steht für den <i>Teil</i> [<i>totum pro parte</i>]).</p>
<p>In der ugaritischen Poesie steht das metonymisch gebrauchte Lexem zuweilen parallel zu dem Lexem, dessen Bedeutung es übernimmt. Die parallel gestellten Lexeme, von denen eines metonymisch gebraucht ist, beziehen sich in dem Fall auf dieselbe real- oder vorstellungsweltliche Sache. Wenngleich Rezipient:innen erkannt haben mögen, dass eines der beiden Lexeme des Wortpaars im übertragenen Sinn zu verstehen ist, ist zu vermuten, dass sie sich zumindest kurzzeitig auch die Grundbedeutung des metonymisch gebrauchten Lexems vor Augen führten. Der metonymische Ausdruck lenkte die Aufmerksamkeit auf eine Sache, die mit dem eigentlich Gemeinten (bezeichnet durch das parallel gestellte Lexem) auf die eine oder andere Sache verwandt ist, und hatte so vermutlich Einfluss darauf, welche Assoziationen das Publikum mit dem parallel gestellten Lexem verband. Der metonymische Ausdruck trug so wohl zur Anschaulichkeit des Aussagenpaars bei.</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.14 i 6b-8a</th></tr>
    <tr>
        <td>ˀummatu <sup>7</sup> [kirti] ˁaruwat<br/>
            bêtu <sup>8</sup> [ma]lki ˀîtab(i)da<sup>!</sup></td>
        <td>Die Sippe [Kirtus] war vernichtet,<br/>
            [des Kö]nigs Haus war völlig zerstört.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>BT</i> "Haus", metonymisch für "Dynastie" (i. e. die Angehörigen der königlichen Familie, die im Palast leben bzw. lebten) ~ <i>UMT</i> "Familie, Sippe, Dynastie".</p></td></tr>
<tr><th colspan="2">KTU 1.14 ii 27b-29</th></tr>
    <tr>
        <td>ˁadaba <sup>28</sup> ˀakla li qar(i)yati<br/>
            <sup>29</sup> ḥiṭṭata li bêti-ḫābūri</td>
        <td>Er bereite Speise für die Stadt,<br/>
            Weizen für Bêtu-Ḫabūri!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>ḤṬṬ</i> "Weizen", metonymisch für "Speise, Brot" (i. e. das, was aus Weizen zubereitet wird) ~ <i>AKL</i> "Speise, Brot".</p></td></tr>
</table>
</details>
</details>

<details class="done">
<summary class="doneSummary">Monokolon</summary>
<p>Das Monokolon ist ein &#x2197; Vers, der ein einziges &#x2197; Kolon umfasst.</p>
</details>

<details class="done">
<summary class="doneSummary">Monostichon</summary>
<p>Das Monostichon ist eine &#x2197; Strophe, die einen einzigen &#x2197; Vers enthält.</p>
</details>

<details class="undone">
<summary>Musik / Musikalische Gestaltung</summary>
</details>

## P

<details class="undone">
<summary>Parallelismus</summary>
</details>

<details class="undone">
<summary>Parenthese</summary>
</details>

<details class="done">
<summary class="doneSummary">Paronomasie</summary>
<p>&#x2197; Figura etymologica / Paronomasie.</p>
</details>

<details class="done">
<summary class="doneSummary"><i>pars pro toto</i></summary>
<p>&#x2197; Metonymie.</p>
</details>

<details class="done">
<summary class="doneSummary">Pentakolon</summary>
<p>Das Pentakolon ist ein &#x2197; Vers, der sich aus fünf &#x2197; Kola zusammensetzt.</p>
</details>

<details class="done">
<summary class="doneSummary">Pentastichon</summary>
<p>Das Pentastichon ist eine &#x2197; Strophe, die sich aus fünf &#x2197; Versen zusammensetzt.</p>
</details>

<details class="done">
<summary class="doneSummary">Periphrase</summary>
<p>Die Periphrase (Umschreibung) ist ein Begriff oder eine mehrgliedrige Phrase, der / die ein anderes Lexem umschreibt (Beispiel: "Blut der Bäume" für "Wein"). Die Periphrase stellt ein Charakteristikum des Umschriebenen in den Fokus und illustriert so das Konzept, das dem umschriebenen Begriff zugrunde liegt (das Lexem X und die Periphrase Y können gewöhnlich in der Aussage "X ist / bedeutet Y" verbaut werden [Beispiel: "<i>Brüder</i> sind <i>Söhne einer Mutter</i>"]; so gesehen definiert die Periphrase den umschriebenen Begriff; die Periphrase enthält i. d. R. mindestens ein Lexem, dessen Grundbedeutung sich nicht mit der Bedeutung des umschriebenen Begriffs deckt).</p>
<p>In der ugaritischen Poesie setzt sich die Periphrase oft aus einem Substantiv und einem untergeordneten Attribut zusammen; sie umscheibt ein einfaches Substantiv (vereinzelt tritt die Periphrase auch in Form einer zweigliedrigen Verbalphrase auf, die ein Substantiv [KTU 1.17 ii 5b–6a] oder ein Verb umschreibt [KTU 1.16 vi 41b–42]; s. Beispiele A). Eine andere Form der Periphrase gründet auf der Negation eines Begriffs, der das Gegenteil des umschriebenen Lexems bezeichnet (i. e. eines Atonyms oder eines komplementären Begriffs des Umschriebenen). Wird das Gegenteil des umschriebenen Lexems verneint, ergibt sich eine partiell-synonyme Periphrase des Begriffs (dies entspricht einer doppelten Verneinung [Beispiel: "nicht am Leben lassen" für "töten"; "nicht am Leben lassen" ~ "nicht nicht töten"]; solche und ähnliche Konstruktionen werden in der Stilistik als Litotes bezeichnet [scheinbare Abschwächung der Aussage, i. d. R. durch einen verneinten / doppelt-verneinten Ausdruck, wodurch die eigentliche Aussage verstärkt wird: "nicht unwahrscheinlich" ~ "ziemlich wahrscheinlich"]; die Litotes wird in der Stilistik zuweilen von der Periphrase getrennt behandelt; <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/HWFZJWVP/item-list">Braak / Neubauer, 2001: 52-55</a>). In der ugaritischen Dichtung diente diese Form der Periphrase meist der Umschreibung eines Verbalausdrucks (vereinzelt auch eines Substantivs [KTU 1.17 vi 26b–28a]; s. Beispiele B). Zu den Sonderformen der Periphrase zählt der &#x2197; Euphemismus. Zuweilen werden auch die &#x2197; Antonomasie und die &#x2197; Metonymie als Formen der Periphrase betrachtet (<a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/HWFZJWVP/item-list">Braak / Neubauer, 2001: 47 / 54</a>; <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/DB62UX55/item-list">Schweikle, 2007</a>).</p>
<p>Die ugaritischen Dichter gebrauchten die Periphrase u. a. in Versgefügen, in denen einander zwei referenzidentische Ausdrücke gegenübergestellt werden sollten (Ausdrücke sind referenzidentisch, wenn sie dieselbe real- oder vorstellungsweltliche Sache bezeichnen): Die Periphrase ermöglichte ihnen, einem Lexem aus der einen Verseinheit ein mehr oder weniger bedeutungsgleiches Pendant in der anderen Verseinheit gegenüberzustellen, ohne das konkrete Lexem (für das es möglicherweise kein Synonym gab) zu wiederholen. Gleichzeitig erklärt die Periphrase den parallel gestellten Begriff und trägt somit zur Anschaulichkeit und Verständlichkeit der Aussage bei. Die Periphrase, in der das Gegenteil des umschriebenen Begriffs verneint wird, wirkt mitunter verstärkend ("niederschlagen" // "nicht am Leben lassen" ~ "töten, erschlagen").</p>
<p>Die Periphrase ist meist länger als der umschriebene Begriff; nicht selten setzt sich die Periphrase aus zwei Substantiven oder einem Substantiv und einem Verb zusammen. Im Vers, der sich aus mindestens zwei Kola zusammensetzt, kann die Periphrase die Ellipse eines anderen Satzteils längenmäßig kompensieren, sodass sich trotz der Auslassung eines Glieds gleich oder ähnlich lange Kola ergeben (KTU 1.14 i 8b-9: im zweiten Kolon sind die Partikel <i>D</i> und der Präpositionalausdruck <i>LH</i> ausgelassen; da aber das Substantiv <i>AḪM</i> durch die längere Periphrase <i>BN UM</i> ersetzt ist [und außerdem das längere Zahlwort <i>ṮMNT</i> an die Stelle des kürzeren <i>ŠBˁ</i> tritt], sind die beiden Kola vermutlich exakt gleich lang).</p>
<details class="example">
<summary>Beispiele A</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.14 i 8b-9</th></tr>
    <tr>
        <td>dā šabˁu <sup>9</sup> ˀaḫḫūma lahu<br/>
            ṯamānîtu <b>banū ˀummi</b></td>
        <td>(des Königs,) der (einst) sieben Brüder hatte,<br/>
            acht Söhne einer Mutter</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>AḪM</i> "Brüder" ~ <i>BN UM</i> "Söhne einer Mutter".</p></td></tr>
<tr><th colspan="2">KTU 1.17 i 11c–13a</th></tr>
    <tr>
        <td>ˀuzūra/u <sup>12</sup> [ˀilī]ma danīˀilu<br/>
            ˀuzūra/u ˀilīma yulaḥḥimu<br/>
            <sup>13</sup> [ˀuzū]ra/u yašaqqiyu <b>banī qudši</b></td>
        <td>Gegürtet (hat) [den Göt]tern Danīˀilu,<br/>
            gegürtet hat er den Göttern zu Essen gegeben,<br/>
            [gegürt]et hat er zu Trinken gegeben den Söhnen des Heiligen.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>ILM</i> "Götter" ~ <i>BN QDŠ</i> "Söhne des Heiligen (scil. des ˀIlu)".</p></td></tr>
<tr><th colspan="2">KTU 1.4 iii 43b–44</th></tr>
    <tr>
        <td>[tištayū] karpānīma yêna<br/>
            <sup>44</sup> [bi kāsī ḫurāṣi <b>da</b>]<b>ma ˁiṣṣīma</b></td>
        <td>[Sie tranken] aus Bechern Wein,<br/>
            [aus Goldbechern das "Bl]ut der Bäume".</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>YN</i> "Wein" ~ <i>DM ˁṢM</i> "Blut der Bäume".</p></td></tr>
<tr><th colspan="2">KTU 1.17 ii 5b–6a (und Par.)</th></tr>
    <tr>
        <td>ˀāḫidu yadaka bi ša[karāni]<br/>
        <sup>6</sup> muˁammisuka <b>kī šaba/iˁta yêna</b></td>
        <td>der deine Hand packt bei Tru[nkenheit],<br/>
            dich stützt, wenn du gesättigt bist mit Wein</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>B ŠKRN</i> "im Fall von (deiner) Trunkenheit" ~ <i>K ŠBˁt YN</i> "wenn du gesättigt bis mit Wein".</p></td></tr>
<tr><th colspan="2">KTU 1.16 vi 41b–42</th></tr>
    <tr>
        <td>šamaˁ maˁ(ˁa) lV Kirtu <sup>42</sup> Ṯˁ-u<br/>
            ˀištamVˁ wa <b>taqġû/î ˀudna</b></td>
        <td>Hör' doch, o edler Kirtu,<br/>
            horch her, ja, schenk (mir) Gehör!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>ŠMˁ Mˁ</i> "Hör' doch!" // <i>IŠTMˁ</i> "Hör' genau zu!" ~ <i>TQĠ UDN</i> "Schenk Gehör!"</p></td></tr>
</table>
</details>
<p/>
<details class="example">
<summary>Beispiele B</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.19 i 14b–16a</th></tr>
    <tr>
        <td>KD ˁalê/â qaštihu <sup>15</sup> ˀimḫaṣhu<br/>
            ˁalê/â qaṣaˁātihu huwati <sup>16</sup> <b>lā ˀaḥawwî</b></td>
        <td>Für seinen Bogen schlug ich ihn nämlich nieder,<br/>
            für seine Pfeile ließ ich ihn nicht am Leben.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>IMḪṢ</i> "ich schlug nieder" ~ <i>L AḤW</i> "ich ließ nicht am Leben"; vgl. auch KTU 1.18 iv 12b–13.</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 19b</th></tr>
    <tr>
        <td>tabiˁā ġalmāmi <b>lā yaṯabā</b></td>
        <td>Die (beiden) Jünglinge machten sich auf, verweilten nicht.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>TBˁ</i> "sie machten sich auf" ~ <i>L YṮB</i> "sie verweilten nicht".</p></td></tr>
<tr><th colspan="2">KTU 1.17 vi 26b–28a</th></tr>
    <tr>
        <td>ˀiriš ḥayyīma lV ˀAqhatu ġāziru<br/>
            <sup>27</sup> ˀiriš ḥayyīma wa ˀâtinaka<br/>
            <b>balî(-)môta</b> <sup>28</sup> wa ˀašalliḥaka</td>
        <td>Wünsch’ (dir) Leben, o ˀAqhatu, Held,<br/>
            wünsch’ (dir) Leben und ich will es dir geben,<br/>
            Unsterblichkeit und ich will sie dir überreichen!</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p>Anm.: <i>ḤYM</i> "Leben, ewiges Leben" ~ <i>BL</i>(-)<i>MT</i> "Nicht-Tod, Unsterblichkeit".</p></td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Personifikation</summary>
</details>

<details class="undone">
<summary>Pleonasmus</summary>
</details>

<details class="done">
<summary class="doneSummary">Polyptoton</summary>
<p>&#x2197; Repetitio.</p>
</details>

<details class="done">
<summary  class="doneSummary">Polysyndeton</summary>
<p>Im Polysyndeton folgt auf jedes Glied (außer auf das letzte Glied) einer Aufzählung, die sich aus drei oder mehreren syntaktisch gleichrangigen Begriffen oder Phrasen zusammensetzt, eine Konjunktion (ug. <i>w</i>; Gegenstück zum &#x2197; Asyndeton). Das Polysyndeton ist in der ugaritischen Poesie recht selten bezeugt. Im angeführten Beispiel (KTU 1.4 iii 17-21a) ist die Figur möglicherweise eingesetzt, um den Eindruck einer umfassenden Aufzählung zu erwecken (nach dem zweiten Glied der Aufzählung geht der aufnehmende Verstand davon aus, dass die Aufzählung abgeschlossen ist; anschließend muss er feststellen, dass die Aufzählung noch weitergeht). Die Aufzählung erstreckt sich dort über zwei Kola. Das Polysyndeton innerhalb eines Kolons (i. e. X <i>w</i> Y <i>w</i> Z) konnte im ugaritischen Korpus bislang nicht identifiziert werden.</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.4 iii 17-21a</th></tr>
    <tr>
        <td><sup>17</sup> dāma ṯinê dabḥêma šaniˀa baˁlu<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;ṯalāṯa <sup>18</sup> rākibu ˁarapāti<br/>
            dabḥa <sup>19</sup> BṮ-(a)ti <b>wa</b> dabḥa <sup>20</sup> DN-(V)ti<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<b>wa</b> dabḥa tudāmim(a)&lt;ti&gt; <sup>21</sup> ˀamahāti</td>
        <td>Fürwahr, zwei Feste hasst Baˁlu,<br/>
         &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;drei der Wolkenfahrer:<br/>
         das Fest der Schande und das Fest der Minderwertigkeit<sup>?</sup><br/>
         &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;und das Fest des Fehlverhaltens<sup>?</sup> der Dienerinnen.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: Sofern sich die drei Phrasen <i>DBḤ BṮT</i>, <i>DBḤ DNT</i> und <i>DBḤ TDMM</i>&lt;<i>T</i>&gt; <i>AMHT</i> auf je ein Fest (bzw. eine bestimmte Art von Fest) beziehen, das Baˁlu hasst, liegt eine dreigliedrige Aufzählung vor. Sowohl vor dem zweiten Glied der Aufzählung als auch vor dem dritten (i. e. am Anfang des letzten Kolons) steht die Konjunktion <i>w</i>.</p></td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Praeteritio</summary>
</details>

## R

<details class="undone">
<summary>Reim / Reimschemata</summary>
</details>

<details class="undone">
<summary>Repetitio</summary>
</details>

<details class="done">
<summary class="doneSummary">Rhetorische Frage</summary>
<p>Die rhetorische Frage ist eine Frage, auf die der Fragende keine Antwort erwartet, da sie aus seiner Sicht nur eine einzige Antwort zulässt (in KTU 1.4 iv 59a: "Bin ich ein Sklave?" → Antwort: "Nein!"). Die rhetorische Frage ist damit weniger eine Frage als eine nachdrückliche Aussage (<a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/HWFZJWVP/item-list">Braak / Neubauer, 2001: 63</a>; <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/EQAMRDJK/collection">Moennighoff, 2007: 653</a>; in KTU 1.4 iv 59a: "Bin ich ein Sklave?" ~ "Ich bin doch sicher kein Sklave!"). Dem Angesprochenen wird versagt, dem in der rhetorischen Frage durchschimmernden Standpunkt des Fragenden zu widersprechen. Die rhetorische Frage bietet dem Sprechenden damit ein Mittel, den Angesprochenen von seinem Standpunkt zu überzeugen. Die rhetorische Frage ist im ugaritischen Korpus recht selten belegt. Eine Frage lässt sich u. a. daran als rhetorische Frage erkennen, dass sie nicht beantwortet wird bzw. der Fragende den Angesprochenen gar nicht erst zu Wort kommen lässt, um die Frage zu beantworten.</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.4 iv 59-v 1</th></tr>
    <tr>
        <td><sup>59</sup> pa ˁabdu ˀanā ˁNN-u ˀaṯiratu<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>60</sup> pa ˁabdu ˀanāku ˀâḫudu ˀULṮ-a<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>61</sup> him(ma) ˀam(a)tu ˀaṯiratu tulabbinu <sup>62</sup> labināti<br/>
            yabnû(/î) bêta li baˁli <sup>v 1</sup> kama ˀilīma<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;wa ḥaẓira ka banī ˀaṯirati</td>
        <td>Bin ich etwa ein Sklave, ist ˀAṯiratu (etwa) eine Dienerin (oder: ein Diener der ˀAṯiratu)?<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;Bin ich etwa ein Sklave, halte ich (etwa) (selbst) die Hacke / Ziegelform?<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;Oder ist (etwa) ˀAṯiratu eine Dienerin, formt sie (etwa) (selbst) die Ziegel?<br/>
            Er (selbst; scil. Baˁlu) soll ein Haus für Baˁlu bauen wie (für) die (anderen) Götter<br/>
            ja, eine Wohnstatt wie (für) die Söhne der ˀAṯiratu!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: Die rhetorischen Fragen in iv 59-62a dienen dazu, Offensichtliches klarzustellen und eine aus der Sicht des Sprechers absurde Vorstellung aus der Welt zu schaffen: Der Sprecher, ˀIlu, mag eingewilligt haben, dass Baˁlu einen Palast erhält, doch er und seine Frau ˀAṯiratu sind sicher keine Sklaven. ˀIlu und ˀAṯiratu werden also keinen Finger rühren beim Bau des neuen Palastes. Baˁlu soll sich selbst darum kümmern.</p></td></tr>
<tr><th colspan="2">KTU 1.14 iii 33b-40 (und Par.)</th></tr>
    <tr>
        <td>limā ˀanāku <sup>34</sup> kaspa wa yarqa<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;ḫurāṣa <sup>35</sup> yada maqâmihu<br/>
            wa ˁabda <sup>36</sup> ˁālami ṯalāṯa sus(s)uwīma<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;markabta <sup>37</sup> bi tarbaṣi bina ˀam(a)ti<br/>
            <sup>38</sup> pa dā ˀêna bi bêtiya tâtin<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>39</sup> tin liya MṮ-(a)ta ḥurriya<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>40</sup> naˁīm(a)ta šapḥa bukraka</td>
        <td>Warum (sollte) ich Silber und Gelbgold (nehmen),<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;Gold samt seinem Fundort,<br/>
            und einen ewig gebundenen Knecht (und) drei Pferde,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;einen Streitwagen aus (deinem) Stall (und) den Sohn einer Magd?<br/>
            Vielmehr gib, was in meinem Haus fehlt,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;gib mir das Mädchen Ḥurriya,<br/>
            &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;die Liebliche, deinen erstgeborenen Spross,</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: "Warum (sollte) ich Silber (...) (nehmen)?" ~ "Es gibt für mich keinen Grund, Silber (...) zu nehmen!"</p></td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Rhythmus</summary>
</details>

<details class="undone">
<summary>Ringegefüge</summary>
</details>

## S

<details class="undone">
<summary><i>Split couplet</i></summary>
</details>

<details class="done">
<summary class="doneSummary">Strophe</summary>
<p>Die Strophe ist eine Verseinheit (&#x2197; Versgliederung), die einen oder mehrere &#x2197; Verse umfasst. Das Monostichon enthält einen einzigen Vers. Das Distichon setzt sich aus zwei Versen zusammen, das Tristichon aus drei, das Tetrastichon aus vier, das Pentastichon aus fünf. Die Strophengrenzen wurden im Vortrag vermutlich durch markante Zäsuren angezeigt (&#x2197; Zäsur).</p>
<details class="example">
<summary>Beispiele</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.6 iii 10–13 (Distichon aus zwei Bikola)</th></tr>
    <tr>
        <td><sup>10</sup> bi ḥi/ulmi laṭ(a)pāni ˀili dā PID-i // <sup>11</sup> bi ḎR-(a)ti bāniyi bu/inwati<br/>
        <sup>12</sup> šamûma šamna tamṭurūnna // <sup>13</sup> naḫalūma talikū nubtama</td>
        <td>Im Traum des Scharfsinnigen, des ˀIlu, des Verständigen, // in der Vision des Schöpfers der Schöpfung,<br/>
        regneten die Himmel Öl, // flossen die Wadis voll Honig!</td></tr>
<tr><th colspan="2">KTU 1.14 ii 43-50a (Tristichon aus zwei Bikola und einem Trikolon)</th></tr>
    <tr>
        <td><sup>43</sup> yaḥīdu bêtahu sagara // <sup>44</sup> ˀalmānatu šakāru <sup>45</sup> taškir<br/>
            ZBL-u ˁaršama <sup>46</sup> yiššaˀu // ˁawwiru mazālu <sup>47</sup> yamzilu<br/>
            wa yâṣiˀ tarīḫu <sup>48</sup> ḥadaṯu // yubaˁˁir li ṯanî <sup>49</sup> ˀaṯṯatahu // lima nakari <sup>50</sup> môdādatahu</td>
        <td>Der einzig Verbliebene schließe sein Haus, // die Witwe stelle einen Tagelöhner ein.<br/>
            Der Kranke hebe (sein) Bett auf, // der Blinde taste sich hinterher.<br/>
            Auch ziehe aus der frisch Vermählte, // schaffe seine Frau zu einem anderen, // seine Geliebte zu einem Fremden.</td></tr>
<tr><th colspan="2">KTU 1.6 vi 16b–22a (Tetrastichon aus vier Bikola)</th></tr>
    <tr>
        <td>yittâˁāni ka GMR-êma // <sup>17</sup> môtu ˁazza baˁlu ˁazza<br/>
            yinnagiḥāni <sup>18</sup> ka ruˀumêma<sup>?</sup> // môtu ˁazza baˁlu <sup>19</sup> ˁazza<br/>
            yinnaṯikāni ka baṯnêma // <sup>20</sup> môtu ˁazza baˁlu ˁazza<br/>
            yimmaṣiḫāni <sup>21</sup> ka LSM-êma // môtu qâla <sup>22</sup> baˁlu qâla</td>
        <td>Sie rüttelten aneinander wie zwei GMR – // Môtu war stark, Baˁlu war stark.<br/>
            Sie stießen einander wie zwei Wildstiere – // Môtu war stark, Baˁlu war stark.<br/>
            Sie bissen einander wie zwei Schlangen – // Môtu war stark, Baˁlu war stark.<br/>
            Sie rissen einander zu Boden wie zwei LSM – // Môtu fiel, Baˁlu fiel.</td></tr>
<tr><th colspan="2">KTU 1.14 i 12-21a (Pentastichon aus vier Bikola und einem Monokolon)</th></tr>
    <tr>
        <td><sup>12</sup> ˀaṯṯata ṣidqihu lā yapuq // <sup>13</sup> matrVḫ(a)ta yušrihu<br/>
            <sup>14</sup> ˀaṯṯata tariḫa wa tabiˁat // <sup>15</sup> ṮAR-a ˀummi takûn lahu<br/>
            <sup>16</sup> muṯallaṯ(a)tu kuṯruma tamut // <sup>17</sup> murabbaˁ(a)tu zVb(V)lānuma<br/>
            <sup>18</sup> muḫammaš(a)ta yiˀtasap <sup>19</sup> rašpu // muṯaddaṯ(a)ta ġalamū <sup>20</sup> yammi<br/>
            mušabbaˁ(a)tuhun(n)ā bi šVlḥi <sup>21</sup> tittapal</td>
        <td>Die Frau, die ihm rechtmäßig zustand, hatte er nicht gewonnen, // die Gattin, die ihm angemessen war.<br/>
            Eine (andere) Frau hatte er geheiratet, doch die war fortgegangen, // die ṮAR der Mutter, die ihm übrig geblieben war.<br/>
            Die Dritte (Frau) war bei bester Gesundheit gestorben, // die Vierte in Krankheit.<br/>
            Die Fünfte hatte Rašpu an sich gerissen, // die Sechste die Gefolgsleute Yammus.<br/>
            Die Siebte von ihnen war durch die ŠLḤ-Waffe gefallen.</td></tr>
<tr><th colspan="2">KTU 1.6 i 18b–29 (Hexastichon aus sechs Bikola)</th></tr>
    <tr>
        <td>tiṭbaḫu šabˁīma <sup>19</sup> ruˀumīma<sup>?</sup> // ka GMN-i ˀalˀiyāni <sup>20</sup> baˁli<br/>
            tiṭbaḫu šabˁīma ˀalapīma // <sup>21</sup> [ka G]MN-i ˀalˀiyāni baˁli<br/>
            <sup>22</sup> [tiṭ]baḫu šabˁīma ṣaˀna // <sup>23</sup> [ka GM]N-i ˀalˀiyāni baˁli<br/>
            <sup>24</sup> [tiṭ]baḫu šabˁīma ˀayyalīma // <sup>25</sup> [ka GMN-i] ˀalˀiyāni baˁli<br/>
            <sup>26</sup> [tiṭbaḫu ša]bˁīma yaˁilīma // <sup>27</sup> [ka GMN-i ˀal]ˀiyāni baˁli<br/>
            <sup>28</sup> [tiṭbaḫu šabˁīma ya]ḥmūrīma // <sup>29</sup> [ka GM]N-i ˀalˀiyāni baˁli</td>
        <td>Sie schlachtete 70 Wildstiere, // als Begräbnisopfer für den Mächtigen, Baˁlu.<br/>
            Sie schlachtete 70 Rinder, // [als] Begräbnisopfer für den Mächtigen, Baˁlu.<br/>
            [Sie sch]lachtete 70 Schafe, // [als Begräb]nisopfer für den Mächtigen, Baˁlu.<br/>
            [Sie sch]lachtete 70 Hirsche, // [als Begräbnisopfer] für den Mächtigen, Baˁlu.<br/>
            [Sie schlachtete 7]0 Steinböcke, // [als Begräbnisopfer für den Mäch]tigen, Baˁlu.<br/>
            [Sie schlachtete 70 R]ehböcke, // [als Begräbnisop]fer für den Mächtigen, Baˁlu.</td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Stufenparallelismus</summary>
</details>

<details class="done">
<summary class="doneSummary">Synekdoche</summary>
<p>&#x2197; Metonymie.</p>
</details>

## T

<details class="undone">
<summary>Tautologie</summary>
</details>

<details class="undone">
<summary>Terrassenparallelismus</summary>
</details>

<details class="done">
<summary  class="doneSummary">Tetrakolon</summary>
<p>Das Tetrakolon ist ein &#x2197; Vers, der sich aus vier &#x2197; Kola zusammensetzt.</p>
</details>

<details class="done">
<summary class="doneSummary">Tetrastichon</summary>
<p>Das Tetrastichon ist eine &#x2197; Strophe, die sich aus vier &#x2197; Versen zusammensetzt.</p>
</details>

<details class="done">
<summary class="doneSummary"><i>totum pro parte</i></summary>
<p>&#x2197; Metonymie.</p>
</details>

<details class="done">
<summary class="doneSummary">Trikolon</summary>
<p>Das Trikolon ist ein &#x2197; Vers, der sich aus drei &#x2197; Kola zusammensetzt.</p>
</details>

<details class="done">
<summary class="doneSummary">Tristichon</summary>
<p>Das Tristichon ist eine &#x2197; Strophe, die sich aus drei &#x2197; Versen zusammensetzt.</p>
</details>

## V

<details class="done">
<summary class="doneSummary">Vergleich</summary>
<p>&#x2197; Metapher / Vergleich.</p>
</details>

<details class="done">
<summary class="doneSummary">Vers</summary>
<p>Der ugaritische Vers setzt sich aus einem oder mehreren &#x2197; Kola zusammen (s. auch &#x2197; Versgliederung). Das Monokolon enthält ein einziges Kolon, das Bikolon zwei Kola, das Trikolon drei, das Tetrakolon vier, das Pentakolon fünf. Das Bikolon ist die gängigste Versform in der ugaritischen Dichtung; daneben treten immer wieder Monokola und Trikola auf (Tetrakola und Pentakola sind deutlich seltener belegt). Die Kola, die zu einem Vers verbunden sind, stehen oft parallel zueinander (&#x2197; Parallelismus) oder enthalten <i>einen</i> zusammenhängenden Satz (im Enjambement-Vers; &#x2197; Enjambement). Der Vers enthält meist mindestens einen vollständigen Hauptsatz. Daneben sind syntaktisch unvollständige Versformen belegt, in denen der Vers nur einen Teil des übergeordneten Hauptsatzes enthält (und sich mit einem oder mehreren angrenzenden Versen zu einer Enjambement-Strophe verbindet; &#x2197; Enjambement). Die Versgrenzen wurden im Vortrag vermutlich durch Zäsuren kenntlich gemacht (&#x2197; Zäsur).</p>
<details class="example">
<summary>Beispiele: Versformen</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.14 i 37b-38a (Monokolon)</th></tr>
    <tr>
        <td>wa yaqrub <sup>38</sup> bi šaˀāli kirti</td>
        <td>Und er trat heran, um Kirtu zu fragen.</td></tr>
<tr><th colspan="2">KTU 1.14 i 35b-37a (Bikolon)</th></tr>
    <tr>
        <td>wa bi ḥilmihu <sup>36</sup> ˀilu yârid<br/>
            bi ḎHR-(a)tihu <sup>37</sup> ˀabū ˀadami</td>
        <td>Da stieg in seinem Traum ˀIlu herab,<br/>
            in seiner Vision der Vater der Menschheit.</td></tr>
<tr><th colspan="2">KTU 1.14 i 28-30 (Trikolon)</th></tr>
    <tr>
        <td><sup>28</sup> tinnatikna ˀudmaˁātuhu<br/>
            <sup>29</sup> kama ṯiqalīma ˀarṣah<br/>
            <sup>30</sup> kama ḫamušāti maṭṭâtah</td>
        <td>Seine Tränen flossen<br/>
            wie Schekel zur Erde,<br/>
            wie Fünftel (eines Schekels) aufs Bett.</td></tr>
<tr><th colspan="2">KTU 1.6 ii 31b–34a (Tetrakolon)</th></tr>
    <tr>
        <td>bi ḥarbi <sup>32</sup> tibqaˁ(V)ninnu<br/>
            bi ḪṮR-i tadriyV<sup>33</sup>ninnu<br/>
            bi ˀiš(V)ti tašrup(V)ninnu<br/>
            <sup>34</sup> bi riḥêma tiṭḥanninnu</td>
        <td>Mit einem Messer schlitzte sie ihn auf,<br/>
            mit einer Worfgabel<sup>?</sup> worfelte sie ihn,<br/>
            mit Feuer verbrannte sie ihn,<br/>
            mit (zwei) Mühlsteinen zermahlte sie ihn.</td></tr>
<tr><th colspan="2">KTU 1.10 ii 26–30 (Pentakolon)</th></tr>
    <tr>
        <td><sup>26</sup> wa tiššaˀu ˁênêha batūl(a)tu ˁanatu<br/>
            <sup>27</sup> wa tiššaˀu ˁênêha wa taˁîn(u)<br/>
            <sup>28</sup> wa taˁîn(u) ˀarḫa wa târu bi likti<br/>
            <sup>29</sup> târu bi likti wa târu bi ḫîli<sup>?</sup><br/>
            <sup>30</sup> [bi] nuˁmima bi YSM-ima ḥabli kôṯarāti</td>
        <td>Da hob ihre Augen die Jungfrau, ˁAnatu,<br/>
            da hob sie ihre Augen und sah,<br/>
            da sah sie eine Kuh und die zog im Gehen herum,<br/>
            die zog im Gehen herum, ja sie zog im Springen<sup>?</sup> herum<br/>
            [in / samt] der Lieblichkeit, dem Anmut der Schar der Kôṯarātu.</td></tr>
</table>
</details>
<p/>
<details class="exampleFirstOrder">
<summary class="exampleFirstOrderSummary">Beispiele: Verskonstruktionen</summary>
<p>Zu den Sigeln s. <a href="https://eupt-dev.sub.uni-goettingen.de/Einfuehrung/Editorische_Prinzipien_Kommentar.html">Abkürzungen und Sigel zur Analyse der Versstruktur</a>.</p>
<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary">a-b-c // a&#8596;b&#8596;c</summary>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">a-b-(c) // a'-b'-(c')</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.5 ii 6b–7</th></tr>
    <tr>
        <td>yara(/i)ˀaninnu<sup>!</sup> ˀalˀiyānu baˁlu<br/>
            <sup>7</sup> ṯata(/i)ˁaninnu rākibu ˁarapāti</td>
        <td>Der Mächtige, Baˁlu, fürchtete sich vor ihm,<br/>
            der Wolkenfahrer hatte Angst vor ihm.</td>
        <td>P<sup>+O4</sup>-S<br/>
         P<sup>+O4</sup>-S</td>
        <td>a-b<br/>
         a'-b'</td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">a-b-c // (c')-b'-a' (&#x2197; Chiasmus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.17 v 10b–11</th></tr>
    <tr>
        <td>hVl(V)ka kôṯari <sup>11</sup> kī yaˁin(/-înu)<br/>
            wa yaˁin(/-înu) tadrVqa ḫasīsi</td>
        <td>Dass Kôṯaru kam, sah er fürwahr,<br/>
            ja, er sah, dass Ḫasīsu heranschritt.</td>
        <td>O<sub>4</sub>-P<br/>
         P-O<sub>4</sub></td>
        <td>a-b<br/>
         b-a'</td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">a-{b-c} // a'-{c'-b'} (anaphorischer partieller &#x2197; Chiasmus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.3 iii 19–20a</th></tr>
    <tr>
        <td><sup>19</sup> ˁimmaya paˁnāki talsumānna<br/>
            ˁimmaya <sup>20</sup> tawattiḥā ˀišdâki</td>
        <td>Zu mir mögen deine Füße laufen,<br/>
            zu mir mögen eilen deine Beine!</td>
        <td>A-S-P<br/>
         A-P-S</td>
        <td>a-{b-c}<br/>
         a-{c'-b'}</td></tr>
<tr><th colspan="4">KTU 1.3 v 33b–34b</th></tr>
    <tr>
        <td>kullunāyaya qašâhu <sup>34</sup> nâbilanna<br/>
            kullunāyaya nâbila kāsahu</td>
        <td>Wir alle wollen (ihm) seine Schale bringen,<br/>
            wir alle wollen (ihm) bringen seinen Becher.</td>
        <td><i>Pron.</i>-O<sub>4</sub>-P<br/>
         <i>Pron.</i>-P-O<sub>4</sub></td>
        <td>a-{b-c}<br/>
         a-{c'-b'}</td></tr>
<tr><th colspan="4">KTU 1.17 i 2b–3a</th></tr>
    <tr>
        <td>ˀuzūru/a ˀilīma yulaḥḥim(u)<br/>
            <sup>3</sup> [ˀuzūru/a yušaqqiyu] banī qudši<sup>?</sup></td>
        <td>Gegürtet gab er den Göttern zu Essen,<br/>
            [gegürtet gab er zu Trinken] den Söhnen des Heiligen.</td>
        <td>A-O<sub>4</sub>-P<br/>
         A-P-O<sub>4</sub></td>
        <td>a-{b-c}<br/>
         a-{c'-b'}</td></tr>
<tr><th colspan="4">KTU 1.6 vi 45b–47</th></tr>
    <tr>
        <td>šapšu <sup>46</sup> rāpiˀīma tuḥattikī<br/>
            <sup>47</sup> šapšu tuḥattikī ˀilānīyīma</td>
        <td>Šapšu, über die Rāpiˀūma sollst du herrschen,<br/>
            Šapšu, du sollst herrschen über die Göttlichen!</td>
        <td>S-O<sub>4</sub>-P<br/>
         S-P-O<sub>4</sub></td>
        <td>a-{b-c}<br/>
         a-{c'-b'}</td></tr>
<tr><th colspan="4">KTU 1.15 iv 17–18</th></tr>
    <tr>
        <td><sup>17</sup> ˁalê/âhu ṯôrīhu tušaˁrib<br/>
            <sup>18</sup> ˁalê/âhu tušaˁrib ẓabayīhu</td>
        <td>Zu ihm brachte sie seine Stiere,<br/>
            zu ihm brachte sie seine Gazellen,</td>
        <td>A-O<sub>4</sub>-P<br/>
         A-P-O<sub>4</sub></td>
        <td>a-{b-c}<br/>
         a-{c'-b'}</td></tr>  
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">{a-b}-c // {b'-a'}-c' (epiphorischer partieller &#x2197; Chiasmus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.19 iv 44b–45</th></tr>
    <tr>
        <td>tašit(-îtu) Ḫx[x x]-a bi <sup>45</sup> NŠG-iha<br/>
            ḥarba tašit(-îtu) bi TˁR-[a](tiha)</td>
        <td>Sie steckte einen D[olch]<sup>?</sup> in ihre Scheide<sup>?</sup>,<br/>
            ein Messer steckte sie in [ihre] Scheid[e]<sup>?</sup>.</td>
        <td>P-O<sub>4</sub>-A<br/>
            O<sub>4</sub>-P-A</td>
        <td>{a-b}-c<br/>
            {b'-a}-c'</td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">{a-b}-c // c'-{a'-b'} (anadiplotischer partieller &#x2197; Chiasmus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.19 iv 8–9a</th></tr>
    <tr>
        <td><sup>8</sup> danīˀilu bêtahu yamġiyu/anna<br/>
            yišta<sup>9</sup>qVl(u)<sup>?</sup> danīˀilu li hêkalihu</td>
        <td>Danīˀilu kam zu seinem Haus,<br/>
            Danīˀilu erreichte seinen Palast.</td>
        <td>S-A-P<br/>
         P-S-A</td>
        <td>{a-b}-c<br/>
         c'-{a-b'}</td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">a-{b-c} // {b'-c'}-a' (rahmender partieller &#x2197; Chiasmus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.19 iii 8–9a</th></tr>
    <tr>
        <td><sup>8</sup> kanapê našarīma baˁlu yaṯbu/ir(u)<br/>
            <sup>9</sup> baˁlu ṯabara DˀIY-ê/ī humūti</td>
        <td>Baˁlu zerbrach die Flügel der Adler,<br/>
            Baˁlu zerbrach die Schwingen von jenen.</td>
        <td>O<sub>4</sub>-S-P<br/>
         S-P-O<sub>4</sub></td>
        <td>a-{b-c}<br/>
         {b-c}-a'</td></tr>
</table>
</details>

</details>

<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary">a-b-c // ◌&#8596;b'&#8596;c' (&#x2197; Ellipse)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.4 v 61–62</th></tr>
    <tr>
        <td><sup>61</sup> bal(î) ˀašit ˀurubbata bi baha[tīma]<br/>
            <sup>62</sup> ḥallāna bi qarbi hêkalīma</td>
        <td>Soll ich kein Fenster im Ha[us (Pl.)] einsetzen,<br/>
            (keine) Fensteröffnung inmitten des Palastes (Pl.)?</td>
        <td>P-O<sub>4</sub>-A<br/>
         O<sub>4</sub>-A</td>
        <td>a-b-c<br/>
         ◌-b'-c'</td></tr>
</table>
</details>

<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary">a&#8596;b&#8596;◌ // ◌&#8596;b'&#8596;c</summary>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">a-b-◌ // ◌-b-c (und Varianten; &#x2197; Terrassenparallelismus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.3 ii 23–24</th></tr>
    <tr>
        <td><sup>23</sup> maˀda timtaḫaṣa/unna wa taˁînu<br/>
            <sup>24</sup> tiḫtaṣab(u) wa taḥdiyu ˁanatu</td>
        <td>Heftig kämpfte und schaute umher,<br/>
            focht und und blickte umher die ˁAnatu.</td>
        <td>A-P ; P<br/>
         P ; P-S</td>
        <td>a-b ; c-◌<br/>
         ◌-b' ; c'-d</td></tr>
<tr><th colspan="4">KTU 1.114 2c–4a</th></tr>
    <tr>
        <td>tilḥamūna <sup>3</sup> ˀilūma wa tištûna<br/>
            tištûna yê&#9001;na&#10217; ˁadê šubˁi<br/>
            <sup>4</sup> tê/îrāṯa ˁadê<sup>?</sup> šukri</td>
        <td>Die Götter essen und trinken,<br/>
            trinken Wein bis zur Sättigung,<br/>
            Most bis zur Trunkenheit.</td>
        <td>P-S ; P<br/>
            P-O<sub>4</sub>-A<br/>
            O<sub>4</sub>-A</td>
        <td>a-b ; c-◌-◌<br/>
         ◌-◌ ; c-d-e<br/>
         ◌-◌ ; ◌-d'-e'</td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">a-b-◌ // a-◌-c (und Varianten; &#x2197; Stufenparallelismus)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.2 iv 8c–9</th></tr>
    <tr>
        <td>hitta ˀêbaka <sup>9</sup> baˁluma<br/>
            hitta ˀêbaka timḫaṣ<br/>
            hitta taṣammit ṣarrataka</td>
        <td>Nun, deinen Gegner, Baˁlu,<br/>
            nun schlag deinen Gegner nieder,<br/>
            nun vernichte deinen Feind!</td>
        <td>A-O<sub>4</sub>-Anr.<br/>
            A-O<sub>4</sub>-P<br/>
            A-P-O<sub>4</sub></td>
        <td>a-b-◌-c<br/>
      a-b-d-◌</br>
      a-d'-b'-◌</td></tr>
<tr><th colspan="4">KTU 1.17 i 13b–15a</th></tr>
    <tr>
        <td>yâdî ṣûtahu<sup>?</sup> <sup>14</sup> [danī]ˀilu<br/>
            yâdî ṣûtahu<sup>?</sup> yaˁlû wa yaški/ub(u)<br/>
            <sup>15</sup> [yâdî] ma/iˀzartêhu pa yalin(/-înu)</td>
        <td>Er legte ab sein Gewand, er, [Danī]ˀilu,<br/>
            er legte ab sein Gewand, stieg hinauf und legte sich hin,<br/>
            [er legte ab] seinen Mantel und bettete sich.</td>
        <td>P-O<sub>4</sub>-S<br/>
            P-O<sub>4</sub> ; P ; w-P<br/>
            P-O<sub>4</sub> ; ◌ ; p-P</td>
        <td>a-b-c ; ◌ ; ◌<br/>
      a-b-◌ ; d ; e<br/>
       a-b'-◌ ; ◌ ; e</td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary">Weitere Formen</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.18 iv 23b–24b</th></tr>
    <tr>
        <td>šupuk(/ šipik) kama šVˀ(V)yi <sup>24</sup> dama<br/>
            kama šāḫiṭi<sup>?</sup> li birkêhu</td>
        <td>Vergieß wie ein Mörder (sein) Blut,<br/>
            wie ein Schlächter bis zu seinen Knien!</td>
        <td>P-A<sub>1</sub>-O<sub>4</sub><br/>
            A<sub>1</sub>-A<sub>2</sub></td>
        <td>a-b-◌-d<br/>
            ◌-b'-c-◌</td></tr>
</table>
</details>
</details>

<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary"><u>a</u> ; <u>b</u> // <u>a</u>' ; <u>◌</u> (und Varianten)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.17 ii 12b–14a</th></tr>
    <tr>
        <td>ˀâṯibanna ˀanāku <sup>13</sup> wa ˀanûḫanna<br/>
            wa tanuḫ bi ˀir(a)tiya <sup>14</sup> napšu</td>
        <td>Ich will mich setzen, (dass) ich zur Ruhe komme,<br/>
            ja, (dass) die Seele in meiner Brust zur Ruhe komme.</td>
        <td>P-S ; P<br/>
         P-A-S</td>
        <td><u>a</u> ; <u>b</u><sub>a</sub><br/>
         <u>◌</u> ; <u>b</u><sub>a'(x-y-z)</sub></td></tr>
<tr><th colspan="4">KTU 1.14 ii 32-34</th></tr>
    <tr>
        <td><sup>32</sup> ˁadānu nugiba wa yâṣiˀ<br/>
            <sup>33</sup> ṣabaˀu ṣabaˀi nugiba<br/>
            <sup>34</sup> wa yâṣiˀ ˁadānu maˁˁu</td>
        <td>Die Armee sei mit Proviant versorgt, dann ziehe sie aus,<br/>
            das Heer des Heeres sei mit Proviant ausgerüstet,<br/>
            dann ziehe die gewaltige Armee aus!</td>
        <td>S-P ; <i>w</i>-P<br/>
            S-P<br/>
            <i>w</i>-P-S</td>
        <td><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c</sub><br/>
            <u>a</u><sub>a'-b</sub> ; <u>◌</u><br/>
            <u>◌</u> ; <u>b</u><sub>c-a'</sub></td></tr>
</table>
</details>

<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary"><u>a</u> ; <u>b</u> // <u>a</u>' ; <u>b</u>' (&#x2197; Alternation)</summary>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary"><u>a</u><sub>a-(b)</sub> ; <u>b</u><sub>c-(d)</sub> // <u>a</u>'<sub>a'-(b')</sub> ; <u>b</u>'<sub>c'-(d')</sub></summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.19 iv 32–33</th></tr>
    <tr>
        <td><sup>32</sup> lV tabarriknī ˀa/âlika barī/ūkatu/ama<br/>
            <sup>33</sup> tamurrānī ˀa/âlika namarratu/ama</td>
        <td>Segne mich doch, (dass) ich gesegnet (hinfort)gehe,<br/>
            segne mich, (dass) ich gesegnet (hinfort)gehe!</td>
        <td>P<sup>+O4</sup> ; P-präd.Attr.(/ A)<br/>
         P<sup>+O4</sup> ; P-präd.Attr.(/ A)</td>
        <td><u>a</u><sub>a</sub> ; <u>b</u><sub>c-d</sub><br/>
            <u>a</u>'<sub>a'</sub> ; <u>b</u>'<sub>c-d'</sub></td></tr>
</table>
</details>

<details class="exampleThirdOrder">
<summary class="exampleThirdOrderSummary"><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c-(d)</sub> // <u>a</u>'<sub>◌-b'</sub> ; <u>b</u>'<sub>c'-(d')</sub></summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.14 i 33–35a</th></tr>
    <tr>
        <td><sup>33</sup> šinatu talˀûninnu<sup>!</sup> <sup>34</sup> wa yiškab<br/>
            nahamâmatu <sup>35</sup> wa yaqmiṣ</td>
        <td>Schlaf überwältigte ihn, da legte er sich nieder,<br/>
            Schlummer, da sank er nieder.</td>
        <td>S-P<sup>+O4</sup> ; <i>w</i>-P<br/>
            S ; <i>w</i>-P</td>
        <td><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c</sub><br/>
            <u>a</u>'<sub>a'-◌</sub> ; <u>b</u>'<sub>c'</sub></td></tr>
<tr><th colspan="4">KTU 1.17 vi 26b–28a</th></tr>
    <tr>
        <td>ˀiriš ḥayyīma lV ˀaqhatu ġāziru<br/>
            <sup>27</sup> ˀiriš ḥayyīma wa ˀâtinaka<br/>
            balî(-)môta <sup>28</sup> wa ˀašalliḥaka</td>
        <td>Wünsch (dir) Leben, o ˀAqhatu, Held,<br/>
            wünsch (dir) Leben, und ich will es dir geben,<br/>
            Unsterblichkeit, und ich will sie dir überreichen!</td>
        <td>P-O<sub>4</sub>-Anr.<br/>
            P-O<sub>4</sub> ; w-P<sup>+O</sup><br/>
            O<sub>4</sub> ; w-P<sup>+O</sup></td>
        <td><u>a</u><sub>a-b-c</sub> ; <u>◌</u><br/>
            <u>a</u>'<sub>a-b-◌</sub> ; <u>b</u>'<sub>d</sub><br/>
            <u>a</u>''<sub>◌-b'-◌</sub> ; <u>b</u>''<sub>d</sub></td></tr>
        <tr><td colspan="4" style="white-space: normal"><p>Anm.: 2.-3. Kolon; zum 1.-2. Kolon &#x2197; Stufenparallelismus.</p></td></tr>
</table>
</details>

</details>

<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary"><u>a</u> // <u>b</u> (&#x2197; Konnexion)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.4 vii 23–25a</th></tr>
    <tr>
        <td><sup>23</sup> lV ragamtu laka lV ˀalˀi<sup>24</sup>yānu baˁlu<br/>
            taṯûbunna baˁlu <sup>25</sup> li hawâtiya</td>
        <td>Hab ich dir doch gesagt, o Mächtiger, Baˁlu,<br/>
            (dass) du, Baˁlu, auf mein Wort zurückkommen wirst!</td>
        <td>P<sub>1</sub>-A<sub>1</sub>-Anr.<br/>
         P<sub>2</sub>-Anr.-A<sub>2</sub></td>
        <td><u>a</u><sub>a-b-c</sub><br/>
         <u>b</u><sub>d-c-e</sub></td></tr>
</table>
</details>

<details class="exampleSecondOrder">
<summary class="exampleSecondOrderSummary">a- // b (&#x2197; Enjambement)</summary>
<table class="exampleTable">
<tr><th colspan="4">KTU 1.6 i 56–57</th></tr>
    <tr>
        <td><sup>56</sup> ˀappūnaka ˁaṯtaru ˁarīẓu<sup>?</sup><br/>
            <sup>57</sup> yaˁlû/î bi ṢRR-āti ṣapāni</td>
        <td>Sodann (ist) ˁAṯtaru, der Starke,<br/>
            hinaufgestiegen auf die Höhen<sup>?</sup> des Zaphon.</td>
        <td>A<sub>1</sub>-S-<br/>
            P-A<sub>2</sub></td>
        <td>a-b-<br/>
            c-d</td></tr>
</table>
</details>

</details>
</details>

<details class="done">
<summary class="doneSummary">Versgliederung</summary>
<p>Die ugaritischen poetischen Texte gliedern sich in verschieden große Verseinheiten. Dazu zählen das &#x2197; Kolon, der &#x2197; Vers und die &#x2197; Strophe (daneben lassen sich größere strukturelle Einheiten identifizieren, die sich aus mehreren Strophen zusammensetzen). Verseinheiten, die sich aus mindestens zwei kleineren Verseinheiten zusammensetzen, werden zusammenfassend als Versgefüge bezeichnet.</p>
<p>Da die ugaritischen Schreiber die Grenzen zwischen Verseinheiten nicht immer graphisch kenntlich machten (etwa durch Zeilensprünge oder horizontale Trennlinien), erweist sich die Rekonstruktion der Versgliederung zuweilen als schwierig. Wichtige Indizien liefern:</p>
<ol type="a">
    <li>das Text- und Zeilenlayout einzelner Manuskripte (die Tafelzeile entspricht zuweilen einem Kolon; horizontale Trennlinien [sofern sie nicht das Ende der Kolumne / die untere Grenze des beschriebenen Bereichs der Tafelseite anzeigen] korrelieren meist mit Strophengrenzen);</li>
    <li style="margin-top: 12px;">bestimmte Lexeme und morphosyntaktische Spezifika (so treten beispielsweise die Lexeme <i>APNK</i> "danach, daraufhin", <i>MK</i> "dann, schließlich" und <i>DM</i> "denn; fürwahr" vorrangig am Anfang des ersten Kolons des Verses auf, die vokativische Anrede oft am Ende des ersten Kolons des Verses und die Konjunktion / Partikel <i>W</i> "und; ja!" immer wieder am Anfang des zweiten Kolons des Verses; Satzgrenzen stimmen oft mit Kolongrenzen überein, fast immer mit Versgrenzen und immer mit Strophengrenzen);</li>
    <li style="margin-top: 12px;">die Analyse paralleler Strukturen (aufeinanderfolgende Phrasen, Kola oder Verse, die parallel zueinander stehen, gehören i. d. R. zum selben übergeordneten Versgefüge);</li>
    <li style="margin-top: 12px;">die Zählung der Silben im Kolon (aufeinanderfolgende Kola und vor allem Kola, die zum selben Vers gehören, sind oft gleich oder ähnlich lang);</li>
    <li style="margin-top: 12px;">textlogische Überlegungen (Verseinheiten, in denen zwei Sachverhalte beschrieben sind, von denen der eine den anderen bedingt oder unmittelbar aus ihm hervorgeht, sind immer wieder <i>einem</i> übergeordneten Versgefüge zuzuordnen).</li>
</ol>
<p>Die Texte richtig zu phrasieren und die Versgliederung auf diese Weise "hörbar" (i. e. auditiv erfahrbar) zu machen, war Aufgabe des Vortragenden, der die Texte rezitierte oder sang (in welchen Kontexten die Texte vorgetragen wurden, ist weitgehend unbekannt; es ist nicht auszuschließen, dass unterschiedliche Vortragende die Texte zuweilen unterschiedlich phrasierten; die rekonstruierte Versgliederung spiegelt also im besten Fall <i>eine</i> Möglichkeit wider, die Texte zu gliedern). Die prosodische Gestaltung der Texte (Rhythmus, Sprechtempo, unterschiedlich ausgeprägte Zäsuren etc.) zeigte dem Publikum, welche Phrasen zu einer strukturellen, grammatischen und inhaltlichen Einheit zu verbinden sind und welche voneinander abzugrenzen sind (die Versgliederung ist folglich entscheidend für das Textverständnis).</p>
<details class="example">
<summary>Beispiel</summary>
<table class="exampleTable">
<tr><th colspan="2">KTU 1.14 iii 14b-19a (Strophe aus drei Bikola)</th></tr>
    <tr>
        <td>wa hanna šapšuma <sup>15</sup> bi šābiˁi<br/>
        &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;wa lā yîšanu pabilu <sup>16</sup> malku<br/>
        li QR-i ṯaˀgati ˀibbīrīhu<br/>
        &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;<sup>17</sup> li qâli nahaqati ḥimārīhu<br/>
        <sup>18</sup> li gaˁâti ˀalapī ḥarṯi<br/>
        &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;zaġâti <sup>19</sup> kalabī ṢPR-i</td>
        <td>Und siehe, bei Sonnenuntergang, am siebten (Tag),<br/>
        &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;da wird König Pabilu nicht schlafen können<br/>
        wegen des Lärms des Gebrülls seiner Stiere,<br/>
        &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;wegen des Dröhnens des Geschreis seiner Esel,<br/>
        wegen des Gebrülls der Pflugrinder,<br/>
        &#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;&#8239;(wegen) des Gebells der Wachhunde<sup>?</sup></td></tr>
    <tr><td colspan="2" style="white-space: normal"><p>Anm.: Die Strophe setzt sich aus drei Bikola zusammen (i. e. aus drei Versen zu je zwei Kola). Die ersten beiden Kola formen einen Enjambement-Vers (&#x2197; Enjambement; der im ersten Kolon eingeleitete Satz ist im zweiten Kolon fortgesetzt; SG-Str.: Interj.-A<sub>1</sub>- // P-S).</p>
    <p>Die Kola, die sich im zweiten und dritten Vers gegenüberstehen, sind jeweils semantisch und grammatisch parallel gestaltet (die parallel gestellten Kola sind jeweils grammatisch identisch aufgebaut; nur im zweiten Kolon des dritten Verses ist die Präposition <i>L</i> am Kolonanfang ausgelassen; &#x2197; Grammatische Varianz, und &#x2197; Ellipse):</p>
    <p>Str. Vers 2: a<sub>"Lärm"</sub>-b<sub>"Gebrüll"</sub>-c<sub>"ein Tier"</sub> // a'-b'-c'<br/>
    Str. Vers 3: a<sub>"Gebrüll"</sub>-b<sub>"ein Tier"</sub>-c<sub>"Einsatzgebiet des Tieres"</sub> //  a'-b'-c'.</p>
    <p>Gleichzeitig stehen der zweite und der dritte Vers - jeweils als Ganzes betrachtet - parallel zueinander. Die einzelnen Kola enthalten jeweils eine mit <i>L</i> eingeleitete Präpositionalphrase (Ausnahme: zweites Kolon des dritten Verses; s. o.), auf die zwei Genitivattribute folgen:</p>
    <p>Str. Vers 2-3:<br/>
    a<sub>1-"Lärm des Gebrülls"</sub>-b<sub>1-"ein Tier"</sub> // a<sub>1</sub>'-b<sub>1</sub>'<br/>
    a<sub>2-"Gebrüll"</sub>-b<sub>2-"ein Tier in best. Einsatzgebiet"</sub> // a<sub>2</sub>'-b<sub>2</sub>'</p>
    <p>Außerdem reimen sich die vier Kola des zweiten und dritten Verses; nicht zuletzt die Kola, die sich jeweils innerhalb eines Verses gegenüberstehen, sind sich klanglich ganz ähnlich (Vers 2: <b><i>li</i></b> // <b><i>li</i></b> - <i>ṯ</i><b><i>aˀgati</i></b> // <i>n</i><b><i>ah</i></b><i>a</i><b><i>qati</i></b> - <i>ˀ</i><b><i>i</i></b><i>bbī</i><b><i>rīhu</i></b> // <i>ḥ</i><b><i>i</i></b><i>mār</i><b><i>īhu</i></b>; Vers 3: <i>g</i><b><i>aˁâti</i></b> // <i>z</i><b><i>aġâti</i></b> - <i>ˀ</i><b><i>alapī</i></b> // <i>k</i><b><i>alabī</i></b>; Vers 2-3: <i>ṯ</i><b><i>aˀ</i></b><i>g</i><b><i>ati</i></b> // <i>n</i><b><i>ah</i></b><i>aq</i><b><i>ati</i></b> | <i>g</i><b><i>aˁâti</i></b> // <i>z</i><b><i>aġâti</i></b>; beachte außerdem <i>ṯ</i><b><i>aˀgati</i></b> | <b><i>gaˁâti</i></b>).</p>
    <p>Der zweite und der dritte Vers sind durch Enjambement mit dem ersten Vers verknüpft (der zweite und der dritte Vers enthalten das Kausaladverbial des übergeordneten Satzes, der im ersten Vers beginnt; SG-Str. Strophe: Interj.-A<sub>1</sub>- // P-S- | A<sub>2</sub> // A<sub>2</sub> | A<sub>2</sub> // A<sub>2</sub>).</p></td></tr>
</table>
</details>
</details>

<details class="undone">
<summary>Vokabular</summary>
</details>

## Z

<details class="undone">
<summary>Zäsur</summary>
</details>

<details class="undone">
<summary>Zeugma</summary>
</details>
</div>