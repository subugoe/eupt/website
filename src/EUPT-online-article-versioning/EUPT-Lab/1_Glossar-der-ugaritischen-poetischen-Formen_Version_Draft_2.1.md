---
title: EUPT-Lab 1 - Steinberger - Glossar der ugaritischen poetischen Formen
lang: de
layout: Layout
head:
  - - meta
    - name: citation_author
      content: Clemens Steinberger
  - - meta
    - name: citation_author_orcid
      content: https://orcid.org/0009-0003-4268-1596
  - - meta
    - name: citation_author_email
      content: clemens.steinberger@theologie.uni-goettingen.de
  - - meta
    - name: citation_author_institution
      content: Universität Göttingen
  - - meta
    - name: citation_title
      content: Glossar der ugaritischen poetischen Formen
  - - meta
    - name: citation_publication_date
      content: 2024-05-03
  - - meta
    - name: citation_language
      content: de
  - - meta
    - name: citation_doi
      content:
  - - meta
    - name: citation_publisher
      content: Reinhard Müller, Clemens Steinberger, Noah Kröll; Edition des ugaritischen poetischen Textkorpus (EUPT)
  - - meta
    - name: citation_journal_title
      content: EUPT-Laboratory
  - - meta
    - name: citation_journal_abbrev
      content: EUPT-Lab
  - - meta
    - name: citation_issn
      content:
  - - meta
    - name: citation_issue
      content: 0001
  - - meta
    - name: citation_keywords
      content: Ugaritic poetry, stylistics, verse structure, poetology
  - - meta
    - name: citation_abstract
      content:
  - - meta
    - name: citation_fulltext_html_url
      content: https://eupt.uni-goettingen.de/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html
  - - meta
    - name: citation_pdf_url
      content:
---
<span class="EUPT-HTML-article-versioning-warning">Version 2.1. This is an outdated version of the site (data may have been updated since this version's publication; styling and functionality are not maintained). For the current version, please return to <a href="/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html">this page</a>.</span>
<a href="#top" id="top-link"></a>

<h1 class="GMU-h1">Glossar der ugaritischen poetischen Formen</h1>
    <p class="GMU-sub-h1"><b>Clemens Steinberger</b></p>
    <p class="GMU-sub-h1">EUPT-<i>Lab</i> 1 | 2024</p>

<div class="EUPT-lab-article-tabs">

<input id="tab1" type="radio" name="tabs" checked="checked">
    <label class="EUPT-lab-article-tabs-label" for="tab1"><p>Info</p></label>
    <div class="EUPT-lab-article-tabs-content">

<div class="GMU-metadata">
<table class="GMU-metadata-table">
	<tr>
        <td>Titel:</td>
        <td><button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">Glossar der ugaritischen poetischen Formen</button></td></tr>
    <tr>
    	<td>Autor:</td>
        <td>Clemens Steinberger <span class="noBreakEUPT">
            <a class="Metadata-Logo-orcid" href="https://orcid.org/0009-0003-4268-1596" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Metadata-Logo-academia" href="https://uni-goettingen.academia.edu/ClemensSteinberger" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Metadata-Logo-university" href="https://uni-goettingen.de/de/219789.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span><br/>
            <span class="noBreakEUPT"><a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Metadata-Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank"><img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span><br/>
            <a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a></td></tr>
    <tr>
        <td><i>Keywords</i>:</td>
        <td>Ugaritic poetry (Ugaritische Poesie), stylistics (Stilistik), verse structure (Versstruktur), poetology (Poetologie)</td></tr>
    <tr>
        <td>Updates:</td>
        <td>Version <i>Draft 2.1</i>: <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">2024-11-15</button>
                <!-- (<a href="" target="_blank" onclick="return confirm('You are accessing an outdated version of this page. This allows you to view data that has been updated since it was first published.\n\nPlease note, however, that this version does not necessarily represent the current state of the site.')">access via archive.org</a>) --><br/>
            Version <i>Draft 2.0</i>: 2024-08-06 
                (<a href="/EUPT-online-article-versioning/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen_Version_Draft_2.0.html" target="_blank" onclick="return confirm('You are accessing an outdated version of this page. This allows you to view data that has been updated since it was first published.\n\nPlease note, however, that this version does not necessarily represent the current state of the site. Styling and functionality of this version are not maintained.')">access HTML</a>)<br/>
            Version <i>Draft 1.0</i>: 2024-05-03 
                (<a href="/EUPT-online-article-versioning/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen_Version_Draft_1.0.html" target="_blank" onclick="return confirm('You are accessing an outdated version of this page. This allows you to view data that has been updated since it was first published.\n\nPlease note, however, that this version does not necessarily represent the current state of the site. Styling and functionality of this version are not maintained.')">access HTML</a>)</td></tr>
    <tr>
        <td>Zugänglich als:</td>
        <td><a href="/EUPT-Lab/Einfuehrung.html" target="_blank">EUPT-<i>Lab</i></a> 1</td></tr>
    <tr>
        <td>DOI:</td>
        <td><i>Der Beitrag erhält erst bei Veröffentlichung der Endfassung eine DOI-Nummer.</i></td></tr>
    <tr>
        <td>Lizenz:</td>
        <td><a rel="license" href="https://creativecommons.org/licenses/by/4.0/" target="_blank"><img class="Metadata-Logo-cc" src="https://licensebuttons.net/l/by/4.0/88x31.png"></a><br/>
        This work is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a>. <span class="noBreakEUPT">© Clemens</span> Steinberger 2024.</td></tr>
    <tr>
        <td>Zitation:</td>
        <td>Steinberger, Clemens. 2024. „Glossar der ugaritischen poetischen Formen (Version #)“. <i>EUPT</i>-Laboratory 1. <a href="/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html" target="_self">https://eupt.uni-goettingen.de/<wbr>EUPT-Lab/<wbr>1_<wbr>Glossar-der-ugaritischen-poetischen-Formen.html</a>. Aufgerufen am YYYY-MM-DD. <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/GMXBZIIC/item-details" target="_blank"><img class="Bibliography-Logo-zotero" src="https://www.zotero.org/support/_media/logo/zotero_32x32x32.png"></a></td></tr>
</table>
</div>
</div>

<input id="tab2" type="radio" name="tabs">
<label class="EUPT-lab-article-tabs-label" for="tab2"><p>Artikel</p></label>
<div class="EUPT-lab-article-tabs-content">

<p>Die ugaritische Poesie ist durch sprachliche, strukturelle und stilistische Besonderheiten geprägt, die die poetischen Texte von nicht-poetischen unterscheiden. Die Gedichte gliedern sich in verschieden große Verseinheiten, die oft parallel gestaltet sind, und enthalten eine Fülle von Stilmitteln.</p>

<p>Das folgende Glossar gibt einen Überblick über diverse Formen, die in der ugaritischen Dichtkunst belegt sind. Um Vergleiche mit verwandten Poesien zu erleichtern, sind auch einige Stilfiguren genannt, für die sich im ugaritischen Korpus bislang keine Beispiele gefunden haben. Unter jedem Glossar-Eintrag in grüner Schrift findet sich eine knappe Definition des behandelten Phänomens mit einigen Beispielen. Die Liste wird fortlaufend erweitert und verbessert.</p>

<p class="GUPF-open-button-container"><button class="EUPT-html-button" onclick="document.querySelectorAll('details.doneEntryGUPF').forEach((e) => {(e.hasAttribute('open')) ? e.removeAttribute('open') : e.setAttribute('open',true)})">Alle Einträge aus-/einklappen</button></p>

<h2>A</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Anakoluth</summary>
<p>&#x2197; Wortstellung.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Anastrophe</summary>
<p>&#x2197; Wortstellung.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Abbildende Wortstellung</summary>
<p>&#x2197; Wortstellung.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Accumulatio</summary>
<!--
<p>
In der Akkumulation sind zwei oder mehrere gleichwertige Nomina (meist Substantive) aneinandergereiht, die gemeinsam ein nominales (i.d.R. selbständiges) Satzglied ergeben. Die Nomina können (müssen aber nicht) durch eine Konjunktion miteinander verbunden sein. 
Stehen immer nebeneinander.
Die Akkumulation verbindet meist Bezeichnungen unterschiedlicher Teile desselben Ganzen (z. B. Bezeichnungen von Mitgliedern derselben Gruppe). Es können aber zum Beispiel auch partielle Synonyme in Form einer Akkumulation aneinandergereiht sein. In dem Fall liegt eine Tautologie vor: Der zweite Begriff betont den gemeinsamen Bedeutungskern und ergänzt die Darstellung gleichzeitig um eine inhaltliche Nuance, die im ersten Begriff nicht zum Ausdruck kommt (analog zur Gegenüberstellung von partiellen Synonymen im semantischen Parallelismus). 
Zwischen den Elementen von Appositions- und Akkumulationsphrasen herrschen z.T. dieselben semantischen Relationen wie zwischen den Elementen von Nominalpaaren, die sich in semantisch parallel gestalteten Versgefügen gegenüberstehen.
Die Akkumulation von partiellen Synonymen (wie in Gilgameš [SBZ; Emar-2a(+)b], i 14'–15'a) wird in der Stilistik meist als Tautologie bezeichnet:
In der akkadischen und ugaritischen Poesie sind oft Bezeichnungen unterschiedlicher Teile desselben Ganzen einander parallel gegenübergestellt. Ebenso verbindet die Akkumulation meist Bezeichnungen unterschiedlicher Teile desselben Ganzen. In Enlil und Namzitarra [SBZ; Ugarit] sind beispielsweise die Teile der Aufzählung kaspu „Silber“, uqnû „Lapis“, alpū „Rinder“, immerū „Schafe“ (Teile des Ganzen „weltliche Reichtümer“) einmal in Form einer Akkumulation verbunden, einmal verteilt über mehrere parallel gestaltete Phrasen.
Sonderformen: Merismus; Enumeratio.
Dient meist dazu, Gesamtheit bildlich zu beschreiben, indem Einzelteile aufgezählt werden.
</p>
-->
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Akrostichon</summary>
<p>Im Akrostichon ergeben die Zeichen, die jeweils am Anfang aufeinanderfolgender Zeilen oder Abschnitte stehen, aneinandergereiht ein Wort, einen Namen oder einen Satz (vgl. <bibl>Grimm, 2007<VuepressApiPlayground url="/api/eupt/biblio/CDFUWIJB" method="get" :data="[]"/></bibl>). Das Akrostichon ist im ugaritischen Korpus bislang nicht bezeugt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Alliteration</summary>
<p>Die Alliteration ist eine Klangfigur. Mindestens zwei Begriffe, die innerhalb eines Kolons aufeinander folgen, klingen gleich an (die alliterierenden Wörter folgen unmittelbar aufeinander). Vermutlich ist nicht jede Alliteration, die sich in den ugaritischen poetischen Texten ausmachen lässt, bewusst gesetzt (s. KTU 1.14 iii 31b-32a oder KTU 1.14 iii 35b-36a). Zuweilen gebrauchten die ugaritischen Dichter die Alliteration aber wohl bewusst, um die Aufmerksamkeit des Publikums auf bestimmte Aussagen zu lenken und diese so hervorzuheben (Alliterationen ließen die Rezipienten des Texts wohl kurz aufhorchen; außerdem haben alliterierend gestaltete Aussagen wahrscheinlich hohen Wiedererkennungswert; s. KTU 1.17 vi 32b-33a). Vielleicht wurden Alliterationen mitunter auch aus euphonischen Gründen (i. e. zugunsten des Wohlklangs) eingesetzt.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 iii 31b-32a</th></tr>
    <tr>
        <td>wa ˀôšānu <span class="ugaritic-text-line-number">32</span>&#160;<b>ˀa</b>bī <b>ˀa</b>dami</td>
        <td>ja, ein Geschenk des Vaters der Menschheit</td></tr>
<tr><th colspan="2">KTU 1.14 iii 35b-36a</th></tr>
    <tr>
        <td>wa <b>ˁa</b>bda <span class="ugaritic-text-line-number">36</span>&#160;<b>ˁā</b>lami ṯalāṯa sus<span class="non-ugaritic-text">(</span>s<span class="non-ugaritic-text">)</span>uwīma</td>
        <td>und einen ewig (gebundenen) Knecht (und) drei Pferde</td></tr>
<tr><th colspan="2">KTU 1.14 iii 43a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">43</span>&#160;dā ˁQ-āha <b>ˀi</b>bbā <b>ˀi</b>qnaˀi</td>
        <td>deren Pupillen (wie) zwei Lapislazuli-Steine sind</td></tr>
<tr><th colspan="2">KTU 1.14 iii 53</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">53</span>&#160;<b>y</b>irḥaṣ <b>y</b>adêhu ˀammatah</td>
        <td>Er wusch seine Hände bis zum Ellbogen.</td></tr>
<tr><th colspan="2">KTU 1.14 v 1b-2</th></tr>
    <tr>
        <td>wa bi <span class="ugaritic-text-line-number">2</span>&#160;<b>m</b>aqâri <b>m</b>umalliˀatu</td>
        <td>ja, die an der Quelle (Schläuche) füllte</td></tr>
<tr><th colspan="2">KTU 1.17 vi 32b-33a</th></tr>
    <tr>
        <td><b>ˀa</b>ppV <b>ˀa</b>nāku <b>ˀa</b>ḥawwiya <span class="ugaritic-text-line-number">33</span>&#160;<b>ˀa</b>qhata <span class="non-ugaritic-text">[</span>ġāzi<span class="non-ugaritic-text">]</span>ra</td>
        <td>(So) will auch ich beleben ˀAqhatu, [den Hel]den!</td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Alternation</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Anadiplose</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Anapher</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Antonomasie</summary>
<!--
<p>
Ersetzung durch Epitheton.
</p>
-->

</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Apokoinu</summary>
<p>Das Apokoinu ist eine Konstruktion, in der sich eine Phrase, ein Satzglied oder ein Satzgliedteil (i. e. das Koinon) gleichzeitig auf die voranstehende und die nachfolgende Phrase bezieht (Sonderform des Zeugmas; vgl. <bibl>Steinhoff / Burdorf, 2007<VuepressApiPlayground url="/api/eupt/biblio/VMKEQ6IB" method="get" :data="[]"/></bibl> und <bibl>Schweikle / Schlösser, 2007<VuepressApiPlayground url="/api/eupt/biblio/ADRMUJGR" method="get" :data="[]"/></bibl>). In der ugaritischen Poesie ist die Figur u. a. in der Kolonkonstruktion P<sup>V</sup> &#8594; SG<sup>N</sup> &#8592; P<sup>V</sup> bezeugt: Das Kolon setzt sich aus zwei verbalen Prädikaten (P<sup>V</sup>) und einem nominalen Satzglied (SG<sup>N</sup>) zusammen. Das nominale Satzglied steht zwischen den beiden Prädikaten; die Prädikate stehen jeweils am Kolonrand. Weder auf das erste Prädikat (am Kolonanfang) noch auf das nominale Satzglied (in der Kolonmitte) folgt eine Konjunktion. Das nominale Satzglied in der Kolonmitte (i. e. das Koinon) bezieht sich gleichermaßen auf das voranstehende und das nachfolgende Prädikat. In den unten zitierten Beispielen bezeichnet das Koinon jeweils das Subjekt der beiden Prädikate. Das Apokoinu kann als Sonderform eines elliptischen Satzpaars begriffen werden: Die Konstruktion P<sup>V</sup>&#8594;S&#8592;P<sup>V</sup> kann auch als P<sup>V</sup>-S / ◌-P<sup>V</sup> (&lt; P<sup>V</sup>-S / S-P<sup>V</sup>) analysiert werden (das Koinon-Element, i. e. das nominal ausgedrückte Subjekt, ist im zweiten Satz ausgelassen).</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.2 i 19b (P&#8594;S&#8592;P)</th></tr>
    <tr>
        <td>tabiˁā ġalmāmi lā yaṯabā</td>
        <td>Es machten sich auf &#8594; die (beiden) Jünglinge &#8592; verweilten nicht.</td></tr>
<tr><th colspan="2">KTU 1.15 iii 17 (P&#8594;S&#8592;P)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17</span>&#160;tabarrikū ˀilūma taˀtiyū</td>
        <td>Es sprachen den Segen &#8594; die Götter &#8592; gingen (heim).</td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Archaismus</summary>
<!--
<p>U.a. y-t-Wechsel im Dual-Präfix</p>
-->

</details>

<details class="undoneEntryGUPF">
<summary>Assonanz und Konsonanz</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Asyndeton</summary>
<p>Im Asyndeton sind die Glieder einer Aufzählung, die sich aus zwei oder mehreren syntaktisch gleichrangigen Lexemen oder Phrasen zusammensetzt, ohne Konjunktion aneinandergereiht (<bibl>Steinhoff, 2007: 51<VuepressApiPlayground url="/api/eupt/biblio/QS82WPVF" method="get" :data="[]"/></bibl>); die erwartete Konjunktion vor dem letzten Glied der Aufzählung ist ausgelassen (Gegenstück zum &#x2197; Polysyndeton). Gleichgeordnete Nomina / Nominalphrasen (keine vollständigen Sätze), die innerhalb eines Kolons aufeinanderfolgen, sind in der ugaritischen Poesie nur selten asyndetisch aneinandergereiht (s. Beispiele A; vgl. <bibl>Steinberger, 2022: 302 Anm. 44<VuepressApiPlayground url="/api/eupt/biblio/ZXWKYWAA" method="get" :data="[]"/></bibl>, u.a. zu KTU 1.4 vi 47-54). Im weiteren Sinn ist jedoch jedes Versgefüge als asyndetisch zu betrachten, das sich in zwei oder mehrere Verseinheiten gliedert, vorausgesetzt dass a) in den einzelnen Verseinheiten je <i>ein</i> Teil desselben übergeordneten Sachverhalts beschrieben ist und b) die einzelnen Verseinheiten nicht durch eine Konjunktion miteinander verbunden sind. Solche Konstruktionen finden sich in der ugaritischen Dichtung häufiger (s. Beispiele B).</p>
<details class="exampleGUPF">
<summary>Beispiele A</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 vi 47-54</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">47</span>&#160;šapîqa ˀilīma karrīma <b>ø</b> yêna</td>
        <td>Er reichte den Göttern (junge) Widder (und) Wein,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">48</span>&#160;šapîqa ˀilahāti ḫupārāti<span class="non-ugaritic-text">(/</span> ḫapūrāti<span class="non-ugaritic-text">)</span></td>
        <td class="further-colon-of-verse">er reichte den Göttinnen (weibliche) Lämmer,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">49</span>&#160;šapîqa ˀilīma ˀalapīma <b>ø</b> yê<span class="non-ugaritic-text">[</span>na<span class="non-ugaritic-text">]</span></td>
        <td>Er reichte den Göttern Ochsen (und) We[in],</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">50</span>&#160;šapîqa ˀilahāti ˀaraḫāti</td>
        <td class="further-colon-of-verse">er reichte den Göttinnen Kühe,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">51</span>&#160;šapîqa ˀilīma kaḥ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ṯīma<span class="non-ugaritic-text"><sup>?</sup></span> <b>ø</b> yêna</td>
        <td>Er reichte den Göttern Herrschersitze (und) Wein,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">52</span>&#160;šapîqa ˀilahāti kussiˀāti</td>
        <td class="further-colon-of-verse">er reichte den Göttinnen Throne,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">53</span>&#160;šapîqa ˀilīma rVḥabāti yêni</td>
        <td>Er reichte den Göttern Krüge voll Wein,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">54</span>&#160;šapîqa ˀilahā<span class="non-ugaritic-text">&lt;</span>ti<span class="non-ugaritic-text">&gt;</span> DKR-ā<span class="non-ugaritic-text">&lt;</span>ti<span class="non-ugaritic-text">&gt;</span></td>
        <td class="further-colon-of-verse">er reichte den Göttinnen Gefäße / (Wein-)Schläuche.</td></tr>
<tr><th colspan="2">KTU 1.14 iii 22-25</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;qaḥ kaspa wa yarqa</td>
        <td>Nimm Silber und Gelbgold,</td></tr>
    <tr>
        <td class="further-colon-of-verse">ḫurāṣa <span class="ugaritic-text-line-number">23</span>&#160;yada maqâmihu</td>
        <td class="further-colon-of-verse">Gold samt seinem Fundort,</td></tr>
    <tr>
        <td>wa ˁabda ˁālami <span class="ugaritic-text-line-number">24</span>&#160;<b>ø</b> ṯalāṯa sus<span class="non-ugaritic-text">(</span>s<span class="non-ugaritic-text">)</span>uwīma</td>
        <td>und einen ewig gebundenen Knecht (und) drei Pferde,</td></tr>
    <tr>
        <td class="further-colon-of-verse">markabta <span class="ugaritic-text-line-number">25</span>&#160;bi tarbaṣi <b>ø</b> bina ˀam<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti</td>
        <td class="further-colon-of-verse">einen Streitwagen aus (meinem) Stall (und) den Sohn einer Magd!</td></tr>
<tr><th colspan="2">KTU 1.14 iii 2-3a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">2</span>&#160;lik yôma wa ṯānâ</td>
        <td>Geh einen Tag lang und einen zweiten,</td></tr>
    <tr>
        <td>ṯāliṯa <b>ø</b> rābiˁa yôma</td>
        <td>einen dritten (und) einen vierten Tag,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">3</span>&#160;ḫāmiša <b>ø</b> ṯādiṯa yôma</td>
        <td>einen fünften (und) einen sechsten Tag!</td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.3 iii 43–44</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">43</span>&#160;maḫaštu môdāda ˀilima ARŠ-a</td>
        <td>Ich schlug nieder den Liebling des ˀIlu, ARŠ,</td></tr>
    <tr>
        <td><b>ø</b> <span class="ugaritic-text-line-number">44</span>&#160;ṣammittu ˁigla ˀili ˁTK-a</td>
        <td>(und) ich vernichtete das Kalb des ˀIlu, ˁTK!</td></tr>
</table>
</details>
<p/>
</details>

<h2>B</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Bikolon</summary>
<p>Das Bikolon ist ein &#x2197; Vers, der sich aus zwei &#x2197; Kola zusammensetzt. Das Bikolon ist die gängigste Versform der ugaritischen Dichtung.</p>
</details>

<h2>C</h2>

<details class="undoneEntryGUPF">
<summary>Chiasmus</summary>
<!--
<p>
Im Chiasmus sind Wörter oder Phrasen, die semantisch und / oder grammatisch korrespondieren, kreuzweise angeordnet.
Chiasmus, partieller (anaphorisch, epiphorisch, anadiplotisch, rahmend)
Anaphorischer partieller Chiasmus: Versmuster: a-{b-c} // a'-{c'-b'}.
Epiphorischer partieller Chiasmus: Versmuster: {a-b}-c // {b'-a'}-c'.
Anadiplotischer partieller Chiasmus: Versmuster: {a-b}-c // c'-{a'-b'}.
Rahmender partieller Chiasmus: Versmuster: a-{b-c} // {b'-c'}-a'.
</p>
-->
</details>

<details class="undoneEntryGUPF">
<summary>Constructio ad sensum</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Conversio</summary>
<p>&#x2197; Geminatio.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Correctio</summary>
</details>

<h2>D</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Distichon</summary>
<p>Das Distichon ist eine &#x2197; Strophe, die sich aus zwei &#x2197; Versen zusammensetzt.</p>
</details>

<h2>E</h2>

<details class="undoneEntryGUPF">
<summary>Ellipse</summary>
<!--
Prädikat-Ellipse
Subjekt-Ellipse
Akkusativobjekt-Ellipse
Adverbial-Ellipse
P ; P-Ellipse
P-SGN-Ellipse
Ellipse + Ergänzung
Ellipse + Appendix
Appendix
-->
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Enjambement</summary>
<p>Im Enjambement sind die Glieder eines zusammenhängenden Satzes (i. d. R. ein Verbalsatz) über zwei (oder mehrere) Verseinheiten verteilt. In der ugaritischen Poesie ist das Enjambement sowohl auf Versebene (i. e. das Kolon-Enjambement zwischen aufeinanderfolgenden Kola) als auch auf Strophenebene belegt (i. e. das das Vers-Enjambement zwischen aufeinanderfolgenden Versen; das Kolon-Enjambement ist öfter belegt als das Vers-Enjambement).</p>
<p>Im Kolon-Enjambement verteilen sich die Glieder eines zusammenhängenden Satzes über zwei (nur in Ausnahmefällen drei) Kola (s. Beispiele A). Die Kola gehören zu <i>einem</i> Vers (der Vers wird als Enjambement-Vers bezeichnet). Nicht selten tritt vor oder hinter die beiden durch Enjambement verbundenen Kola ein weiteres Kolon, das parallel zu einem der beiden Kola steht (so ergibt sich ein &#x2197; Trikolon). Das Kolon des Enjambement-Verses, das des Prädikats des übergeordneten Satzes entbehrt, enthält meist ein einziges selbständiges Satzglied (Subjekt, Akkusativobjekt oder Adverbial; häufig enthält das prädikatlose Kolon ein Adverbial; seltener zwei nicht-prädikativische Satzglieder); die restlichen Satzteile samt dem Prädikat des Satzes stehen im anderen Kolon. Gleichwohl sind die Kola des Enjambement-Verses meist ungefähr gleich lang: Das prädikatlose Kolon setzt sich i. d. R. aus mehreren Worteinheiten zusammen (der nominale Satzteil, der in dem prädikatlosen Kolon steht, ist um ein oder mehrere Attribute / Appositionen erweitert oder setzt sich aus mehreren Nomina zusammen, die in Form einer &#x2197; Accumulatio miteinander verbunden sind).</p>
<details class="exampleGUPF">
<summary>Beispiele A (Kolon-Enjambement)</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.15 ii 11–12</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">11</span>&#160;<span class="non-ugaritic-text">[</span>ˀaḫ<span class="non-ugaritic-text">(</span>ḫa<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">]</span>ra maġāyi ˁidati ˀilīma</td>
        <td>[Nachd]em die Götterversammlung gekommen war,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span>&#160;<span class="non-ugaritic-text">[</span>wa<span class="non-ugaritic-text">]</span> yaˁnî ˀalˀiyā<span class="non-ugaritic-text">[</span>nu<span class="non-ugaritic-text">]</span> baˁlu</td>
        <td>[da] sprach der Mächti[ge], Baˁlu.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A- // P-S; vgl. auch KTU 1.6 ii 26b–27 (A- // S-P); KTU 1.6 i 9b–10a (A<sub>1</sub>- // P-A<sub>2</sub>-O<sub>4</sub>).</p></td></tr>
<tr><th colspan="2">KTU 1.14 iii 3b–5</th></tr>
    <tr>
        <td>maka šapšuma <span class="ugaritic-text-line-number">4</span>&#160;bi šābiˁi</td>
        <td>Dann, bei Sonnenaufgang, am siebten (Tag),</td></tr>
    <tr>
        <td>wa tamġiyu li ˀud<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span>mi <span class="ugaritic-text-line-number">5</span>&#160;rabbati</td>
        <td>da wirst du nach ˀUd(u)mu, zur großen (Stadt), kommen,</td></tr>
    <tr>
        <td>wa li ˀud<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span>mi ṮRR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti</td>
        <td>ja, nach ˀUd(u)mu, zur starken (Stadt).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>- // P-A<sub>2</sub> // A<sub>2</sub> &#8594; a- // b-c // ◌-c'; vgl. auch KTU 1.14 iv 32b–36a; KTU 1.4 iv 27–28 (A- // P-O<sub>4</sub> ; P).</p></td></tr>
<tr><th colspan="2">KTU 1.4 iv 20–22</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">20</span>&#160;ˀid<span class="non-ugaritic-text">(</span>d<span class="non-ugaritic-text">)</span>āka lV tâtin<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> panîma</td>
        <td>Dann machte sie sich auf den Weg</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">21</span>&#160;ˁimma ˀili mabbakV nah<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>rêma</td>
        <td>zu ˀIlu, zur Quelle der beiden Flüsse,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;qarba ˀapīqi<span class="non-ugaritic-text"><sup>?</sup></span> tahāmatêma</td>
        <td>ins Flussbett der beiden Urfluten.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-P-O<sub>4</sub>- // A<sub>2</sub> // A<sub>2</sub> &#8594; a-b-c- // d-e // ◌-e'. Zum Enjambement-Vers, dessen zweites Kolon eine durch <i>ˁM</i> eingeleitete Präpositionalphrase enthält, vgl. auch KTU 1.15 i 3–4 (P-S- // A) und KTU 1.24 16–17a (P-S- // A).</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 27–28a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">27</span>&#160;šaˀū ˀilūma raˀašātikumū</td>
        <td>Erhebt, Götter, eure Häupter</td></tr>
    <tr>
        <td>li ẓûri bir<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>kātikumū</td>
        <td>von euren Knien,</td></tr>
    <tr>
        <td>lina<span class="non-ugaritic-text"><sup>?</sup></span> kaḥ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ṯī<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">28</span>&#160;ZBL-ikumū</td>
        <td>von euren fürstlichen Thronen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: P-Anr.-O<sub>4</sub>- // A // A &#8594; a-b-c- // d // d'. Zum Enjambement-Vers, dessen zweites Kolon eine durch <i>L</i> eingeleitete Präpositionalphrase enthält, vgl. auch KTU 1.2 iii 16b–d (P-S- // A // A; a-b- // c // c') und KTU 1.4 v 46b–48a (P-S ; P- // A).</p></td></tr>
<tr><th colspan="2">KTU 1.4 vii 19b–20</th></tr>
    <tr>
        <td>wa <span class="non-ugaritic-text">[</span>pa<span class="non-ugaritic-text">]</span>taḥ BDQ-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ˁarapāti</td>
        <td>Ja, [ö]ffne einen Spalt in den Wolken</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">20</span>&#160;ˁalê<span class="non-ugaritic-text">/</span>â hawâti kôṯari-wa-ḫasīsi</td>
        <td>gemäß dem Wort des Kôṯaru-wa-Ḫasīsu!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: P-O<sub>4</sub>- // A.</p></td></tr>
<tr><th colspan="2">KTU 1.15 iii 13–15</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">13</span>&#160;maˀda râma <span class="non-ugaritic-text">[</span>kirtu<span class="non-ugaritic-text">]</span></td>
        <td>Hoch erhaben ist [Kirtu]</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">14</span>&#160;bi tôki rāpiˀī ˀar<span class="non-ugaritic-text">[</span>ṣi<span class="non-ugaritic-text">]</span></td>
        <td>inmitten der Rāpiˀūma der ‚Er[de‘],</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">15</span>&#160;bi puḫri QBṢ-i ditāni</td>
        <td>in der Zusammenkunft der Versammlung des Ditānu.</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-P-S- // A<sub>2</sub> // A<sub>2</sub> &#8594; a-b-c- // d // d'.</p></td></tr>
<tr><th colspan="2">KTU 1.14 i 28–30</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">28</span>&#160;tinnatikna ˀudmaˁātuhu</td>
        <td>Seine Tränen ergossen sich</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">29</span>&#160;kama ṯiqalīma ˀarṣah</td>
        <td>wie Schekel zur Erde,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">30</span>&#160;kama ḫamušāti maṭṭâtah</td>
        <td>wie Fünftel (eines Schekels) aufs Bett.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: P-S- // A<sub>1</sub>-A<sub>2</sub> // A<sub>1</sub>-A<sub>2</sub> &#8594; a-b- // c-d // c'-d'.</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 25d–26</th></tr>
    <tr>
        <td>ˀaḥda<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">26</span>&#160;ˀilūma taˁniyū</td>
        <td>Einstimmig mögen die Götter antworten</td></tr>
    <tr>
        <td>lûḥāti malˀakê yammi</td>
        <td>den Tafeln der Boten des Yammu,</td></tr>
    <tr>
        <td>taˁûdati ṯāpiṭi nah<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">&lt;</span>ri<span class="non-ugaritic-text">&gt;</span></td>
        <td>der Gesandtschaft des Herrschers Nah(a)ru.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A-S-P- // O<sub>4</sub> // O<sub>4</sub> &#8594; a-b-c- // d<sub>x-y-z</sub> // d'<sub>◌-y'-z'.</sub></p></td></tr>
<tr><th colspan="2">KTU 1.6 i 56–57</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">56</span>&#160;ˀappūnaka ˁaṯtaru ˁarīẓu<span class="non-ugaritic-text"><sup>?</sup></span></td>
        <td>Sodann (ist) ˁAṯtaru, der Starke,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">57</span>&#160;yaˁlû<span class="non-ugaritic-text">/</span>î bi ṢRR-āti ṣapāni</td>
        <td>hinaufgestiegen auf die Höhen<sup>?</sup> des Zaphon.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-S- // P-A<sub>2</sub>. Zum Enjambement-Vers, dessen erstes Kolon die Phrase <i>APNK</i> - <i>Subjekt<sub>Nomen proprium + Epitheton</sub></i> enthält, vgl. auch KTU 1.5 vi 11–14a (A<sub>1</sub>-S- // P-A<sub>2</sub> ; P-A<sub>2</sub> // A<sub>2</sub> ; P-A<sub>2</sub>; a-b- // c-d ; e-f // ◌-d' ; e'-f') und KTU 1.15 ii 8–9a (A<sub>1</sub>-S- // O<sub>4</sub>-A<sub>2</sub>-P).</p></td></tr>
</table>
</details>
<p>Im Vers-Enjambement sind die Glieder eines zusammenhängenden Satzes über zwei oder mehrere Verse verteilt, die zur selben Strophe gehören (die Strophe wird als Enjambement-Strophe bezeichnet; s. Beispiele B). Die durch Enjambement verbundenen Verse sind meist &#x2197; Bikola. Der Satzteil, der im ersten Kolon jedes Verses steht, wird im zweiten Kolon des Verses parallel aufgegriffen.</p>
<details class="exampleGUPF">
<summary>Beispiele B (Vers-Enjambement)</summary>
<table class="exampleGUPF-Table strophe">
<tr><th colspan="2">KTU 1.14 iv 40–43</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">40</span>&#160;himma ḥurriya bêtaya <span class="ugaritic-text-line-number">41</span>&#160;ˀiqqaḥu <span class="non-ugaritic-text">//</span> ˀašaˁribu ġalmata <span class="ugaritic-text-line-number">42</span>&#160;ḥaẓiraya</td>
        <td>Wenn ich Ḥurriya in mein Haus nehmen kann, // das Mädchen in meine Wohnstatt führen kann,</td></tr>
    <tr>
        <td>ṯinêha kaspima<span class="non-ugaritic-text"><sup>!</sup></span> <span class="ugaritic-text-line-number">43</span>&#160;ˀâtina <span class="non-ugaritic-text">//</span> wa ṯalāṯataha ḫurāṣima</td>
        <td>(dann) will ich ihr Doppeltes an Silber darbringen, // ja, ihr Dreifaches an Gold!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A- // (…) ǀ O<sub>4</sub>-P // (…) &#8594; a<sub><i>HM</i>-x-y-z</sub>- // a'<sub>◌-z'-x'-y'</sub>- ǀ b-c // b'-◌.</p></td></tr>
<tr><th colspan="2">KTU 1.19 iii 42b–45a</th></tr>
    <tr>
        <td>kanapê našarīma <span class="ugaritic-text-line-number">43</span>&#160;baˁlu yaṯbi<span class="non-ugaritic-text">/</span>ur <span class="non-ugaritic-text">//</span> baˁlu yaṯbi<span class="non-ugaritic-text">/</span>ur DˀIY-ê <span class="ugaritic-text-line-number">44</span>&#160;humūti</td>
        <td>Baˁlu möge die Flügel der Adler zerbrechen, // Baˁlu möge die Schwingen von jenen zerbrechen,</td></tr>
    <tr>
        <td>himma taˁûpūna ˁalê<span class="non-ugaritic-text">/</span>â qubūri biniya <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">45</span>&#160;tašaḫîṭūnaninnu bi šinatihu</td>
        <td>wenn sie über das Grab meines Sohnes fliegen, // (und) ihn wecken aus seinem Schlaf!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: O<sub>4</sub>-S-P // (…) ǀ -A // (…) &#8594; a-b-c // b-c-a' ǀ -d // e.</p></td></tr>
<tr><th colspan="2">KTU 1.17 v 4b–7a (Par. KTU 1.19 i 19b–23a)</th></tr>
    <tr>
        <td>ˀappūnaka danīˀilu mutu <span class="ugaritic-text-line-number">5</span>&#160;rāpiˀi <span class="non-ugaritic-text">//</span> ˀa<span class="non-ugaritic-text">&lt;</span>ppV<span class="non-ugaritic-text">&gt;</span>hinnā ġāziru mutu harnamī<span class="non-ugaritic-text">[</span>yi<span class="non-ugaritic-text">]</span><span class="non-ugaritic-text"><sup>?</sup></span></td>
        <td>Sodann (ist) Danīˀilu, der Mann des Rāpiˀu, // sodann (ist) der Held, der Mann des Harnamiten,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">6</span>&#160;yittaša<span class="non-ugaritic-text">/</span>iˀu yâṯib<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> bi ˀappi<span class="non-ugaritic-text">/</span>ê ṯaġri <span class="non-ugaritic-text">//</span> taḥta <span class="ugaritic-text-line-number">7</span>&#160;ˀadurīma dā bi gurni</td>
        <td>aufgestanden (und) hat sich an der Vorderseite des Tores gesetzt, // unter den Noblen, die bei der Tenne (waren).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-S- // (…) ǀ P ; P-A<sub>2</sub> // (…) &#8594; a-b- // a'-b'- ǀ c ; d-e // ◌ ; ◌-e'; vgl. auch KTU 1.17 i 0–3a: A<sub>1</sub>-S- // (…) ǀ A<sub>2</sub>-O<sub>4</sub>-P // (…) &#8594; a-b- // a'-b'- ǀ c-d-e // c-e'-d'.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Epipher</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Enumeratio</summary>
<p>&#x2197; Accumulatio.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Erzählzeit und erzählte Zeit</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Euphemismus</summary>
<p>Der Euphemismus ist eine Form der &#x2197; Periphrase. Ein negativ konnotierter oder tabuisierter Begriff ist durch einen beschönigenden Ausdruck ersetzt. Oberflächlich betrachtet verschleiert der euphemistisch gebrauchte Ausdruck, worum es eigentlich geht (das Beschriebene wird nicht benannt; es wird lediglich darauf angespielt). Steht die wörtliche Bedeutung des euphemistisch gebrauchten Ausdrucks in Kontrast zum Umschriebenen, wirkt die beschönigende Umschreibung ironisierend (&#x2197; Ironie / Witz / Zweideutigkeit). Der Euphemismus benennt in dem Fall genau das, was das Beschriebene <i>nicht</i> ist.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 VIII 7–9</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">7</span>&#160;wa ridā bêta ḫupṯati<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">8</span>&#160;ˀarṣi</td>
        <td>Und steigt hinab in das ‚Haus der Freiheit‘ der ‚Erde‘!</td></tr>
    <tr>
        <td>tissapirā bi yā<span class="ugaritic-text-line-number">9</span>ridī<span class="non-ugaritic-text">(-)</span>ma ˀarṣi<span class="non-ugaritic-text">/</span>a</td>
        <td>Möget ihr gezählt werden zu denen, die in die ‚Erde‘ (i. e. die Unterwelt) hinabsteigen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>BT ḪPṮT</i> „Haus der Freiheit“ in viii 7-8a ist wahrscheinlich eine euphemistische Umschreibung der Unterwelt (s. <i>ARṢ</i> „Erde; Unterwelt“ in viii 8b-9), also für jenes „Haus“, das seinen „Bewohnern“ kaum Freiheiten bietet (wer einmal eingetreten ist, wird gewöhnlich nie wieder freigelassen). Die Unterwelt ist im Grunde also das glatte Gegenteil eines „Hauses der Freiheit“.</p></td></tr>
</table>
</details>
<p/>
</details>

<h2>F</h2>

<details class="undoneEntryGUPF">
<summary>Figura etymologica / Paronomasie</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Fremdwort / Lehnwort</summary>
<p>In den ugaritischen Texten finden sich zahlreiche Fremd- und Lehnwörter, die aus dem Akkadischen oder einer anderen, nicht-semitischen Sprache stammen (<bibl>Watson, 2007: 63-151<VuepressApiPlayground url="/api/eupt/biblio/AE8M9Z9R" method="get" :data="[]"/></bibl>). Manche dieser Lexeme sind ausschließlich im poetischen Korpus belegt (s. Beispiele). Dort treten Fremd- und Lehnwörter immer wieder semantisch / grammatisch parallel (&#8599; Parallelismus) zu ugaritischen Lexemen auf. Das Fremdwort und das ugaritische Lexem bezeichnen in dem Fall dieselbe Sache oder zwei ähnliche oder in der Vorstellungswelt des Dichters und des Publikums miteinander verwandte Sachen.</p>
<p>Vorausgesetzt, dass Dichter und Publikum ein Lexem fremdsprachigen Ursprungs als Fremdwort erkannten (es also <i>nicht</i> als mutmaßlich eigensprachliches, ugaritisches Lexem galt), vermochte das Fremdwort möglicherweise a) die Wortgewandtheit und die hohe Bildung des Dichters herauszustellen (vgl. <bibl>Korpel, 1998: 98-99 / 101<VuepressApiPlayground url="/api/eupt/biblio/8DCQ7D9N" method="get" :data="[]"/></bibl>), b) die poetische Sprache von der Alltagssprache abzuheben, und / oder c) Ugarit als kosmopolitischen Knotenpunkt einer stark vernetzten spätbronzezeitlichen Welt zu inszenieren, an dem unterschiedliche Sprachen und Literaturen aufeinandertrafen (vgl. <bibl>Watson, 2007: 151<VuepressApiPlayground url="/api/eupt/biblio/AE8M9Z9R" method="get" :data="[]"/></bibl>). Fremdwörter kamen aber vermutlich auch aus anderen, praktischen Gründen zum Einsatz: Grundsätzlich ermöglichen Fremdwörter, spezifische real- oder vorstellungsweltliche Konzepte zu bezeichnen, für die keine eigensprachlichen Bezeichnungen etabliert sind. Gleichzeitig erweitern Fremdwörter das lexikalische Repertoire des Dichters, aus dem dieser schöpft, wenn er im parallel gestalteten Versgefüge einem bestimmten (eigensprachlichen) Lexem einen sinnverwandten Begriff gegenüberzustellen sucht.</p>

<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 iv 1-2a (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">1</span>&#160;yaṣaqa bi gulli ḥattuṯi yêna</td>
        <td>Er goss Wein in eine Silberschale,</td></tr>
    <tr>    
        <td><span class="ugaritic-text-line-number">2</span>&#160;bi gulli ḫurāṣi nubta</td>
        <td>Honig in eine Goldschale.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ḤTṮ</i>, vermutlich aus dem Hethitischen (oder Hattischen) entlehnt (DUL<sup>3</sup> 372 s.v. <i>ḥtṯ</i>; <bibl class="bibl-long-spec1">Watson, 2007: 120<VuepressApiPlayground url="/api/eupt/biblio/AE8M9Z9R" method="get" :data="[]"/></bibl>).</p></td></tr>
<tr><th colspan="2">KTU 1.14 iv 49-50 (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">49</span>&#160;garrâninna ˁîrama</td>
        <td>Er stürmte an gegen sie, die Stadt,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">50</span>&#160;šarâninna PDR-ama</td>
        <td>rückte vor gegen sie, die Stadt.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>PDR</i> „Stadt“, vermutlich nicht-semitischen Ursprungs (vgl. DUL<sup>3</sup> 652 s.v. <i>pdr</i> I).</p></td></tr>
</table>
</details>
<p/>
</details>

<h2>G</h2>

<details class="undoneEntryGUPF">
<summary>Genus-komplementärer Parallelismus (<i>gender-matched parallelism</i>)</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Geminatio</summary>
<p>Als Geminatio wird die Wortdoppelung bezeichnet. Ein Wort wird innerhalb des Kolons wiederholt (&#x2197; Repetitio); die beiden Begriffe folgen unmittelbar aufeinander. Die Geminatio ist in der ugaritischen Poesie selten bezeugt. Sie diente wohl der besonderen Hervorhebung des gedoppelten Lexems. In KTU 1.3 iv 32b ist das erste Wort des Kolons verdoppelt, in KTU 1.14 iii 26-27a (und Par.) das letzte (die Geminatio am Kolonende wird als Conversio bezeichnet).</p>
<details class="exampleGUPF">
<summary>Beispiel</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.3 iv 32b</th></tr>
    <tr>
        <td><b>likā likā</b> ˁNN ˀilīma</td>
        <td>Geht, geht, Diener der Götter!</td></tr>
<tr><th colspan="2">KTU 1.14 iii 26-27a (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">26</span>&#160;qaḥ kirtu <b>ŠLM-īma</b> <span class="ugaritic-text-line-number">27</span>&#160;<b>ŠLM-īma</b></td>
        <td>Nimm, Kirtu, Friedensgeschenke, (ja,) Friedensgeschenke!</td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Grammatische Varianz</summary>
</details>

<h2>H</h2>

<details class="undoneEntryGUPF">
<summary>Hendiadyoin</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Hexastichon</summary>
<p>Das Hexastichon ist eine &#x2197; Strophe, die sich aus sechs &#x2197; Versen zusammensetzt.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Homöarkton</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Homöoprophoron</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Homöoptoton</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Homöoteleuton</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Hyperbel</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Hysteron proteron</summary>
</details>  

<h2>I</h2>

<details class="undoneEntryGUPF">
<summary>Inclusio</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Inversion</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Ironie / Witz / Zweideutigkeit</summary>
</details>

<h2>K</h2>

<details class="undoneEntryGUPF">
<summary>Klimax / Steigerung</summary>
<!--
Auch Antiklimax(?) und dabei Verweis aufs Hysteron proteron.
-->
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Kolon</summary>
<p>Das Kolon ist eine elementare Verseinheit (&#x2197; Versgliederung) der ugaritischen Dichtungkunst. Auf manchen Tafeln (bzw. in einzelnen Tafelabschnitten) entspricht das Kolon je einer Zeile. Dies weist darauf hin, dass die ugaritischen Schreiber das Kolon als konstitutive Verseinheit erachteten. Das Kolon enthält i. d. R. mindestens ein selbständiges Satzglied (Prädikat, Subjekt, Akkusativobjekt und / oder Adverbial; oft setzt sich das Kolon aus einem Prädikat und einem oder mehreren nominalen Satzgliedern zusammen). Es umfasst meist drei oder vier Worteinheiten (i. e. Lexemen samt Präpositionen / Partikeln). Am Ende des Kolons steht vermutlich eine &#x2197; Zäsur.</p>
<p>Das Kolon ist meist mit einem oder zwei angrenzenden Kola zu einem &#x2197; Vers verbunden (es ergibt sich ein &#x2197; Bikolon [Vers aus zwei Kola] oder ein &#x2197; Trikolon [Vers aus drei Kola]). Im Fall des Verses, der nur ein Kolon enthält (i. e. des &#x2197; Monokolons), entspricht das Kolon gleichzeitig einem Vers.</p>
<!--
Formen: einfach, komplex, syntaktisch unvollständig intern-parallel
-->
</details>

<details class="undoneEntryGUPF">
<summary>Konnexion</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Konsonanz</summary>
<p>&#x2197; Assonanz und Konsonanz.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Koppelung</summary>
</details>

<h2>L</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Lehnwort</summary>
<p>&#x2197; Fremdwort / Lehnwort.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Litanei</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Litotes</summary>
<p>&#x2197; Periphrase.</p>
</details>

<h2>M</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Merismus</summary>
<p>Im Merismus sind zwei Begriffe miteinander verknüpft, die zwei gegensätzliche Teile desselben Ganzen bezeichnen („Himmel“ ↔ „Erde“; „Sohn“ ↔ „Tochter“; die Teile, die die beiden Begriffe bezeichnen, sind die einzigen Teile des Ganzen oder zwei charakteristische Teile). Der Merismus dient dazu, das Ganze, zu dem die bezeichneten Teile gehören, zu veranschaulichen („Himmel“ ↔ „Erde“ ~ „Kosmos“; „Sohn“ ↔ „Tochter“ ~ „Kinder, Nachkommen“). Die Begriffe können aneinandergereiht und durch eine Konjunktion miteinander verbunden sein (in Form einer &#x2197; Accumulatio; s. Beispiele A) oder einander in zwei (parallelen) Verseinheiten gegenübergestellt sein (s. Beispiele B).</p>
<details class="exampleGUPF">
<summary>Beispiele A</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.16 III 2</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">2</span>&#160;ˁînā tûrā ˀarṣa wa šamîma</td>
        <td>Schaut, durchstreift Erde und Himmel!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ARṢ W ŠMM</i> „Erde und Himmel“ ~ „Kosmos“.</p></td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.15 iii 22-25a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;maka bi šabūˁi šanāti</td>
        <td>Dann, im siebten Jahr,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">23</span>&#160;banū kirti kama<span class="non-ugaritic-text">(-)</span>humū tuddarū</td>
        <td>waren (da) die Söhne Kirtus, wie sie (ihm) versprochen worden waren,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">24</span>&#160;ˀappV binātu ḥurriyi <span class="ugaritic-text-line-number">25</span>&#160;kama<span class="non-ugaritic-text">(-)</span>humū</td>
        <td>und ebenso die Töchter Ḥurriyas, wie sie (ihr versprochen worden waren).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>BN KRT</i> „Söhne Kirtus“ // <i>BNT ḤRY</i> „Töchter Ḥurriyas“ ~ „Kinder von Kirtu und Ḥurriya“ (in Kolon 2-3).</p></td></tr>
<tr><th colspan="2">KTU 1.6 iii 6–7</th></tr>
    <tr>
        <td>šamûma šamna tamṭurūnna</td>
        <td>Die Himmel mögen Öl regnen,</td></tr>
    <tr>
        <td>naḫalūma talikū nubtama</td>
        <td>die Wadis mögen voll Honig fließen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ŠMM</i> „Himmel“ steht parallel zu <i>NḪLM</i> „Wadis“. Die beiden Begriffe bezeichnen die beiden entgegengesetzten Regionen (oben und unten), denen gewöhnlich Wasser entspringt. Die S-P-Phrasen <i>ŠMM TMṬRN</i> „die Himmel mögen regnen“ und <i>NḪLM TLK</i> „die Wadis mögen (voll XY) fließen“ beschreiben zwei Teilaspekte des übergeordneten Sachverhalts „die wasserführenden / -spendenden Regionen des Kosmos mögen (XY über die Erde) fließen lassen“. Das Akkusativobjekt zeigt jeweils an, was sich aus Himmel und Wadi ergießen solle: Nicht etwa Wasser, sondern <i>ŠMN</i> „Öl“ und <i>NBT-</i> „Honig“.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Metapher / Vergleich</summary>
<!--
In parallel gestalteten Verseinheiten treten immer wieder Metaphern oder Vergleichsphrasen auf.  Metaphorische oder vergleichende Umschreibungen wurden dazu eingesetzt, die Schilderung eines Sachverhalts bildlicher und folglich für das Publikum anschaulicher zu gestalten. 
Entweder enthält nur eine der beiden Verseinheiten, die einander parallel gegenübergestellt sind, einen metaphorischen oder vergleichenden Ausdruck (s. Kap. 3.3.2.5.2.1), oder aber beide Verseinheiten sind bildhaft ausgestaltet (s. Kap. 3.3.2.5.2.2).
In der Metapher und im Vergleich sind Begriffe im übertragenen Sinn gebraucht. Dabei gibt es immer einen „Bildempfänger“ und einen „Bildspender“ (z. B. Bildempfänger „Mutter“ / Bildspender „Dattel“ in der metaphorischen Aussage „Meine Mutter ist eine Dattel“ bzw. in der vergleichenden Aussage „Meine Mutter ist wie eine Dattel“ [Beispiel in Anlehnung an Ludingira [RS 25.421; SBZ; Ugarit], Rs. 5'–6']; im Gegensatz zur Metapher ist der Bildspender im Vergleich durch eine Partikel oder ein Morphem ausgewiesen, das dem deutschen „wie“ entspricht). Der Bildempfänger und der Bildspender haben mindestens ein semantisches Merkmal (Sem) gemeinsam; gleichzeitig besitzt der eine Begriff mindestens ein semantisches Merkmal, das auf den anderen nicht zutrifft (vgl. ausführlich Streck, 1999: 30–53). Die Phrase, in der der Bildspender genannt ist, wird als metaphorischer (ohne „wie“) oder vergleichender Ausdruck (mit „wie“) bezeichnet.
BEISPIEL 2: KTU 1.3 iii 47b–iv 2a: In den beiden Kola stehen sich zwei Partizipialphrasen gegenüber (sie beschreiben den Feind, von dem ˁAnatu im vorausgegangenen Abschnitt behauptet, ihn bezwungen zu haben). Es geht um den Feind, der Baˁlu vom Zaphon vertrieben habe. Akkusativobjekt der beiden bivalenten Verben ṭāridi < ṬRD G „vertreiben“ und mušaṣṣiṣi < NṢṢ Š „verscheuchen“ ist Baˁlu, der im ersten Kolon namentlich genannt wird (im zweiten Kolon ist das Nomen proprium Baˁla ausgelassen). Die Partizipialform ṭāridi „(derjenige), der (den Baˁlu) vertrieb“ ist im zweiten Kolon durch die bildhafte Phrase mušaṣṣiṣi ka ˁiṣṣūri „(derjenige), der (den Baˁlu) wie einen Vogel verscheuchte“ ersetzt. Dem Lokaladverbial bi marya/āmī Ṣapāni „von den Höhen des Zaphon“ steht im zweiten Kolon die metaphorische Umschreibung ˀudāna- „aus dem Nest“ gegenüber. Baˁlu, der von den Höhen des Zaphon vertrieben worden sei, wird im zweiten Kolon also mit einem Vogel verglichen, der aus seinem Nest verscheucht wird:
KTU 1.3 iii 47b–iv 2a   
ṭāridi Baˁla iv 1 bi marya/āmī Ṣapāni   (Von dem), der Baˁlu vertrieb von den Höhen des Zaphon, 
mušaṣṣiṣi ka ˁiṣṣūri 2 ˀudānahu?    der (ihn) wie einen Vogel aus seinem Nest scheuchte
BEISPIEL 3: KTU 1.3 iv 42b–43a: Im ersten Kolon des Verses heißt es, dass die Göttin ˁAnatu māha „Wasser“ geschöpft habe, um sich zu waschen (zur Struktur des Verses s. § 766). An die Stelle des Lexems māha „Wasser“ tritt in der ersten Hälfte des zweiten Kolons die Genitivphrase ṭalla šamîma „Tau des Himmels“. Darauf folgt die Genitivphrase šamna ˀarṣi „Öl der Erde“. Die Phrase šamna ˀarṣi umschreibt den realweltlichen Referenten des Nominalpaares māha // ṭalla šamîma, i.e. Wasser, das vom Himmel kommt. šamnu „Öl“ ist eine Metapher für ṭallu „Tau, Tauregen“: Der Regen sei für die Erde wie wertvolles Öl. Vielleicht gründet das Bild auf der Erfahrung, dass die Erde trockener Felder durch den „Tau des Himmels“ (i.e. Regen) zu einer geschmeidigen Masse wird, durch die der Pflug gezogen werden kann (vergleichbar etwa mit Brotteig, der durch die Zugabe von Öl geschmeidiger wird).  Die Genitivattribute šamîma und ˀarṣi, die einander in dem intern-parallelen Kolon gegenübergestellt sind, stehen in direktionaler Opposition: Der erste Begriff bezeichnet den Ursprungsort des Regens, der zweite den Ort, auf den der Regen fällt.
KTU 1.3 iv 42b–43a  
taḥsu/ipunna māha wa tirḥaṣu    Sie schöpfte Wasser und wusch sich
43 ṭalla šamîma šamna ˀarṣi     mit dem Tau des Himmels, mit dem Öl der Erde.
§ 565 Die referenzidentische, zweiseitig-bildhafte Umschreibung: Die beiden Einheiten des Versgefüges enthalten unterschiedliche Metaphern oder Vergleiche. Die parallel gestellten bildhaften Wendungen umschreiben denselben Sachverhalt.  Neben dem bildhaften Wort- oder Phrasenpaar können weitere, nicht-bildhafte Wort- oder Phrasenpaare auftreten (deren Elemente beispielsweise in Teil-Ganzes-Relation stehen können; Atra-ḫasīs [SBZ; Ugarit-2], 5'–6' [Anm. 1446]; Early Rulers [SBZ; Emar-1], Vs. iii 1–2).
BEISPIEL 1:  KTU 1.3 iii 20b–25: Im einleitenden Bikolon (Z. 20b–22a) kündigt Baˁlu an, ˁAnatu seinen rigmu „Ausspruch“ // sein hawâtu „Wort“ kundzutun. Der „Ausspruch“ Baˁlus wird im anschließenden Trikolon bildhaft beschrieben. Die Botschaft des Gottes wird mit den Äußerungen naturweltlicher und kosmischer Entitäten verglichen, die für Normalsterbliche nicht verständlich bzw. gar nicht erst hörbar sind. 
Im ersten Kolon des Trikolons (Z. 22b–23) heißt es, Baˁlus „Ausspruch“ sei (wie) der rigmu ˁiṣṣi „Ausspruch des Baumes“ und das lVḫ(V)šatu ˀabni „Geflüster des Steines“. Die Begriffe rigmu und lVḫ(V)šatu stehen, für sich genommen, in Kontrastrelation: Das erste Lexem bezeichnet einen deutlich hörbaren Ausspruch, das zweite leises Flüstern.  Im zweiten Kolon (Z. 24) wird Baˁlus Botschaft als taˀannîtu? šamîma ˁimma ˀarṣi „Seufzen des Himmels mit der Erde“ beschrieben.  Es geht um eine Äußerung des oberen Bereichs des Kosmos, die an den unteren Bereich des Kosmos gerichtet ist (beachte den Gegensatz zwischen šamûma „Himmel“ und arṣu „Erde“; vielleicht spielt die Phrase auf den Donner an, den Baˁlu vom Himmel auf die Erde schickt). Im letzten Kolon (Z. 25) wird Baˁlus Botschaft schließlich mit dem „(Seufzen) der Urfluten mit den Sternen“ verglichen (das Lexem taˀannîtu? ist am Anfang des Kolons ausgelassen). Baˁlus Botschaft gleiche dem Ausspruch der Urfluten (i.e. dem unteren Bereich des Kosmos), die sich mit den Sternen (i.e. dem oberen Bereich des Kosmos) unterhalten:
KTU 1.3 iii 20b–25  
dāma? rigmu 21 ˀiṯê/â liya wa ˀargumaki Denn eine Nachricht habe ich, und ich will sie dir sagen,
22 hawâtu wa ˀaṯniyaki  eine Botschaft, und ich will sie dir berichten:
rigmu 23 ˁiṣṣi wa lVḫ(V)šatu ˀabni  das Wort des Baumes und das Geflüster des Steines,
24 taˀannîtu? šamîma ˁimma ˀarṣi    das Seufzen des Himmels mit der Erde,
25 tahāmāti ˁimmana kabkabīma   der Urfluten mit den Sternen!
§ 566 Die nicht-referenzidentische, zweiseitig-bildhafte Umschreibung: Die einander gegenüber-gestellten Verseinheiten stellen zwei Teile desselben, übergeordneten Sachverhalts dar (sie stehen in Teil1-Teil2-Relation). Die beiden Einheiten enthalten unterschiedliche Metaphern oder Vergleiche.
BEISPIEL 2: KTU 1.5 vi 20b–22a: Der Vers setzt sich aus zwei Verbalsätzen zusammen (Subjekt ist ˀIlu, der gerade erfahren hat, dass Baˁlu tot sei). Die verbalen Prädikate yaḥriṯu < ḤRṮ G „pflügen“ // yaṯalliṯu < ṮLṮ D „zerfurchen“ sind hier wohl partiell-synonym gebraucht. Die Verben beschreiben die Arbeit des Pflügers, der Furchen in die Erde zieht. Die beiden Prädikate sind jeweils mit einer Vergleichsphrase verbunden: ka ganni „wie ein/en Garten“ // ka ˁi/amqi „wie ein Tal“. Die beiden Vergleiche verstärken das Bild des Pflügers, der den Boden der Gärten und der Felder im Tal zerfurcht. Die beiden Phrasen yaḥriṯu ka ganni „er zerpflügte gleich einem Garten“ und ka ˁi/amqi yaṯalliṯu „er zerfurchte gleich einem Tal“ stehen hier metaphorisch für „er kratzte sich (in Trauer die Haut auf)“.
Die Nominalphrasen ˀappa/ê libbi „Brustkorb“ und bâmata „Rumpf“ (jeweils Akkusativobjekt) sind in ihrer wörtlichen Bedeutung gebraucht. Sie beschreiben den Körper des Subjekts. Die Semantik der beiden Lexeme ist nicht ganz sicher (ˀappa/ê libbi „Brustkorb“ < „Vorderseite des Herzens“; bâmata „Rumpf?“ < „vorderer oder hinterer, unterer Bereich des Rippenbogens“). Vermutlich bezeichnen sie zwei Körperteile des Trauernden (sie stehen also in Teil1-Teil2-Relation). Die Kola verbildlichen demnach die beiden Teilvorgänge „er kratzte (seinen) Brustkorb auf“ und „er kratzte (seinen) Rumpf auf“. Sie sind zurückzuführen auf die holonymische Aussage (übergeordnetes Ganzes) „er kratzte (seinen) Torso auf“:
KTU 1.5 vi 20b–22a (Vers)   
yaḥriṯu 21 ka ganni ˀappa/ê libbi   Er zerpflügte gleich einem Garten den Brustkorb,
ka ˁi/amqi yaṯalliṯu 22 bâmata  zerfurchte gleich einem Tal den Rumpf.
-->
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Metonymie</summary>
<p>In der Metonymie ist ein Wort durch ein anderes (metonymisch gebrauchtes) Wort ersetzt, das (im vorliegenden Kontext) die Bedeutung des ersetzten Begriffs übernimmt, obwohl die Grundbedeutung des metonymisch gebrauchten Lexems von der Bedeutung des ersetzten Begriffs abweicht. Die Grundbedeutung des metonymisch gebrauchten Lexems (bzw. deren Referent, also das, was das Lexem kontextunabhängig bezeichnet) und der ersetzte Begriff (bzw. dessen Referent) stehen aus der Sicht des Autors / der Rezipient:innen des Texts in real- oder vorstellungsweltlicher Beziehung zueinander. Meist stehen die Grundbedeutung des metonymisch gebrauchten Lexems und die im Kontext übernommene Wortbedeutung in <i>Teil-Ganzes</i>- oder <i>Ganzes-Teil</i>-Relation (bzw. einer verwandten Sinnrelation, z. B. <i>Enthaltenes-Behälter</i>; Beispiel: „das Haus des Königs war vollkommen zerstört“; „Haus des Königs“ meint hier nicht den königlichen Palast, sondern - metonymisch - die Königsfamilie / Dynastie; stehen die Grundbedeutung des metonymisch gebrauchten Lexems und die im Kontext übernommene Wortbedeutung in <i>Teil-Ganzes</i>-Beziehung, wird die Figur auch als Synekdoche bezeichnet; entweder steht der <i>Teil</i> für das <i>Ganze</i> [<i>pars pro toto</i>] oder das <i>Ganze</i> steht für den <i>Teil</i> [<i>totum pro parte</i>]).</p>
<p>In der ugaritischen Poesie steht das metonymisch gebrauchte Lexem zuweilen parallel zu dem Lexem, dessen Bedeutung es übernimmt. Die parallel gestellten Lexeme, von denen eines metonymisch gebraucht ist, beziehen sich in dem Fall auf dieselbe real- oder vorstellungsweltliche Sache. Wenngleich Rezipient:innen erkannt haben mögen, dass eines der beiden Lexeme des Wortpaars im übertragenen Sinn zu verstehen ist, ist zu vermuten, dass sie sich zumindest kurzzeitig auch die Grundbedeutung des metonymisch gebrauchten Lexems vor Augen führten. Der metonymische Ausdruck lenkte die Aufmerksamkeit auf eine Sache, die mit dem eigentlich Gemeinten (bezeichnet durch das parallel gestellte Lexem) auf die eine oder andere Sache verwandt ist, und hatte so vermutlich Einfluss darauf, welche Assoziationen das Publikum mit dem parallel gestellten Lexem verband. Der metonymische Ausdruck trug so wohl zur Anschaulichkeit des Aussagenpaars bei.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 i 6b-8a</th></tr>
    <tr>
        <td>ˀummatu <span class="ugaritic-text-line-number">7</span>&#160;<span class="non-ugaritic-text">[</span>kirti<span class="non-ugaritic-text">]</span> ˁaruwat</td>
        <td>Die Sippe [Kirtus] war vernichtet,</td></tr>
    <tr>
        <td>bêtu <span class="ugaritic-text-line-number">8</span>&#160;<span class="non-ugaritic-text">[</span>ma<span class="non-ugaritic-text">]</span>lki ˀîtab<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>da<span class="non-ugaritic-text"><sup>!</sup></span></td>
        <td>[des Kö]nigs Haus war völlig zerstört.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>BT</i> „Haus“, metonymisch für „Dynastie“ (i. e. die Angehörigen der königlichen Familie, die im Palast leben bzw. lebten) ~ <i>UMT</i> „Familie, Sippe, Dynastie“.</p></td></tr>
<tr><th colspan="2">KTU 1.14 ii 27b-29</th></tr>
    <tr>
        <td>ˁadaba <span class="ugaritic-text-line-number">28</span>&#160;ˀakla li qar<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>yati</td>
        <td>Er bereite Speise für die Stadt,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">29</span>&#160;ḥiṭṭata li bêti-ḫābūri</td>
        <td>Weizen für Bêtu-Ḫabūri!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ḤṬṬ</i> „Weizen“, metonymisch für „Speise, Brot“ (i. e. das, was aus Weizen zubereitet wird) ~ <i>AKL</i> „Speise, Brot“.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Monokolon</summary>
<p>Das Monokolon ist ein &#x2197; Vers, der ein einziges &#x2197; Kolon umfasst.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Monostichon</summary>
<p>Das Monostichon ist eine &#x2197; Strophe, die einen einzigen &#x2197; Vers enthält.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Musik / Musikalische Gestaltung</summary>
</details>

<h2>P</h2>

<details class="undoneEntryGUPF">
<summary>Parallelismus</summary>
<!--
Aufeinanderfolgende Versteile korrespondieren semantisch, grammatisch und gelegentlich auch klanglich (klassisch als ). Das Phänomen wird als poetischer Parallelismus (klassisch <i>parallelismus membrorum</i>) bezeichnet und ist ein Kerncharakteristikum der ugaritischen Poesie
Der Parallelismus  ist ein konstitutives Prinzip der akkadischen und ugaritischen Dichtkunst.  Der Parallelismus ist eine Wiederholungsfigur: Er gründet auf der Rekurrenz  eines oder mehrerer sprachlicher Merkmale  in aufeinanderfolgenden Verseinheiten (s. Kap. 2.1). Parallele Verseinheiten korrespondieren semantisch, syntaktisch, morphologisch und / oder klanglich. Die korrespondierenden Elemente stehen in den einzelnen Verseinheiten oft an der jeweils gleichen Position. Sie können aber auch unterschiedlich angeordnet sein, woraus sich beispielsweise Chiasmen ergeben (s. Kap. 3.5).
Der Parallelismus prägt den Aufbau unterschiedlich großer Verseinheiten: Die Phrasen, aus denen das Kolon aufgebaut ist, die Kola, die sich im Vers gegenüberstehen, die Verse, die zu einer Strophe verbunden sind, und auch aufeinanderfolgende Strophen und ganze Erzählabschnitte können jeweils parallel gestaltet sein.
Komplementär zum Parallelismus steht das Prinzip der Varianz:  Parallel gestellte Verselemente korrespondieren meist in einigen, aber nicht in all ihren semantischen, grammatischen und klanglichen Merkmalen. Außerdem hat nicht zwingend jedes Element der ersten Verseinheit ein (auf die eine oder andere Weise) korrespondierendes Gegenstück in der zweiten Verseinheit, und es finden sich in den untersuchten Korpora auch Versgefüge, deren einzelne Einheiten gar nicht parallel gestaltet sind (beispielsweise im Enjambement-Vers). Poetische Texte konstituieren sich also durch das Zusammenspiel von Rekurrenz und Varianz.
Semantik, Grammatik, Klanggestalt, Struktur: Der Parallelismus prägt die Semantik, die Grammatik, die Klanggestalt und die Wortstellung poetischer Texte: Parallel gestellte Verseinheiten enthalten meist inhaltlich verwandte Aussagen (s. Kap. 3.3.) und gleichen einander syntaktisch und morphologisch (s. Kap. 3.4.). Sie sind oft gleich oder ähnlich lang und enthalten zuweilen die gleichen Phoneme, woraus sich Reime ergeben (s. Kap. 3.6.).  Die Elemente, die semantisch und grammatisch (und zuweilen auch klanglich) korrespondieren, sind in den aneinandergereihten Verseinheiten in derselben Reihenfolge (i.e. parallel) oder unterschiedlich angeordnet (chiastisch oder partiell-chiastisch). Dabei können Elemente der ersten Verseinheit in der zweiten entfallen oder Elemente in der zweiten Verseinheit ergänzt sein, die in der ersten Verseinheit ausgelassen sind (s. Kap. 3.5.).
Die verschiedenen Ebenen des poetischen Parallelismus beeinflussen sich gegenseitig: Beispielsweise beeinflussen die Wortwahl und das darzustellende Sinnverhältnis zwischen zwei aufeinanderfolgenden Verseinheiten die Form der grammatischen (und auch der klanglichen) Korrespondenz zwischen den einander gegenübergestellten Versteilen (s. § 641a). Gleichzeitig beeinflusste das Bestreben akkadischer und ugaritischer Dichter, zusammengehörige Verseinheiten syntaktisch und morphologisch identisch oder ähnlich zu gestalten, die Wortwahl (s. § 623). Die Konvention, aufeinanderfolgende Verseinheiten (in erster Linie Kola) gleich oder ähnlich lang zu gestalten, und der Reim hatten wiederum Einfluss auf die Wortwahl und folglich das Sinnverhältnis zwischen den beiden Einheiten sowie auf ihre grammatische Relation (s. § 889).
BEISPIEL 2: KTU 1.3 v 33b–34b: Das Bikolon enthält die Rede der ˁAnatu, die ˀIlu klarzumachen versucht, dass die Götterschaft gesammelt hinter Baˁlu stehe und diesem (als ihrem König) dienen wolle. Die beiden Kola beschreiben denselben Sachverhalt und setzen sich aus den gleichen syntaktischen Gliedern zusammen. Das Pronomen klnyy wird im zweiten Kolon wörtlich wiederholt. Die korrespondierenden Verbalformen sind fast identisch (nâbilanna // nâbila). Sie unterscheiden sich nur dadurch, dass im zweiten Kolon die Energikusendung -nna ausgelassen ist (s. § 642). Die Akkusativobjekte qašâ-hu und kāsa-hu sind partiell-synonym, also mehr oder weniger bedeutungsgleich. Außerdem klingen sie ganz ähnlich.
Die einander gegenübergestellten Elemente entsprechen sich also z.T. wörtlich und korrespondieren grammatisch, semantisch und klanglich. Die Versbausteine sind in den beiden Kola allerdings in unterschiedlicher Reihenfolge angeordnet: kullunāya-ya steht jeweils am Kolonanfang. Das Akkusativobjekt qašâ-hu // kāsa-hu und das Prädikat nâbilanna // nâbila formen eine Einheit, deren Glieder chiastisch angeordnet sind: Im ersten Kolon steht das Akkusativobjekt vor dem Prädikat, im zweiten Kolon dahinter (es liegt ein sogenannter anaphorischer partieller Chiasmus vor; s. Kap. 3.5.2.3.2.1.): 
KTU 1.3 v 33b–34b (Vers)	
kullunāya-ya qašâhu 34 nâbilanna 	Wir alle wollen (ihm) seine Schale bringen,
kullunāya-ya nâbila kāsahu	wir alle wollen (ihm) bringen seinen Becher!
§ 319 Mittels Parallelismus konnten unterschiedlich große Verseinheiten konstruiert werden. Die Verseinheit, die in der folgenden Einheit aufgegriffen wird, kann also unterschiedlich lang sein:  Die Einheit kann sich auf einen Kolonteil beschränken, der semantisch, grammatisch und / oder klanglich mit dem folgenden Kolonteil korrespondiert. Besonders oft ist es ein Kolon, das parallel zu einem angrenzenden Kolon steht und sich mit diesem zu einem Vers verbindet. Jedoch können auch ganze Verse parallel zueinander stehen, die sich zu einer Strophe verbinden. 
§ 320 Der Parallelismus im Kolon: Wie bereits oben festgestellt (s. Kap. 21.14), gliedert sich das Kolon zuweilen in zwei (seltener drei) parallele Teile. Dies ist einerseits im komplexen Kolons zu beobachten, das zwei prädikathaltige Phrasen verbindet, andererseits im syntaktisch unvollständigen Kolon, das zwei gleich aufgebaute Nominal- oder Präpositionalphrasen einschließt.
BEISPIEL 1: KTU 1.4 iv 9: Das Kolon schließt zwei selbständige Sätze ein, die gleich aufgebaut sind: Auf das verbale Prädikat (in der SK) folgt jeweils das substantivische Akkusativobjekt. Die beiden Sätze korrespondieren semantisch: Die Verben MDL und ṢMD sind partiell-synonym.  Das Akkusativobjekt des ersten Satzes ist vermutlich ein Unterbegriff (Hyponym) des Akkusativobjekts des zweiten Satzes: Das Lexem ˁêru „Eselhengst, männlicher Esel“ bezeichnet eine bestimmte Art eines paḥlu „männlichen Zuchttiers“ (s. Kap. 3.3.2.1.2.):
KTU 1.4 iv 9	
9 madala ˁêra 	Er schirrte den Eselhengst an, 
ṣamada paḥla?	spannte den Hengst an.
Semantischer Parallelismus  
Grammatischer Parallelismus  
Struktureller Parallelismus  
-->
</details>

<details class="undoneEntryGUPF">
<summary>Parenthese</summary>
<!--
S. auch Split Couplet.
-->
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Paronomasie</summary>
<p>&#x2197; Figura etymologica / Paronomasie.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary"><i>pars pro toto</i></summary>
<p>&#x2197; Metonymie.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Partieller Chiasmus</summary>
<p>&#x2197; Chiasmus.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Pentakolon</summary>
<p>Das Pentakolon ist ein &#x2197; Vers, der sich aus fünf &#x2197; Kola zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Pentastichon</summary>
<p>Das Pentastichon ist eine &#x2197; Strophe, die sich aus fünf &#x2197; Versen zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Periphrase</summary>
<p>Die Periphrase (Umschreibung) ist ein Begriff oder eine mehrgliedrige Phrase, der / die ein anderes Lexem umschreibt (Beispiel: "Blut der Bäume" für "Wein"). Die Periphrase stellt ein Charakteristikum des Umschriebenen in den Fokus und illustriert so das Konzept, das dem umschriebenen Begriff zugrunde liegt (das Lexem X und die Periphrase Y können gewöhnlich in der Aussage "X ist / bedeutet Y" verbaut werden [Beispiel: "<i>Brüder</i> sind <i>Söhne einer Mutter</i>"]; so gesehen definiert die Periphrase den umschriebenen Begriff; die Periphrase enthält i. d. R. mindestens ein Lexem, dessen Grundbedeutung sich nicht mit der Bedeutung des umschriebenen Begriffs deckt).</p>
<p>In der ugaritischen Poesie setzt sich die Periphrase oft aus einem Substantiv und einem untergeordneten Attribut zusammen; sie umscheibt ein einfaches Substantiv (vereinzelt tritt die Periphrase auch in Form einer zweigliedrigen Verbalphrase auf, die ein Substantiv [KTU 1.17 ii 5b–6a] oder ein Verb umschreibt [KTU 1.16 vi 41b–42]; s. Beispiele A). Eine andere Form der Periphrase gründet auf der Negation eines Begriffs, der das Gegenteil des umschriebenen Lexems bezeichnet (i. e. eines Atonyms oder eines komplementären Begriffs des Umschriebenen). Wird das Gegenteil des umschriebenen Lexems verneint, ergibt sich eine partiell-synonyme Periphrase des Begriffs (dies entspricht einer doppelten Verneinung [Beispiel: "nicht am Leben lassen" für "töten"; "nicht am Leben lassen" ~ "nicht nicht töten"]; solche und ähnliche Konstruktionen werden in der Stilistik als Litotes bezeichnet [scheinbare Abschwächung der Aussage, i. d. R. durch einen verneinten / doppelt-verneinten Ausdruck, wodurch die eigentliche Aussage verstärkt wird: "nicht unwahrscheinlich" ~ "ziemlich wahrscheinlich"]; die Litotes wird in der Stilistik zuweilen von der Periphrase getrennt behandelt; <bibl>Braak / Neubauer, 2001: 52-55<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>). In der ugaritischen Dichtung diente diese Form der Periphrase meist der Umschreibung eines Verbalausdrucks (vereinzelt auch eines Substantivs [KTU 1.17 vi 26b–28a]; s. Beispiele B). Zu den Sonderformen der Periphrase zählt der &#x2197; Euphemismus. Zuweilen werden auch die &#x2197; Antonomasie und die &#x2197; Metonymie als Formen der Periphrase betrachtet (<bibl>Braak / Neubauer, 2001: 47 / 54<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>; <bibl>Schweikle, 2007<VuepressApiPlayground url="/api/eupt/biblio/DB62UX55" method="get" :data="[]"/></bibl>).</p>
<p>Die ugaritischen Dichter gebrauchten die Periphrase u. a. in Versgefügen, in denen einander zwei referenzidentische Ausdrücke gegenübergestellt werden sollten (Ausdrücke sind referenzidentisch, wenn sie dieselbe real- oder vorstellungsweltliche Sache bezeichnen): Die Periphrase ermöglichte ihnen, einem Lexem aus der einen Verseinheit ein mehr oder weniger bedeutungsgleiches Pendant in der anderen Verseinheit gegenüberzustellen, ohne das konkrete Lexem (für das es möglicherweise kein Synonym gab) zu wiederholen. Gleichzeitig erklärt die Periphrase den parallel gestellten Begriff und trägt somit zur Anschaulichkeit und Verständlichkeit der Aussage bei. Die Periphrase, in der das Gegenteil des umschriebenen Begriffs verneint wird, wirkt mitunter verstärkend ("niederschlagen" // "nicht am Leben lassen" ~ "töten, erschlagen").</p>
<p>Die Periphrase (Umschreibung) ist ein Begriff oder eine mehrgliedrige Phrase, der / die ein anderes Lexem umschreibt (Beispiel: „Blut der Bäume“ für „Wein“). Die Periphrase stellt ein Charakteristikum des Umschriebenen in den Fokus und illustriert so das Konzept, das dem umschriebenen Begriff zugrunde liegt (das Lexem X und die Periphrase Y können gewöhnlich in der Aussage „X ist / bedeutet Y“ verbaut werden [Beispiel: „<i>Brüder</i> sind <i>Söhne einer Mutter</i>“]; so gesehen definiert die Periphrase den umschriebenen Begriff; die Periphrase enthält i. d. R. mindestens ein Lexem, dessen Grundbedeutung sich nicht mit der Bedeutung des umschriebenen Begriffs deckt).</p>
<p>In der ugaritischen Poesie setzt sich die Periphrase oft aus einem Substantiv und einem untergeordneten Attribut zusammen; sie umscheibt ein einfaches Substantiv (vereinzelt tritt die Periphrase auch in Form einer zweigliedrigen Verbalphrase auf, die ein Substantiv [KTU 1.17 ii 5b–6a] oder ein Verb umschreibt [KTU 1.16 vi 41b–42]; s. Beispiele A). Eine andere Form der Periphrase gründet auf der Negation eines Begriffs, der das Gegenteil des umschriebenen Lexems bezeichnet (i. e. eines Atonyms oder eines komplementären Begriffs des Umschriebenen). Wird das Gegenteil des umschriebenen Lexems verneint, ergibt sich eine partiell-synonyme Periphrase des Begriffs (dies entspricht einer doppelten Verneinung [Beispiel: „nicht am Leben lassen“ für „töten“; „nicht am Leben lassen“ ~ „nicht nicht töten“]; solche und ähnliche Konstruktionen werden in der Stilistik als Litotes bezeichnet [scheinbare Abschwächung der Aussage, i. d. R. durch einen verneinten / doppelt-verneinten Ausdruck, wodurch die eigentliche Aussage verstärkt wird: „nicht unwahrscheinlich“ ~ „ziemlich wahrscheinlich“]; die Litotes wird in der Stilistik zuweilen von der Periphrase getrennt behandelt; <bibl>Braak / Neubauer, 2001: 52-55<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>). In der ugaritischen Dichtung diente diese Form der Periphrase meist der Umschreibung eines Verbalausdrucks (vereinzelt auch eines Substantivs [KTU 1.17 vi 26b–28a]; s. Beispiele B). Zu den Sonderformen der Periphrase zählt der &#x2197; Euphemismus. Zuweilen werden auch die &#x2197; Antonomasie und die &#x2197; Metonymie als Formen der Periphrase betrachtet (<bibl>Braak / Neubauer, 2001: 47 / 54<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>; <bibl>Schweikle, 2007<VuepressApiPlayground url="/api/eupt/biblio/DB62UX55" method="get" :data="[]"/></bibl>).</p>
<p>Die ugaritischen Dichter gebrauchten die Periphrase u. a. in Versgefügen, in denen einander zwei referenzidentische Ausdrücke gegenübergestellt werden sollten (Ausdrücke sind referenzidentisch, wenn sie dieselbe real- oder vorstellungsweltliche Sache bezeichnen): Die Periphrase ermöglichte ihnen, einem Lexem aus der einen Verseinheit ein mehr oder weniger bedeutungsgleiches Pendant in der anderen Verseinheit gegenüberzustellen, ohne das konkrete Lexem (für das es möglicherweise kein Synonym gab) zu wiederholen. Gleichzeitig erklärt die Periphrase den parallel gestellten Begriff und trägt somit zur Anschaulichkeit und Verständlichkeit der Aussage bei. Die Periphrase, in der das Gegenteil des umschriebenen Begriffs verneint wird, wirkt mitunter verstärkend („niederschlagen“ // „nicht am Leben lassen“ ~ „töten, erschlagen“).</p>
<p>Die Periphrase ist meist länger als der umschriebene Begriff; nicht selten setzt sich die Periphrase aus zwei Substantiven oder einem Substantiv und einem Verb zusammen. Im Vers, der sich aus mindestens zwei Kola zusammensetzt, kann die Periphrase die Ellipse eines anderen Satzteils längenmäßig kompensieren, sodass sich trotz der Auslassung eines Glieds gleich oder ähnlich lange Kola ergeben (KTU 1.14 i 8b-9: im zweiten Kolon sind die Partikel <i>D</i> und der Präpositionalausdruck <i>LH</i> ausgelassen; da aber das Substantiv <i>AḪM</i> durch die längere Periphrase <i>BN UM</i> ersetzt ist [und außerdem das längere Zahlwort <i>ṮMNT</i> an die Stelle des kürzeren <i>ŠBˁ</i> tritt], sind die beiden Kola vermutlich exakt gleich lang).</p>
<details class="exampleGUPF">
<summary>Beispiele A</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 i 8b-9</th></tr>
    <tr>
        <td>dā šabˁu <span class="ugaritic-text-line-number">9</span>&#160;ˀaḫḫūma lahu</td>
        <td>(des Königs,) der (einst) sieben Brüder hatte,</td></tr>
    <tr>
        <td>ṯamānîtu <b>banū ˀummi</b></td>
        <td>acht Söhne einer Mutter</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>AḪM</i> „Brüder“ ~ <i>BN UM</i> „Söhne einer Mutter“.</p></td></tr>
<tr><th colspan="2">KTU 1.17 i 11c–13a</th></tr>
    <tr>
        <td>ˀuzūra<span class="non-ugaritic-text">/</span>u <span class="ugaritic-text-line-number">12</span>&#160;<span class="non-ugaritic-text">[</span>ˀilī<span class="non-ugaritic-text">]</span>ma danīˀilu</td>
        <td>Gegürtet (hat) [den Göt]tern Danīˀilu,</td></tr>
    <tr>
        <td>ˀuzūra<span class="non-ugaritic-text">/</span>u ˀilīma yulaḥḥimu</td>
        <td>gegürtet hat er den Göttern zu Essen gegeben,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">13</span>&#160;<span class="non-ugaritic-text">[</span>ˀuzū<span class="non-ugaritic-text">]</span>ra<span class="non-ugaritic-text">/</span>u yašaqqiyu <b>banī qudši</b></td>
        <td>[gegürt]et hat er zu Trinken gegeben den Söhnen des Heiligen.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ILM</i> „Götter“ ~ <i>BN QDŠ</i> „Söhne des Heiligen (scil. des ˀIlu)“.</p></td></tr>
<tr><th colspan="2">KTU 1.4 iii 43b–44</th></tr>
    <tr>
        <td><span class="non-ugaritic-text">[</span>tištayū<span class="non-ugaritic-text">]</span> karpānīma yêna</td>
        <td>[Sie tranken] aus Bechern Wein,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">44</span>&#160;<span class="non-ugaritic-text">[</span>bi kāsī ḫurāṣi <b>da</b><span class="non-ugaritic-text">]</span><b>ma ˁiṣṣīma</b></td>
        <td>[aus Goldbechern das „Bl]ut der Bäume“.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>YN</i> „Wein“ ~ <i>DM ˁṢM</i> „Blut der Bäume“.</p></td></tr>
<tr><th colspan="2">KTU 1.17 ii 5b–6a (und Par.)</th></tr>
    <tr>
        <td>ˀāḫidu yadaka bi ša<span class="non-ugaritic-text">[</span>karāni<span class="non-ugaritic-text">]</span></td>
        <td>der deine Hand packt bei Tru[nkenheit],</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">6</span>&#160;muˁammisuka <b>kī šaba<span class="non-ugaritic-text">/</span>iˁta yêna</b></td>
        <td>dich stützt, wenn du gesättigt bist mit Wein</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>B ŠKRN</i> „im Fall von (deiner) Trunkenheit“ ~ <i>K ŠBˁt YN</i> „wenn du gesättigt bis mit Wein“.</p></td></tr>
<tr><th colspan="2">KTU 1.16 vi 41b–42</th></tr>
    <tr>
        <td>šamaˁ maˁ<span class="non-ugaritic-text">(</span>ˁa<span class="non-ugaritic-text">)</span> lV Kirtu <span class="ugaritic-text-line-number">42</span>&#160;Ṯˁ-u</td>
        <td>Hör’ doch, o edler Kirtu,</td></tr>
    <tr>
        <td>ˀištamVˁ wa <b>taqġû<span class="non-ugaritic-text">/</span>î ˀudna</b></td>
        <td>horch her, ja, schenk (mir) Gehör!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ŠMˁ Mˁ</i> „Hör’ doch!“ // <i>IŠTMˁ</i> „Hör’ genau zu!“ ~ <i>TQĠ UDN</i> „Schenk Gehör!“</p></td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.19 i 14b–16a</th></tr>
    <tr>
        <td>KD ˁalê<span class="non-ugaritic-text">/</span>â qaštihu <span class="ugaritic-text-line-number">15</span>&#160;ˀimḫaṣhu</td>
        <td>Für seinen Bogen schlug ich ihn nämlich nieder,</td></tr>
    <tr>
        <td>ˁalê<span class="non-ugaritic-text">/</span>â qaṣaˁātihu huwati <span class="ugaritic-text-line-number">16</span>&#160;<b>lā ˀaḥawwî</b></td>
        <td>für seine Pfeile ließ ich ihn nicht am Leben.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>IMḪṢ</i> „ich schlug nieder“ ~ <i>L AḤW</i> „ich ließ nicht am Leben“; vgl. auch KTU 1.18 iv 12b–13.</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 19b</th></tr>
    <tr>
        <td>tabiˁā ġalmāmi <b>lā yaṯabā</b></td>
        <td>Die (beiden) Jünglinge machten sich auf, verweilten nicht.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>TBˁ</i> „sie machten sich auf“ ~ <i>L YṮB</i> „sie verweilten nicht“.</p></td></tr>
<tr><th colspan="2">KTU 1.17 vi 26b–28a</th></tr>
    <tr>
        <td>ˀiriš ḥayyīma lV ˀAqhatu ġāziru</td>
        <td>Wünsch’ (dir) Leben, o ˀAqhatu, Held,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">27</span>&#160;ˀiriš ḥayyīma wa ˀâtinaka</td>
        <td>wünsch’ (dir) Leben und ich will es dir geben,</td></tr>
    <tr>
        <td><b>balî<span class="non-ugaritic-text">(-)</span>môta</b> <span class="ugaritic-text-line-number">28</span>&#160;wa ˀašalliḥaka</td>
        <td>Unsterblichkeit und ich will sie dir überreichen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ḤYM</i> „Leben, ewiges Leben“ ~ <i>BL</i>(-)<i>MT</i> „Nicht-Tod, Unsterblichkeit“.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Personifikation</summary>
<!--
Môtu in KTU 1.16
Keulen Baals
-->
</details>

<details class="undoneEntryGUPF">
<summary>Pleonasmus</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Polyptoton</summary>
<p>&#x2197; Repetitio.</p>
</details>

<details class="doneEntryGUPF">
<summary  class="doneEntryGUPFSummary">Polysyndeton</summary>
<p>Im Polysyndeton folgt auf jedes Glied (außer auf das letzte Glied) einer Aufzählung, die sich aus drei oder mehreren syntaktisch gleichrangigen Begriffen oder Phrasen zusammensetzt, eine Konjunktion (ug. <i>w</i>; Gegenstück zum &#x2197; Asyndeton). Das Polysyndeton ist in der ugaritischen Poesie recht selten bezeugt. Im angeführten Beispiel (KTU 1.4 iii 17-21a) ist die Figur möglicherweise eingesetzt, um den Eindruck einer umfassenden Aufzählung zu erwecken (nach dem zweiten Glied der Aufzählung geht der aufnehmende Verstand davon aus, dass die Aufzählung abgeschlossen ist; anschließend muss er feststellen, dass die Aufzählung noch weitergeht). Die Aufzählung erstreckt sich dort über zwei Kola. Das Polysyndeton innerhalb eines Kolons (i. e. X <i>w</i> Y <i>w</i> Z) konnte im ugaritischen Korpus bislang nicht identifiziert werden.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 iii 17-21a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17</span>&#160;dāma ṯinê dabḥêma šaniˀa baˁlu</td>
        <td>Fürwahr, zwei Feste hasst Baˁlu,</td></tr>
    <tr>
        <td class="further-colon-of-verse">ṯalāṯa <span class="ugaritic-text-line-number">18</span>&#160;rākibu ˁarapāti</td>
        <td class="further-colon-of-verse">drei der Wolkenfahrer:</td></tr>
    <tr>
        <td>dabḥa <span class="ugaritic-text-line-number">19</span>&#160;BṮ-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti <b>wa</b> dabḥa <span class="ugaritic-text-line-number">20</span>&#160;DN-<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ti</td>
        <td>das Fest der Schande und das Fest der Minderwertigkeit<sup>?</sup></td></tr>
    <tr>
        <td class="further-colon-of-verse"><b>wa</b> dabḥa tudāmim<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">&lt;</span>ti<span class="non-ugaritic-text">&gt;</span> <span class="ugaritic-text-line-number">21</span>&#160;ˀamahāti</td>
        <td class="further-colon-of-verse">und das Fest des Fehlverhaltens<sup>?</sup> der Dienerinnen.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Sofern sich die drei Phrasen <i>DBḤ BṮT</i>, <i>DBḤ DNT</i> und <i>DBḤ TDMM</i>&lt;<i>T</i>&gt; <i>AMHT</i> auf je ein Fest (bzw. eine bestimmte Art von Fest) beziehen, das Baˁlu hasst, liegt eine dreigliedrige Aufzählung vor. Sowohl vor dem zweiten Glied der Aufzählung als auch vor dem dritten (i. e. am Anfang des letzten Kolons) steht die Konjunktion <i>w</i>.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Praeteritio</summary>
</details>

<h2>R</h2>

<details class="undoneEntryGUPF">
<summary>Reim / Reimschemata</summary>
<!--Konsonanz  
Assonanz  
Anfangsreim  
Endreim  
Binnenreim-->
</details>

<details class="undoneEntryGUPF">
<summary>Repetitio</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Rhetorische Frage</summary>
<p>Die rhetorische Frage ist eine Frage, auf die der Fragende keine Antwort erwartet, da sie aus seiner Sicht nur eine einzige Antwort zulässt (in KTU 1.4 iv 59a: „Bin ich ein Sklave?“ → Antwort: „Nein!“). Die rhetorische Frage ist damit weniger eine Frage als eine nachdrückliche Aussage (<bibl>Braak / Neubauer, 2001: 63<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>; <bibl>Moennighoff, 2007: 653<VuepressApiPlayground url="/api/eupt/biblio/EQAMRDJK" method="get" :data="[]"/></bibl>; in KTU 1.4 iv 59a: „Bin ich ein Sklave?“ ~ „Ich bin doch sicher kein Sklave!“). Dem Angesprochenen wird versagt, dem in der rhetorischen Frage durchschimmernden Standpunkt des Fragenden zu widersprechen. Die rhetorische Frage bietet dem Sprechenden damit ein Mittel, den Angesprochenen von seinem Standpunkt zu überzeugen. Die rhetorische Frage ist im ugaritischen Korpus recht selten belegt. Eine Frage lässt sich u. a. daran als rhetorische Frage erkennen, dass sie nicht beantwortet wird bzw. der Fragende den Angesprochenen gar nicht erst zu Wort kommen lässt, um die Frage zu beantworten.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 iv 59-v 1</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">59</span>&#160;pa ˁabdu ˀanā ˁNN-u ˀaṯiratu</td>
        <td>Bin ich etwa ein Sklave, ist ˀAṯiratu (etwa) eine Dienerin (oder: ein Diener der ˀAṯiratu)?</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">60</span>&#160;pa ˁabdu ˀanāku ˀâḫudu ˀULṮ-a</td>
        <td class="further-colon-of-verse">Bin ich etwa ein Sklave, halte ich (etwa) (selbst) die Hacke / Ziegelform?</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">61</span>&#160;him<span class="non-ugaritic-text">(</span>ma<span class="non-ugaritic-text">)</span> ˀam<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu ˀaṯiratu tulabbinu <span class="ugaritic-text-line-number">62</span>&#160;labināti</td>
        <td class="further-colon-of-verse">Oder ist (etwa) ˀAṯiratu eine Dienerin, formt sie (etwa) (selbst) die Ziegel?</td></tr>
    <tr>
        <td>yabnû<span class="non-ugaritic-text">(/</span>î<span class="non-ugaritic-text">)</span> bêta li baˁli <span class="ugaritic-text-line-number">v 1</span>&#160;kama ˀilīma</td>
        <td>Er (selbst; scil. Baˁlu) soll ein Haus für Baˁlu bauen wie (für) die (anderen) Götter,</td></tr>
    <tr>
        <td class="further-colon-of-verse">wa ḥaẓira ka banī ˀaṯirati</td>
        <td class="further-colon-of-verse">ja, eine Wohnstatt wie (für) die Söhne der ˀAṯiratu!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Die rhetorischen Fragen in iv 59-62a dienen dazu, Offensichtliches klarzustellen und eine aus der Sicht des Sprechers absurde Vorstellung aus der Welt zu schaffen: Der Sprecher, ˀIlu, mag eingewilligt haben, dass Baˁlu einen Palast erhält, doch er und seine Frau ˀAṯiratu sind sicher keine Sklaven. ˀIlu und ˀAṯiratu werden also keinen Finger rühren beim Bau des neuen Palastes. Baˁlu soll sich selbst darum kümmern.</p></td></tr>
<tr><th colspan="2">KTU 1.14 iii 33b-40 (und Par.)</th></tr>
    <tr>
        <td>limā ˀanāku <span class="ugaritic-text-line-number">34</span>&#160;kaspa wa yarqa</td>
        <td>Warum (sollte) ich Silber und Gelbgold (nehmen),</td></tr>
    <tr>
        <td class="further-colon-of-verse">ḫurāṣa <span class="ugaritic-text-line-number">35</span>&#160;yada maqâmihu</td>
        <td class="further-colon-of-verse">Gold samt seinem Fundort,</td></tr>
    <tr>
        <td>wa ˁabda <span class="ugaritic-text-line-number">36</span>&#160;ˁālami ṯalāṯa sus<span class="non-ugaritic-text">(</span>s<span class="non-ugaritic-text">)</span>uwīma</td>
        <td>und einen ewig gebundenen Knecht (und) drei Pferde,</td></tr>
    <tr>
        <td class="further-colon-of-verse">markabta <span class="ugaritic-text-line-number">37</span>&#160;bi tarbaṣi bina ˀam<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti</td>
        <td class="further-colon-of-verse">einen Streitwagen aus (deinem) Stall (und) den Sohn einer Magd?</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">38</span>&#160;pa dā ˀêna bi bêtiya tâtin</td>
        <td>Vielmehr gib, was in meinem Haus fehlt,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">39</span>&#160;tin liya MṮ-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ḥurriya</td>
        <td class="further-colon-of-verse">gib mir das Mädchen Ḥurriya,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">40</span>&#160;naˁīm<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta šapḥa bukraka</td>
        <td class="further-colon-of-verse">die Liebliche, deinen erstgeborenen Spross,</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: „Warum (sollte) ich Silber (...) (nehmen)?“ ~ „Es gibt für mich keinen Grund, Silber (...) zu nehmen!“</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Rhythmus</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Ringegefüge</summary>
</details>

<h2>S</h2>

<details class="undoneEntryGUPF">
<summary><i>Split couplet</i></summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Steigerung</summary>
<p>&#x2197; Klimax / Steigerung.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Strophe</summary>
<p>Die Strophe ist eine Verseinheit (&#x2197; Versgliederung), die einen oder mehrere &#x2197; Verse umfasst. Das Monostichon enthält einen einzigen Vers. Das Distichon setzt sich aus zwei Versen zusammen, das Tristichon aus drei, das Tetrastichon aus vier, das Pentastichon aus fünf. Die Strophengrenzen wurden im Vortrag vermutlich durch markante Zäsuren angezeigt (&#x2197; Zäsur).</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table strophe">

<tr><th colspan="2">KTU 1.6 iii 10–13 (Distichon aus zwei Bikola)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">10</span>&#160;bi ḥi<span class="non-ugaritic-text">/</span>ulmi laṭ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>pāni ˀili dā PID-i <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">11</span>&#160;bi ḎR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti bāniyi bu<span class="non-ugaritic-text">/</span>inwati</td>
        <td>Im Traum des Scharfsinnigen, des ˀIlu, des Verständigen, // in der Vision des Schöpfers der Schöpfung,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span>&#160;šamûma šamna tamṭurūnna <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">13</span>&#160;naḫalūma talikū nubtama</td>
        <td>regneten die Himmel Öl, // flossen die Wadis voll Honig!</td></tr>

<tr><th colspan="2">KTU 1.14 ii 43-50a (Tristichon aus zwei Bikola und einem Trikolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">43</span>&#160;yaḥīdu bêtahu sagara <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">44</span>&#160;ˀalmānatu šakāru <span class="ugaritic-text-line-number">45</span>&#160;taškir</td>
        <td>Der einzig Verbliebene schließe sein Haus, // die Witwe stelle einen Tagelöhner ein.</td></tr>
    <tr>
        <td>ZBL-u ˁaršama <span class="ugaritic-text-line-number">46</span>&#160;yiššaˀu <span class="non-ugaritic-text">//</span> ˁawwiru mazālu <span class="ugaritic-text-line-number">47</span>&#160;yamzilu</td>
        <td>Der Kranke hebe (sein) Bett auf, // der Blinde taste sich hinterher.</td></tr>
    <tr>
        <td>wa yâṣiˀ tarīḫu <span class="ugaritic-text-line-number">48</span>&#160;ḥadaṯu <span class="non-ugaritic-text">//</span> yubaˁˁir li ṯanî <span class="ugaritic-text-line-number">49</span>&#160;ˀaṯṯatahu <span class="non-ugaritic-text">//</span> lima nakari <span class="ugaritic-text-line-number">50</span>&#160;môdādatahu</td>
        <td>Auch ziehe aus der frisch Vermählte, // schaffe seine Frau zu einem anderen, // seine Geliebte zu einem Fremden.</td></tr>
<tr><th colspan="2">KTU 1.6 vi 16b–22a (Tetrastichon aus vier Bikola)</th></tr>
    <tr>
        <td>yittâˁāni ka GMR-êma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">17</span>&#160;môtu ˁazza baˁlu ˁazza</td>
        <td>Sie rüttelten aneinander wie zwei GMR – // Môtu war stark, Baˁlu war stark.</td></tr>
    <tr>
        <td>yinnagiḥāni <span class="ugaritic-text-line-number">18</span>&#160;ka ruˀumêma<span class="non-ugaritic-text"><sup>?</sup></span> <span class="non-ugaritic-text">//</span> môtu ˁazza baˁlu <span class="ugaritic-text-line-number">19</span>&#160;ˁazza</td>
        <td>Sie stießen einander wie zwei Wildstiere – // Môtu war stark, Baˁlu war stark.</td></tr>
    <tr>
        <td>yinnaṯikāni ka baṯnêma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">20</span>&#160;môtu ˁazza baˁlu ˁazza</td>
        <td>Sie bissen einander wie zwei Schlangen – // Môtu war stark, Baˁlu war stark.</td></tr>
    <tr>
        <td>yimmaṣiḫāni <span class="ugaritic-text-line-number">21</span>&#160;ka LSM-êma <span class="non-ugaritic-text">//</span> môtu qâla <span class="ugaritic-text-line-number">22</span>&#160;baˁlu qâla</td>
        <td>Sie rissen einander zu Boden wie zwei LSM – // Môtu fiel, Baˁlu fiel.</td></tr>
<tr><th colspan="2">KTU 1.14 i 12-21a (Pentastichon aus vier Bikola und einem Monokolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span>&#160;ˀaṯṯata ṣidqihu lā yapuq <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">13</span>&#160;matrVḫ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta yušrihu</td>
        <td>Die Frau, die ihm rechtmäßig zustand, hatte er nicht gewonnen, // die Gattin, die ihm angemessen war.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">14</span>&#160;ˀaṯṯata tariḫa wa tabiˁat<span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">15</span>&#160;ṮAR-a ˀummi takûn lahu</td>
        <td>Eine (andere) Frau hatte er geheiratet, doch die war fortgegangen, // die Verwandte<sup>?</sup> der Mutter, die ihm übrig geblieben war.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">16</span>&#160;muṯallaṯ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu kuṯruma tamut <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">17</span>&#160;murabbaˁ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu zVb<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>lānuma</td>
        <td>Die Dritte (Frau) war bei bester Gesundheit gestorben, // die Vierte in Krankheit.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">18</span>&#160;muḫammaš<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta yiˀtasap <span class="ugaritic-text-line-number">19</span>&#160;rašpu <span class="non-ugaritic-text">//</span> muṯaddaṯ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ġalamū <span class="ugaritic-text-line-number">20</span>&#160;yammi</td>
        <td>Die Fünfte hatte Rašpu an sich gerissen, // die Sechste die Gefolgsleute Yammus.</td></tr>
    <tr>
        <td>mušabbaˁ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tuhun<span class="non-ugaritic-text">(</span>n<span class="non-ugaritic-text">)</span>ā bi šVlḥi <span class="ugaritic-text-line-number">21</span>&#160;tittapal</td>
        <td>Die Siebte von ihnen war durch die ŠLḤ-Waffe gefallen.</td></tr>
<tr><th colspan="2">KTU 1.6 i 18b–29 (Hexastichon aus sechs Bikola)</th></tr>
    <tr>
        <td>tiṭbaḫu šabˁīma <span class="ugaritic-text-line-number">19</span>&#160;ruˀumīma<span class="non-ugaritic-text"><sup>?</sup></span> <span class="non-ugaritic-text">//</span> ka GMN-i ˀalˀiyāni <span class="ugaritic-text-line-number">20</span>&#160;baˁli</td>
        <td>Sie schlachtete 70 Wildstiere, // als Begräbnisopfer für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td>tiṭbaḫu šabˁīma ˀalapīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">21</span>&#160;<span class="non-ugaritic-text">[</span>ka G<span class="non-ugaritic-text">]</span>MN-i ˀalˀiyāni baˁli</td>
        <td>Sie schlachtete 70 Rinder, // [als] Begräbnisopfer für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;<span class="non-ugaritic-text">[</span>tiṭ<span class="non-ugaritic-text">]</span>baḫu šabˁīma ṣaˀna <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">23</span>&#160;<span class="non-ugaritic-text">[</span>ka GM<span class="non-ugaritic-text">]</span>N-i ˀalˀiyāni baˁli</td>
        <td>[Sie sch]lachtete 70 Schafe, // [als Begräb]nisopfer für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">24</span>&#160;<span class="non-ugaritic-text">[</span>tiṭ<span class="non-ugaritic-text">]</span>baḫu šabˁīma ˀayyalīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">25</span>&#160;<span class="non-ugaritic-text">[</span>ka GMN-i<span class="non-ugaritic-text">]</span> ˀalˀiyāni baˁli</td>
        <td>[Sie sch]lachtete 70 Hirsche, // [als Begräbnisopfer] für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">26</span>&#160;<span class="non-ugaritic-text">[</span>tiṭbaḫu ša<span class="non-ugaritic-text">]</span>bˁīma yaˁilīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">27</span>&#160;<span class="non-ugaritic-text">[</span>ka GMN-i ˀal<span class="non-ugaritic-text">]</span>ˀiyāni baˁli</td>
        <td>[Sie schlachtete 7]0 Steinböcke, // [als Begräbnisopfer für den Mäch]tigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">28</span>&#160;<span class="non-ugaritic-text">[</span>tiṭbaḫu šabˁīma ya<span class="non-ugaritic-text">]</span>ḥmūrīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">29</span>&#160;<span class="non-ugaritic-text">[</span>ka GM<span class="non-ugaritic-text">]</span>N-i ˀalˀiyāni baˁli</td>
        <td>[Sie schlachtete 70 R]ehböcke, // [als Begräbnisop]fer für den Mächtigen, Baˁlu.</td></tr>
</table>
</details>
<p/>
</details>

<!--
<details id="undoneEntryGUPF">
<summary>Strophencluster</summary>
KTU 1.14 i 6b-25
</details>
-->

<details class="undoneEntryGUPF">
<summary>Stufenparallelismus</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Synekdoche</summary>
<p>&#x2197; Metonymie.</p>
</details>

<h2>T</h2>

<details class="undoneEntryGUPF">
<summary>Tautologie</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Terrassenparallelismus</summary>
<!--
Der Terrassenparallelismus ist eine bestimmte strukturelle Erscheinungsform des Parallelismus.
-->
</details>

<details class="doneEntryGUPF">
<summary  class="doneEntryGUPFSummary">Tetrakolon</summary>
<p>Das Tetrakolon ist ein &#x2197; Vers, der sich aus vier &#x2197; Kola zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Tetrastichon</summary>
<p>Das Tetrastichon ist eine &#x2197; Strophe, die sich aus vier &#x2197; Versen zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary"><i>totum pro parte</i></summary>
<p>&#x2197; Metonymie.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Trikolon</summary>
<p>Das Trikolon ist ein &#x2197; Vers, der sich aus drei &#x2197; Kola zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Tristichon</summary>
<p>Das Tristichon ist eine &#x2197; Strophe, die sich aus drei &#x2197; Versen zusammensetzt.</p>
</details>

<h2>V</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Vergleich</summary>
<p>&#x2197; Metapher / Vergleich.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Vers</summary>
<p>Der ugaritische Vers setzt sich aus einem oder mehreren &#x2197; Kola zusammen (s. auch &#x2197; Versgliederung). Das Monokolon enthält ein einziges Kolon, das Bikolon zwei Kola, das Trikolon drei, das Tetrakolon vier, das Pentakolon fünf. Das Bikolon ist die gängigste Versform in der ugaritischen Dichtung; daneben treten immer wieder Monokola und Trikola auf (Tetrakola und Pentakola sind deutlich seltener belegt). Die Kola, die zu einem Vers verbunden sind, stehen oft parallel zueinander (&#x2197; Parallelismus) oder enthalten <i>einen</i> zusammenhängenden Satz (im Enjambement-Vers; &#x2197; Enjambement). Der Vers enthält meist mindestens einen vollständigen Hauptsatz. Daneben sind syntaktisch unvollständige Versformen belegt, in denen der Vers nur einen Teil des übergeordneten Hauptsatzes enthält (und sich mit einem oder mehreren angrenzenden Versen zu einer Enjambement-Strophe verbindet; &#x2197; Enjambement). Die Versgrenzen wurden im Vortrag vermutlich durch Zäsuren kenntlich gemacht (&#x2197; Zäsur).</p>

<details class="exampleGUPF-FirstOrder">
<summary class="exampleGUPF-FirstOrderSummary">Versformen</summary>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Monokolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.14 i 37b-38a</th></tr>
                    <tr>
                        <td>wa yaqrub <span class="ugaritic-text-line-number">38</span>&#160;bi šaˀāli kirti</td>
                        <td>Und er trat heran, um Kirtu zu fragen.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Bikolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.14 i 35b-37a</th></tr>
                    <tr>
                        <td>wa bi ḥilmihu <span class="ugaritic-text-line-number">36</span>&#160;ˀilu yârid</td>
                        <td>Da stieg in seinem Traum ˀIlu herab,</td></tr>
                    <tr>
                        <td>bi ḎHR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tihu <span class="ugaritic-text-line-number">37</span>&#160;ˀabū ˀadami</td>
                        <td>in seiner Vision der Vater der Menschheit.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Trikolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.14 i 28-30</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">28</span>&#160;tinnatikna ˀudmaˁātuhu</td>
                        <td>Seine Tränen flossen</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">29</span>&#160;kama ṯiqalīma ˀarṣah</td>
                        <td>wie Schekel zur Erde,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">30</span>&#160;kama ḫamušāti maṭṭâtah</td>
                        <td>wie Fünftel (eines Schekels) aufs Bett.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Tetrakolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.6 ii 31b–34a</th></tr>
                    <tr>
                        <td>bi ḥarbi <span class="ugaritic-text-line-number">32</span>&#160;tibqaˁ<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ninnu</td>
                        <td>Mit einem Messer schlitzte sie ihn auf,</td></tr>
                    <tr>
                        <td>bi ḪṮR-i tadriyV<span class="ugaritic-text-line-number">33</span>ninnu</td>
                        <td>mit einer Worfgabel<sup>?</sup> worfelte sie ihn,</td></tr>
                    <tr>
                        <td>bi ˀiš<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ti tašrup<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ninnu</td>
                        <td>mit Feuer verbrannte sie ihn,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">34</span>&#160;bi riḥêma tiṭḥanninnu</td>
                        <td>mit (zwei) Mühlsteinen zermahlte sie ihn.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Pentakolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.10 ii 26–30</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">26</span>&#160;wa tiššaˀu ˁênêha batūl<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu ˁanatu</td>
                        <td>Da hob ihre Augen die Jungfrau, ˁAnatu,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">27</span>&#160;wa tiššaˀu ˁênêha wa taˁîn<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>
                        <td>da hob sie ihre Augen und sah,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">28</span>&#160;wa taˁîn<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> ˀarḫa wa târu bi likti</td>
                        <td>da sah sie eine Kuh und die zog im Gehen herum,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">29</span>&#160;târu bi likti wa târu bi ḫîli<span class="non-ugaritic-text"><sup>?</sup></span></td>
                        <td>die zog im Gehen herum, ja sie zog im Springen<sup>?</sup> herum</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">30</span>&#160;<span class="non-ugaritic-text">[</span>bi<span class="non-ugaritic-text">]</span> nuˁmima bi YSM-ima ḥabli kôṯarāti</td>
                        <td>[in / samt] der Lieblichkeit, dem Anmut der Schar der Kôṯarātu.</td></tr>
            </table></details>
</details>
</details>
<p/>

<details class="exampleGUPF-FirstOrder">
<summary class="exampleGUPF-FirstOrderSummary">Verskonstruktionen</summary>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a-b-c // a&#8596;b&#8596;c</summary>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-(c) // a'-b'-(c')</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.5 ii 6b–7</th></tr>
                <tr>
                    <td>yara<span class="non-ugaritic-text">(/</span>i<span class="non-ugaritic-text">)</span>ˀaninnu<span class="non-ugaritic-text"><sup>!</sup></span> ˀalˀiyānu baˁlu</td>
                    <td>Der Mächtige, Baˁlu, fürchtete sich vor ihm,</td>
                    <td>P<sup>+O4</sup>-S</td>
                    <td>a-b</td></tr>
                <tr>
                    <td><span class="ugaritic-text-line-number">7</span>&#160;ṯata<span class="non-ugaritic-text">(/</span>i<span class="non-ugaritic-text">)</span>ˁaninnu rākibu ˁarapāti</td>
                    <td>der Wolkenfahrer hatte Angst vor ihm.</td>
                    <td>P<sup>+O4</sup>-S</td>
                    <td>a'-b'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-c // (c')-b'-a' (&#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.17 v 10b–11</th></tr>
                    <tr>
                        <td>hVl<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ka kôṯari <span class="ugaritic-text-line-number">11</span>&#160;kī yaˁin<span class="non-ugaritic-text">(/</span>-înu<span class="non-ugaritic-text">)</span></td>
                        <td>Dass Kôṯaru kam, sah er fürwahr,</td>
                        <td>O<sub>4</sub>-P</td>
                        <td>a-b</td></tr>
                    <tr>
                        <td>wa yaˁin<span class="non-ugaritic-text">(/</span>-înu<span class="non-ugaritic-text">)</span> tadrVqa ḫasīsi</td>
                        <td>ja, er sah, dass Ḫasīsu heranschritt.</td>
                        <td>P-O<sub>4</sub></td>
                        <td>b-a'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-{b-c} // a'-{c'-b'} (anaphorischer partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.3 iii 19–20a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">19</span>&#160;ˁimmaya paˁnāki talsumānna</td>
                        <td>Zu mir mögen deine Füße laufen,</td>
                        <td>A-S-P</td>
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td>ˁimmaya <span class="ugaritic-text-line-number">20</span>&#160;tawattiḥā ˀišdâki</td>
                        <td>zu mir mögen eilen deine Beine!</td>
                        <td>A-P-S</td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.3 v 33b–34b</th></tr>
                    <tr>
                        <td>kullunāyaya qašâhu <span class="ugaritic-text-line-number">34</span>&#160;nâbilanna</td>
                        <td>Wir alle wollen (ihm) seine Schale bringen,</td>
                        <td><i>Pron.</i>-O<sub>4</sub>-P</td>
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td>kullunāyaya nâbila kāsahu</td>
                        <td>wir alle wollen (ihm) bringen seinen Becher.</td>
                        <td><i>Pron.</i>-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.17 i 2b–3a</th></tr>
                    <tr>
                        <td>ˀuzūru<span class="non-ugaritic-text">/</span>a ˀilīma yulaḥḥim<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>
                        <td>Gegürtet gab er den Göttern zu Essen,</td>
                        <td>A-O<sub>4</sub>-P</td>
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">3</span>&#160;<span class="non-ugaritic-text">[</span>ˀuzūru<span class="non-ugaritic-text">/</span>a yušaqqiyu<span class="non-ugaritic-text">]</span> banī qudši<span class="non-ugaritic-text"><sup>?</sup></span></td>
                        <td>[gegürtet gab er zu Trinken] den Söhnen des Heiligen.</td>
                        <td>A-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.6 vi 45b–47</th></tr>
                    <tr>
                        <td>šapšu <span class="ugaritic-text-line-number">46</span>&#160;rāpiˀīma tuḥattikī</td>
                        <td>Šapšu, über die Rāpiˀūma sollst du herrschen,</td>        
                        <td>S-O<sub>4</sub>-P</td>        
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">47</span>&#160;šapšu tuḥattikī ˀilānīyīma</td>
                        <td>Šapšu, du sollst herrschen über die Göttlichen!</td>
                        <td>S-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.15 iv 17–18</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">17</span>&#160;ˁalê<span class="non-ugaritic-text">/</span>âhu ṯôrīhu tušaˁrib</td>        
                        <td>Zu ihm brachte sie seine Stiere,</td>        
                        <td>A-O<sub>4</sub>-P</td>        
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">18</span>&#160;ˁalê<span class="non-ugaritic-text">/</span>âhu tušaˁrib ẓabayīhu</td>
                        <td>zu ihm brachte sie seine Gazellen,</td>
                        <td>A-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr></table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">{a-b}-c // {b'-a'}-c' (epiphorischer partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iv 44b–45</th></tr>
                    <tr>
                        <td>tašit<span class="non-ugaritic-text">(/</span>-îtu<span class="non-ugaritic-text">)</span> Ḫx<span class="non-ugaritic-text">[x x]</span>-a bi <span class="ugaritic-text-line-number">45</span>&#160;NŠG-iha</td>
                        <td>Sie steckte einen D[olch]<sup>?</sup> in ihre Scheide<sup>?</sup>,</td>
                        <td>P-O<sub>4</sub>-A</td>
                        <td>{a-b}-c</td></tr>
                    <tr>
                        <td>ḥarba tašit<span class="non-ugaritic-text">(/</span>-îtu<span class="non-ugaritic-text">)</span> bi TˁR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">[</span>tiha<span class="non-ugaritic-text">]</span></td>
                        <td>ein Messer steckte sie in [ihre] Scheid[e]<sup>?</sup>.</td>
                        <td>O<sub>4</sub>-P-A</td>
                        <td>{b'-a}-c'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">{a-b}-c // c'-{a'-b'} (anadiplotischer partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iv 8–9a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">8</span>&#160;danīˀilu bêtahu yamġiyu<span class="non-ugaritic-text">/</span>anna</td>
                        <td>Danīˀilu kam zu seinem Haus,</td>        
                        <td>S-A-P</td>        
                        <td>{a-b}-c</td></tr>
                    <tr>
                        <td>yišta<span class="ugaritic-text-line-number">9</span>qVl<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text"><sup>?</sup></span> danīˀilu li hêkalihu</td>
                        <td>Danīˀilu erreichte seinen Palast.</td>
                        <td>P-S-A</td>
                        <td>c'-{a-b'}</td></tr>
        </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-{b-c} // {b'-c'}-a' (rahmender partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iii 8–9a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">8</span>&#160;kanapê našarīma baˁlu yaṯbu<span class="non-ugaritic-text">/</span>ir<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>        
                        <td>Baˁlu zerbrach die Flügel der Adler,</td>        
                        <td>O<sub>4</sub>-S-P</td>        
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">9</span>&#160;baˁlu ṯabara DˀIY-ê<span class="non-ugaritic-text">/</span>ī humūti</td>
                        <td>Baˁlu zerbrach die Schwingen von jenen.</td>
                        <td>S-P-O<sub>4</sub></td>
                        <td>{b-c}-a'</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a-b-c // ◌&#8596;b'&#8596;c' (&#x2197; Ellipse)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.4 v 61–62</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">61</span>&#160;bal<span class="non-ugaritic-text">(</span>î<span class="non-ugaritic-text">)</span> ˀašit ˀurubbata bi baha<span class="non-ugaritic-text">[</span>tīma<span class="non-ugaritic-text">]</span></td>
                        <td>Soll ich kein Fenster im Ha[us (Pl.)] einsetzen,</td>        
                        <td>P-O<sub>4</sub>-A</td>        
                        <td>a-b-c</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">62</span>&#160;ḥallāna bi qarbi hêkalīma</td>
                        <td>(keine) Fensteröffnung inmitten des Palastes (Pl.)?</td>
                        <td>O<sub>4</sub>-A</td>
                        <td>◌-b'-c'</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a&#8596;b&#8596;◌ // ◌&#8596;b'&#8596;c</summary>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-◌ // ◌-b-c (und Varianten; &#x2197; Terrassenparallelismus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.3 ii 23–24</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">23</span>&#160;maˀda timtaḫaṣa<span class="non-ugaritic-text">/</span>unna wa taˁînu</td>        
                        <td>Heftig kämpfte und schaute umher,</td>
                        <td>A-P ; P</td>
                        <td>a-b ; c-◌</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">24</span>&#160;tiḫtaṣab<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> wa taḥdiyu ˁanatu</td>
                        <td>focht und und blickte umher die ˁAnatu.</td>
                        <td>P ; P-S</td>
                        <td>◌-b' ; c'-d</td></tr>
                <tr><th colspan="4">KTU 1.114 2c–4a</th></tr>
                    <tr>
                        <td>tilḥamūna <span class="ugaritic-text-line-number">3</span>&#160;ˀilūma wa tištûna</td>        
                        <td>Die Götter essen und trinken,</td>        
                        <td>P-S ; P</td>        
                        <td>a-b ; c-◌-◌</td></tr>
                    <tr>
                        <td>tištûna yê<span class="non-ugaritic-text">&lt;</span>na<span class="non-ugaritic-text">&gt;</span> ˁadê šubˁi</td>
                        <td>trinken Wein bis zur Sättigung,</td>
                        <td>P-O<sub>4</sub>-A</td>
                        <td>◌-◌ ; c-d-e</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">4</span>&#160;tê<span class="non-ugaritic-text">/</span>îrāṯa ˁadê<span class="non-ugaritic-text"><sup>?</sup></span> šukri</td>
                        <td>Most bis zur Trunkenheit.</td>
                        <td>O<sub>4</sub>-A</td>
                        <td>◌-◌ ; ◌-d'-e'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-◌ // a-◌-c (und Varianten; &#x2197; Stufenparallelismus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.2 iv 8c–9</th></tr>
                    <tr>
                        <td>hitta ˀêbaka <span class="ugaritic-text-line-number">9</span>&#160;baˁluma</td>        
                        <td>Nun, deinen Gegner, Baˁlu,</td>        
                        <td>A-O<sub>4</sub>-Anr.</td>        
                        <td>a-b-◌-c</td></tr>
                    <tr>
                        <td>hitta ˀêbaka timḫaṣ</td>            
                        <td>nun schlag deinen Gegner nieder,</td>            
                        <td>A-O<sub>4</sub>-P</td>
                        <td>a-b-d-◌</td></tr>
                    <tr>
                        <td>hitta taṣammit ṣarrataka</td>
                        <td>nun vernichte deinen Feind!</td>
                        <td>A-P-O<sub>4</sub></td>
                        <td>a-d'-b'-◌</td></tr>
                <tr><th colspan="4">KTU 1.17 i 13b–15a</th></tr>
                    <tr>
                        <td>yâdî ṣûtahu<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">14</span>&#160;<span class="non-ugaritic-text">[</span>danī<span class="non-ugaritic-text">]</span>ˀilu</td>        
                        <td>Er legte ab sein Gewand, er, [Danī]ˀilu,</td>        
                        <td>P-O<sub>4</sub>-S</td>
                        <td>a-b-c ; ◌ ; ◌</td></tr>
                    <tr>
                        <td>yâdî ṣûtahu<span class="non-ugaritic-text"><sup>?</sup></span> yaˁlû wa yaški<span class="non-ugaritic-text">/</span>ub<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>    
                        <td>er legte ab sein Gewand, stieg hinauf und legte sich hin,</td>
                        <td>P-O<sub>4</sub> ; P ; <i>w</i>-P</td>
                        <td>a-b-◌ ; d ; e</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">15</span>&#160;<span class="non-ugaritic-text">[</span>yâdî<span class="non-ugaritic-text">]</span> ma<span class="non-ugaritic-text">/</span>iˀzartêhu pa yalin<span class="non-ugaritic-text">(/</span>-înu<span class="non-ugaritic-text">)</span></td>
                        <td>[er legte ab] seinen Mantel und bettete sich.</td>        
                        <td>P-O<sub>4</sub> ; ◌ ; <i>p</i>-P</td>
                        <td>a-b'-◌ ; ◌ ; e</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">Weitere Formen</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.18 iv 23b–24b</th></tr>
                    <tr>
                        <td>šupuk<span class="non-ugaritic-text">(/</span> šipik<span class="non-ugaritic-text">)</span> kama šVˀ<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>yi <span class="ugaritic-text-line-number">24</span>&#160;dama</td>        
                        <td>Vergieß wie ein Mörder (sein) Blut,</td>
                        <td>P-A<sub>1</sub>-O<sub>4</sub></td>  
                        <td>a-b-◌-d</td></tr>
                    <tr>
                        <td>kama šāḫiṭi<span class="non-ugaritic-text"><sup>?</sup></span> li birkêhu</td>
                        <td>wie ein Schlächter bis zu seinen Knien!</td>
                        <td>A<sub>1</sub>-A<sub>2</sub></td>
                        <td>◌-b'-c-◌</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary"><u>a</u> ; <u>b</u> // <u>a</u>' ; <u>◌</u> (und Varianten)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.17 ii 12b–14a</th></tr>
                    <tr>
                        <td>ˀâṯibanna ˀanāku <span class="ugaritic-text-line-number">13</span>&#160;wa ˀanûḫanna</td>
                        <td>Ich will mich setzen, (dass) ich zur Ruhe komme,</td>
                        <td>P-S ; P</td>
                        <td><u>a</u> ; <u>b</u><sub>a</sub></td></tr>
                    <tr>
                        <td>wa tanuḫ bi ˀir<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tiya <span class="ugaritic-text-line-number">14</span>&#160;napšu</td>
                        <td>ja, (dass) die Seele in meiner Brust zur Ruhe komme.</td>
                        <td>P-A-S</td>
                        <td><u>◌</u> ; <u>b</u><sub>a'(x-y-z)</sub></td></tr>
                <tr><th colspan="4">KTU 1.14 ii 32-34</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">32</span>&#160;ˁadānu nugiba wa yâṣiˀ</td>        
                        <td>Die Armee sei mit Proviant versorgt, dann ziehe sie aus,</td>
                        <td>S-P ; <i>w</i>-P</td>
                        <td><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c</sub></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">33</span>&#160;ṣabaˀu ṣabaˀi nugiba</td>
                        <td>das Heer des Heeres sei mit Proviant ausgerüstet,</td>
                        <td>S-P</td>
                        <td><u>a</u><sub>a'-b</sub> ; <u>◌</u></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">34</span>&#160;wa yâṣiˀ ˁadānu maˁˁu</td>
                        <td>dann ziehe die gewaltige Armee aus!</td>
                        <td><i>w</i>-P-S</td>
                        <td><u>◌</u> ; <u>b</u><sub>c-a'</sub></td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary"><u>a</u> ; <u>b</u> // <u>a</u>' ; <u>b</u>' (&#x2197; Alternation)</summary>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary"><u>a</u><sub>a-(b)</sub> ; <u>b</u><sub>c-(d)</sub> // <u>a</u>'<sub>a'-(b')</sub> ; <u>b</u>'<sub>c'-(d')</sub></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iv 32–33</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">32</span>&#160;lV tabarriknī ˀa<span class="non-ugaritic-text">/</span>âlika barī<span class="non-ugaritic-text">/</span>ūkatu<span class="non-ugaritic-text">/</span>ama</td>
                        <td>Segne mich doch, (dass) ich gesegnet (hinfort)gehe,</td>        
                        <td>P<sup>+O4</sup> ; P-präd.Attr.(/ A)</td>
                        <td><u>a</u><sub>a</sub> ; <u>b</u><sub>c-d</sub></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">33</span>&#160;tamurrānī ˀa<span class="non-ugaritic-text">/</span>âlika namarratu<span class="non-ugaritic-text">/</span>ama</td>
                        <td>segne mich, (dass) ich gesegnet (hinfort)gehe!</td>
                        <td>P<sup>+O4</sup> ; P-präd.Attr.(/ A)</td>
                        <td><u>a</u>'<sub>a'</sub> ; <u>b</u>'<sub>c-d'</sub></td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary"><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c-(d)</sub> // <u>a</u>'<sub>◌-b'</sub> ; <u>b</u>'<sub>c'-(d')</sub></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.14 i 33–35a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">33</span>&#160;šinatu talˀûninnu<span class="non-ugaritic-text"><sup>!</sup></span> <span class="ugaritic-text-line-number">34</span>&#160;wa yiškab</td>
                        <td>Schlaf überwältigte ihn, da legte er sich nieder,</td>
                        <td>S-P<sup>+O4</sup> ; <i>w</i>-P</td>        
                        <td><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c</sub></td></tr>
                    <tr>
                        <td>nahamâmatu <span class="ugaritic-text-line-number">35</span>&#160;wa yaqmiṣ</td>
                        <td>Schlummer, da sank er nieder.</td>
                        <td>S ; <i>w</i>-P</td>
                        <td><u>a</u>'<sub>a'-◌</sub> ; <u>b</u>'<sub>c'</sub></td></tr>
                <tr><th colspan="4">KTU 1.17 vi 26b–28a</th></tr>
                    <tr>
                        <td>ˀiriš ḥayyīma lV ˀaqhatu ġāziru</td>      
                        <td>Wünsch (dir) Leben, o ˀAqhatu, Held,</td>
                        <td>P-O<sub>4</sub>-Anr.</td>
                        <td><u>a</u><sub>a-b-c</sub> ; <u>◌</u></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">27</span>&#160;ˀiriš ḥayyīma wa ˀâtinaka</td>            
                        <td>wünsch (dir) Leben, und ich will es dir geben,</td>
                        <td>P-O<sub>4</sub> ; w-P<sup>+O</sup></td>
                        <td><u>a</u>'<sub>a-b-◌</sub> ; <u>b</u>'<sub>d</sub></td></tr>
                    <tr>
                        <td>balî<span class="non-ugaritic-text">(-)</span>môta <span class="ugaritic-text-line-number">28</span>&#160;wa ˀašalliḥaka</td>
                        <td>Unsterblichkeit, und ich will sie dir überreichen!</td>
                        <td>O<sub>4</sub> ; w-P<sup>+O</sup></td>
                        <td><u>a</u>''<sub>◌-b'-◌</sub> ; <u>b</u>''<sub>d</sub></td></tr>
                    <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: 2.-3. Kolon; zum 1.-2. Kolon &#x2197; Stufenparallelismus.</p></td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary"><u>a</u> // <u>b</u> (&#x2197; Konnexion)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.4 vii 23–25a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">23</span>&#160;lV ragamtu laka lV ˀalˀi<span class="ugaritic-text-line-number">24</span>yānu baˁlu</td>
                        <td>Hab ich dir doch gesagt, o Mächtiger, Baˁlu,</td>
                        <td>P<sub>1</sub>-A<sub>1</sub>-Anr.</td>
                        <td><u>a</u><sub>a-b-c</sub></td></tr>
                    <tr>
                        <td>taṯûbunna baˁlu <span class="ugaritic-text-line-number">25</span>&#160;li hawâtiya</td>
                        <td>(dass) du, Baˁlu, auf mein Wort zurückkommen wirst!</td>
                        <td>P<sub>2</sub>-Anr.-A<sub>2</sub></td>
                        <td><u>b</u><sub>d-c-e</sub></td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a- // b (&#x2197; Enjambement)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.6 i 56–57</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">56</span>&#160;ˀappūnaka ˁaṯtaru ˁarīẓu<span class="non-ugaritic-text"><sup>?</sup></span></td>        
                        <td>Sodann (ist) ˁAṯtaru, der Starke,</td>
                        <td>A<sub>1</sub>-S-</td>
                        <td>a-b-</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">57</span>&#160;yaˁlû<span class="non-ugaritic-text">/</span>î bi ṢRR-āti ṣapāni</td>
                        <td>hinaufgestiegen auf die Höhen<sup>?</sup> des Zaphon.</td>
                        <td>P-A<sub>2</sub></td>
                        <td>c-d</td></tr>
            </table></details>
</details>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Versgliederung</summary>
<p>Die ugaritischen poetischen Texte gliedern sich in verschieden große Verseinheiten. Dazu zählen das &#x2197; Kolon, der &#x2197; Vers und die &#x2197; Strophe (daneben lassen sich größere strukturelle Einheiten identifizieren, die sich aus mehreren Strophen zusammensetzen). Verseinheiten, die sich aus mindestens zwei kleineren Verseinheiten zusammensetzen, werden zusammenfassend als Versgefüge bezeichnet.</p>
<p>Da die ugaritischen Schreiber die Grenzen zwischen Verseinheiten nicht immer graphisch kenntlich machten (etwa durch Zeilensprünge oder horizontale Trennlinien), erweist sich die Rekonstruktion der Versgliederung zuweilen als schwierig. Wichtige Indizien liefern:</p>
<ol type="a">
    <li>das Text- und Zeilenlayout einzelner Manuskripte (die Tafelzeile entspricht zuweilen einem Kolon; horizontale Trennlinien [sofern sie nicht das Ende der Kolumne / die untere Grenze des beschriebenen Bereichs der Tafelseite anzeigen] korrelieren meist mit Strophengrenzen);</li>
    <li style="margin-top: 12px;">bestimmte Lexeme und morphosyntaktische Spezifika (so treten beispielsweise die Lexeme <i>APNK</i> „danach, daraufhin“, <i>MK</i> „dann, schließlich“ und <i>DM</i> „denn; fürwahr“ vorrangig am Anfang des ersten Kolons des Verses auf, die vokativische Anrede oft am Ende des ersten Kolons des Verses und die Konjunktion / Partikel <i>W</i> „und; ja!“ immer wieder am Anfang des zweiten Kolons des Verses; Satzgrenzen stimmen oft mit Kolongrenzen überein, fast immer mit Versgrenzen und immer mit Strophengrenzen);</li>
    <li style="margin-top: 12px;">die Analyse paralleler Strukturen (aufeinanderfolgende Phrasen, Kola oder Verse, die parallel zueinander stehen, gehören i. d. R. zum selben übergeordneten Versgefüge);</li>
    <li style="margin-top: 12px;">die Zählung der Silben im Kolon (aufeinanderfolgende Kola und vor allem Kola, die zum selben Vers gehören, sind oft gleich oder ähnlich lang);</li>
    <li style="margin-top: 12px;">textlogische Überlegungen (Verseinheiten, in denen zwei Sachverhalte beschrieben sind, von denen der eine den anderen bedingt oder unmittelbar aus ihm hervorgeht, sind immer wieder <i>einem</i> übergeordneten Versgefüge zuzuordnen).</li>
</ol>
<p>Die Texte richtig zu phrasieren und die Versgliederung auf diese Weise „hörbar“ (i. e. auditiv erfahrbar) zu machen, war Aufgabe des Vortragenden, der die Texte rezitierte oder sang (in welchen Kontexten die Texte vorgetragen wurden, ist weitgehend unbekannt; es ist nicht auszuschließen, dass unterschiedliche Vortragende die Texte zuweilen unterschiedlich phrasierten; die rekonstruierte Versgliederung spiegelt also im besten Fall <i>eine</i> Möglichkeit wider, die Texte zu gliedern). Die prosodische Gestaltung der Texte (Rhythmus, Sprechtempo, unterschiedlich ausgeprägte Zäsuren etc.) zeigte dem Publikum, welche Phrasen zu einer strukturellen, grammatischen und inhaltlichen Einheit zu verbinden sind und welche voneinander abzugrenzen sind (die Versgliederung ist folglich entscheidend für das Textverständnis).</p>
<details class="exampleGUPF">
<summary>Beispiel</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 iii 14b-19a</th></tr>
    <tr>
        <td>wa hanna šapšuma <span class="ugaritic-text-line-number">15</span>&#160;bi šābiˁi</td>
        <td>Und siehe, bei Sonnenuntergang, am siebten (Tag),</td></tr>
    <tr>
        <td class="further-colon-of-verse">wa lā yîšanu pabilu <span class="ugaritic-text-line-number">16</span>&#160;malku</td>
        <td class="further-colon-of-verse">da wird König Pabilu nicht schlafen können</td></tr>
    <tr>
        <td>li QR-i ṯaˀgati ˀibbīrīhu</td>
        <td>wegen des Lärms des Gebrülls seiner Stiere,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">17</span>&#160;li qâli nahaqati ḥimārīhu</td>
        <td class="further-colon-of-verse">wegen des Dröhnens des Geschreis seiner Esel,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">18</span>&#160;li gaˁâti ˀalapī ḥarṯi</td>
        <td>wegen des Gebrülls der Pflugrinder,</td></tr>
    <tr>
        <td class="further-colon-of-verse">zaġâti <span class="ugaritic-text-line-number">19</span>&#160;kalabī ṢPR-i</td>
        <td class="further-colon-of-verse">(wegen) des Gebells der Wachhunde<sup>?</sup></td></tr>
    <tr><td colspan="2" style="white-space: normal">
        <p class="exampleGUPF-Anm">Anm.: Die Strophe setzt sich aus drei Bikola zusammen (i. e. aus drei Versen zu je zwei Kola). Die ersten beiden Kola formen einen Enjambement-Vers (&#x2197; Enjambement; der im ersten Kolon eingeleitete Satz ist im zweiten Kolon fortgesetzt; SG-Str.: Interj.-A<sub>1</sub>- // P-S).</p>
        <p class="exampleGUPF-Anm">Die Kola, die sich im zweiten und dritten Vers gegenüberstehen, sind jeweils semantisch und grammatisch parallel gestaltet (die parallel gestellten Kola sind jeweils grammatisch identisch aufgebaut; nur im zweiten Kolon des dritten Verses ist die Präposition <i>L</i> am Kolonanfang ausgelassen; &#x2197; Grammatische Varianz, und &#x2197; Ellipse):</p>
        <p class="exampleGUPF-Anm">
            Str. Vers 2: a<sub>„Lärm“</sub>-b<sub>„Gebrüll“</sub>-c<sub>„ein Tier“</sub> // a'-b'-c'<br/>
            Str. Vers 3: a<sub>„Gebrüll“</sub>-b<sub>„ein Tier“</sub>-c<sub>„Einsatzgebiet des Tieres“</sub> //  a'-b'-c'.</p>
        <p class="exampleGUPF-Anm">Gleichzeitig stehen der zweite und der dritte Vers - jeweils als Ganzes betrachtet - parallel zueinander. Die einzelnen Kola enthalten jeweils eine mit <i>L</i> eingeleitete Präpositionalphrase (Ausnahme: zweites Kolon des dritten Verses; s. o.), auf die zwei Genitivattribute folgen:</p>
        <p class="exampleGUPF-Anm">Str. Vers 2-3:<br/>
            a<sub>1-„Lärm des Gebrülls“</sub>-b<sub>1-„ein Tier“</sub> // a<sub>1</sub>'-b<sub>1</sub>'<br/>
            a<sub>2-„Gebrüll“</sub>-b<sub>2-„ein Tier in best. Einsatzgebiet“</sub> // a<sub>2</sub>'-b<sub>2</sub>'</p>
        <p class="exampleGUPF-Anm">Außerdem reimen sich die vier Kola des zweiten und dritten Verses; nicht zuletzt die Kola, die sich jeweils innerhalb eines Verses gegenüberstehen, sind sich klanglich ganz ähnlich (Vers 2: <b><i>li</i></b> // <b><i>li</i></b> - <i>ṯ</i><b><i>aˀgati</i></b> // <i>n</i><b><i>ah</i></b><i>a</i><b><i>qati</i></b> - <i>ˀ</i><b><i>i</i></b><i>bbī</i><b><i>rīhu</i></b> // <i>ḥ</i><b><i>i</i></b><i>mār</i><b><i>īhu</i></b>; Vers 3: <i>g</i><b><i>aˁâti</i></b> // <i>z</i><b><i>aġâti</i></b> - <i>ˀ</i><b><i>alapī</i></b> // <i>k</i><b><i>alabī</i></b>; Vers 2-3: <i>ṯ</i><b><i>aˀ</i></b><i>g</i><b><i>ati</i></b> // <i>n</i><b><i>ah</i></b><i>aq</i><b><i>ati</i></b> | <i>g</i><b><i>aˁâti</i></b> // <i>z</i><b><i>aġâti</i></b>; beachte außerdem <i>ṯ</i><b><i>aˀgati</i></b> | <b><i>gaˁâti</i></b>).</p>
        <p class="exampleGUPF-Anm">Der zweite und der dritte Vers sind durch Enjambement mit dem ersten Vers verknüpft (der zweite und der dritte Vers enthalten das Kausaladverbial des übergeordneten Satzes, der im ersten Vers beginnt; SG-Str. Strophe: Interj.-A<sub>1</sub>- // P-S- | A<sub>2</sub> // A<sub>2</sub> | A<sub>2</sub> // A<sub>2</sub>).</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Vokabular</summary>
<!--
Dichterische Wörter
-->
</details>

<h2>W</h2>

<details class="undoneEntryGUPF">
<summary>Wortpaar</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Wortstellung</summary>
<!--
<p>
Wortstellung relativ frei (mit bestimmten Tendenzen); jedoch hat Wortstellung zuweilen wahrscheinlich eigene Aussagekraft.
Topikalisierung / exponierte Wortstellung
Sonderform der Vorrangstellung: Pendens (wenn das Pendens-Element nicht dem Subj. des Folgesatzes entspricht, kann die Pendenskonstruktion als Anakoluth beschrieben werden)
Umstellung von syntaktisch zusammengehörigen Elementen (Anastrophe): Vorrangstellung des Attributs.
Abbildende Wortstellung innerhalb eines Kolons / Satzes im ugaritischen Korpus bislang nicht bekannt. Aber bestimmte Verskonstruktionen können unter diesem Gesichtspunkt betrachtet werden, namentlich der Chiasmus, wenn er Gegensatz abbildet. Chiasmus aber nicht immer Kontrast!
</p>
-->
</details>


<h2>Z</h2>

<details class="undoneEntryGUPF">
<summary>Zäsur</summary>
<!--
<p/>
Unterschiedlich stark zwischen verschiedenen Verseinheiten.
<p/>
-->
</details>

<details class="undoneEntryGUPF">
<summary>Zeugma</summary>
<!--
<p>Da gehört auch so was wie 1.14 iv 19-20 dazu: Ellipse eines Worts; Wort muss aber in anderer Form ergänzt werden.</p>
-->
</details>

</div>


<input id="tab3" type="radio" name="tabs">
    <label class="EUPT-lab-article-tabs-label  EUPT-lab-article-tabs-last-label" for="tab3"><p>Bibliographie</p></label>
    <div class="EUPT-lab-article-tabs-content">

<h4>Abkürzungen und Sigel</h4>
    <p style="margin-top: 1em;">Die Abkürzungen und Sigel folgen EUPT. Die bibliographischen Abkürzungen sind <a href="/Abkuerzungen.html" target="_self">hier</a> zusammengestellt. Die editorischen Sigel sowie die Abkürzungen und Sigel zur grammatikalischen und versstrukturellen Analyse sind <a href="/Editorische-Prinzipien.html" target="_self">hier</a> erklärt.</p>

<h4>Literaturverzeichnis <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/tags/EUPT-Lab-1/collection" target="_blank"><img class="Bibliography-Logo-zotero" src="https://www.zotero.org/support/_media/logo/zotero_32x32x32.png"></a></h4>

<p class="GMU-bibliography-entry">Braak, Ivo, und Martin Neubauer. 2001. <i>Poetik in Stichworten. Literaturwissenschaftliche Grundbegriffe. Eine Einführung</i>. 8., überarb. und erw. Aufl. Berlin / Stuttgart: Gebrüder Borntraeger.</p>

<p class="GMU-bibliography-entry">Grimm, Gunter E. 2007. „Akrostichon“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 9. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Korpel, Marjo C. A. 1998. „Exegesis in the Work of Ilimilku of Ugarit“. In <i>Intertextuality in Ugarit and Israel</i>, herausgegeben von Johannes C. de Moor, 86–111. Oudtestamentische Studiën 40. Leiden / Boston / Köln: Brill.</p>

<p class="GMU-bibliography-entry">Moennighoff, Burkhard. 2007. „Rhetorische Frage“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 653. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Schweikle, Günther. 2007. „Periphrase“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 578. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Schweikle, Günther, und Christian Schlösser. 2007. „Zeugma“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 842. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Steinberger, Clemens. 2022. „Die A // B | A<sup>(</sup>'<sup>)</sup> // B<sup>(</sup>'<sup>)</sup>-Strophe in der akkadischen und ugaritischen Poesie“. <i>AulaOr.</i> 40: 293–308.</p>

<p class="GMU-bibliography-entry">Steinhoff, Hans-Hugo. 2007. „Asyndeton“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 51. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Steinhoff, Hans-Hugo, und Dieter Burdorf. 2007. „Apokoinu“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 37. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Watson, Wilfred G. E. 2007. <i>Lexical Studies in Ugaritic</i>. AulaOr. Suppl. 19. Sabadell: Editorial Ausa.</p>

</div>

<!--End of EUPT-lab-article-tabs-->
</div>