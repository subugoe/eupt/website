---
title: Technische Dokumentation
lang: en
layout: Layout
---

# Technical Documentation

The software implemented for the EUPT project is realized with several components, which are mainly the following:

<ol type="a">
    <li>Website</li>
    <li>back end (all ETL processes / offering <a href="https://textapi.sub.uni-goettingen.de" target="_blank">TextAPI</a> and AnnotationAPI)</li>
    <li><a href="https://gitlab.gwdg.de/subugoe/emo/tido" target="_blank">TIDO Viewer</a> (a viewer meant to display digital editions – and everything else delivered via TextAPI)</li>
</ol>

A back end made with FastAPI ([git repo](https://gitlab.gwdg.de/subugoe/eupt/eupt-textapi)) provides the TextAPI ([TextAPI for EUPT](https://subugoe.pages.gwdg.de/eupt/textapi-specs/page/specs/)).
A client application within the browser, TIDO, interacts with the TextAPI to present all the data in a well-known order (e.g. synoptic view with image and a transcription converted to HTML).  
Beforehand the data is prepared as XML in TextGrid.  

**Git Repository**

All the code used for this project is available at our GitLab: [https://gitlab.gwdg.de/subugoe/eupt](https://gitlab.gwdg.de/subugoe/eupt)
  

*GitLab CI*  
Everything is assembled, tested and deployed with the help of GitLab CI. Please see the `.gitlab-ci.yaml` files in all the repositories.
  

*Editing TEI*  
For editing, storing and exporting the XML documents the virtual research environment [TextGrid](https://textgrid.de/) is used.
TextGrid offers all facilities to encode, validate, interlink, store and export the data.
  

*Schema*  
RNG is used for validating documents during the process of encoding utilizing oXygen XML editor as part of the TextGrid Laboratory.
A complementary Schematron file ensures data quality. ([git repo](https://gitlab.gwdg.de/subugoe/eupt/eupt-xml))
  

**Website**

This website is prepared with Vue.js and VuePress2.  
Pages are written in Markdown.