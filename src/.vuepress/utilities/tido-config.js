export const baseUrl = ((typeof window !== 'undefined') ? window.location.origin : "");

export const tidoConfig = {
    collection: baseUrl + "/api/eupt/collection.json",
    container: "#viewer",
    colors: {
        primary: "#3aa675"
    },
    labels: {
        item: "Kolumne",
        manifest: "Tafel"
    },
    lang: "de",
    fitPanels: true,
    panels: [
        {
            label: "Inhalt & Metadaten",
            views: [
                {
                    id: "tree",
                    label: "Inhalt",
                    connector: {
                        id: 1
                    }
                },
                {
                    id: "metadata",
                    label: "Metadaten",
                    connector: {
                        id: 2,
                        options: {
                            collection: {
                                all: true
                            },
                            manifest: {
                                all: true
                            },
                            item: {
                                all: true
                            }
                        }
                    }
                }
            ]
        },
        {
            label: "Autographie",
            views: [
                {
                    id: "image",
                    label: "Autographie",
                    connector: {
                        id: 3
                    }
                }
            ]
        },
        {
            label: "Transliteration",
            views: [
                {
                    id: "text1",
                    label: "Transliteration",
                    connector: {
                        id: 4,
                        options: {
                            type: "facsimile"
                        }
                    }
                }
            ]
        },
        {
            label: "Vokalisation & Übersetzung",
            views: [
                {
                    id: "text2",
                    label: "Vokalisation & Übersetzung",
                    connector: {
                        id: 4,
                        options: {
                            type: "philology"
                        } 
                    }
                }
            ],
            width: 2
        }
    ],
    translations: {
        de: {
            contents_and_metadata: "Inhalt & Metadaten",
            next_item: "Nächste Kolumne",
            previous_item: "Vorige Kolumne",
            next_manifest: "Nächste Tafel",
            previous_manifest: "Vorige Tafel",
        },
        en: {
            contents_and_metadata: "Inhalt & Metadaten",
            next_item: "Nächste Kolumne",
            previous_item: "Vorige Kolumne",
            next_manifest: "Nächste Tafel",
            previous_manifest: "Vorige Tafel",
        }
    }
}

// TODO: remove condition if images are to be shown on live website
if(baseUrl.includes('eupt.uni-goettingen')){
    tidoConfig.panels.splice(1, 1);
}
