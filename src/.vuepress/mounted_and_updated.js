import DataTable from 'datatables.net-dt';
import { contentbar } from './contents';

// Create a dictionary:
// `page_to_images` maps a page suffix to an image name and image_position
//    Example: "Einfuehrung/Editorische Prinzipien_Zitation.html" -> ["EUPT_Korpus", "right"]
var page_to_image = {}
function get_images(contentlist, default_image_name = "", default_image_position = ""){
  for(var i = 0; i < contentlist.length; ++i){
    var contentitem = contentlist[i];
    var page = contentitem.link.substring(1);
    var image_name = default_image_name;
    if(contentitem.hasOwnProperty("image_name")){
      image_name = contentitem.image_name;
    }
    var image_position = default_image_position;
    if(contentitem.hasOwnProperty("image_position")){
      image_position = contentitem.image_position;
    }
    page_to_image[page] = [image_name, image_position];
    if(contentitem.hasOwnProperty("children")){
      get_images(contentitem.children, image_name, image_position);
    }
  }
}
get_images(contentbar);

// This function updates the background image URL and sets the class for background positioning.
function update_background_image(){
  const page = window.location.pathname.substring(1);
  const mode = document.getElementsByTagName("html")[0].classList.contains("dark") ? "dark" : "light";
  const image = page_to_image[page];
  const image_name = image[0];
  const image_position = image[1];
  const image_path = "/assets/images/" + image_name + "_" + mode + ".svg";
  const elem = document.querySelector(".theme-container.no-sidebar").querySelector(".page").querySelector(".theme-default-content");
  if(elem){
    elem.classList.remove("bg-left");
    elem.classList.remove("bg-right");
    if(image_name == ""){
        elem.style.backgroundImage = "none";
    }
    else {
        elem.style.backgroundImage = "url(" + image_path + ")";
        elem.classList.add("bg-" + image_position);
    }
  }
}

// This function updates the favicon.
function update_favicon(){
  const mode = document.getElementsByTagName("html")[0].classList.contains("dark") ? "dark" : "light";
  const image_path = "/assets/images/eupt_fav_" + mode + ".svg";
  document.querySelector('link[rel="icon"]').setAttribute("href", image_path);
}

// This function updates the title image.
function update_title_image(){
  const mode = document.getElementsByTagName("html")[0].classList.contains("dark") ? "dark" : "light";
  const image_path = "/assets/images/eupt_logo_" + mode + "_pathonly.svg";
  const elem = document.getElementsByClassName("site-name")[0];
  const img = elem.querySelector("img");
  const title_text = (img ? img.alt : elem.innerHTML);
  elem.innerHTML = '<img src="' + image_path + '" alt="' + title_text + '" class="title-img" onerror="this.classList.remove(...this.classList)"/>';
}

// This function loads the bibliographic entries through the VuepressApiPlayground buttons via the Zotero API.
async function executeApiButton(button){
  if(button.children.length > 0 && button.children[0].tagName.toLowerCase() == "span" && button.children[0].innerText.trim() == "Execute"){
    button.click();
    while(!(button.nextElementSibling && button.nextElementSibling.classList.contains("vp-playground__response"))){
      await new Promise(r => setTimeout(r, 250));
    }
    var biblHTML = new DOMParser().parseFromString(button.nextElementSibling.getElementsByTagName("code")[0].innerHTML, "text/html").documentElement.textContent;
    button.parentElement.outerHTML = '<span class="bibl-long">' + biblHTML + '</span>';
  }
}

// This function contains code that should be executed on both `mounted` and `updated`.
function mounted_and_updated(){

  // SIDEBAR TOGGLE
  // The sidebar toggle button does not work on the viewer page (probably an issue caused by the layout template inheritance),
  // so a new click event is registered here for this button (only on the viewer page) that mimics the toggle behaviour.
  if(document.querySelector(".page.viewer-page")){
    document.querySelector(".toggle-sidebar-button").addEventListener("click", function(){
      var themeContainerClassList = document.querySelector(".theme-container").classList;
      if(themeContainerClassList.contains("sidebar-open")){
        themeContainerClassList.remove("sidebar-open");
      }
      else {
        themeContainerClassList.add("sidebar-open");
      }
    });
  }

  // BACKGROUND IMAGE
  update_background_image();

  // FAVICON
  update_favicon();

  // TITLE IMAGE
  update_title_image();

  // BIBLIOGRAPHIC REFERENCES
  Array.from(document.getElementsByTagName("button")).forEach(executeApiButton);
  Array.from(document.getElementsByTagName("details")).forEach((details) => {
    // (necessary for Chrome, where buttons in details are only clickable when visible)
    details.addEventListener("toggle", (event) => {
      if(details.open){
        Array.from(details.getElementsByTagName("button")).forEach(executeApiButton);
      }
    });
  });
  Array.from(document.getElementsByTagName("bibl")).forEach((bibl) => {
    bibl.classList.add("bib-container");
    bibl.setAttribute("tabindex", "0");
    var biblShort = document.createElement("span");
    biblShort.classList.add("bibl-short");
    bibl.insertBefore(biblShort, bibl.firstChild);
    while(bibl.childNodes.length > 2){
      biblShort.appendChild(bibl.childNodes[1]);
    }
  });

  // DATATABLES
  Array.from(document.getElementsByClassName("EUPT-RSTI-KonkordanzTable")).forEach((dataTable) => new DataTable(dataTable, {
    paging: false,
    language: { search: "", searchPlaceholder: "Search" },
    order: [[2, 'asc']],
    layout: {
      topStart: ['search'],
      topEnd: {
        features: [{
          div: {
            html: '<details class="EUPT-RSTI-Konkordanz-Info"><summary></summary><div>Die Tafelkonkordanz gibt einen Überblick über die verschiedenen Identifikationsnummern der in EUPT bearbeiteten keilalphabetischen Tafeln. Die Tabelle wurde in Zusammenarbeit mit <a href="https://digitalculture.uchicago.edu/people/miller-prosser/" target="_blank">Miller Prosser</a>, Co-Direktor des <a href="https://ochre.lib.uchicago.edu/RSTI/" target="_blank">Ras Shamra Tablet Inventory</a>, erstellt. Über die Suchleiste (<i>Search</i>) kann die Konkordanz durchsucht werden. Per Klick auf eine Spaltenüberschrift wird die Tabelle entsprechend den Daten in der jeweiligen Spalte sortiert.</div></details>'
          }
        }]
      },
      bottomStart: ['info'],
      bottomEnd: null
    }
  }));
  Array.from(document.getElementsByClassName("EUPT-Lab-PublikationenTable")).forEach((dataTable) => new DataTable(dataTable, {
    paging: false,
    language: { search: "", searchPlaceholder: "Search" },
    order: [[0, 'asc']], 
    columnDefs: [
      { type: 'date', targets: 1 },
      { orderable: false, targets: [3, 4, 5, 6] }
    ],
    layout: {
      topStart: ['search'],
      topEnd: null,
      bottomStart: ['info'],
      bottomEnd: null
    }
  }));
  Array.from(document.getElementsByClassName("GMU-PublikationenTable")).forEach((dataTable) => new DataTable(dataTable, {
    paging: false,
    language: { search: "", searchPlaceholder: "Search" },
    order: [[0, 'asc']], 
    columnDefs: [
      { type: 'date', targets: 1 },
      { orderable: false, targets: [3, 4, 5, 6] }
    ],
    layout: {
      topStart: ['search'],
      topEnd: null,
      bottomStart: ['info'],
      bottomEnd: null
    }
  }));
}

// This function contains code that should be executed on light/dark mode change.
function toggle_color_mode_button(){

  // BACKGROUND IMAGE
  update_background_image();

  // FAVICON
  update_favicon();

  // TITLE IMAGE
  update_title_image();
}

export { mounted_and_updated, toggle_color_mode_button };