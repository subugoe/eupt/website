import { defineClientConfig } from '@vuepress/client';
import VuepressApiPlayground from 'vuepress-api-playground/src/components/Playground/Playground.vue';
import Bibliography from "./components/Bibliography.vue";
import Breadcrumbs from "./components/Breadcrumbs.vue";
import Contact from "./components/Contact.vue";
import Footer from "./components/Footer.vue";
import TidoPage from "./components/TidoPage.vue";
import Layout from "./layouts/Layout.vue";

export default defineClientConfig({
  async enhance({ app }) {
    app.component('VuepressApiPlayground', VuepressApiPlayground);
    app.component('Bibliography', Bibliography);
    app.component('Breadcrumbs', Breadcrumbs);
    app.component('Contact', Contact);
    app.component('Footer', Footer);
    if (!__VUEPRESS_SSR__) {
      await import('tido/dist/tido');
      await import('tido/dist/tido.css');
      await import('./styles/tido/custom.scss');
      app.component('TidoPage', TidoPage);
    }
  },
  layouts: {
    Layout,
  },
})
