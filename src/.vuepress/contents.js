// used for sidebar and navbar;
// `image_name` and `image_position` refer to the files in `public/assets/images`,
// images are applied to all children and sub-children (can be overwritten for individual children)
var contentbar = [
  { text : 'Start', link : '/',
    image_name : 'EUPT_Start', image_position : 'left',
    children : [
      { text : 'EUPT', link : '/' },
      { text : 'Arbeitsplan', link : '/Arbeitsplan.html' },
      { text : 'Team / Kontakt', link : '/Team_Kontakt.html' },
      { text : 'Technische Dokumentation', link : '/Technische-Dokumentation.html' },
      { text : 'News', link : '/News.html' }
    ]
  },
  { text : 'Ugarit', link : '/Ugarit.html',
    image_name : 'EUPT_Ugarit', image_position : 'left',
    children : [
      { text : 'Ugarit in der Spätbronzezeit', link : '/Ugarit.html' },
      { text : 'Ugarit und Altes Testament', link : '/Ugarit-und-Altes-Testament.html' }
    ]
  },
  { text : 'Korpus', link : '/Korpus.html',
    image_name : 'EUPT_Korpus', image_position : 'left',
    children : [
      { text : 'Edition', link : '/edition.html',
        children : []
      },
      { text : 'Einführung', link : '/Korpus.html',
        children : [
          { text : 'Texte, Tafeln, Forschung', link : '/Korpus.html' },
          { text : 'Editorische Prinzipien', link : '/Editorische-Prinzipien.html' }
        ]
      }
    ]
  },
  { text : 'EUPT-Lab', link : '/EUPT-Lab/Einfuehrung.html',
    image_name : 'EUPT_Lab', image_position : 'left',
    children : [
      { text : 'EUPT-Laboratory', link : '/EUPT-Lab/Einfuehrung.html',
        children : []
      },
      { text : 'Beiträge', link : '/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html',
        children : [
          { text : '1 - Steinberger - Glossar der ugaritischen poetischen Formen', link : '/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html' }
        ]
      }
    ]
  },
  { text : 'GMU', link : '/GMU/Einfuehrung.html',
    image_name : 'EUPT_Miszellen', image_position : 'left',
    children : [
      { text : 'Göttinger Miszellen zur Ugaritistik', link : '/GMU/Einfuehrung.html',
        children : []
      },
      { text : 'Artikel', link : '/GMU/1_Burlingame_Toponymic-Remarks-to-RIH-83-47.html',
        children : [
          { text : '1 - Burlingame - Toponymic Remarks\u00A0\u2026', link : '/GMU/1_Burlingame_Toponymic-Remarks-to-RIH-83-47.html' },
          { text : '2 - Notarius - Ugaritic 𝘱𝘳𝘭𝘯 and Hurrian\u00A0\u2026', link : '/GMU/2_Notarius_Ugaritic-prln-and-Hurrian-furullinni.html' }
        ]
      }
    ]
  },
  { text : 'Literatur', link : '/Bibliographie.html',
    image_name : 'EUPT_Literatur', image_position : 'right',
    children : [
      { text : 'Bibliographie', link : '/Bibliographie.html' },
      { text : 'Publikationen der Ugarit-Forschungsstelle Göttingen', link : '/Publikationen.html' },
      { text : 'Abkürzungen', link : '/Abkuerzungen.html' }
    ]
  }
];

// Make the contentbar collapsible (when used as sidebar) :
function make_collapsible(contentlist){
  for(var i = 0; i < contentlist.length; ++i){
    var contentitem = contentlist[i];
    if(contentitem.hasOwnProperty("children")){
      contentitem.collapsible = true;
      make_collapsible(contentitem.children);
    }
  }
}
make_collapsible(contentbar);

export { contentbar };
