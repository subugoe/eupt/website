import { defaultTheme } from '@vuepress/theme-default';
import { defineUserConfig } from 'vuepress';
import { contentbar } from './contents';

export default defineUserConfig({
  head: [['link', { rel: "icon", href: "/assets/images/eupt_fav_dark.svg" }]],
  theme: defaultTheme({
    lastUpdated: false,
    contributors: false,
    sidebar: false,
    navbar: contentbar
  }),
  title: "Edition des ugaritischen poetischen Textkorpus",
})
