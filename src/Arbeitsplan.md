---
title: Arbeitsplan
lang: de
layout: Layout
---

# {{ $frontmatter.title }}

<p>Im Rahmen des Forschungsvorhabens <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> werden insgesamt ca. 70 keilalphabetische Tafeln bearbeitet. Das Korpus ist in drei Gruppen gegliedert. In jeder Projektphase wird eine Textgruppe ediert und poetologisch analysiert. Eine Übersicht über die bearbeiteten Texte samt Einleitungen, eine Konkordanz der verschiedenen Tafelzählungen sowie ein knapper Überblick über die Forschungsgeschichte finden sich <a href="/Korpus.html">hier</a>.</p>

<div id="ArbeitsplanListe">
    <aside id="ArbeitsplanListeLinkeSpalte">
    	<p><b>1. Phase (2023–2026)</b></p>
    		KTU 1.14 (<i>Kirtu</i> I)<br/>
            KTU 1.15 (<i>Kirtu</i> II)<br/>
            KTU 1.16 (<i>Kirtu</i> III)<br/>
            KTU 1.17 (<i>ˀAqhatu</i> I)<br/>
            KTU 1.18 (<i>ˀAqhatu</i> II)<br/>
            KTU 1.19 (<i>ˀAqhatu</i> III)<br/>
            KTU 1.20 (<i>Rephaim</i> 1)<br/>
            KTU 1.21 (<i>Rephaim</i> 2)<br/>
            KTU 1.22 (<i>Rephaim</i> 3)</aside>
    <main id="ArbeitsplanListeMittlereSpalte">
    	<p><b>2. Phase (2026–2029)</b></p>
        	KTU 1.1 (<i>Baˁlu</i> I)<br/>
            KTU 1.2 (<i>Baˁlu</i> II)<br/>
            KTU 1.3 (<i>Baˁlu</i> III)<br/>
            KTU 1.4 (<i>Baˁlu</i> IV)<br/>
            KTU 1.5 (<i>Baˁlu</i> V)<br/>
            KTU 1.6 (<i>Baˁlu</i> VI)</main>
  	<aside id="ArbeitsplanListeRechteSpalte">
    	<p><b>3. Phase (2029–2032)</b></p>
        	KTU 1.7<br/>
            KTU 1.9<br/>
            KTU 1.10<br/>
            KTU 1.11<br/>
            KTU 1.12<br/>
            KTU 1.13<br/>
            KTU 1.23<br/>
            KTU 1.24<br/>
            KTU 1.25<br/>
            KTU 1.40<br/>
            KTU 1.45<br/>
            KTU 1.55<br/>
            KTU 1.61<br/>
            KTU 1.62<br/>
            KTU 1.63<br/>
            KTU 1.65<br/>
            KTU 1.82<br/>
            KTU 1.83<br/>
            KTU 1.88<br/>
            KTU 1.89<br/>
            KTU 1.92<br/>
            KTU 1.93<br/>
            KTU 1.94<br/>
            KTU 1.95<br/>
            KTU 1.96<br/>
            KTU 1.98<br/>
            KTU 1.100<br/>
            KTU 1.101<br/>
            KTU 1.107<br/>
            KTU 1.108<br/>
            KTU 1.113<br/>
            KTU 1.114<br/>
            KTU 1.117<br/>
            KTU 1.119<br/>
            KTU 1.124<br/>
            KTU 1.129<br/>
            KTU 1.133<br/>
            KTU 1.147<br/>
            KTU 1.151<br/>
            KTU 1.152<br/>
            KTU 1.157<br/>
            KTU 1.158<br/>
            KTU 1.159<br/>
            KTU 1.160<br/>
            KTU 1.161<br/>
            KTU 1.166<br/>
            KTU 1.167<br/>
            KTU 1.169<br/>
            KTU 1.172<br/>
            KTU 1.176<br/>
            KTU 1.178<br/>
            KTU 1.179<br/>
            KTU 1.180</aside>
</div>