---
title: Bibliographische Abkürzungen
lang: de
layout: Layout
---

# {{ $frontmatter.title }}

<p>Die Abkürzungen folgen <a href="https://rla.badw.de/reallexikon.html" target="_blank">Streck, M. P., et al. (Hg.), 1928–2016: Reallexikon der Assyriologie und Vorderasiatischen Archäologie. Berlin etc.</a> (Download <a href="https://rla.badw.de/fileadmin/user_upload/Files/RLA/03_Abkverz_Ende_Nov2018.pdf" target="_blank">PDF</a>).</p>

**Zusätzliche Abkürzungen:**  

<table class="edPrinzAbkuerzungenTable">
    <tr>
        <td>AEL</td>
        <td>Lane, E. W., 1863: <i>An Arabic-English Lexicon</i>. London / Edinburgh.</td></tr>
    <tr>
        <td>ANEM</td>
        <td><i>Ancient Near East Monographs</i> (Schriftenreihe; Atlanta, GA 2008ff.)</td></tr>
    <tr>
        <td>BDFSN Mon.</td>
        <td><i>Banco de Datos Filológicos Semíticos Noroccidentales. Monografías</i> (Schriftenreihe; Madrid 1995ff.)</td></tr>
    <tr>
        <td>ConBOT</td>
        <td><i>Coniectanea Biblica. Old Testament Series</i> (Schriftenreihe; Lund / Stockholm / Winona Lake, IN 1967ff.)</td></tr>
    <tr>
        <td>CTA</td>
        <td>Herdner, A., 1963: <i>Corpus des tablettes en cunéiformes alphabétiques découvertes à Ras Shamra-Ugarit de 1929 à 1939. 1–2</i>. MRS 10. Paris.</td></tr>
    <tr>
        <td>DUL<sup>3</sup></td>
        <td>del Olmo Lete, G. / Sanmartín, J., 2015: <i>A Dictionary of the Ugaritic Language in the Alphabetic Tradition. Third Revised Edition. Translated and Edited by Wilfred G.E. Watson</i>. HdO I 112. Leiden / Boston.</td></tr>
    <tr>
        <td>JNSL</td>
        <td><i>Journal of Northwest Semitic Languages</i> (Zeitschrift; Stellenbosch 1971ff.)</td></tr>
    <tr>
        <td>KTU<sup>1</sup></td>
        <td>Dietrich, M. / Loretz, O. / Sanmartín, J., 1976: <i>Die keilalphabetischen Texte aus Ugarit. Einschließlich der keilalphabetischen Texte außerhalb Ugarits. I. Transkription</i>. AOAT 24. Kevelaer / Neukirchen-Vluyn.</td></tr>
    <tr>
        <td>KTU<sup>2</sup> (CAT)</td>
        <td>Dietrich, M. / Loretz, O. / Sanmartín, J., 1995: <i>The Cuneiform Alphabetic Texts from Ugarit, Ras Ibn Hani and Other Places (KTU: second, enlarged edition)</i>. ALASP 8. Münster.</td></tr>
    <tr>
        <td>KTU<sup>3</sup></td>
        <td>Dietrich, M. / Loretz, O. / Sanmartín, J., 2013: <i>Die keilalphabetischen Texte aus Ugarit, Ras Ibn Hani und anderen Orten. Dritte, erweiterte Auflage</i>. AOAT 360/1. Münster.</td></tr>
    <tr>
        <td>KWU</td>
        <td>Tropper, J., 2008: <i>Kleines Wörterbuch des Ugaritischen</i>. ELO 4. Wiesbaden.</td></tr>
    <tr>
        <td>PhotoArch. dOL</td>
        <td><a href="https://www.academia.edu/8708344/Photographic_Archive-Canaanite_Religion_...._second_revised_and_enlarged_English_edition" target="_blank">del Olmo Lete, G., 2014: <i>Photographic Archive. Canaanite Religion. According to the liturgical texts of Ugarit</i>.</a></td></tr>
    <tr>
        <td>RANT</td>
        <td><i>Res Antiquae</i> (Zeitschrift; Brüssel 2004ff.)</td></tr>
    <tr>
        <td>RBS</td>
        <td>Resources for Biblical Study (Schriftenreihe; Society of Biblical Literature; Atlanta 1988ff.)</td></tr>
    <tr>
        <td>RVO</td>
        <td><i>Religionen im Vorderen Orient</i> (Schriftenreihe; Hamburg 2013ff.)</td></tr>
    <tr>
        <td>SEC</td>
        <td><a href="https://www.brepols.net/series/SEC" target="_blank"><i>Semitica et Classica</i></a> (Zeitschrift; Turnhout 2008ff.).</td></tr>
    <tr>
        <td>StOrE</td>
        <td><a href="https://journal.fi/store" target="_blank"><i>Studia Orientalia Electronica</i></a> (Zeitschrift; Helsinki 2013ff.).</td></tr>
    <tr>
        <td>UDB</td>
        <td><a href="https://www.academia.edu/500096/The_texts_of_the_Ugaritic_data_bank_Ugaritic_Data_Bank_The_Text_with_english_commentaries_all_english_versions_" target="_blank">Cunchillos, J.-L. / Vita, J.-P. / Zamora, J.-Á., 2003: <i>Ugaritic Data Bank. The Texts</i>. Madrid (elektronische Fassung).</a></td></tr>
    <tr>
        <td>UG<sup>2</sup></td>
        <td>Tropper, J., 2012: <i>Ugaritische Grammatik. Zweite, stark überarbeitete und erweiterte Auflage</i>. AOAT 273. Münster.</td></tr>
    <tr>
        <td>WAW</td>
        <td>Writings from the Ancient World (Schriftenreihe; Society of Biblical Literature; Atlanta 1990ff.)</td></tr>
    <tr>
        <td>WSRP</td>
        <td><a href="https://digitallibrary.usc.edu/CS.aspx?VP3=SearchResult&VBID=2A3BXZS3K8SER&PN=1&WS=SearchResults" target="_blank">West Semitic Research Project / InscriptiFact</a></td></tr>
    <tr>
        <td>ZIG</td>
        <td><a href="https://www.chbeck.de/buecher/zeitschriften/zeitschrift-fuer-ideengeschichte/" target="_blank"><i>Zeitschrift für Ideengeschichte</i></a> (Zeitschrift; München 2007ff.).</td></tr>
</table>