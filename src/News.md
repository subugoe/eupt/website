---
title: News
lang: de
layout: Layout
---

<h1>News</h1>

<h2>Veranstaltungen</h2>

<details class="EUPT-NewsSeite-details">
<summary><h4>2023-11-02: EUPT-Auftaktveranstaltung</h4></summary>

<div class="allTextPagesIncludingFigures-spec-1">
<figure class="float-right">
    <img src="/assets/images-content/EUPT_Kick-Off_Plakat.jpg" alt="EUPT_Kick-Off_Plakat"/>
</figure>

<div class="mainTextPagesIncludingFigures">
<div class="Absatz-hängend" style="margin-top: 1em;">Auftaktveranstaltung des DFG-Projekts</div>
<div class="Absatz-hängend"><i>Edition des ugaritischen poetischen Textkorpus</i> (EUPT)</div>
<div class="Absatz-hängend">2. November 2023 – 16 Uhr s.t. – Theologicum, Hörsaal -1.113</div>

<p><b>Programm:</b></p>

<table class="EUPT-Veranstaltungen-Programm">
    <tr>
        <td>16:00–16:30</td>
        <td>Grußworte</td></tr>
    <tr>
        <td>16:30–17:00</td>
        <td>
            <div>Festvortrag</div>
            <div>Prof. Dr. Herbert Niehr (Universität Tübingen)</div></td></tr>
    <tr>
        <td>17:00–17:30</td>
        <td>
            <div>Festvortrag</div>
            <div>Dr. Carole Roche-Hawley (Directrice scientifique de l’Institut français du Proche-Orient, Département d’archéologie et d’histoire de l’Antiquité)</div></td></tr>
    <tr>
        <td>17:30–18.00</td>
        <td>
            <div>Vorstellung des DFG-Projekts „Edition des ugaritischen poetischen Textkorpus (EUPT)“ und Einführung in die Ugarit-Bibliothek Göttingen</div>
            <div>Prof. Dr. Reinhard Müller / Clemens Steinberger (Universität Göttingen)</div></td></tr>
    <tr>
        <td>18:00–19:30</td>
        <td>Empfang</td></tr>
</table>

</div><!--End of mainText-->
</div><!--End of allText-->
</details>

<h2>Updates des EUPT-Webportals</h2>
<p>Die folgende Liste gibt einen Überblick über die Updates des EUPT-Webportals (ausgenommen fortlaufende Ergänzungen und Korrekturen in den Bereichen <i>Start</i> und <i>Literatur</i>). Der Versionsverlauf der einzelnen Textbearbeitungen ist im Editionsviewer einsehbar (am Ende der <i>Metadaten</i> im linken Panel <i>Inhalt & Metadaten</i>), die Änderungshistorie der im EUPT-<i>Laboratory</i> zugänglichen Beiträge in der <i>Info</i> zum jeweiligen Beitrag.</p>

<details class="EUPT-NewsSeite-details">
<summary><h4>2025-03-05</h4></summary><!-- Requires update on release; check also README.md -->
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Einführung: Texte / Tafeln / Forschung: Korrekturen und Anpassungen in der Tafelkonkordanz.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Anpassungen.</li>
            <li>Beiträge: Veröffentlichung von GMU 2.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2025-02-19</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Optimierung der Darstellung von Vollzitaten, die sich per Klick auf ein Kurzzitat öffnen (außerhalb des Editionsviewers; betrifft bibliographische Angaben in Tabellen).</span></li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition / Editionsviewer: Veröffentlichung einer neuen Version der Edition von KTU 1.14; Optimierung der Darstellung der Editionsdaten.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">EUPT-Laboratory:</span>
        <ol>
            <li>Beiträge: Veröffentlichung einer neuen Version des Beitrags EUPT-<i>Lab</i> 1.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Korrekturen und Anpassungen (in der Manuskriptvorlage für GMU-Beiträge).</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2025-02-05</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Einführung: Texte / Tafeln / Forschung: Kleinere Anpassungen und Korrekturen.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2025-01-29</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition / Editionsviewer: Optimierung der Anzeige der Lemmatisierung / der morphologischen Analyse (per Klick auf ein Wort in der Vokalisation öffnet sich eine Infobox).</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Anpassungen und Ergänzungen.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2025-01-15</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition / Editionsviewer: Kleinere Korrekturen und Anpassungen.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Kleinere Anpassungen.</li>
        </ol>
    </li>
</ol>
</details>


<details class="EUPT-NewsSeite-details">
<summary><h4>2025-01-08</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Start:</span>
        <ol>
            <li>Veröffentlichung der Seite <i>News</i>.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition / Editionsviewer: Veröffentlichung einer neuen Version der Edition von KTU 1.14; Optimierung der Darstellung der Editionsdaten.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Korrekturen und Anpassungen.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2024-11-27</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Korrekturen und Anpassungen.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2024-11-15</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Allgemein:</span>
        <ol>
            <li>Neues Feature: Per Klick auf ein Kurzzitat lässt sich das dazugehörige Vollzitat anzeigen (vorher nur im Editionsviewer möglich).</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Ugarit:</span>
        <ol>
            <li>Ugarit in der Spätbronzezeit / Ugarit und Altes Testament: Ergänzung von Abbildungen.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition / Editionsviewer: Veröffentlichung einer neuen Version der Edition von KTU 1.14; Optimierung der Darstellung der Editionsdaten; neues Feature: die Panels <i>Transliteration</i> und <i>Vokalisation & Übersetzung</i> können synchron gescrollt werden.</li>
            <li>Editorische Prinzipien: Umstrukturierung und Umgestaltung der Inhalte; Korrekturen und Anpassungen.</li>
            <li>Einführung: Texte / Tafeln / Forschung: Umstrukturierung und Umgestaltung der Inhalte; Veröffentlichung der Tafelkonkordanz (im gleichnamigen Bereich); Ergänzung von Abbildungen im Bereich <i>Forschungsgeschichte</i>.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">EUPT-Laboratory:</span>
        <ol>
            <li>Einführung / Startseite des EUPT-Lab: Ergänzungen.</li>
            <li>Beiträge: Veröffentlichung einer neuen Version des Beitrags EUPT-<i>Lab</i> 1; Optimierung der Darstellung der Forschungsdaten.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite: Ergänzungen.</li>
            <li>Beiträge: Veröffentlichung von GMU 1.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2024-08-06</h4></summary>
<p><b>Neuigkeiten:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Start:</span>
        <ol>
            <li>Veröffentlichung der Seite <i>Arbeitsplan</i>.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition / Editionsviewer: Veröffentlichung einer neuen Version der Edition von KTU 1.14; Optimierung der Darstellung der Editionsdaten; neue Features: a) per Klick auf ein Kurzzitat in den Kommentaren lässt sich das dazugehörige Vollzitat anzeigen; b) Versgrenzen werden angezeigt (durch eine Leerzeile).</li>
            <li>Einführung: Textübersicht / Forschungsgeschichte: Umstrukturierung und Umgestaltung der Inhalte.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">EUPT-Laboratory:</span>
        <ol>
            <li>Beiträge: Veröffentlichung einer neuen Version des Beitrags EUPT-<i>Lab</i> 1.</li>
        </ol>
    </li>
</ol>
</details>

<details class="EUPT-NewsSeite-details">
<summary><h4>2024-05-07</h4></summary>
<p><b>Launch; Website-Inhalte:</b></p>
<ol>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Start:</span>
        <ol>
            <li>EUPT (Startseite)</li>
            <li>Team / Kontakt</li>
            <li>Technische Dokumentation</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Ugarit:</span>
        <ol>
            <li>Ugarit in der Spätbronzezeit</li>
            <li>Ugarit und Altes Testament</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Korpus:</span>
        <ol>
            <li>Edition: Veröffentlichung der Edition von KTU 1.14.</li>
            <li>Editorische Prinzipien</li>
            <li>Einführung (Einleitungen zu den Hauptwerken der ugaritischen Poesie und Abriss der Forschungsgeschichte)</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">EUPT-<i>Laboratory</i>:</span>
        <ol>
            <li>Einführung / Startseite des EUPT-<i>Lab</i></li>
            <li>Beiträge: Veröffentlichung des Beitrags EUPT-<i>Lab</i> 1.</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Göttinger Miszellen zur Ugaritistik:</span>
        <ol>
            <li>Einführung / GMU-Startseite</li>
        </ol>
    </li>
    <li><span class="EUPT-Update-Neuigkeiten-h-sub">Literatur:</span>
        <ol>
            <li>Bibliographie</li>
            <li>Publikationen der Ugarit-Forschungsstelle</li>
            <li>Abkürzungen</li>
        </ol>
    </li>
</ol>
</details>