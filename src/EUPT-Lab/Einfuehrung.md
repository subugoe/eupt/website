---
title: EUPT-Laboratory
lang: de
layout: Layout
---

<h1 class="h1-in-tab-layout">EUPT-<i>Laboratory</i></h1>

<div class="EUPT-lab-tabs">

<input id="tab1" type="radio" name="tabs" checked="checked">
    <label class="EUPT-lab-tabs-label" for="tab1"><p>Über das EUPT-<i>Lab</i></p></label>
    <div class="EUPT-lab-tabs-content">

<div style="margin-top: 1em;"><b>Forschungsplattform (<i>online</i> – <i>open access</i>)</b></div>
<div class="Absatz-hängend"><b>Bearbeitet und herausgegeben von Reinhard Müller, Clemens Steinberger und Noah Kröll</b></div>

<p>Im EUPT-<i>Laboratory</i> werden Ergebnisse der laufenden Arbeit an der Edition der ugaritischen Texte zugänglich gemacht. Der Fokus liegt auf der literaturwissenschaftlichen und linguistischen Analyse der Sprache und der Form der ugaritischen Dichtung sowie auf der Interpretation der poetischen Texte. Unter anderem ist im Lab der <a href="/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html">Katalog poetologischer Stichwörter</a> zugänglich, auf dessen Grundlage die Texte in den Editionen annotiert werden. Das <i>Lab</i> bietet jedoch ebenso Platz für anders ausgerichtete Untersuchungen, die der Überlieferung Ugarits gewidmet sind und sich mit text- und manuskriptübergreifenden Phänomenen und Problemen befassen.</p>

<p>Das EUPT-<i>Laboratory</i> gewährt Einblick in laufende Forschung. Die Beiträge, die teils umfassende Datensammlungen enthalten, werden regelmäßig überarbeitet und aktualisiert. Das <i>Lab</i> ermöglicht Wissenschaftlerinnen und Wissenschaftlern, neue Forschungsergebnisse unmittelbar mit der wissenschaftlichen Öffentlichkeit zu teilen. Sämtliche Beiträge sind gebührenfrei online abrufbar (sowohl vor als auch nach der Publikation über das <a href="https://zenodo.org/communities/eupt-lab/records?q=&l=list&p=1&s=10&sort=newest" target="_blank">Zenodo-Portal</a>; s.u. mit Anm. b). Alle Artikel werden unter einer <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC-BY-Lizenz</a> veröffentlicht. Autorinnen und Autoren behalten die Urheberrechte ihrer Arbeiten.</p>

<p><div>Datenverarbeitung, Datenpräsentation und Update-Modus werden individuell auf die Anforderungen des jeweiligen Beitrags abgestimmt. Jeder Beitrag wird intern begutachtet, bevor er erstmals online gestellt wird.<details class="GMU-guidelines-notes-details-box">
    <summary><sup>a</sup></summary>
    <div>Der jeweilige Beitrag wird von den Herausgebern begutachtet, die nicht Autoren des Beitrags sind. Der Beitrag wird erst auf dem EUPT-Portal zugänglich gemacht, nachdem alle Herausgeber zugestimmt haben. Im Zweifelsfall holen die Herausgeber das Gutachten einer externen Expertin / eines externen Experten ein.</div></details> 
    Im Unterschied zu den <a href="/GMU/Einfuehrung.html" target="_self">Göttinger Miszellen zur Ugaritistik</a> werden die <i>Lab</i>-Beiträge im ersten Schritt ausschließlich auf dem EUPT-Webportal zugänglich gemacht und erhalten keine DOI-Nummer. Erst nachdem die Arbeiten an dem jeweiligen Beitrag abgeschlossen sind, wird der Beitrag angelehnt an den Publikationsmodus der GMU über das <a href="https://zenodo.org/communities/eupt-lab/" target="_blank">Zenodo-Portal</a> veröffentlicht.<details class="GMU-guidelines-notes-details-box">
    <summary><sup>b</sup></summary>
    <div>Für das EUPT-Lab wurde auf <a href="https://zenodo.org/" target="_blank">zenodo.org</a> eine sogenannte <i>Community</i> eingerichtet (s. <a href="https://zenodo.org/communities/eupt-lab/records?q=&l=list&p=1&s=10&sort=newest" target="_blank">https://zenodo.org/<wbr>communities/<wbr>eupt-lab/</a>). Phasen des Publikationsprozesses: a) Peer-Review der vorläufigen Endfassung des Beitrags durch mindestens zwei unabhängige Gutachtende (die Gutachtenden gehören nicht dem Herausgeberteam an); b) finale Korrekturen; c) Veröffentlichung des Beitrags auf dem Zenodo-Portal (samt Zuteilung einer DOI-Nummer) und Aktualisierung der Web-Version des Beitrags auf dem EUPT-Portal. Für das Publikationsverfahren gelten grundsätzlich die ethischen Grundsätze der <i>Göttinger Miszellen zur Ugaritistik</i>. Ausnahme: Das Peer-Review-Verfahren wird nicht im Doppelblind-Modus durchgeführt, da jeder Beitrag (samt Informationen über die Autorinnen und Autoren) bereits vor der eigentlichen Publikation im EUPT-Laboratory frei zugänglich ist.</div>
    </details></div></p>
<p>Das EUPT-<i>Laboratory</i> wird von Mitgliedern des EUPT-Projektteams betreut und präsentiert in erster Linie Forschungsergebnisse des EUPT-Teams (um einen Beitrag anlegen, aufbauen und betreuen zu können, bedarf es des Zugangs zur digitalen Infrastruktur des EUPT-Projekts). Kooperationen mit externen Forscherinnen und Forschenden sind jedoch möglich. Bei Interesse, gemeinsam mit dem EUPT-Team einen <i>Lab</i>-Beitrag (auf Englisch oder Deutsch) aufzubauen und langfristig zu betreuen, richten Sie sich bitte an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a> (Interessierte werden gegebenenfalls aufgefordert, eine Beitragsskizze einzureichen).</p>

<details class="GMU-Einfuehrung-h-details" style="text-align: left;">
<summary><h3>Bearbeiter / Herausgeber</h3></summary>
    <div class="Absatz-hängend" style="margin-top: 1em;"><b>Prof. Dr. Reinhard Müller</b> <span class="noBreakEUPT">
            <a class="Logo-orcid" href="https://orcid.org/0000-0002-7433-2273" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Logo-academia" href="https://xn--uni-gttingen-8ib.academia.edu/ReinhardM%C3%BCller" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Logo-university" href="https://www.uni-goettingen.de/de/prof.+dr.+reinhard+m%C3%BCller/56732.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span></div>
        <div class="Absatz-hängend">Professor für Altes Testament an der 
            <a href="https://www.uni-goettingen.de/de/die+fakult%c3%a4t/55363.html" target="_blank">Theologischen Fakultät</a> der <span class="noBreakEUPT"><a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend">Leiter der <a href="https://uni-goettingen.de/de/431176.html" target="_blank">Ugarit-Forschungsstelle</a> an der Universität Göttingen</div>
        <div class="Absatz-hängend">Leiter des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> an der Universität Göttingen</div>
        <div class="Absatz-hängend"><a href="mailto:Reinhard.Mueller@theologie.uni-goettingen.de">reinhard.mueller@theologie.uni-goettingen.de</a></div>
    <div class="Absatz-hängend" style="margin-top: 1em;"><b>Clemens Steinberger, BA MA</b> <span class="noBreakEUPT">
            <a class="Logo-orcid" href="https://orcid.org/0009-0003-4268-1596" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Logo-academia" href="https://uni-goettingen.academia.edu/ClemensSteinberger" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Logo-university" href="https://uni-goettingen.de/de/219789.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span></div>
        <div class="Absatz-hängend">Koordinator der <a href="https://uni-goettingen.de/de/431176.html" target="_blank">Ugarit-Forschungsstelle</a> an der <span class="noBreakEUPT">
            <a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend">Wissenschaftlicher Mitarbeiter (Postdoc) und Koordinator des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> an der Universität Göttingen</div>
        <div class="Absatz-hängend"><a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a></div>
    <div class="Absatz-hängend" style="margin-top: 1em;"><b>Noah Kröll, BA MA</b> <span class="noBreakEUPT">
            <a class="Logo-orcid" href="https://orcid.org/0009-0000-9138-9928" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Logo-academia" href="https://uibk.academia.edu/NoahKr%C3%B6ll" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Logo-university" href="https://www.uni-goettingen.de/en/68717.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span></div>
        <div class="Absatz-hängend">Koordinator der <a href="https://uni-goettingen.de/de/431176.html" target="_blank">Ugarit-Forschungsstelle</a> an der <span class="noBreakEUPT">
            <a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend">Wissenschaftlicher Mitarbeiter (Doktorand) des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> an der <span class="noBreakEUPT">
            <a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend" style="margin-bottom: 1em;"><a href="mailto:Noah.Kroell@theologie.uni-goettingen.de">noah.kroell@theologie.uni-goettingen.de</a></div>
    <!-- Ehemalige Bearbeiter*innen / Herausgeber*innen: ---></details>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Kontakt</h3></summary>
<p>Fragen, Anmerkungen und Verbesserungsvorschläge richten Sie bitte an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a> oder <a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a>.</p>
</details>

</div>






<input id="tab2" type="radio" name="tabs">
    <label class="EUPT-lab-tabs-label EUPT-lab-tabs-last-label" for="tab2"><p>Beiträge</p></label>
    <div class="EUPT-lab-tabs-content">

<div class="EUPT-Lab-Publikationen">
<table class="EUPT-Lab-PublikationenTable">
<thead>
    <tr>
        <th>Nr.</th>
        <th>Erschienen am</th>
        <th>Autor*innen</th>
        <th>Titel</th>
        <th><i>Keywords</i></th>
        <th>DOI (via Zenodo)</th>
        <th>Webversion (HTML)</th></tr></thead>
<tbody>
    <tr>
        <td><span class="sort-id">0001</span>1</td>
        <td>2024-05-03<br/>
            <i>work in progress</i></td>
        <td>Steinberger, Clemens</td>
        <td>Glossar der ugaritischen poetischen Formen</td>
        <td>Ugaritic poetry, stylistics, verse structure, poetology</td>
        <td>-</td>
        <td><a href="/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html">HTML</a></td></tr></tbody>
</table>
</div>

</div>




<!--End of EUPT-Lab-Tabs-->
</div>