---
title: EUPT-Lab 1 - Steinberger - Glossar der ugaritischen poetischen Formen
lang: de
layout: Layout
head:
  - - meta
    - name: citation_author
      content: Clemens Steinberger
  - - meta
    - name: citation_author_orcid
      content: https://orcid.org/0009-0003-4268-1596
  - - meta
    - name: citation_author_email
      content: clemens.steinberger@theologie.uni-goettingen.de
  - - meta
    - name: citation_author_institution
      content: Universität Göttingen
  - - meta
    - name: citation_title
      content: Glossar der ugaritischen poetischen Formen
  - - meta
    - name: citation_publication_date
      content: 2024-05-03
  - - meta
    - name: citation_language
      content: de
  - - meta
    - name: citation_doi
      content:
  - - meta
    - name: citation_publisher
      content: Reinhard Müller, Clemens Steinberger, Noah Kröll; Edition des ugaritischen poetischen Textkorpus (EUPT)
  - - meta
    - name: citation_journal_title
      content: EUPT-Laboratory
  - - meta
    - name: citation_journal_abbrev
      content: EUPT-Lab
  - - meta
    - name: citation_issn
      content:
  - - meta
    - name: citation_issue
      content: 0001
  - - meta
    - name: citation_keywords
      content: Ugaritic poetry, stylistics, verse structure, poetology
  - - meta
    - name: citation_abstract
      content:
  - - meta
    - name: citation_fulltext_html_url
      content: https://eupt.uni-goettingen.de/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html
  - - meta
    - name: citation_pdf_url
      content:
---
<a href="#top" id="top-link"></a>

<h1 class="GMU-h1">Glossar der ugaritischen poetischen Formen</h1>
    <p class="GMU-sub-h1"><b>Clemens Steinberger</b></p>
    <p class="GMU-sub-h1">EUPT-<i>Lab</i> 1 | 2024–2025</p>

<div class="EUPT-lab-article-tabs">

<input id="tab1" type="radio" name="tabs" checked="checked">
    <label class="EUPT-lab-article-tabs-label" for="tab1"><p>Info</p></label>
    <div class="EUPT-lab-article-tabs-content">

<div class="GMU-metadata">
<table class="GMU-metadata-table">
	<tr>
        <td>Titel:</td>
        <td><button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">Glossar der ugaritischen poetischen Formen</button></td></tr>
    <tr>
    	<td>Autor:</td>
        <td>Clemens Steinberger <span class="noBreakEUPT">
            <a class="Metadata-Logo-orcid" href="https://orcid.org/0009-0003-4268-1596" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Metadata-Logo-academia" href="https://uni-goettingen.academia.edu/ClemensSteinberger" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Metadata-Logo-university" href="https://uni-goettingen.de/de/219789.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span><br/>
            <span class="noBreakEUPT"><a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Metadata-Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank"><img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span><br/>
            <a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a></td></tr>
    <tr>
        <td><i>Keywords</i>:</td>
        <td>Ugaritic poetry (Ugaritische Poesie), stylistics (Stilistik), verse structure (Versstruktur), poetology (Poetologie)</td></tr>
    <tr>
        <td>Updates:</td>
        <td>Version <i>Draft 2.2</i>: <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">2025-02-19</button><br/>
            Version <i>Draft 2.1</i>: 2024-11-15 
                (<a href="/EUPT-online-article-versioning/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen_Version_Draft_2.1.html" target="_blank" onclick="return confirm('You are accessing an outdated version of this page. This allows you to view data that has been updated since it was first published.\n\nPlease note, however, that this version does not necessarily represent the current state of the site. Styling and functionality of this version are not maintained.')">access HTML</a>)<br/>
            Version <i>Draft 2.0</i>: 2024-08-06 
                (<a href="/EUPT-online-article-versioning/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen_Version_Draft_2.0.html" target="_blank" onclick="return confirm('You are accessing an outdated version of this page. This allows you to view data that has been updated since it was first published.\n\nPlease note, however, that this version does not necessarily represent the current state of the site. Styling and functionality of this version are not maintained.')">access HTML</a>)<br/>
            Version <i>Draft 1.0</i>: 2024-05-03 
                (<a href="/EUPT-online-article-versioning/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen_Version_Draft_1.0.html" target="_blank" onclick="return confirm('You are accessing an outdated version of this page. This allows you to view data that has been updated since it was first published.\n\nPlease note, however, that this version does not necessarily represent the current state of the site. Styling and functionality of this version are not maintained.')">access HTML</a>)</td></tr>
    <tr>
        <td>Zugänglich als:</td>
        <td><a href="/EUPT-Lab/Einfuehrung.html" target="_blank">EUPT-<i>Lab</i></a> 1</td></tr>
    <tr>
        <td>DOI:</td>
        <td><i>Der Beitrag erhält erst bei Veröffentlichung der Endfassung eine DOI-Nummer.</i></td></tr>
    <tr>
        <td>Lizenz:</td>
        <td><a rel="license" href="https://creativecommons.org/licenses/by/4.0/" target="_blank"><img class="Metadata-Logo-cc" src="https://licensebuttons.net/l/by/4.0/88x31.png"></a><br/>
        This work is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a>. <span class="noBreakEUPT">© Clemens</span> Steinberger 2024.</td></tr>
    <tr>
        <td>Zitation:</td>
        <td>Steinberger, Clemens. 2024. „Glossar der ugaritischen poetischen Formen (Version #)“. <i>EUPT</i>-Laboratory 1. <a href="/EUPT-Lab/1_Glossar-der-ugaritischen-poetischen-Formen.html" target="_self">https://eupt.uni-goettingen.de/<wbr>EUPT-Lab/<wbr>1_<wbr>Glossar-der-ugaritischen-poetischen-Formen.html</a>. Aufgerufen am YYYY-MM-DD. <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/GMXBZIIC/item-details" target="_blank"><img class="Bibliography-Logo-zotero" src="https://www.zotero.org/support/_media/logo/zotero_32x32x32.png"></a></td></tr>
</table>
</div>
</div>

<input id="tab2" type="radio" name="tabs">
<label class="EUPT-lab-article-tabs-label" for="tab2"><p>Artikel</p></label>
<div class="EUPT-lab-article-tabs-content">

<p>Die ugaritische Poesie ist durch sprachliche, strukturelle und stilistische Besonderheiten geprägt, die die poetischen Texte von nicht-poetischen unterscheiden. Die Gedichte gliedern sich in verschieden große Verseinheiten, die oft parallel gestaltet sind, und enthalten eine Fülle von Stilmitteln.</p>

<p>Das folgende Glossar gibt einen Überblick über diverse Formen, die in der ugaritischen Dichtkunst belegt sind. Um Vergleiche mit verwandten Poesien zu erleichtern, sind auch einige Stilfiguren genannt, für die sich im ugaritischen Korpus bislang keine Beispiele gefunden haben. Unter jedem Glossar-Eintrag in grüner Schrift findet sich eine knappe Definition des behandelten Phänomens mit einigen Beispielen. Die Liste wird fortlaufend erweitert und verbessert.</p>

<p class="GUPF-open-button-container"><button class="EUPT-html-button" onclick="document.querySelectorAll('details.doneEntryGUPF').forEach((e) => {(e.hasAttribute('open')) ? e.removeAttribute('open') : e.setAttribute('open',true)})">Alle Einträge aus-/einklappen</button></p>

<h2>A</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Anakoluth</summary>
<p>&#x2197; Wortstellung.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Anastrophe</summary>
<p>&#x2197; Wortstellung.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Abbildende Wortstellung</summary>
<p>&#x2197; Wortstellung.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Accumulatio</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Akrostichon</summary>
<p>Im Akrostichon ergeben die Zeichen, die jeweils am Anfang aufeinanderfolgender Zeilen oder Abschnitte stehen, aneinandergereiht ein Wort, einen Namen oder einen Satz (vgl. <bibl>Grimm, 2007<VuepressApiPlayground url="/api/eupt/biblio/CDFUWIJB" method="get" :data="[]"/></bibl>). Das Akrostichon ist im ugaritischen Korpus bislang nicht bezeugt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Alliteration</summary>
<p>Die Alliteration ist eine Klangfigur. Zwei oder mehrere Begriffe, die innerhalb eines Kolons aufeinander folgen, klingen gleich an (die alliterierenden Wörter folgen unmittelbar aufeinander). Vermutlich ist nicht jede Alliteration, die sich in den ugaritischen poetischen Texten ausmachen lässt, bewusst gesetzt (s. KTU 1.14 iii 31b–32a oder 35b–36a). Zuweilen gebrauchten die ugaritischen Dichter die Alliteration aber wohl bewusst, um die Aufmerksamkeit des Publikums auf bestimmte Aussagen zu lenken und diese so hervorzuheben (Alliterationen ließen die Rezipient*innen des Texts vermutlich kurz aufhorchen; außerdem haben alliterierend gestaltete Aussagen hohen Wiedererkennungswert; s. KTU 1.17 vi 32b–33a). Vielleicht wurden Alliterationen mitunter auch aus euphonischen Gründen (i. e. zugunsten des Wohlklangs) eingesetzt.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 iii 31b–32a</th></tr>
    <tr>
        <td>wa ˀôšānu <span class="ugaritic-text-line-number">32</span>&#160;<b>ˀa</b>bī <b>ˀa</b>dami</td>
        <td>ja, ein Geschenk des Vaters der Menschheit</td></tr>
<tr><th colspan="2">KTU 1.14 iii 35b–36a</th></tr>
    <tr>
        <td>wa <b>ˁa</b>bda <span class="ugaritic-text-line-number">36</span>&#160;<b>ˁā</b>lami ṯalāṯa sus<span class="non-ugaritic-text">(</span>s<span class="non-ugaritic-text">)</span>uwīma</td>
        <td>und einen ewig (gebundenen) Knecht (und) drei Pferde</td></tr>
<tr><th colspan="2">KTU 1.14 iii 43a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">43</span>&#160;dā ˁQ-āha <b>ˀi</b>bbā <b>ˀi</b>qnaˀi</td>
        <td>deren Pupillen (wie) zwei Lapislazuli-Steine sind</td></tr>
<tr><th colspan="2">KTU 1.14 iii 53</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">53</span>&#160;<b>y</b>irḥaṣ <b>y</b>adêhu ˀammatah</td>
        <td>Er wusch seine Hände bis zum Ellbogen.</td></tr>
<tr><th colspan="2">KTU 1.14 v 1b–2</th></tr>
    <tr>
        <td>wa bi <span class="ugaritic-text-line-number">2</span>&#160;<b>m</b>aqâri <b>m</b>umalliˀatu</td>
        <td>ja, die an der Quelle (Schläuche) füllte</td></tr>
<tr><th colspan="2">KTU 1.17 vi 32b–33a</th></tr>
    <tr>
        <td><b>ˀa</b>ppV <b>ˀa</b>nāku <b>ˀa</b>ḥawwiya <span class="ugaritic-text-line-number">33</span>&#160;<b>ˀa</b>qhata <span class="non-ugaritic-text">[</span>ġāzi<span class="non-ugaritic-text">]</span>ra</td>
        <td>(So) will auch ich beleben ˀAqhatu, [den Hel]den!</td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Alternation</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Anadiplose</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Anapher</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Antonomasie</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Apokoinu</summary>
<p>Das Apokoinu ist eine Konstruktion, in der sich eine Phrase, ein Satzglied oder ein Satzgliedteil (i. e. das Koinon) gleichzeitig auf die voranstehende und die nachfolgende Phrase bezieht (Sonderform des Zeugmas; vgl. <bibl>Steinhoff / Burdorf, 2007<VuepressApiPlayground url="/api/eupt/biblio/VMKEQ6IB" method="get" :data="[]"/></bibl> und <bibl>Schweikle / Schlösser, 2007<VuepressApiPlayground url="/api/eupt/biblio/ADRMUJGR" method="get" :data="[]"/></bibl>). In der ugaritischen Poesie ist die Figur u. a. in der Kolonkonstruktion P<sup>V</sup> &#8594; SG<sup>N</sup> &#8592; P<sup>V</sup> bezeugt: Das Kolon setzt sich aus zwei verbalen Prädikaten (P<sup>V</sup>) und einem nominalen Satzglied (SG<sup>N</sup>) zusammen. Das nominale Satzglied steht zwischen den beiden Prädikaten; die Prädikate stehen jeweils am Kolonrand. Weder auf das erste Prädikat (am Kolonanfang) noch auf das nominale Satzglied (in der Kolonmitte) folgt eine Konjunktion. Das nominale Satzglied in der Kolonmitte (i. e. das Koinon) bezieht sich gleichermaßen auf das voranstehende und das nachfolgende Prädikat. In den unten zitierten Beispielen bezeichnet das Koinon jeweils das Subjekt der beiden Prädikate. Das Apokoinu kann als Sonderform eines elliptischen Satzpaars begriffen werden: Die Konstruktion P<sup>V</sup>&#8594;S&#8592;P<sup>V</sup> kann auch als P<sup>V</sup>-S / ◌-P<sup>V</sup> (&lt; P<sup>V</sup>-S / S-P<sup>V</sup>) analysiert werden (das Koinon-Element, i. e. das nominal ausgedrückte Subjekt, ist im zweiten Satz ausgelassen).</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.2 i 19b (P&#8594;S&#8592;P)</th></tr>
    <tr>
        <td>tabiˁā ġalmāmi lā yaṯabā</td>
        <td>Es machten sich auf &#8594; die (beiden) Jünglinge &#8592; verweilten nicht.</td></tr>
<tr><th colspan="2">KTU 1.15 iii 17 (P&#8594;S&#8592;P)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17</span>&#160;tabarrikū ˀilūma taˀtiyū</td>
        <td>Es sprachen den Segen &#8594; die Götter &#8592; gingen (heim).</td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Archaismus</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Assonanz und Konsonanz</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Asyndeton</summary>
<p>Im Asyndeton sind die Glieder einer Aufzählung, die sich aus zwei oder mehreren syntaktisch gleichrangigen Lexemen oder Phrasen zusammensetzt, ohne Konjunktion aneinandergereiht (<bibl>Steinhoff, 2007: 51<VuepressApiPlayground url="/api/eupt/biblio/QS82WPVF" method="get" :data="[]"/></bibl>); die erwartete Konjunktion vor dem letzten Glied der Aufzählung ist ausgelassen (Gegenstück zum &#x2197; Polysyndeton). Gleichgeordnete Nomina / Nominalphrasen (keine vollständigen Sätze), die innerhalb eines Kolons aufeinanderfolgen, sind in der ugaritischen Poesie nur selten asyndetisch aneinandergereiht (s. Beispiele A; vgl. <bibl>Steinberger, 2022: 302 Anm. 44<VuepressApiPlayground url="/api/eupt/biblio/ZXWKYWAA" method="get" :data="[]"/></bibl>, u.a. zu KTU 1.4 vi 47–54). Im weiteren Sinn ist jedoch jedes Versgefüge als asyndetisch zu betrachten, das sich in zwei oder mehrere Verseinheiten gliedert, vorausgesetzt dass a) in den einzelnen Verseinheiten je <i>ein</i> Teil desselben übergeordneten Sachverhalts beschrieben ist und b) die einzelnen Verseinheiten nicht durch eine Konjunktion miteinander verbunden sind. Solche Konstruktionen finden sich in der ugaritischen Dichtung häufiger (s. Beispiele B).</p>
<details class="exampleGUPF">
<summary>Beispiele A</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 vi 47–54</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">47</span>&#160;šapîqa ˀilīma karrīma <b>ø</b> yêna</td>
        <td>Er reichte den Göttern (junge) Widder (und) Wein,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">48</span>&#160;šapîqa ˀilahāti ḫupārāti<span class="non-ugaritic-text">(/</span> ḫapūrāti<span class="non-ugaritic-text">)</span></td>
        <td class="further-colon-of-verse">er reichte den Göttinnen (weibliche) Lämmer,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">49</span>&#160;šapîqa ˀilīma ˀalapīma <b>ø</b> yê<span class="non-ugaritic-text">[</span>na<span class="non-ugaritic-text">]</span></td>
        <td>Er reichte den Göttern Ochsen (und) We[in],</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">50</span>&#160;šapîqa ˀilahāti ˀaraḫāti</td>
        <td class="further-colon-of-verse">er reichte den Göttinnen Kühe,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">51</span>&#160;šapîqa ˀilīma kaḥ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ṯīma<span class="non-ugaritic-text"><sup>?</sup></span> <b>ø</b> yêna</td>
        <td>Er reichte den Göttern Herrschersitze (und) Wein,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">52</span>&#160;šapîqa ˀilahāti kussiˀāti</td>
        <td class="further-colon-of-verse">er reichte den Göttinnen Throne,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">53</span>&#160;šapîqa ˀilīma rVḥabāti yêni</td>
        <td>Er reichte den Göttern Krüge voll Wein,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">54</span>&#160;šapîqa ˀilahā<span class="non-ugaritic-text">&lt;</span>ti<span class="non-ugaritic-text">&gt;</span> DKR-ā<span class="non-ugaritic-text">&lt;</span>ti<span class="non-ugaritic-text">&gt;</span></td>
        <td class="further-colon-of-verse">er reichte den Göttinnen Gefäße / (Wein-)Schläuche.</td></tr>
<tr><th colspan="2">KTU 1.14 iii 22–25</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;qaḥ kaspa wa yarqa</td>
        <td>Nimm Silber und Gelbgold,</td></tr>
    <tr>
        <td class="further-colon-of-verse">ḫurāṣa <span class="ugaritic-text-line-number">23</span>&#160;yada maqâmihu</td>
        <td class="further-colon-of-verse">Gold samt seinem Fundort,</td></tr>
    <tr>
        <td>wa ˁabda ˁālami <span class="ugaritic-text-line-number">24</span>&#160;<b>ø</b> ṯalāṯa sus<span class="non-ugaritic-text">(</span>s<span class="non-ugaritic-text">)</span>uwīma</td>
        <td>und einen ewig gebundenen Knecht (und) drei Pferde,</td></tr>
    <tr>
        <td class="further-colon-of-verse">markabta <span class="ugaritic-text-line-number">25</span>&#160;bi tarbaṣi <b>ø</b> bina ˀam<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti</td>
        <td class="further-colon-of-verse">einen Streitwagen aus (meinem) Stall (und) den Sohn einer Magd!</td></tr>
<tr><th colspan="2">KTU 1.14 iii 2–3a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">2</span>&#160;lik yôma wa ṯānâ</td>
        <td>Geh einen Tag lang und einen zweiten,</td></tr>
    <tr>
        <td>ṯāliṯa <b>ø</b> rābiˁa yôma</td>
        <td>einen dritten (und) einen vierten Tag,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">3</span>&#160;ḫāmiša <b>ø</b> ṯādiṯa yôma</td>
        <td>einen fünften (und) einen sechsten Tag!</td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.3 iii 43–44</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">43</span>&#160;maḫaštu môdāda ˀilima ARŠ-a</td>
        <td>Ich schlug nieder den Liebling des ˀIlu, ARŠ,</td></tr>
    <tr>
        <td><b>ø</b> <span class="ugaritic-text-line-number">44</span>&#160;ṣammittu ˁigla ˀili ˁTK-a</td>
        <td>(und) ich vernichtete das Kalb des ˀIlu, ˁTK!</td></tr>
</table>
</details>
<p/>
</details>

<h2>B</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Bikolon</summary>
<p>Das Bikolon ist ein &#x2197; Vers, der sich aus zwei &#x2197; Kola zusammensetzt. Das Bikolon ist die gängigste Versform der ugaritischen Dichtung.</p>
</details>

<h2>C</h2>

<details class="undoneEntryGUPF">
<summary>Chiasmus</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Constructio ad sensum</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Conversio</summary>
<p>&#x2197; Geminatio.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Correctio</summary>
</details>

<h2>D</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Distichon</summary>
<p>Das Distichon ist eine &#x2197; Strophe, die sich aus zwei &#x2197; Versen zusammensetzt.</p>
</details>

<h2>E</h2>

<details class="undoneEntryGUPF">
<summary>Ellipse</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Enjambement</summary>
<p>Im Enjambement sind die Glieder eines zusammenhängenden Satzes (i. d. R. ein Verbalsatz) über zwei (oder mehrere) Verseinheiten verteilt. In der ugaritischen Poesie ist das Enjambement sowohl auf Versebene (i. e. das Kolon-Enjambement zwischen aufeinanderfolgenden Kola) als auch auf Strophenebene belegt (i. e. das das Vers-Enjambement zwischen aufeinanderfolgenden Versen; das Kolon-Enjambement ist öfter belegt als das Vers-Enjambement).</p>
<p>Im Kolon-Enjambement verteilen sich die Glieder eines zusammenhängenden Satzes über zwei (nur in Ausnahmefällen drei) Kola (s. Beispiele A). Die Kola gehören zu <i>einem</i> Vers (der Vers wird als Enjambement-Vers bezeichnet). Nicht selten tritt vor oder hinter die beiden durch Enjambement verbundenen Kola ein weiteres Kolon, das parallel zu einem der beiden Kola steht (so ergibt sich ein &#x2197; Trikolon). Das Kolon des Enjambement-Verses, das des Prädikats des übergeordneten Satzes entbehrt, enthält meist ein einziges selbständiges Satzglied (Subjekt, Akkusativobjekt oder Adverbial; häufig enthält das prädikatlose Kolon ein Adverbial; seltener zwei nicht-prädikativische Satzglieder); die restlichen Satzteile samt dem Prädikat des Satzes stehen im anderen Kolon. Gleichwohl sind die Kola des Enjambement-Verses meist ungefähr gleich lang: Das prädikatlose Kolon setzt sich i. d. R. aus mehreren Worteinheiten zusammen (der nominale Satzteil, der in dem prädikatlosen Kolon steht, ist um ein oder mehrere Attribute / Appositionen erweitert oder setzt sich aus mehreren Nomina zusammen, die in Form einer &#x2197; Accumulatio miteinander verbunden sind).</p>
<details class="exampleGUPF">
<summary>Beispiele A: Das Kolon-Enjambement</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.15 ii 11–12</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">11</span>&#160;<span class="non-ugaritic-text">[</span>ˀaḫ<span class="non-ugaritic-text">(</span>ḫa<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">]</span>ra maġāyi ˁidati ˀilīma</td>
        <td>[Nachd]em die Götterversammlung gekommen war,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span>&#160;<span class="non-ugaritic-text">[</span>wa<span class="non-ugaritic-text">]</span> yaˁnî ˀalˀiyā<span class="non-ugaritic-text">[</span>nu<span class="non-ugaritic-text">]</span> baˁlu</td>
        <td>[da] sprach der Mächti[ge], Baˁlu.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A- // P-S; vgl. auch KTU 1.6 ii 26b–27 (A- // S-P); KTU 1.6 i 9b–10a (A<sub>1</sub>- // P-A<sub>2</sub>-O<sub>4</sub>).</p></td></tr>
<tr><th colspan="2">KTU 1.14 iii 3b–5</th></tr>
    <tr>
        <td>maka šapšuma <span class="ugaritic-text-line-number">4</span>&#160;bi šābiˁi</td>
        <td>Dann, bei Sonnenaufgang, am siebten (Tag),</td></tr>
    <tr>
        <td>wa tamġiyu li ˀud<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span>mi <span class="ugaritic-text-line-number">5</span>&#160;rabbati</td>
        <td>da wirst du nach ˀUd(u)mu, zur großen (Stadt), kommen,</td></tr>
    <tr>
        <td>wa li ˀud<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span>mi ṮRR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti</td>
        <td>ja, nach ˀUd(u)mu, zur starken (Stadt).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>- // P-A<sub>2</sub> // A<sub>2</sub> &#8594; a- // b-c // ◌-c'; vgl. auch KTU 1.14 iv 32b–36a; KTU 1.4 iv 27–28 (A- // P-O<sub>4</sub> ; P).</p></td></tr>
<tr><th colspan="2">KTU 1.4 iv 20–22</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">20</span>&#160;ˀid<span class="non-ugaritic-text">(</span>d<span class="non-ugaritic-text">)</span>āka lV tâtin<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> panîma</td>
        <td>Dann machte sie sich auf den Weg</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">21</span>&#160;ˁimma ˀili mabbakV nah<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>rêma</td>
        <td>zu ˀIlu, zur Quelle der beiden Flüsse,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;qarba ˀapīqi<span class="non-ugaritic-text"><sup>?</sup></span> tahāmatêma</td>
        <td>ins Flussbett der beiden Urfluten.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-P-O<sub>4</sub>- // A<sub>2</sub> // A<sub>2</sub> &#8594; a-b-c- // d-e // ◌-e'. Zum Enjambement-Vers, dessen zweites Kolon eine durch <i>ˁM</i> eingeleitete Präpositionalphrase enthält, vgl. auch KTU 1.15 i 3–4 (P-S- // A) und KTU 1.24 16–17a (P-S- // A).</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 27–28a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">27</span>&#160;šaˀū ˀilūma raˀašātikumū</td>
        <td>Erhebt, Götter, eure Häupter</td></tr>
    <tr>
        <td>li ẓûri bir<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>kātikumū</td>
        <td>von euren Knien,</td></tr>
    <tr>
        <td>lina<span class="non-ugaritic-text"><sup>?</sup></span> kaḥ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ṯī<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">28</span>&#160;ZBL-ikumū</td>
        <td>von euren fürstlichen Thronen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: P-<i>Anr.</i>-O<sub>4</sub>- // A // A &#8594; a-b-c- // d // d'. Zum Enjambement-Vers, dessen zweites Kolon eine durch <i>L</i> eingeleitete Präpositionalphrase enthält, vgl. auch KTU 1.2 iii 16b–d (P-S- // A // A; a-b- // c // c') und KTU 1.4 v 46b–48a (P-S ; P- // A).</p></td></tr>
<tr><th colspan="2">KTU 1.4 vii 19b–20</th></tr>
    <tr>
        <td>wa <span class="non-ugaritic-text">[</span>pa<span class="non-ugaritic-text">]</span>taḥ BDQ-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ˁarapāti</td>
        <td>Ja, [ö]ffne einen Spalt in den Wolken</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">20</span>&#160;ˁalê<span class="non-ugaritic-text">/</span>â hawâti kôṯari-wa-ḫasīsi</td>
        <td>gemäß dem Wort des Kôṯaru-wa-Ḫasīsu!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: P-O<sub>4</sub>- // A.</p></td></tr>
<tr><th colspan="2">KTU 1.15 iii 13–15</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">13</span>&#160;maˀda râma <span class="non-ugaritic-text">[</span>kirtu<span class="non-ugaritic-text">]</span></td>
        <td>Hoch erhaben ist [Kirtu]</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">14</span>&#160;bi tôki rāpiˀī ˀar<span class="non-ugaritic-text">[</span>ṣi<span class="non-ugaritic-text">]</span></td>
        <td>inmitten der Rāpiˀūma der ‚Er[de‘],</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">15</span>&#160;bi puḫri QBṢ-i ditāni</td>
        <td>in der Zusammenkunft der Versammlung des Ditānu.</td></tr>
        <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-P-S- // A<sub>2</sub> // A<sub>2</sub> &#8594; a-b-c- // d // d'.</p></td></tr>
<tr><th colspan="2">KTU 1.14 i 28–30</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">28</span>&#160;tinnatikna ˀudmaˁātuhu</td>
        <td>Seine Tränen ergossen sich</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">29</span>&#160;kama ṯiqalīma ˀarṣah</td>
        <td>wie Schekel zur Erde,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">30</span>&#160;kama ḫamušāti maṭṭâtah</td>
        <td>wie Fünftel (eines Schekels) aufs Bett.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: P-S- // A<sub>1</sub>-A<sub>2</sub> // A<sub>1</sub>-A<sub>2</sub> &#8594; a-b- // c-d // c'-d'.</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 25d–26</th></tr>
    <tr>
        <td>ˀaḥda<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">26</span>&#160;ˀilūma taˁniyū</td>
        <td>Einstimmig mögen die Götter antworten</td></tr>
    <tr>
        <td>lûḥāti malˀakê yammi</td>
        <td>den Tafeln der Boten des Yammu,</td></tr>
    <tr>
        <td>taˁûdati ṯāpiṭi nah<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">&lt;</span>ri<span class="non-ugaritic-text">&gt;</span></td>
        <td>der Gesandtschaft des Herrschers Nah(a)ru.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A-S-P- // O<sub>4</sub> // O<sub>4</sub> &#8594; a-b-c- // d<sub>x-y-z</sub> // d'<sub>◌-y'-z'.</sub></p></td></tr>
<tr><th colspan="2">KTU 1.6 i 56–57</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">56</span>&#160;ˀappūnaka ˁaṯtaru ˁarīẓu<span class="non-ugaritic-text"><sup>?</sup></span></td>
        <td>Sodann (ist) ˁAṯtaru, der Starke,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">57</span>&#160;yaˁlû<span class="non-ugaritic-text">/</span>î bi ṢRR-āti ṣapāni</td>
        <td>hinaufgestiegen auf die Höhen<sup>?</sup> des Zaphon.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-S- // P-A<sub>2</sub>. Zum Enjambement-Vers, dessen erstes Kolon die Phrase <i>APNK</i> – <i>Subjekt<sub>Nomen proprium + Epitheton</sub></i> enthält, vgl. auch KTU 1.5 vi 11–14a (A<sub>1</sub>-S- // P-A<sub>2</sub> ; P-A<sub>2</sub> // A<sub>2</sub> ; P-A<sub>2</sub>; a-b- // c-d ; e-f // ◌-d' ; e'-f') und 1.15 ii 8–9a (A<sub>1</sub>-S- // O<sub>4</sub>-A<sub>2</sub>-P).</p></td></tr>
</table>
</details>
<p>Im Vers-Enjambement sind die Glieder eines zusammenhängenden Satzes über zwei oder mehrere Verse verteilt, die zur selben Strophe gehören (die Strophe wird als Enjambement-Strophe bezeichnet; s. Beispiele B). Die durch Enjambement verbundenen Verse sind meist &#x2197; Bikola. Der Satzteil, der im ersten Kolon jedes Verses steht, wird im zweiten Kolon des Verses parallel aufgegriffen.</p>
<details class="exampleGUPF">
<summary>Beispiele B: Das Vers-Enjambement</summary>
<table class="exampleGUPF-Table strophe">
<tr><th colspan="2">KTU 1.14 iv 40–43</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">40</span>&#160;himma ḥurriya bêtaya <span class="ugaritic-text-line-number">41</span>&#160;ˀiqqaḥu <span class="non-ugaritic-text">//</span> ˀašaˁribu ġalmata <span class="ugaritic-text-line-number">42</span>&#160;ḥaẓiraya</td>
        <td>Wenn ich Ḥurriya in mein Haus nehmen kann, // das Mädchen in meine Wohnstatt führen kann,</td></tr>
    <tr>
        <td>ṯinêha kaspima<span class="non-ugaritic-text"><sup>!</sup></span> <span class="ugaritic-text-line-number">43</span>&#160;ˀâtina <span class="non-ugaritic-text">//</span> wa ṯalāṯataha ḫurāṣima</td>
        <td>(dann) will ich ihr Doppeltes an Silber darbringen, // ja, ihr Dreifaches an Gold!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A- // (…) ǀ O<sub>4</sub>-P // (…) &#8594; a<sub><i>HM</i>-x-y-z</sub>- // a'<sub>◌-z'-x'-y'</sub>- ǀ b-c // b'-◌.</p></td></tr>
<tr><th colspan="2">KTU 1.19 iii 42b–45a</th></tr>
    <tr>
        <td>kanapê našarīma <span class="ugaritic-text-line-number">43</span>&#160;baˁlu yaṯbi<span class="non-ugaritic-text">/</span>ur <span class="non-ugaritic-text">//</span> baˁlu yaṯbi<span class="non-ugaritic-text">/</span>ur DˀIY-ê <span class="ugaritic-text-line-number">44</span>&#160;humūti</td>
        <td>Baˁlu möge die Flügel der Adler zerbrechen, // Baˁlu möge die Schwingen von jenen zerbrechen,</td></tr>
    <tr>
        <td>himma taˁûpūna ˁalê<span class="non-ugaritic-text">/</span>â qubūri biniya <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">45</span>&#160;tašaḫîṭūnaninnu bi šinatihu</td>
        <td>wenn sie über das Grab meines Sohnes fliegen, // (und) ihn wecken aus seinem Schlaf!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: O<sub>4</sub>-S-P // (…) ǀ -A // (…) &#8594; a-b-c // b-c-a' ǀ -d // e.</p></td></tr>
<tr><th colspan="2">KTU 1.17 v 4b–7a (Par. KTU 1.19 i 19b–23a)</th></tr>
    <tr>
        <td>ˀappūnaka danīˀilu mutu <span class="ugaritic-text-line-number">5</span>&#160;rāpiˀi <span class="non-ugaritic-text">//</span> ˀa<span class="non-ugaritic-text">&lt;</span>ppV<span class="non-ugaritic-text">&gt;</span>hinnā ġāziru mutu harnamī<span class="non-ugaritic-text">[</span>yi<span class="non-ugaritic-text">]</span><span class="non-ugaritic-text"><sup>?</sup></span></td>
        <td>Sodann (ist) Danīˀilu, der Mann des Rāpiˀu, // sodann (ist) der Held, der Mann des Harnamiten,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">6</span>&#160;yittaša<span class="non-ugaritic-text">/</span>iˀu yâṯib<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> bi ˀappi<span class="non-ugaritic-text">/</span>ê ṯaġri <span class="non-ugaritic-text">//</span> taḥta <span class="ugaritic-text-line-number">7</span>&#160;ˀadurīma dā bi gurni</td>
        <td>aufgestanden (und) hat sich an der Vorderseite des Tores gesetzt, // unter den Noblen, die bei der Tenne (waren).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: A<sub>1</sub>-S- // (…) ǀ P ; P-A<sub>2</sub> // (…) &#8594; a-b- // a'-b'- ǀ c ; d-e // ◌ ; ◌-e'; vgl. auch KTU 1.17 i 0–3a: A<sub>1</sub>-S- // (…) ǀ A<sub>2</sub>-O<sub>4</sub>-P // (…) &#8594; a-b- // a'-b'- ǀ c-d-e // c-e'-d'.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Epipher</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Enumeratio</summary>
<p>&#x2197; Accumulatio.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Erzählzeit und erzählte Zeit</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Euphemismus</summary>
<p>Der Euphemismus ist eine Form der &#x2197; Periphrase. Ein negativ konnotierter oder tabuisierter Begriff ist durch einen beschönigenden Ausdruck ersetzt. Oberflächlich betrachtet verschleiert der euphemistisch gebrauchte Ausdruck, worum es eigentlich geht (das Beschriebene wird nicht benannt; es wird lediglich darauf angespielt). Steht die wörtliche Bedeutung des euphemistisch gebrauchten Ausdrucks in Kontrast zum Umschriebenen, wirkt die beschönigende Umschreibung ironisierend (&#x2197; Ironie / Witz / Zweideutigkeit). Der Euphemismus benennt in dem Fall genau das, was das Beschriebene <i>nicht</i> ist (vgl. <bibl>Watson, 1986a: 308–309<VuepressApiPlayground url="/api/eupt/biblio/AAJ26GVN" method="get" :data="[]"/></bibl>, und <bibl>idem, 1986b: 419<VuepressApiPlayground url="/api/eupt/biblio/5JSFP5T6" method="get" :data="[]"/></bibl>).</p>

<details class="exampleGUPF">
<summary>Beispiele</summary>

<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 viii 7–9 (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">7</span>&#160;wa ridā bêta ḫupṯati<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">8</span>&#160;ˀarṣi</td>
        <td>Und steigt hinab in das ‚Haus der Freiheit der Erde‘!</td></tr>
    <tr>
        <td>tissapirā bi yā<span class="ugaritic-text-line-number">9</span>ridī<span class="non-ugaritic-text">(-)</span>ma ˀarṣi<span class="non-ugaritic-text">/</span>a</td>
        <td>Möget ihr gezählt werden zu denen, die in die ‚Erde‘ (i. e. die Unterwelt) hinabsteigen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>BT ḪPṮT</i> „Haus der Freiheit“ in viii 7–8a ist wahrscheinlich eine euphemistische Umschreibung der Unterwelt (s. <i>ARṢ</i> „Erde; Unterwelt“ in viii 8b–9), also für jenes „Haus“, das seinen „Bewohnern“ kaum Freiheiten bietet (wer einmal eingetreten ist, wird gewöhnlich nie wieder freigelassen). Die Unterwelt ist im Grunde also das glatte Gegenteil eines „Hauses der Freiheit“ (vgl. <bibl>Watson, 1986b: 419<VuepressApiPlayground url="/api/eupt/biblio/5JSFP5T6" method="get" :data="[]"/></bibl>). Vgl. auch KTU 1.5 vi 5b–7 (und Par.), wo das Steppenland am Eingang zur Unterwelt als <i>NˁMY</i> „lieblicher“ und <i>YSMT</i> „schöner Ort“ bezeichnet wird (vgl. <bibl>Watson, 1986b: 419<VuepressApiPlayground url="/api/eupt/biblio/5JSFP5T6" method="get" :data="[]"/></bibl>, und <bibl>Müller / Steinberger, 2022: 114 [Anm. 304 mit Lit.]<VuepressApiPlayground url="/api/eupt/biblio/BZUP9PRB" method="get" :data="[]"/></bibl>).</p></td></tr>
</table>
</details>
<p/>
</details>

<h2>F</h2>

<details class="undoneEntryGUPF">
<summary>Figura etymologica / Paronomasie</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Fremdwort / Lehnwort</summary>
<p>In den ugaritischen Texten finden sich zahlreiche Fremd- und Lehnwörter, die aus dem Akkadischen oder einer anderen, nicht-semitischen Sprache stammen (<bibl>Watson, 2007: 63–151<VuepressApiPlayground url="/api/eupt/biblio/AE8M9Z9R" method="get" :data="[]"/></bibl>). Manche dieser Lexeme sind ausschließlich im poetischen Korpus belegt (s. Beispiele). Dort treten Fremd- und Lehnwörter immer wieder semantisch / grammatisch parallel (&#8599; Parallelismus) zu ugaritischen Lexemen auf. Das Fremdwort und das ugaritische Lexem bezeichnen in dem Fall dieselbe Sache oder zwei ähnliche oder in der Vorstellungswelt des Dichters und des Publikums miteinander verwandte Sachen.</p>
<p>Vorausgesetzt, dass Dichter und Publikum ein Lexem fremdsprachigen Ursprungs als Fremdwort erkannten (es also <i>nicht</i> als mutmaßlich eigensprachliches, ugaritisches Lexem galt), vermochte das Fremdwort möglicherweise a) die Wortgewandtheit und die hohe Bildung des Dichters herauszustellen (vgl. <bibl>Korpel, 1998: 98–99 / 101<VuepressApiPlayground url="/api/eupt/biblio/8DCQ7D9N" method="get" :data="[]"/></bibl>), b) die poetische Sprache von der Alltagssprache abzuheben, und / oder c) Ugarit als kosmopolitischen Knotenpunkt einer stark vernetzten spätbronzezeitlichen Welt zu inszenieren, an dem unterschiedliche Sprachen und Literaturen aufeinandertrafen (vgl. <bibl>Watson, 2007: 151<VuepressApiPlayground url="/api/eupt/biblio/AE8M9Z9R" method="get" :data="[]"/></bibl>). Fremdwörter kamen aber vermutlich auch aus anderen, praktischen Gründen zum Einsatz: Grundsätzlich ermöglichen Fremdwörter, spezifische real- oder vorstellungsweltliche Konzepte zu bezeichnen, für die keine eigensprachlichen Bezeichnungen etabliert sind. Gleichzeitig erweitern Fremdwörter das lexikalische Repertoire des Dichters, aus dem dieser schöpft, wenn er im parallel gestalteten Versgefüge einem bestimmten (eigensprachlichen) Lexem einen sinnverwandten Begriff gegenüberzustellen sucht.</p>

<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 iv 1–2a (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">1</span>&#160;yaṣaqa bi gulli ḥattuṯi yêna</td>
        <td>Er goss Wein in eine Silberschale,</td></tr>
    <tr>    
        <td><span class="ugaritic-text-line-number">2</span>&#160;bi gulli ḫurāṣi nubta</td>
        <td>Honig in eine Goldschale.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ḤTṮ</i>, vermutlich aus dem Hethitischen (oder Hattischen) entlehnt (DUL<sup>3</sup> 372 s.v. <i>ḥtṯ</i>; <bibl>Watson, 2007: 120<VuepressApiPlayground url="/api/eupt/biblio/AE8M9Z9R" method="get" :data="[]"/></bibl>).</p></td></tr>
<tr><th colspan="2">KTU 1.14 iv 49–50 (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">49</span>&#160;garrâninna ˁîrama</td>
        <td>Er stürmte an gegen sie, die Stadt,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">50</span>&#160;šarâninna PDR-ama</td>
        <td>rückte vor gegen sie, die Stadt.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>PDR</i> „Stadt“, vermutlich nicht-semitischen Ursprungs (vgl. DUL<sup>3</sup> 652 s.v. <i>pdr</i> I).</p></td></tr>
</table>
</details>
<p/>
</details>

<h2>G</h2>

<details class="undoneEntryGUPF">
<summary>Genus-komplementärer Parallelismus (<i>gender-matched parallelism</i>)</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Geminatio</summary>
<p>Als Geminatio wird die Wortdoppelung bezeichnet. Ein Wort wird innerhalb des Kolons wiederholt (&#x2197; Repetitio); die beiden Begriffe folgen unmittelbar aufeinander. Die Geminatio ist in der ugaritischen Poesie selten bezeugt. Sie diente wohl der besonderen Hervorhebung des gedoppelten Lexems. In KTU 1.3 iv 32b ist das erste Wort des Kolons verdoppelt, in KTU 1.14 iii 26–27a (und Par.) das letzte (die Geminatio am Kolonende wird als Conversio bezeichnet).</p>
<details class="exampleGUPF">
<summary>Beispiel</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.3 iv 32b</th></tr>
    <tr>
        <td><b>likā likā</b> ˁNN ˀilīma</td>
        <td>Geht, geht, Diener der Götter!</td></tr>
<tr><th colspan="2">KTU 1.14 iii 26–27a (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">26</span>&#160;qaḥ kirtu <b>ŠLM-īma</b> <span class="ugaritic-text-line-number">27</span>&#160;<b>ŠLM-īma</b></td>
        <td>Nimm, Kirtu, Friedensgeschenke, (ja,) Friedensgeschenke!</td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Grammatische Varianz</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Graphische Wiederholungsmuster</summary>
<p>&#x2197; Visuelle Poesie.</p>
</details>

<h2>H</h2>

<details class="undoneEntryGUPF">
<summary>Hendiadyoin</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Hexastichon</summary>
<p>Das Hexastichon ist eine &#x2197; Strophe, die sich aus sechs &#x2197; Versen zusammensetzt.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Homöarkton</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Homöoprophoron</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Homöoptoton</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Homöoteleuton</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Hyperbel</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Hysteron proteron</summary>
</details>  

<h2>I</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Inclusio</summary>
<p>In der Inclusio (oder Rahmung, Kyklos, Epanadiplosis) korrespondiert das erste Element der Verseinheit, die sich aus mindestens zwei kleineren Einheiten zusammensetzt, mit dem letzten Element derselben Verseinheit. Das erste und das letzte Element rahmen die ganze Verseinheit (Struktur: a … // … a<sup>(</sup>'<sup>)</sup> oder a … // … // … a<sup>(</sup>'<sup>)</sup> o. Ä.). In der Inclusio im engeren Sinn entsprechen sich die rahmenden Elemente wörtlich (sie sind identisch oder unterschiedliche Ableitungen desselben Lexems [&#x2197; Polyptoton]; vgl. <bibl>Schweikle / Burdorf / Moennighoff, 2007a<VuepressApiPlayground url="/api/eupt/biblio/A72IDAF4" method="get" :data="[]"/></bibl>: „Übereinstimmung des Wortmaterials am Anfang und am Ende desselben Teilsatzes, Satzes, Absatzes oder Textes“). In der Inclusio im weiteren Sinn sind die rahmenden Elemente nicht identisch; die beiden Lexeme oder Phrasen stehen aber in semantischer Relation und korrespondieren grammatisch.</p>
<p>Das rahmende Element ist in den hier besprochenen Konstruktionen jeweils ein einzelnes Wort oder eine zusammengesetzte Nominalphrase und steht am Kolonanfang bzw. am Kolonende. Diese Form der Inclusio wird vom (verwandten) &#x2197; Ringgefüge unterschieden. Im Ringgefüge sind mehrere Verseinheiten miteinander verbunden, von denen die erste und die letzte Verseinheit miteinander korrespondieren und die dazwischenliegende(n) Verseinheit(en) rahmen. Die letzte Einheit greift die erste auf oder nimmt auf einzelne Elemente der ersten Einheit Bezug; jedoch ist das Pendant des ersten Elements der ersten Verseinheit nicht ans Ende der letzten Verseinheit gestellt (sofern das erste Element der ersten Verseinheit überhaupt ein semantisch / grammatisch korrespondierendes Pendant in der letzten Verseinheit hat).</p>
<p>Die Inclusio spannt den Bogen vom Anfang des Versgefüges zum Ende desselben und macht so deutlich, dass die Phrasen / Verseinheiten, die zwischen den beiden rahmenden Elementen liegen, als inhaltliche und versstrukturelle Einheit zu betrachten sind (am Ende des Versgefüges werden Rezipient*innen gewissermaßen noch einmal an den Anfang erinnert). Vor allem wenn sich das erste und das letzte Element wörtlich entsprechen, wird das rahmende Element in der Inclusio hervorgehoben.</p>
<p>In den ugaritischen Belegen der Inclusio korrespondiert i. d. R. nicht nur das erste Element der ersten Verseinheit mit dem letzten Element der letzten Verseinheit; mindestens ein weiteres Element der ersten Verseinheit hat ein semantisch / grammatisch korrespondierendes Pendant in der letzten Verseinheit. Die Inclusio tritt in der ugaritischen Dichtung folglich primär in chiastischen Verskonstruktionen auf; Struktur: a-b(-c) // (c-)b-a oder a-b-c // b-c-a o. Ä. (s. &#x2197; Chiasmus; s. &#x2197; rahmender partieller Chiasmus).</p>
<p>Die Inclusio kann ein Kolon rahmen, das sich aus zwei gleichrangigen Phrasen zusammensetzt (KTU 1.3 iv 55; 1.24 38b–39). Häufiger fasst die Inclusio einen Vers ein, der sich in zwei oder mehrere Kola gliedert (bislang nicht bekannt sind mir Beispiele für die Inclusio in der Strophe, die sich aus mindestens zwei Versen zusammensetzt, und in der das letzte Element des letzten Verses das erste Element des ersten Verses aufgreift).</p>

<details class="exampleGUPF">
<summary>Beispiele</summary>

<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.3 iv 55 (Kolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">55</span>&#160;yaṯub liya <span class="non-ugaritic-text">/</span> wa lahu <span class="non-ugaritic-text">[</span>ˀaṯûba<span class="non-ugaritic-text">]</span></td>
        <td>Er möge mir antworten, / dann [will ich] ihm [antworten].</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Inclusio <i>yaṯub</i> … / … <i>ˀaṯûba</i> (&#x2197; Polyptoton); Struktur des Kolons (&#x2197; Chiasmus): a-b / (<i>wa</i>-)b'-a'.</p></td></tr>
<tr><th colspan="2">KTU 1.24 38b–39 (Kolon)</th></tr>
    <tr>
        <td>ˀârī yar<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>ḫa <span class="non-ugaritic-text">/</span> wa ya<span class="ugaritic-text-line-number">39</span>r<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>ḫu yiˀarki</td>
        <td>leuchte dem Yar(i)ḫu, / und (od. dann) Yar(i)ḫu möge dir leuchten!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Inclusio <i>ˀârī</i> … / … <i>yiˀar-ki</i> (&#x2197; Polyptoton); Struktur des Kolons (&#x2197; Chiasmus); Struktur: a-b / (<i>wa</i>-)b'-a'.</p></td></tr>
<tr><th colspan="2">KTU 1.3 v 30–31 (Vers / Trikolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">30</span>&#160;taḥmuka ˀilu ḥaka<span class="non-ugaritic-text">/</span>ima</td>
        <td>Deine Botschaft, ˀIlu, ist weise,</td></tr>
    <tr>
        <td>ḥukmuka <span class="ugaritic-text-line-number">31</span>&#160;ˁimma ˁālami</td>
        <td>dein weises Urteil (gilt) bis in alle Ewigkeit,</td></tr>
    <tr>
        <td>ḤYT-u ḥuẓûti taḥmuka</td>
        <td>ein Leben (voll) Glück ist deine Botschaft!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Inclusio <i>taḥmu-ka</i> … // … // … <i>taḥmu-ka</i> (vgl. schon <bibl>Dahood, 1969: 25–26<VuepressApiPlayground url="/api/eupt/biblio/5P7XR6PK" method="get" :data="[]"/></bibl>).</p></td></tr>
<tr><th colspan="2">KTU 1.14 ii 32–34 (Vers / Trikolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">32</span>&#160;ˁadānu nugiba wa yâṣiˀ</td>
        <td>Die Armee sei mit Proviant versorgt, dann ziehe sie aus,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">33</span>&#160;ṣabaˀu ṣabaˀi nugiba</td>
        <td>das Heer des Heeres sei mit Proviant ausgerüstet,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">34</span>&#160;wa yâṣiˀ ˁadānu maˁˁu</td>
        <td>dann ziehe die gewaltige Armee aus!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Inclusio <i>ˁadānu</i> … // … // … <i>ˁadānu maˁˁu</i>; Versstruktur: <u>a</u><sub>(a-b)</sub> ; <u>b</u><sub>(<i>wa</i>-c)</sub> // <u>a</u><sub>(a'-b)</sub> // <u>b</u><sub>(<i>wa</i>-c-a'')</sub>.</p></td></tr>
<tr><th colspan="2">KTU 1.17 vi 35b–36a (Vers / Bikolon)</th></tr>
    <tr>
        <td>môt<span class="non-ugaritic-text"><sup>!</sup></span>a ˀuḫrīyati maha yiqqaḥu</td>
        <td>Den Tod am Ende – was kann ihn wegnehmen?</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">36</span>&#160;maha yiqqaḥu môta ˀaṯrīyati</td>
        <td>Was kann wegnehmen den Tod am Schluss?</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Inclusio <i>môta ˀuḫrīyati</i> … // … <i>môta ˀaṯrīyati</i>; Versstruktur (&#x2197; partieller Chiasmus): a<sub>(x>y)</sub>-{b-c} // {b-c}-a'<sub>(x>y')</sub>.</p></td></tr>
</table>
</details>
<p/>

</details>

<details class="undoneEntryGUPF">
<summary>Inversion</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Ironie / Witz / Zweideutigkeit</summary>
</details>

<h2>K</h2>

<details class="undoneEntryGUPF">
<summary>Klimax / Steigerung</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Kolon</summary>
<p>Das Kolon ist eine elementare Verseinheit (&#x2197; Versgliederung) der ugaritischen Dichtungkunst. Auf manchen Tafeln (bzw. in einzelnen Tafelabschnitten) entspricht das Kolon je einer Zeile. Dies weist darauf hin, dass die ugaritischen Schreiber das Kolon als konstitutive Verseinheit erachteten. Das Kolon enthält i. d. R. mindestens ein selbständiges Satzglied (Prädikat, Subjekt, Akkusativobjekt und / oder Adverbial; oft setzt sich das Kolon aus einem Prädikat und einem oder mehreren nominalen Satzgliedern zusammen). Es umfasst meist drei oder vier Worteinheiten (i. e. Lexemen samt Präpositionen / Partikeln). Am Ende des Kolons steht vermutlich eine &#x2197; Zäsur.</p>
<p>Das Kolon ist meist mit einem oder zwei angrenzenden Kola zu einem &#x2197; Vers verbunden (es ergibt sich ein &#x2197; Bikolon [Vers aus zwei Kola] oder ein &#x2197; Trikolon [Vers aus drei Kola]). Im Fall des Verses, der nur ein Kolon enthält (i. e. des &#x2197; Monokolons), entspricht das Kolon gleichzeitig einem Vers.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Konnexion</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Konsonanz</summary>
<p>&#x2197; Assonanz und Konsonanz.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Koppelung</summary>
</details>

<h2>L</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Lehnwort</summary>
<p>&#x2197; Fremdwort / Lehnwort.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Litanei</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Litotes</summary>
<p>&#x2197; Periphrase.</p>
</details>

<h2>M</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Merismus</summary>
<p>Im Merismus sind zwei Begriffe miteinander verknüpft, die zwei gegensätzliche Teile desselben Ganzen bezeichnen („Himmel“ ↔ „Erde“; „Sohn“ ↔ „Tochter“; die Teile, die die beiden Begriffe bezeichnen, sind die einzigen Teile des Ganzen oder zwei charakteristische Teile). Der Merismus dient dazu, das Ganze, zu dem die bezeichneten Teile gehören, zu veranschaulichen („Himmel“ ↔ „Erde“ ~ „Kosmos“; „Sohn“ ↔ „Tochter“ ~ „Kinder, Nachkommen“). Die Begriffe können aneinandergereiht und durch eine Konjunktion miteinander verbunden sein (in Form einer &#x2197; Accumulatio; s. Beispiele A) oder einander in zwei (parallelen) Verseinheiten gegenübergestellt sein (s. Beispiele B).</p>
<details class="exampleGUPF">
<summary>Beispiele A</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.16 III 2</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">2</span>&#160;ˁînā tûrā ˀarṣa wa šamîma</td>
        <td>Schaut, durchstreift Erde und Himmel!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ARṢ W ŠMM</i> „Erde und Himmel“ ~ „Kosmos“.</p></td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.15 iii 22–25a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;maka bi šabūˁi šanāti</td>
        <td>Dann, im siebten Jahr,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">23</span>&#160;banū kirti kama<span class="non-ugaritic-text">(-)</span>humū tuddarū</td>
        <td>waren (da) die Söhne Kirtus, wie sie (ihm) versprochen worden waren,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">24</span>&#160;ˀappV binātu ḥurriyi <span class="ugaritic-text-line-number">25</span>&#160;kama<span class="non-ugaritic-text">(-)</span>humū</td>
        <td>und ebenso die Töchter Ḥurriyas, wie sie (ihr versprochen worden waren).</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>BN KRT</i> „Söhne Kirtus“ // <i>BNT ḤRY</i> „Töchter Ḥurriyas“ ~ „Kinder von Kirtu und Ḥurriya“ (in Kolon 2–3).</p></td></tr>
<tr><th colspan="2">KTU 1.6 iii 6–7</th></tr>
    <tr>
        <td>šamûma šamna tamṭurūnna</td>
        <td>Die Himmel mögen Öl regnen,</td></tr>
    <tr>
        <td>naḫalūma talikū nubtama</td>
        <td>die Wadis mögen voll Honig fließen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ŠMM</i> „Himmel“ steht parallel zu <i>NḪLM</i> „Wadis“. Die beiden Begriffe bezeichnen die beiden entgegengesetzten Regionen (oben und unten), denen gewöhnlich Wasser entspringt. Die S-P-Phrasen <i>ŠMM TMṬRN</i> „die Himmel mögen regnen“ und <i>NḪLM TLK</i> „die Wadis mögen (voll XY) fließen“ beschreiben zwei Teilaspekte des übergeordneten Sachverhalts „die wasserführenden / -spendenden Regionen des Kosmos mögen (XY über die Erde) fließen lassen“. Das Akkusativobjekt zeigt jeweils an, was sich aus Himmel und Wadi ergießen solle: Nicht etwa Wasser, sondern <i>ŠMN</i> „Öl“ und <i>NBT-</i> „Honig“.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Metapher / Vergleich</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Metonymie</summary>
<p>In der Metonymie ist ein Wort durch ein anderes (metonymisch gebrauchtes) Wort ersetzt, das (im vorliegenden Kontext) die Bedeutung des ersetzten Begriffs übernimmt, obwohl die Grundbedeutung des metonymisch gebrauchten Lexems von der Bedeutung des ersetzten Begriffs abweicht. Die Grundbedeutung des metonymisch gebrauchten Lexems (bzw. deren Referent, also das, was das Lexem kontextunabhängig bezeichnet) und der ersetzte Begriff (bzw. dessen Referent) stehen aus der Sicht des Autors / der Rezipient:innen des Texts in real- oder vorstellungsweltlicher Beziehung zueinander. Meist stehen die Grundbedeutung des metonymisch gebrauchten Lexems und die im Kontext übernommene Wortbedeutung in <i>Teil-Ganzes</i>- oder <i>Ganzes-Teil</i>-Relation (bzw. einer verwandten Sinnrelation, z. B. <i>Enthaltenes-Behälter</i>; Beispiel: „das Haus des Königs war vollkommen zerstört“; „Haus des Königs“ meint hier nicht den königlichen Palast, sondern - metonymisch - die Königsfamilie / Dynastie; stehen die Grundbedeutung des metonymisch gebrauchten Lexems und die im Kontext übernommene Wortbedeutung in <i>Teil-Ganzes</i>-Beziehung, wird die Figur auch als Synekdoche bezeichnet; entweder steht der <i>Teil</i> für das <i>Ganze</i> [<i>pars pro toto</i>] oder das <i>Ganze</i> steht für den <i>Teil</i> [<i>totum pro parte</i>]).</p>
<p>In der ugaritischen Poesie steht das metonymisch gebrauchte Lexem zuweilen parallel zu dem Lexem, dessen Bedeutung es übernimmt. Die parallel gestellten Lexeme, von denen eines metonymisch gebraucht ist, beziehen sich in dem Fall auf dieselbe real- oder vorstellungsweltliche Sache. Wenngleich Rezipient:innen erkannt haben mögen, dass eines der beiden Lexeme des Wortpaars im übertragenen Sinn zu verstehen ist, ist zu vermuten, dass sie sich zumindest kurzzeitig auch die Grundbedeutung des metonymisch gebrauchten Lexems vor Augen führten. Der metonymische Ausdruck lenkte die Aufmerksamkeit auf eine Sache, die mit dem eigentlich Gemeinten (bezeichnet durch das parallel gestellte Lexem) auf die eine oder andere Sache verwandt ist, und hatte so vermutlich Einfluss darauf, welche Assoziationen das Publikum mit dem parallel gestellten Lexem verband. Der metonymische Ausdruck trug so wohl zur Anschaulichkeit des Aussagenpaars bei.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 i 6b–8a</th></tr>
    <tr>
        <td>ˀummatu <span class="ugaritic-text-line-number">7</span>&#160;<span class="non-ugaritic-text">[</span>kirti<span class="non-ugaritic-text">]</span> ˁaruwat</td>
        <td>Die Sippe [Kirtus] war vernichtet,</td></tr>
    <tr>
        <td>bêtu <span class="ugaritic-text-line-number">8</span>&#160;<span class="non-ugaritic-text">[</span>ma<span class="non-ugaritic-text">]</span>lki ˀîtab<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>da<span class="non-ugaritic-text"><sup>!</sup></span></td>
        <td>[des Kö]nigs Haus war völlig zerstört.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>BT</i> „Haus“, metonymisch für „Familie, Dynastie“ (i. e. die Angehörigen der königlichen Familie, die im Palast leben bzw. lebten) ~ <i>UMT</i> „Familie, Sippe, Dynastie“.</p></td></tr>
<tr><th colspan="2">KTU 1.14 ii 27b–29</th></tr>
    <tr>
        <td>ˁadaba <span class="ugaritic-text-line-number">28</span>&#160;ˀakla li qar<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>yati</td>
        <td>Er bereite Speise für die Stadt,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">29</span>&#160;ḥiṭṭata li bêti-ḫābūri</td>
        <td>Weizen für Bêtu-Ḫabūri!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ḤṬṬ</i> „Weizen“, metonymisch für „Speise, Brot“ (i. e. das, was aus Weizen zubereitet wird) ~ <i>AKL</i> „Speise, Brot“.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Monokolon</summary>
<p>Das Monokolon ist ein &#x2197; Vers, der ein einziges &#x2197; Kolon umfasst.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Monostichon</summary>
<p>Das Monostichon ist eine &#x2197; Strophe, die einen einzigen &#x2197; Vers enthält.</p>
</details>

<details class="undoneEntryGUPF">
<summary>Musik / Musikalische Gestaltung</summary>
</details>

<h2>O</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Oxymoron</summary>
<p>Als Oxymoron bezeichnet man die „Verbindung zweier sich logisch ausschließender Begriffe, sei es in einem Kompositum (‚traurigfroh‘, F. Hölerlin) oder bei einem attribuierten Substantiv, z. B. ‚stets wacher Schlaf‘, ‚liebender Hass‘, ‚kalte Glut‘“ (<bibl>Schweikle / Burdorf / Moennighoff, 2007b<VuepressApiPlayground url="/api/eupt/biblio/4ZA3R9XM" method="get" :data="[]"/></bibl>). In der ugaritischen Dichtung ist das Oxymoron meines Wissens bislang nicht bezeugt (vgl. auch <bibl>Watson, 1986b: 419<VuepressApiPlayground url="/api/eupt/biblio/5JSFP5T6" method="get" :data="[]"/></bibl>).</p>
</details>

<h2>P</h2>

<details class="undoneEntryGUPF">
<summary>Parallelismus</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Parenthese</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Paronomasie</summary>
<p>&#x2197; Figura etymologica / Paronomasie.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary"><i>pars pro toto</i></summary>
<p>&#x2197; Metonymie.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Partieller Chiasmus</summary>
<p>&#x2197; Chiasmus.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Pentakolon</summary>
<p>Das Pentakolon ist ein &#x2197; Vers, der sich aus fünf &#x2197; Kola zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Pentastichon</summary>
<p>Das Pentastichon ist eine &#x2197; Strophe, die sich aus fünf &#x2197; Versen zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Periphrase</summary>
<p>Die Periphrase (Umschreibung) ist ein Begriff oder eine mehrgliedrige Phrase, der / die ein anderes Lexem umschreibt (Beispiel: „Blut der Bäume“ für „Wein“). Die Periphrase stellt ein Charakteristikum des Umschriebenen in den Fokus und illustriert so das Konzept, das dem umschriebenen Begriff zugrunde liegt (das Lexem X und die Periphrase Y können gewöhnlich in der Aussage „X ist / bedeutet Y“ verbaut werden [Beispiel: „<i>Brüder</i> sind <i>Söhne einer Mutter</i>“]; so gesehen definiert die Periphrase den umschriebenen Begriff; die Periphrase enthält i. d. R. mindestens ein Lexem, dessen Grundbedeutung sich nicht mit der Bedeutung des umschriebenen Begriffs deckt).</p>
<p>In der ugaritischen Poesie setzt sich die Periphrase oft aus einem Substantiv und einem untergeordneten Attribut zusammen; sie umscheibt ein einfaches Substantiv (vereinzelt tritt die Periphrase auch in Form einer zweigliedrigen Verbalphrase auf, die ein Substantiv [KTU 1.17 ii 5b–6a] oder ein Verb umschreibt [KTU 1.16 vi 41b–42]; s. Beispiele A). Eine andere Form der Periphrase gründet auf der Negation eines Begriffs, der das Gegenteil des umschriebenen Lexems bezeichnet (i. e. eines Atonyms oder eines komplementären Begriffs des Umschriebenen). Wird das Gegenteil des umschriebenen Lexems verneint, ergibt sich eine partiell-synonyme Periphrase des Begriffs (dies entspricht einer doppelten Verneinung [Beispiel: „nicht am Leben lassen“ für „töten“; „nicht am Leben lassen“ ~ „nicht nicht töten“]; solche und ähnliche Konstruktionen werden in der Stilistik als Litotes bezeichnet [scheinbare Abschwächung der Aussage, i. d. R. durch einen verneinten / doppelt-verneinten Ausdruck, wodurch die eigentliche Aussage verstärkt wird: „nicht unwahrscheinlich“ ~ „ziemlich wahrscheinlich“]; die Litotes wird in der Stilistik zuweilen von der Periphrase getrennt behandelt; <bibl>Braak / Neubauer, 2001: 52–55<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>). In der ugaritischen Dichtung diente diese Form der Periphrase meist der Umschreibung eines Verbalausdrucks (vereinzelt auch eines Substantivs [KTU 1.17 vi 26b–28a]; s. Beispiele B). Zu den Sonderformen der Periphrase zählt der &#x2197; Euphemismus. Zuweilen werden auch die &#x2197; Antonomasie und die &#x2197; Metonymie als Formen der Periphrase betrachtet (<bibl>Braak / Neubauer, 2001: 47 / 54<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>; <bibl>Schweikle, 2007<VuepressApiPlayground url="/api/eupt/biblio/DB62UX55" method="get" :data="[]"/></bibl>).</p>
<p>Die ugaritischen Dichter gebrauchten die Periphrase u. a. in Versgefügen, in denen einander zwei referenzidentische Ausdrücke gegenübergestellt werden sollten (Ausdrücke sind referenzidentisch, wenn sie dieselbe real- oder vorstellungsweltliche Sache bezeichnen): Die Periphrase ermöglichte ihnen, einem Lexem aus der einen Verseinheit ein mehr oder weniger bedeutungsgleiches Pendant in der anderen Verseinheit gegenüberzustellen, ohne das konkrete Lexem (für das es möglicherweise kein Synonym gab) zu wiederholen. Gleichzeitig erklärt die Periphrase den parallel gestellten Begriff und trägt somit zur Anschaulichkeit und Verständlichkeit der Aussage bei. Die Periphrase, in der das Gegenteil des umschriebenen Begriffs verneint wird, wirkt mitunter verstärkend („niederschlagen“ // „nicht am Leben lassen“ ~ „töten, erschlagen“).</p>
<p>Die Periphrase ist meist länger als der umschriebene Begriff; nicht selten setzt sich die Periphrase aus zwei Substantiven oder einem Substantiv und einem Verb zusammen. Im Vers, der sich aus mindestens zwei Kola zusammensetzt, kann die Periphrase die Ellipse eines anderen Satzteils längenmäßig kompensieren, sodass sich trotz der Auslassung eines Glieds gleich oder ähnlich lange Kola ergeben (KTU 1.14 i 8b–9: im zweiten Kolon sind die Partikel <i>D</i> und der Präpositionalausdruck <i>LH</i> ausgelassen; da aber das Substantiv <i>AḪM</i> durch die längere Periphrase <i>BN UM</i> ersetzt ist [und außerdem das längere Zahlwort <i>ṮMNT</i> an die Stelle des kürzeren <i>ŠBˁ</i> tritt], sind die beiden Kola vermutlich exakt gleich lang).</p>
<details class="exampleGUPF">
<summary>Beispiele A</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 i 8b–9</th></tr>
    <tr>
        <td>dā šabˁu <span class="ugaritic-text-line-number">9</span>&#160;ˀaḫḫūma lahu</td>
        <td>(des Königs,) der (einst) sieben Brüder hatte,</td></tr>
    <tr>
        <td>ṯamānîtu <b>banū ˀummi</b></td>
        <td>acht Söhne einer Mutter</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>AḪM</i> „Brüder“ ~ <i>BN UM</i> „Söhne einer Mutter“.</p></td></tr>
<tr><th colspan="2">KTU 1.17 i 11c–13a</th></tr>
    <tr>
        <td>ˀuzūra<span class="non-ugaritic-text">/</span>u <span class="ugaritic-text-line-number">12</span>&#160;<span class="non-ugaritic-text">[</span>ˀilī<span class="non-ugaritic-text">]</span>ma danīˀilu</td>
        <td>Gegürtet (hat) [den Göt]tern Danīˀilu,</td></tr>
    <tr>
        <td>ˀuzūra<span class="non-ugaritic-text">/</span>u ˀilīma yulaḥḥimu</td>
        <td>gegürtet hat er den Göttern zu Essen gegeben,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">13</span>&#160;<span class="non-ugaritic-text">[</span>ˀuzū<span class="non-ugaritic-text">]</span>ra<span class="non-ugaritic-text">/</span>u yušaqqiyu <b>banī qudši</b></td>
        <td>[gegürt]et hat er zu Trinken gegeben den Söhnen des Heiligen.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ILM</i> „Götter“ ~ <i>BN QDŠ</i> „Söhne des Heiligen (scil. des ˀIlu)“.</p></td></tr>
<tr><th colspan="2">KTU 1.4 iii 43b–44</th></tr>
    <tr>
        <td><span class="non-ugaritic-text">[</span>tištayū<span class="non-ugaritic-text">]</span> karpānīma yêna</td>
        <td>[Sie tranken] aus Bechern Wein,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">44</span>&#160;<span class="non-ugaritic-text">[</span>bi kāsī ḫurāṣi <b>da</b><span class="non-ugaritic-text">]</span><b>ma ˁiṣṣīma</b></td>
        <td>[aus Goldbechern das „Bl]ut der Bäume“.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>YN</i> „Wein“ ~ <i>DM ˁṢM</i> „Blut der Bäume“.</p></td></tr>
<tr><th colspan="2">KTU 1.17 ii 5b–6a (und Par.)</th></tr>
    <tr>
        <td>ˀāḫidu yadaka bi ša<span class="non-ugaritic-text">[</span>karāni<span class="non-ugaritic-text">]</span></td>
        <td>der deine Hand packt bei Tru[nkenheit],</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">6</span>&#160;muˁammisuka <b>kī šaba<span class="non-ugaritic-text">/</span>iˁta yêna</b></td>
        <td>dich stützt, wenn du gesättigt bist mit Wein</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>B ŠKRN</i> „im Fall von (deiner) Trunkenheit“ ~ <i>K ŠBˁt YN</i> „wenn du gesättigt bis mit Wein“.</p></td></tr>
<tr><th colspan="2">KTU 1.16 vi 41b–42</th></tr>
    <tr>
        <td>šamaˁ maˁ<span class="non-ugaritic-text">(</span>ˁa<span class="non-ugaritic-text">)</span> lV Kirtu <span class="ugaritic-text-line-number">42</span>&#160;Ṯˁ-u</td>
        <td>Hör’ doch, o edler Kirtu,</td></tr>
    <tr>
        <td>ˀištamVˁ wa <b>taqġû<span class="non-ugaritic-text">/</span>î ˀudna</b></td>
        <td>horch her, ja, schenk (mir) Gehör!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ŠMˁ Mˁ</i> „Hör’ doch!“ // <i>IŠTMˁ</i> „Hör’ genau zu!“ ~ <i>TQĠ UDN</i> „Schenk Gehör!“</p></td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.19 i 14b–16a</th></tr>
    <tr>
        <td>KD ˁalê<span class="non-ugaritic-text">/</span>â qaštihu <span class="ugaritic-text-line-number">15</span>&#160;ˀimḫaṣhu</td>
        <td>Für seinen Bogen schlug ich ihn nämlich nieder,</td></tr>
    <tr>
        <td>ˁalê<span class="non-ugaritic-text">/</span>â qaṣaˁātihu huwati <span class="ugaritic-text-line-number">16</span>&#160;<b>lā ˀaḥawwî</b></td>
        <td>für seine Pfeile ließ ich ihn nicht am Leben.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ˀimḫaṣ-</i> „ich schlug nieder“ ~ <i>lā ˀaḥawwî</i> „ich ließ nicht am Leben“; vgl. auch KTU 1.18 iv 12b–13.</p></td></tr>
<tr><th colspan="2">KTU 1.2 i 19b</th></tr>
    <tr>
        <td>tabiˁā ġalmāmi <b>lā yaṯabā</b></td>
        <td>Die (beiden) Jünglinge machten sich auf, verweilten nicht.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>TBˁ</i> „sie machten sich auf“ ~ <i>L YṮB</i> „sie verweilten nicht“.</p></td></tr>
<tr><th colspan="2">KTU 1.17 vi 26b–28a</th></tr>
    <tr>
        <td>ˀiriš ḥayyīma lV ˀAqhatu ġāziru</td>
        <td>Wünsch’ (dir) Leben, o ˀAqhatu, Held,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">27</span>&#160;ˀiriš ḥayyīma wa ˀâtinaka</td>
        <td>wünsch’ (dir) Leben und ich will es dir geben,</td></tr>
    <tr>
        <td><b>balî<span class="non-ugaritic-text">(-)</span>môta</b> <span class="ugaritic-text-line-number">28</span>&#160;wa ˀašalliḥaka</td>
        <td>Unsterblichkeit und ich will sie dir überreichen!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: <i>ḤYM</i> „Leben, ewiges Leben“ ~ <i>BL</i>(-)<i>MT</i> „Nicht-Tod, Unsterblichkeit“.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Personifikation</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Pleonasmus</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Polyptoton</summary>
<p>&#x2197; Repetitio.</p>
</details>

<details class="doneEntryGUPF">
<summary  class="doneEntryGUPFSummary">Polysyndeton</summary>
<p>Im Polysyndeton folgt auf jedes Glied (außer auf das letzte Glied) einer Aufzählung, die sich aus drei oder mehreren syntaktisch gleichrangigen Begriffen oder Phrasen zusammensetzt, eine Konjunktion (ug. <i>wa</i>; Gegenstück zum &#x2197; Asyndeton). Das Polysyndeton ist in der ugaritischen Poesie recht selten bezeugt. Im angeführten Beispiel (KTU 1.4 iii 17–21a) ist die Figur möglicherweise eingesetzt, um den Eindruck einer umfassenden Aufzählung zu erwecken (nach dem zweiten Glied der Aufzählung geht der aufnehmende Verstand davon aus, dass die Aufzählung abgeschlossen ist; anschließend muss er feststellen, dass die Aufzählung noch weitergeht). Die Aufzählung erstreckt sich dort über zwei Kola. Das Polysyndeton innerhalb eines Kolons (i. e. X <i>wa</i> Y <i>wa</i> Z) konnte im ugaritischen Korpus bislang nicht identifiziert werden.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 iii 17–21a</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17</span>&#160;dāma ṯinê dabḥêma šaniˀa baˁlu</td>
        <td>Fürwahr, zwei Feste hasst Baˁlu,</td></tr>
    <tr>
        <td class="further-colon-of-verse">ṯalāṯa <span class="ugaritic-text-line-number">18</span>&#160;rākibu ˁarapāti</td>
        <td class="further-colon-of-verse">drei der Wolkenfahrer:</td></tr>
    <tr>
        <td>dabḥa <span class="ugaritic-text-line-number">19</span>&#160;BṮ-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti <b>wa</b> dabḥa <span class="ugaritic-text-line-number">20</span>&#160;DN-<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ti</td>
        <td>das Fest der Schande und das Fest der Minderwertigkeit<sup>?</sup></td></tr>
    <tr>
        <td class="further-colon-of-verse"><b>wa</b> dabḥa tudāmim<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">&lt;</span>ti<span class="non-ugaritic-text">&gt;</span> <span class="ugaritic-text-line-number">21</span>&#160;ˀamahāti</td>
        <td class="further-colon-of-verse">und das Fest des Fehlverhaltens<sup>?</sup> der Dienerinnen.</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Sofern sich die drei Phrasen <i>dabḥa BṮ</i>-(<i>a</i>)<i>ti</i>, <i>dabḥa DN</i>-(<i>V</i>)<i>ti</i> und <i>dabḥa tudāmim</i>(<i>a</i>)&lt;<i>ti</i>&gt; <i>ˀamahāti</i> auf je ein Fest (bzw. eine bestimmte Art von Fest) beziehen, das Baˁlu hasst, liegt eine dreigliedrige Aufzählung vor. Sowohl vor dem zweiten Glied der Aufzählung als auch vor dem dritten (i. e. am Anfang des letzten Kolons) steht die Konjunktion <i>wa</i>.</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Praeteritio</summary>
</details>

<h2>R</h2>

<details class="undoneEntryGUPF">
<summary>Reim / Reimschemata</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Repetitio</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Rhetorische Frage</summary>
<p>Die rhetorische Frage ist eine Frage, auf die der Fragende keine Antwort erwartet, da sie aus seiner Sicht nur eine einzige Antwort zulässt (in KTU 1.4 iv 59a: „Bin ich ein Sklave?“ → Antwort: „Nein!“). Die rhetorische Frage ist damit weniger eine Frage als eine nachdrückliche Aussage (<bibl>Braak / Neubauer, 2001: 63<VuepressApiPlayground url="/api/eupt/biblio/HWFZJWVP" method="get" :data="[]"/></bibl>; <bibl>Moennighoff, 2007: 653<VuepressApiPlayground url="/api/eupt/biblio/EQAMRDJK" method="get" :data="[]"/></bibl>). In den beiden unten besprochenen Beispielen verdeutlicht die rhetorische Frage jeweils die ablehnende Haltung des Sprechers (in KTU 1.4 iv 59a: „Bin ich ein Sklave?“ ~ „Ich bin doch sicher kein Sklave!“). Dem Angesprochenen wird versagt, dem in der rhetorischen Frage durchschimmernden Standpunkt des Fragenden zu widersprechen. Die rhetorische Frage bietet dem Sprechenden damit ein Mittel, den Angesprochenen von seinem Standpunkt zu überzeugen. Die rhetorische Frage ist im ugaritischen Korpus recht selten belegt. Eine Frage lässt sich u. a. daran als rhetorische Frage erkennen, dass sie nicht beantwortet wird bzw. der Fragende den Angesprochenen gar nicht erst zu Wort kommen lässt, um die Frage zu beantworten.</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.4 iv 59–v 1</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">59</span>&#160;pa ˁabdu ˀanā ˁNN-u ˀaṯiratu</td>
        <td>Bin ich etwa ein Sklave, ist ˀAṯiratu (etwa) eine Dienerin (oder: ein Diener der ˀAṯiratu)?</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">60</span>&#160;pa ˁabdu ˀanāku ˀâḫudu ˀULṮ-a</td>
        <td class="further-colon-of-verse">Bin ich etwa ein Sklave, halte ich (etwa) (selbst) die Hacke / Ziegelform?</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">61</span>&#160;him<span class="non-ugaritic-text">(</span>ma<span class="non-ugaritic-text">)</span> ˀam<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu ˀaṯiratu tulabbinu <span class="ugaritic-text-line-number">62</span>&#160;labināti</td>
        <td class="further-colon-of-verse">Oder ist (etwa) ˀAṯiratu eine Dienerin, formt sie (etwa) (selbst) die Ziegel?</td></tr>
    <tr>
        <td>yabnû<span class="non-ugaritic-text">(/</span>î<span class="non-ugaritic-text">)</span> bêta li baˁli <span class="ugaritic-text-line-number">v 1</span>&#160;kama ˀilīma</td>
        <td>Er (selbst; scil. Baˁlu) soll ein Haus für Baˁlu bauen wie (für) die (anderen) Götter,</td></tr>
    <tr>
        <td class="further-colon-of-verse">wa ḥaẓira ka banī ˀaṯirati</td>
        <td class="further-colon-of-verse">ja, eine Wohnstatt wie (für) die Söhne der ˀAṯiratu!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Mittels der rhetorischen Fragen in iv 59–62a stellt der Sprecher, ˀIlu, Offensichtliches klar und räumt mit einer aus seiner Sicht absurden Vorstellung auf: ˀIlu erlaubt Baˁlu zwar, einen Palast für sich zu bauen (bzw. bauen zu lassen), doch er und seine Frau ˀAṯiratu sind gewiss keine Sklaven. ˀIlu und ˀAṯiratu werden also keinen Finger rühren beim Bau des neuen Palastes. Baˁlu solle sich selbst darum kümmern.</p></td></tr>
<tr><th colspan="2">KTU 1.14 vi 17'–25' (und Par.)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17'</span>&#160;limā ˀanāku kaspa <span class="ugaritic-text-line-number">18'</span>&#160;wa yarqa</td>
        <td>Warum (sollte) ich Silber und Gelbgold (nehmen),</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="non-ugaritic-text">[</span>ḫu<span class="non-ugaritic-text">]</span>rāṣa <span class="ugaritic-text-line-number">19'</span>&#160;yada maqâmihu</td>
        <td class="further-colon-of-verse">[G]old samt seinem Fundort,</td></tr>
    <tr>
        <td>wa ˁabda <span class="ugaritic-text-line-number">20'</span>&#160;ˁālami ṯalāṯa sus<span class="non-ugaritic-text">(</span>s<span class="non-ugaritic-text">)</span>uwīma</td>
        <td>und einen ewig gebundenen Knecht (und) drei Pferde,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">21'</span>&#160;markabta bi tarbaṣi <span class="ugaritic-text-line-number">22'</span>&#160;bina ˀam<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti</td>
        <td class="further-colon-of-verse">einen Streitwagen aus (deinem) Stall (und) den Sohn einer Magd?</td></tr>
    <tr>
        <td>pa dā ˀêna <span class="ugaritic-text-line-number">23'</span>&#160;bi bêtiya tâtin</td>
        <td>Vielmehr gib, was in meinem Haus fehlt,</td></tr>
    <tr>
        <td class="further-colon-of-verse">tin <span class="ugaritic-text-line-number">24'</span>&#160;liya MṮ-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ḥurriya</td>
        <td class="further-colon-of-verse">gib mir das Mädchen Ḥurriya,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">25'</span>&#160;naˁīm<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta šabḥa bukraka</td>
        <td>die Liebliche, deinen erstgeborenen Spross!</td></tr>
    <tr><td colspan="2" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: „Warum (sollte) ich Silber (...) (nehmen)?“ ~ „Es gibt für mich keinen Grund, Silber (...) zu nehmen!“ Mittels der rhetorischen Frage in vi 17'–22'a lehnt Kirtu die Reichtümer ab, die ihm Pabilu, der König der von Kirtus Heer belagerten Stadt ˀUd(u)mu, anbot (Pabilu versuchte Kirtu so zu bewegen, von ˀUd(u)mu abzuziehen). In den folgenden Versen stellt Kirtu klar, was er stattdessen will, nämlich Pabilus Tochter. Mittels der gleichen rhetorischen Frage lehnte Kirtu vorher schon die Reichtümer ab, die ihm der Gott ˀIlu angeboten hatte (ˀIlu hatte Kirtu auf diese Weise zu trösten versucht; Kirtu war verzweifelt, da er seine ganze Familie verloren hatte und keinen Nachkommen hatte); s. KTU 1.14 i 51b–ii 3. Der auf die rhetorische Frage folgende Vers ii 4–5 ist nur fragmentarisch erhalten; vermutlich war dieser ursprünglich wie vi 22'b–25' durch die Konjunktion <i>pa</i> eingeleitet. Vgl. dazu auch <bibl>Bacci / Harris / Heo / Pardee / Wolf, 2024: 22–23<VuepressApiPlayground url="/api/eupt/biblio/ER53K9TQ" method="get" :data="[]"/></bibl>, die die Konjunktion <i>pa</i> am Anfang des auf die rhetorische Frage folgenden Verses jedoch nicht adversativ verstehen [so bspw. <bibl>UG&#178; 789<VuepressApiPlayground url="/api/eupt/biblio/2WIR48JQ" method="get" :data="[]"/></bibl>] und in der rhetorischen Frage eine höfliche Form der Ablehnung sehen; <bibl>aaO: 22<VuepressApiPlayground url="/api/eupt/biblio/ER53K9TQ" method="get" :data="[]"/></bibl>: „The function of the particle [<i>pa</i> in vi 22'b–25' und Par.; Anm. C. Steinberger] was explicitly to link the request to the preceding rhetorical question (‘What need have I of … ?’) functioning as a rejection of the offer, hence the use of the coordinating conjunction was to explain his implicit rejection as owing to a greater need—the formulation of the rejection as a rhetorical question leads to the conclusion that <i>Kirta</i> was trying to express his rejection as politely as possible. If de Moor’s proposal [u. a. in <bibl>1980: 175<VuepressApiPlayground url="/api/eupt/biblio/ERQYN38Q" method="get" :data="[]"/></bibl>; Anm. C. Steinberger] to restore that conjunction in II 4 is correct, it is difficult to believe that <i>Kirta</i> was impolitely spurning <i>ˀIlu</i>’s offer—and indeed <i>ˀIlu</i> understands his rhetorical care and immediately launches into a long discourse telling <i>Kirta</i> how to go about getting a wife to produce the desired progeny. Thus it is not the <i>p</i> that is adversative, though it does introduce an alternative to the offered gifts, and it need not be translated as such; its function is to express <i>Kirta</i>’s need for something else as the logical basis for the rhetorical question.“</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Rhythmus</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Ringegefüge</summary>
</details>

<h2>S</h2>

<details class="undoneEntryGUPF">
<summary><i>Split couplet</i></summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Steigerung</summary>
<p>&#x2197; Klimax / Steigerung.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Strophe</summary>
<p>Die Strophe ist eine Verseinheit (&#x2197; Versgliederung), die einen oder mehrere &#x2197; Verse umfasst. Das Monostichon enthält einen einzigen Vers. Das Distichon setzt sich aus zwei Versen zusammen, das Tristichon aus drei, das Tetrastichon aus vier, das Pentastichon aus fünf. Die Strophengrenzen wurden im Vortrag vermutlich durch markante Zäsuren angezeigt (&#x2197; Zäsur).</p>
<details class="exampleGUPF">
<summary>Beispiele</summary>
<table class="exampleGUPF-Table strophe">

<tr><th colspan="2">KTU 1.6 iii 10–13 (Distichon aus zwei Bikola)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">10</span>&#160;bi ḥi<span class="non-ugaritic-text">/</span>ulmi laṭ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>pāni ˀili dā PID-i <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">11</span>&#160;bi ḎR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ti bāniyi bu<span class="non-ugaritic-text">/</span>inwati</td>
        <td>Im Traum des Scharfsinnigen, des ˀIlu, des Verständigen, // in der Vision des Schöpfers der Schöpfung,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span>&#160;šamûma šamna tamṭurūnna <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">13</span>&#160;naḫalūma talikū nubtama</td>
        <td>regneten die Himmel Öl, // flossen die Wadis voll Honig!</td></tr>

<tr><th colspan="2">KTU 1.14 ii 43–50a (Tristichon aus zwei Bikola und einem Trikolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">43</span>&#160;yaḥīdu bêtahu sagara <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">44</span>&#160;ˀalmānatu šakāru <span class="ugaritic-text-line-number">45</span>&#160;taškir</td>
        <td>Der einzig Verbliebene schließe sein Haus, // die Witwe stelle einen Tagelöhner ein.</td></tr>
    <tr>
        <td>ZBL-u ˁaršama <span class="ugaritic-text-line-number">46</span>&#160;yiššaˀu <span class="non-ugaritic-text">//</span> ˁawwiru mazālu <span class="ugaritic-text-line-number">47</span>&#160;yamzilu</td>
        <td>Der Kranke hebe (sein) Bett auf, // der Blinde taste sich hinterher.</td></tr>
    <tr>
        <td>wa yâṣiˀ tarīḫu <span class="ugaritic-text-line-number">48</span>&#160;ḥadaṯu <span class="non-ugaritic-text">//</span> yubaˁˁir li ṯanî <span class="ugaritic-text-line-number">49</span>&#160;ˀaṯṯatahu <span class="non-ugaritic-text">//</span> lima nakari <span class="ugaritic-text-line-number">50</span>&#160;môdādatahu</td>
        <td>Auch ziehe aus der frisch Vermählte, // schaffe seine Frau zu einem anderen, // seine Geliebte zu einem Fremden.</td></tr>
<tr><th colspan="2">KTU 1.6 vi 16b–22a (Tetrastichon aus vier Bikola)</th></tr>
    <tr>
        <td>yittâˁāni ka GMR-êma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">17</span>&#160;môtu ˁazza baˁlu ˁazza</td>
        <td>Sie rüttelten aneinander wie zwei GMR – // Môtu war stark, Baˁlu war stark.</td></tr>
    <tr>
        <td>yinnagiḥāni <span class="ugaritic-text-line-number">18</span>&#160;ka ruˀumêma<span class="non-ugaritic-text"><sup>?</sup></span> <span class="non-ugaritic-text">//</span> môtu ˁazza baˁlu <span class="ugaritic-text-line-number">19</span>&#160;ˁazza</td>
        <td>Sie stießen einander wie zwei Wildstiere – // Môtu war stark, Baˁlu war stark.</td></tr>
    <tr>
        <td>yinnaṯikāni ka baṯnêma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">20</span>&#160;môtu ˁazza baˁlu ˁazza</td>
        <td>Sie bissen einander wie zwei Schlangen – // Môtu war stark, Baˁlu war stark.</td></tr>
    <tr>
        <td>yimmaṣiḫāni <span class="ugaritic-text-line-number">21</span>&#160;ka LSM-êma <span class="non-ugaritic-text">//</span> môtu qâla <span class="ugaritic-text-line-number">22</span>&#160;baˁlu qâla</td>
        <td>Sie rissen einander zu Boden wie zwei LSM – // Môtu fiel, Baˁlu fiel.</td></tr>
<tr><th colspan="2">KTU 1.14 i 12–21a (Pentastichon aus vier Bikola und einem Monokolon)</th></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span>&#160;ˀaṯṯata ṣidqihu lā yapuq <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">13</span>&#160;matrVḫ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta yušrihu</td>
        <td>Die Frau, die ihm rechtmäßig zustand, hatte er nicht gewonnen, // die Gattin, die ihm angemessen war.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">14</span>&#160;ˀaṯṯata tariḫa wa tabiˁat<span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">15</span>&#160;ṮAR-a ˀummi takûn lahu</td>
        <td>Eine (andere) Frau hatte er geheiratet, doch die war fortgegangen, // die Verwandte<sup>?</sup> der Mutter, die ihm übrig geblieben war.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">16</span>&#160;muṯallaṯ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu kuṯruma tamut <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">17</span>&#160;murabbaˁ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu zVb<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>lānuma</td>
        <td>Die Dritte (Frau) war bei bester Gesundheit gestorben, // die Vierte in Krankheit.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">18</span>&#160;muḫammaš<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta yiˀtasap <span class="ugaritic-text-line-number">19</span>&#160;rašpu <span class="non-ugaritic-text">//</span> muṯaddaṯ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ġalamū <span class="ugaritic-text-line-number">20</span>&#160;yammi</td>
        <td>Die Fünfte hatte Rašpu an sich gerissen, // die Sechste die Gefolgsleute Yammus.</td></tr>
    <tr>
        <td>mušabbaˁ<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tuhun<span class="non-ugaritic-text">(</span>n<span class="non-ugaritic-text">)</span>ā bi šVlḥi <span class="ugaritic-text-line-number">21</span>&#160;tittapal</td>
        <td>Die Siebte von ihnen war durch die ŠLḤ-Waffe gefallen.</td></tr>
<tr><th colspan="2">KTU 1.6 i 18b–29 (Hexastichon aus sechs Bikola)</th></tr>
    <tr>
        <td>tiṭbaḫu šabˁīma <span class="ugaritic-text-line-number">19</span>&#160;ruˀumīma<span class="non-ugaritic-text"><sup>?</sup></span> <span class="non-ugaritic-text">//</span> ka GMN-i ˀalˀiyāni <span class="ugaritic-text-line-number">20</span>&#160;baˁli</td>
        <td>Sie schlachtete 70 Wildstiere, // als Begräbnisopfer für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td>tiṭbaḫu šabˁīma ˀalapīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">21</span>&#160;<span class="non-ugaritic-text">[</span>ka G<span class="non-ugaritic-text">]</span>MN-i ˀalˀiyāni baˁli</td>
        <td>Sie schlachtete 70 Rinder, // [als] Begräbnisopfer für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">22</span>&#160;<span class="non-ugaritic-text">[</span>tiṭ<span class="non-ugaritic-text">]</span>baḫu šabˁīma ṣaˀna <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">23</span>&#160;<span class="non-ugaritic-text">[</span>ka GM<span class="non-ugaritic-text">]</span>N-i ˀalˀiyāni baˁli</td>
        <td>[Sie sch]lachtete 70 Schafe, // [als Begräb]nisopfer für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">24</span>&#160;<span class="non-ugaritic-text">[</span>tiṭ<span class="non-ugaritic-text">]</span>baḫu šabˁīma ˀayyalīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">25</span>&#160;<span class="non-ugaritic-text">[</span>ka GMN-i<span class="non-ugaritic-text">]</span> ˀalˀiyāni baˁli</td>
        <td>[Sie sch]lachtete 70 Hirsche, // [als Begräbnisopfer] für den Mächtigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">26</span>&#160;<span class="non-ugaritic-text">[</span>tiṭbaḫu ša<span class="non-ugaritic-text">]</span>bˁīma yaˁilīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">27</span>&#160;<span class="non-ugaritic-text">[</span>ka GMN-i ˀal<span class="non-ugaritic-text">]</span>ˀiyāni baˁli</td>
        <td>[Sie schlachtete 7]0 Steinböcke, // [als Begräbnisopfer für den Mäch]tigen, Baˁlu.</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">28</span>&#160;<span class="non-ugaritic-text">[</span>tiṭbaḫu šabˁīma ya<span class="non-ugaritic-text">]</span>ḥmūrīma <span class="non-ugaritic-text">//</span> <span class="ugaritic-text-line-number">29</span>&#160;<span class="non-ugaritic-text">[</span>ka GM<span class="non-ugaritic-text">]</span>N-i ˀalˀiyāni baˁli</td>
        <td>[Sie schlachtete 70 R]ehböcke, // [als Begräbnisop]fer für den Mächtigen, Baˁlu.</td></tr>
</table>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Stufenparallelismus</summary>
<p>Der Stufenparallelismus (zuweilen auch als „climactic parallelism“ bezeichnet), eine strukturelle Spielart des poetischen &#x2197; Parallelismus, verknüpft zwei aufeinanderfolgende Kola miteinander, die zu <i>einem</i> Vers gehören. Das zweite Kolon greift das erste auf; jedoch ist das letzte Element des ersten Kolons im zweiten ausgelassen, und am Ende des zweiten Kolons ist ein Element ergänzt, das im ersten Kolon fehlt (zum Stufenparallelismus in der ugaritischen und hebräischen Dichtung vgl. u. a. <bibl>Greenstein, 1974: 96–104<VuepressApiPlayground url="/api/eupt/biblio/2TCEF77T" method="get" :data="[]"/></bibl>, <bibl>idem, 1977<VuepressApiPlayground url="/api/eupt/biblio/X97ZHDAU" method="get" :data="[]"/></bibl>, und <bibl>Watson, 1986a: 150–154<VuepressApiPlayground url="/api/eupt/biblio/AAJ26GVN" method="get" :data="[]"/></bibl>; wie im Terrassenparallelismus sind im Stufenparallelismus also &#x2197; Ellipse [<i>forwards ellipsis</i>] und &#x2197; Ergänzung [<i>backwards ellipsis</i>] miteinander kombiniert). Es ergibt sich das Versmuster a-b-◌ // a<sup>(</sup>'<sup>)</sup>-◌-c (die Konstruktion lässt sich auf die zusammenhängende Aussage a-b-c zurückführen).</p>
<p>Am Anfang des ersten Kolons steht eine Nominal- oder Verbalphrase, die gewöhnlich zwei Worteinheiten umfasst. Am Kolonende folgt das Subjekt des Satzes oder eine vokativische Anrede; das abschließende Element enthält oft ein Nomen proprium. Der vordere Teil des ersten Kolons wird am Anfang des zweiten Kolons semantisch und grammatisch parallel aufgegriffen; meist entsprechen sich die Satzteile, die jeweils am Anfang der beiden Kola stehen, wörtlich. Das letzte Element des ersten Kolons ist im zweiten Kolon ausgelassen. Dafür ist am Ende des zweiten Kolons der Satzteil ergänzt, der die Darstellung des im ersten Kolon eröffneten Sachverhalts erst komplettiert. Das im zweiten Kolon ergänzte Element schließt meist ein verbales Prädikat ein: Ist im ersten Kolon das verbale Prädikat des übergeordneten Satzes ausgelassen, ist dieses am Ende des zweiten Kolons ergänzt (vgl. <bibl>Miller, 1999: 372<VuepressApiPlayground url="/api/eupt/biblio/9B54UFTT" method="get" :data="[]"/></bibl>; s. Beispiele A; der erste Teil des ersten und zweiten Kolons enthält in dem Fall je ein oder zwei selbständige, nominale Satzglieder). Enthält der vordere Teil des ersten Kolons bereits ein verbales Prädikat (das dann im vorderen Teil des zweiten Kolons aufgegriffen wird), folgt im hinteren Teil des zweiten Kolons entweder ein nominales Satzglied, das im ersten Kolon ausgelassen ist (bspw. in KTU 1.16 vi 54c–57a), oder aber ein oder zwei weitere, verbale Prädikate (das Prädikat am Kolonende ist gewöhnlich durch die Konjunktion <i>wa</i> an den voranstehenden Satz angeschlossen; s. Beispiele B).</p>
<p>Das erste Kolon endet, bevor der darzustellende Sachverhalt vollständig dargelegt wurde. Erkennt das Publikum, dass das erste Kolon die geschilderte Szene nicht in allen Details erfasst, baut sich am Ende des ersten Kolons kurzzeitig Spannung auf (vgl. <bibl>Loewenstamm, 1975: 261–262<VuepressApiPlayground url="/api/eupt/biblio/4CPZCN44" method="get" :data="[]"/></bibl>). Die Spannung wird am Ende des zweiten Kolons aufgelöst, wo der im ersten Kolon fehlende Satzteil ergänzt ist. Erkennt das Publikum am Ende des ersten Kolons nicht, dass das erste Kolon die beschriebene Szene nur teilweise erfasst, registriert es dies erst am Ende des zweiten Kolons (Rezipient*innen sind dann veranlasst, die Aussage des ersten Kolons nachträglich neu zu kontextualisieren und zu interpretieren; vgl. <bibl>Greenstein, 1977<VuepressApiPlayground url="/api/eupt/biblio/X97ZHDAU" method="get" :data="[]"/></bibl>). Das zweite Kolon spezifiziert die Darstellung des ersten Kolons. Vorausgesetzt, dass das Publikum nicht mit dieser konkreten Spezifizierung rechnete, sorgt der letzte Teil des zweiten Kolons wohl für einen gewissen Überraschungseffekt. In der ugaritischen Dichtung treten Verse, deren (erste beiden) Kola mittels Stufenparallelismus verknüpft sind, oft am Anfang direkter Reden auf (vgl. u. a. KTU 1.6 iv 1–2 / 22–24; 1.15 ii 21–23a; 1.17 vi 26b–28a).</p>
<p>Im ugaritischen poetischen Korpus tritt der Stufenparallelismus überwiegend im Trikolon auf: Die ersten beiden Kola des Verses sind als Stufenparallelismus konstruiert (vgl. <bibl>Greenstein, 1974: 96–97<VuepressApiPlayground url="/api/eupt/biblio/2TCEF77T" method="get" :data="[]"/></bibl>; <bibl>idem, 1977: 77–78<VuepressApiPlayground url="/api/eupt/biblio/X97ZHDAU" method="get" :data="[]"/></bibl>; s. Beispiele A–B). Das zweite und das dritte Kolon sind nach anderen Mustern miteinander verknüpft: Das dritte Kolon enthält oft eine elliptische Variante des zweiten Kolons (bspw. in KTU 1.16 vi 54c–57a) oder greift das zweite chiastisch oder partiell-chiastisch auf (bspw. in KTU 1.17 i 11c–13a und 1.3 v 19b–21; vgl. <bibl>Greenstein, 1974: 97<VuepressApiPlayground url="/api/eupt/biblio/2TCEF77T" method="get" :data="[]"/></bibl>). Das zweite und das dritte Kolon können aber auch parallel gestaltet und gleich aufgebaut sein (bspw. in KTU 1.15 ii 21–23a und 1.12 i 9–11; jedes Element des zweiten Kolons hat ein semantisch und grammatisch korrespondierendes Pendant im dritten Kolon; die korrespondierenden Elemente sind in den beiden Kola in derselben Reihenfolge angeordnet), oder in Form eines &#x2197; Terrassenparallelismus miteinander verknüpft sein (bspw. in KTU 1.10 ii 21–23 und 1.16 vi 27–29a; beachte auch das Tetrakolon KTU 1.10 ii 13–16). In KTU 1.10 ii 10–12 formen das zweite und das dritte Kolon ein &#x2197; Enjambement.</p>
<p>Vereinzelt tritt der ugaritische Stufenparallelismus auch im Bikolon auf (s. Beispiele C). Einmal ist der Stufenparallelismus im Tetrakolon belegt (s. Beispiel D).</p>

<details class="exampleGUPF">
<summary>Beispiele A: Der Stufenparallelismus im Trikolon, ohne Prädikat im ersten Kolon</summary>
<table class="exampleGUPF-Table verseWithAnalysis">
    <tr><th colspan="4">KTU 1.2 iv 8c–9</th></tr>
        <tr>
            <td>hitta ˀêbaka <span class="ugaritic-text-line-number">9</span>&#160;baˁluma</td>        
            <td>Nun, deinen Gegner, Baˁlu,</td>
            <td>A-O<sub>4</sub>-<i>Anr.</i></td>
            <td>a-b-◌-c</td></tr>
        <tr>
            <td>hitta ˀêbaka timḫaṣ</td>
            <td>nun schlag deinen Gegner nieder,</td>
            <td>A-O<sub>4</sub>-P</td>
            <td>a-b-d-◌</td></tr>
        <tr>
            <td>hitta tuṣammit ṣarrataka</td>
            <td>nun vernichte deinen Feind!</td>
            <td>A-P-O<sub>4</sub></td>
            <td>a-d'-b'-◌</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Vgl. auch 1.3 v 19b–21 (O<sub>4</sub>/A-<i>Anr.</i> // O<sub>4</sub>/A-P // P-A &#8594; a-◌-b // a-c-◌ // c-a'-◌); KTU 1.10 ii 21–23 (O<sub>4</sub>-<i>Anr.</i> // O<sub>4</sub>-S-P // S-P<sup>+O</sup>-A &#8594; a-◌-◌-◌-b // a-c-d-◌-◌ // c-d'<sub>(~d+a')</sub>-e-◌; das zweite und dritte Kolon formen eine Art &#x2197; Terrassenparallelismus); 1.12 i 9–11 (O<sub>4</sub>-<i>Anr.</i> // O<sub>4</sub>-A-P // O<sub>4</sub>-A-P &#8594; a-◌-◌-b // a-c-d-◌ // a'-c'-d'-◌); vielleicht auch 1.161 20–22a (A<sub>1</sub>-<i>Anr.</i> // A<sub>1</sub>-A<sub>2</sub>-P // A<sub>2</sub>-P ; <i>wa</i>-P-A<sub>2</sub> &#8594; a-◌-◌-b // a-c-d-◌ // ◌-c-d ; d'-c').</p></td></tr>
    <tr><th colspan="2">KTU 1.17 i 11c–13a</th></tr>
        <tr>
            <td>ˀuzūra<span class="non-ugaritic-text">/</span>u <span class="ugaritic-text-line-number">12</span>&#160;<span class="non-ugaritic-text">[</span>ˀilī<span class="non-ugaritic-text">]</span>ma danīˀilu</td>
            <td>Gegürtet (hat) [den Göt]tern Danīˀilu,</td>
            <td>A-O<sub>4</sub>-S</td>
            <td>a-b-◌-c</td></tr>
        <tr>
            <td>ˀuzūra<span class="non-ugaritic-text">/</span>u ˀilīma yulaḥḥimu</td>
            <td>gegürtet hat er den Göttern zu Essen gegeben,</td>
            <td>A-O<sub>4</sub>-P</td>
            <td>a-b-d-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">13</span>&#160;<span class="non-ugaritic-text">[</span>ˀuzū<span class="non-ugaritic-text">]</span>ra<span class="non-ugaritic-text">/</span>u yušaqqiyu banī qudši</td>
            <td>[gegürt]et hat er zu Trinken gegeben den Söhnen des Heiligen.</td>
            <td>A-P-O<sub>4</sub></td>
            <td>a-d'-b'-◌</td></tr>
</table>
</details>

<p/>

<details class="exampleGUPF">
<summary>Beispiele B: Der Stufenparallelismus im Trikolon, samt Prädikat im ersten Kolon</summary>
<table class="exampleGUPF-Table verseWithAnalysis">
    <tr><th colspan="4">KTU 1.16 vi 54c–57a</th></tr>
        <tr>
            <td>yaṯbi<span class="non-ugaritic-text">/</span>ur <span class="ugaritic-text-line-number">55</span>&#160;ḥôrānu yā binu</td>
            <td>Ḥôrānu möge zerbrechen, o Sohn,</td>
            <td>P-S-<i>Anr.</i></td>
            <td>a-b-◌-c</td></tr>
        <tr>
            <td>yaṯbi<span class="non-ugaritic-text">/</span>ur ḥôrānu <span class="ugaritic-text-line-number">56</span>&#160;raˀšaka</td>
            <td>Ḥôrānu möge zerbrechen deinen Kopf,</td>
            <td>P-S-O<sub>4</sub></td>
            <td>a-b-d-◌</td></tr>
        <tr>
            <td>ˁaṯtartu-šu<span class="non-ugaritic-text">/</span>imV-baˁli <span class="ugaritic-text-line-number">57</span>&#160;qadqadak<span class="non-ugaritic-text"><sup>!</sup></span>a</td>
            <td>ˁAṯtartu-šu/imV-Baˁli deinen Schädel!</td>
            <td>S-O<sub>4</sub></td>
            <td>◌-b'-d'-◌</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Am Ende des zweiten Kolons ist die Anrede <i>yā binu</i> ausgelassen und dafür das Akkusativobjekt (des verbalen Prädikats <i>yaṯbi</i>/<i>ur</i>) <i>raˀšaka</i> ergänzt. Vgl. auch KTU 1.2 i 7b–8b (Parallelstelle zu KTU 1.16 vi 54c–57a; statt <i>yā binu</i> ist im bruchstückhaft erhaltenen ersten Kolon die Anrede <i>yā yammu</i>(<i>-ma</i>) „o Yammu“ zu rekonstruieren); 1.15 ii 21–23a (O<sub>4</sub>-P-<i>Anr.</i> // O<sub>4</sub>-P-A // O<sub>4</sub>-P-A &#8594; a-b-◌-c // a-b-d-◌ // a'-b'-d'-◌; am Ende des zweiten Kolons ist die Anrede <i>yā kirtu</i> ausgelassen und dafür das Adverbial <i>bêtaka</i> [Akk.adv.] ergänzt).</p></td></tr>
    <tr><th colspan="4">KTU 1.17 vi 26b–28a</th></tr>
        <tr>
            <td>ˀiriš ḥayyīma la ˀaqhatu ġāziru</td>      
            <td>Wünsch (dir) Leben, o ˀAqhatu, Held,</td>
            <td>P-O<sub>4</sub>-<i>Anr.</i></td>
            <td>a-b-c ; ◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">27</span>&#160;ˀiriš ḥayyīma wa ˀâtinaka</td>            
            <td>wünsch (dir) Leben, und ich will es dir geben,</td>
            <td>P-O<sub>4</sub> ; <i>wa</i>-P<sup>+O</sup></td>
            <td>a-b-◌ ; d</td></tr>
        <tr>
            <td>balî<span class="non-ugaritic-text">(-)</span>môta <span class="ugaritic-text-line-number">28</span>&#160;wa ˀašalliḥaka</td>
            <td>Unsterblichkeit, und ich will sie dir überreichen!</td>
            <td>O<sub>4</sub> ; <i>wa</i>-P<sup>+O</sup></td>
            <td>◌-b'-◌ ; d'</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Am Ende des zweiten Kolons ist die Anrede <i>la ˀaqhatu ġāziru</i> ausgelassen und dafür der Satz <i>wa ˀâtinaka</i> ergänzt. Vgl. auch KTU 1.16 vi 27–29a (P-A-<i>Anr.</i> // P-A ; <i>wa</i>-P // P-A &#8594; a-b-c ; ◌-◌ // a-b-◌ ; d-◌ // ◌-◌-◌ ; d'-e<sub>(~a')</sub>).</p></td></tr>
    <tr><th colspan="4">KTU 1.10 ii 10–12</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">10</span>&#160;tiššaˀu kanapa batūl<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu ˁanatu</td>
            <td>Es hob den Flügel das Mädchen ˁAnatu,</td>
            <td>P-O<sub>4</sub>-S</td>
            <td>a-b-c ; ◌-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">11</span>&#160;tiššaˀu kanapa wa târu bi ˁûpi<span class="non-ugaritic-text"><sup>?</sup></span></td>
            <td>sie hob den Flügel und zog im Fliegen herum,</td>
            <td>P-O<sub>4</sub> ; <i>wa</i>-P-A<sub>1</sub></td>
            <td>a-b-◌ ; d-e</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">12</span>&#160;tôka ˀaḫî<span class="non-ugaritic-text"><sup>?</sup></span> šamaki<span class="non-ugaritic-text"><sup>?</sup></span> maliˀat ruˀumīma<span class="non-ugaritic-text"><sup>?</sup></span></td>
            <td>hin zur Küste / zum Ried von Šamaku, voll von Wildstieren.</td>
            <td>-A<sub>2</sub></td>
            <td>-f</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Am Ende des zweiten Kolons ist das Subjekt <i>batūl</i>(<i>a</i>)<i>tu ˁanatu</i> ausgelassen und dafür der Satz <i>wa târu bi ˁûpi</i><sup>?</sup> ergänzt. Vgl. auch KTU 1.17 i 13b–15a (P-O<sub>4</sub>-S // P-O<sub>4</sub> ; P ; <i>wa</i>-P // P-O<sub>4</sub> ; <i>pa</i>-P &#8594; a-b-c ; ◌ ; ◌ // a-b-◌ ; d ; e // a-b'-◌ ; ◌ ; e'); 1.14 i 21b–23 (P-O<sub>4</sub>-S // P-O<sub>4</sub> ; P // … &#8594; a-b-c ; ◌ // a-b-◌ ; d // …; s. den Kommentar z. St. in der <a href="/edition.html" target="_blank">EUPT-Bearbeitung</a>).</p></td></tr>
</table>
</details>

<p/>

<details class="exampleGUPF">
<summary>Beispiele C: Der Stufenparallelismus im Bikolon</summary>
<table class="exampleGUPF-Table verseWithAnalysis">
    <tr><th colspan="4">KTU 1.3 iv 54–55</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">54</span>&#160;yaṯub liya ṯôru ˀilu <span class="non-ugaritic-text">[</span>ˀabūya<span class="non-ugaritic-text">]</span></td>
            <td>Er möge sich mir zuwenden, der Stier ˀIlu, [mein Vater],</td>
            <td>P-A-S</td>
            <td>a-b-c ; ◌-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">55</span>&#160;yaṯub liya wa lahu <span class="non-ugaritic-text">[</span>ˀaṯûba<span class="non-ugaritic-text">]</span></td>
            <td>er möge sich mir zuwenden, dann [will ich mich] ihm [zuwenden]!</td>
            <td>P-A ; <i>wa</i>-A-P</td>
            <td>a-b-◌ ; b'-a'</td></tr>
    <tr><th colspan="4">KTU 1.169 1b–2</th></tr>
        <tr>
            <td>tigḫaṭ<span class="non-ugaritic-text">(</span>ū<span class="non-ugaritic-text">/</span>ā<span class="non-ugaritic-text">)</span>ka R<span class="non-ugaritic-text">[x x]</span> <span class="ugaritic-text-line-number">2</span>&#160;baˁli</td>
            <td>Möge/n dich vertreiben die … […] Baˁlus,</td>
            <td>P<sup>+O&#8324;</sup>-S</td>
            <td>a<sub>(x+y)</sub>-b ; ◌-◌</td></tr>
        <tr>
            <td>tigḫaṭ<span class="non-ugaritic-text">(</span>ū<span class="non-ugaritic-text">/</span>ā<span class="non-ugaritic-text">)</span>ka wa tâṣiˀu li panî qâli ṯāˁiyi</td>
            <td>möge/n sie dich vertreiben, auf dass du entfliehst auf den Ruf des Beschwörers hin.</td>
            <td>P<sup>+O&#8324;</sup> ; <i>wa</i>-P-A</td>
            <td>a<sub>(x+y)</sub>-◌ ; c-d</td></tr>
    <tr><th colspan="4">KTU 1.4 iv 43b–44 (und Par.)</th></tr>
        <tr>
            <td>malkunā ˀalˀiyā<span class="non-ugaritic-text">[</span>nu<span class="non-ugaritic-text">]</span> baˁlu</td>
            <td>Unser König ist der Mächtige, Baˁlu,</td>
            <td>P<sup>N</sup>-S</td>
            <td>a-b ; ◌-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">44</span>&#160;ṯāpiṭunā wa ˀêna dā ˁalânahu</td>
            <td>unser Herrscher, denn es gibt niemanden, der über ihm steht!</td>
            <td>P<sup>N</sup> ; <i>wa</i>-P<sup>N</sup>-S</td>
            <td>a'-◌ ; c-d</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Anders als in allen anderen hier vorgestellten Belegen wird das erste Element des ersten Kolons, <i>malkunā</i>, am Anfang des zweiten Kolons nicht wörtlich wiederholt (das Lexem <i>malku-</i> ist durch das sinnverwandte Lexem <i>ṯāpiṭu-</i> ersetzt).</p></td></tr>
    <tr><th colspan="4">KTU 1.6 iv 1–2</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">1</span>&#160;pallū ˁN-ātu šadîma yā šapšu</td>
            <td>Vertrocknet sind die Ackerfurchen der Felder, o Šapšu,</td>
            <td>P-S-Attr.<sup>S</sup>-<i>Anr.</i></td>
            <td>a-b<sub>(x>y>◌)</sub>-c</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">2</span>&#160;pallū ˁN-ātu šadîma ˀili</td>
            <td>vertrocknet sind die Ackerfurchen der Felder des ˀIlu / der göttlichen Felder!</td>
            <td>P-S-Attr.<sup>S</sup>-Attr.<sup>Attr.S</sup></td>
            <td>a-b'<sub>(x>y>z)</sub>-◌</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Anders als in allen anderen hier vorgestellten Belegen ist das im zweiten Kolon ergänzte Element kein selbständiges Satzglied, sondern ein Satzgliedteil, namentlich ein Genitivattribut (<i>ˀili</i>).</p></td></tr>
</table>
</details>

<p/>

<details class="exampleGUPF">
<summary>Beispiele D: Der Stufenparallelismus im Tetrakolon</summary>
<table class="exampleGUPF-Table verseWithAnalysis">
<tr><th colspan="4">KTU 1.10 ii 13–16</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">13</span>&#160;wa yiššaˀu ˁênêhu ˀalˀiyānu baˁlu</td>
            <td>Da hob seine Augen der Mächtige, Baˁlu,</td>
            <td><i>wa</i>-P-O<sub>4</sub>-S</td>
            <td>a-b-c ; ◌-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">14</span>&#160;wa yiššaˀu ˁênêhu wa yaˁînu</td>
            <td>da hob er seine Augen und sah,</td>
            <td><i>wa</i>-P-O<sub>4</sub> ; P</td>
            <td>a-b-◌ ; d-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">15</span>&#160;wa yaˁînu batūl<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta ˁanata</td>
            <td>und sah das Mädchen, ˁAnatu,</td>
            <td><i>wa</i>-P-O<sub>4</sub></td>
            <td>◌-◌-◌ ; d-e</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">16</span>&#160;naˁīm<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ta bêna ˀaḫâti baˁli</td>
            <td>die Lieblich(st)e unter den Schwestern des Baˁlu.</td>
            <td>O<sub>4</sub></td>
            <td>◌-◌-◌ ; ◌-e'</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Die ersten beiden Kola formen einen Stufenparallelismus (am Ende des zweiten Kolons ist das Subjekt <i>ˀalˀiyānu baˁlu</i> ausgelassen und die Phrase <i>wa yaˁînu</i> ergänzt). Das zweite und dritte Kolon formen einen &#x2197; Terrassenparallelismus. Im vierten Kolon ist das dritte Kolon elliptisch aufgegriffen (die Phrase <i>wa yaˁînu</i> am Kolonanfang ist ausgelassen; <i>batūl</i>(<i>a</i>)<i>ta ˁanata</i> ist durch den Beinamen <i>naˁīm</i>(<i>a</i>)<i>ta bêna ˀaḫâti baˁli</i> ersetzt).</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Synekdoche</summary>
<p>&#x2197; Metonymie.</p>
</details>

<h2>T</h2>

<details class="undoneEntryGUPF">
<summary>Tautologie</summary>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Terrassenparallelismus</summary>
<p>Der Terrassenparallelismus, eine strukturelle Spielart des poetischen &#x2197; Parallelismus, verknüpft zwei aufeinanderfolgende Verseinheiten miteinander, die zu <i>einem</i> übergeordneten Versgefüge gehören. Die zweite Verseinheit greift die erste Einheit semantisch und grammatisch parallel auf; allerdings ist das erste Element der ersten Verseinheit in der zweiten ausgelassen, und am Ende der zweiten Verseinheit ist ein Element ergänzt, das in der ersten Einheit ausgelassen ist (vgl. <bibl>Watson, 1986a: 208–210<VuepressApiPlayground url="/api/eupt/biblio/AAJ26GVN" method="get" :data="[]"/></bibl>; wie im Stufenparallelismus sind im Terrassenparallelismus also &#x2197; Ellipse [<i>forwards ellipsis</i>] und &#x2197; Ergänzung [<i>backwards ellipsis</i>] miteinander kombiniert). So ergibt sich das charakteristische Versmuster a-b-◌ // ◌-b<sup>(</sup>'<sup>)</sup>-c (die Konstruktion lässt sich auf die zusammenhängende Aussage a-b-c zurückführen).</p>
<p>In der ugaritischen Dichtung ist das Versmuster meines Wissens ausschließlich auf Versebene belegt: Der Terrassenparallelismus verknüpft hier also zwei aufeinanderfolgende Kola miteinander, die zu <i>einem</i> Vers gehören (in der akkadischen Dichtung ist der Terrassenparallelismus auch innerhalb des Kolons bezeugt; vgl. <bibl>Steinberger, 2024: § 799<VuepressApiPlayground url="/api/eupt/biblio/C8UWWRK8" method="get" :data="[]"/></bibl>). Der Teil des ersten Kolons, der im zweiten Kolon aufgegriffen wird, schließt meist ein verbales Prädikat ein (nicht so in KTU 1.14 iii 22–23a; in KTU 1.23 50 ist es ein nicht-verbales Prädikat). Die korrespondierenden Kolonteile entsprechen sich oft wörtlich (in KTU 1.19 iv 41b–43a und 1.2 i 18a–b stehen sich unterschiedliche Ableitungen desselben Lexems gegenüber [die korrespondierenden Elemente formen ein &#x2197; Polyptoton]; in KTU 1.3 ii 23–24 und 1.19 iii 28b–29 ist der letzte Teil des ersten Kolons am Anfang des zweiten Kolons nicht wörtlich wiederholt, sondern durch ein partiell-synonymes Pendant ersetzt). Sofern es nicht ohnehin eine einzige Worteinheit des ersten Kolons ist, die am Anfang des zweiten Kolons aufgegriffen wird (wie bspw. in KTU 1.16 v 25b–28a, 1.19 iii 28b–29, iv 41b–43a und 1.114 2c–4a), sind die korrespondierenden Elemente in den beiden Kola gewöhnlich in derselben Reihenfolge angeordnet (ein Sonderfall liegt in KTU 1.15 iii 17–19 vor; s. Beispiele B).</p>
<p>Wie im Stufenparallelismus entfaltet sich der beschriebene Sachverhalt im ersten Kolon noch nicht vollständig. Dem Publikum werden einzelne Informationen vorenthalten, die dann am Ende des zweiten Kolons ergänzt werden. Dies zögert den Aufnahmeprozess hinaus, an dessen Ende sich der Sachverhalt dem Publikum in seiner Gänze erschließt. Vorausgesetzt, dass Rezipient*innen erkennen, dass das erste Kolon den Sachverhalt noch nicht vollständig erfasst, baut sich am Ende des ersten Kolons Spannung kurzzeitig auf. Das Publikum wird neugierig, wie sich die Szene im zweiten Kolon entfalten wird (vgl. <bibl>Watson, 1986a: 209<VuepressApiPlayground url="/api/eupt/biblio/AAJ26GVN" method="get" :data="[]"/></bibl>; vielleicht in KTU 1.19 iii 28b–29). Entgeht Rezipient*innen, dass das erste Kolon die Szene nur teilweise erfasst, baut sich am Ende des ersten Kolons keine Spannung auf. Auf jeden Fall führt das zweite Kolon den im ersten Kolon aufgenommenen Gedanken fort und spezifiziert die Aussage des ersten Kolons. Angenommen, dass das zweite Kolon die Aussage der ersten Einheit auf eine Weise spezifiziert, mit der das Publikum nicht rechnet, sorgt die zweite Einheit für einen gewissen Überraschungseffekt (für die unten zusammengestellten Belege kann dies jedoch nicht wahrscheinlich gemacht werden).</p>
<p>In der ugaritischen Dichtung tritt der Terrassenparallelismus im Bikolon, im Trikolon und einmal auch im Tetrakolon auf. Im Trikolon und im Tetrakolon sind in dem Fall zwei aufeinanderfolgende Kola als Terrassenparallelismus konstruiert, während die übrigen Kolon-Paare anderen Konstruktionsmustern folgen (im Trikolon, in dem die ersten beiden Kola einen Terrassenparallelismus formen, enthält das dritte Kolon nicht selten eine elliptische Variante des zweiten Kolons [bspw. in KTU 1.2 iv 12b–13a, 1.15 iii 17–19, 1.16 v 25b–28a und 1.114 2c–4a; im dritten Kolon ist hier jeweils das verbale Prädikat ausgelassen; die übrigen Elemente werden semantisch und grammatisch parallel und in derselben Reihenfolge aufgegriffen]; formen das zweite und dritte Kolon einen Terrassenparallelismus, sind das erste und zweite Kolon zuweilen als Stufenparallelismus konstruiert [bspw. in KTU 1.10 ii 21–23 und 1.16 vi 27–29a; beachte auch das Tetrakolon KTU 1.10 ii 13–16; dazu &#x2197; Stufenparallelismus, Beispiel D]).</p>

<details class="exampleGUPF">
<summary>Beispiele A: Der Terrassenparallelismus im Bikolon</summary>
<table class="exampleGUPF-Table verseWithAnalysis">
    <tr><th colspan="4">KTU 1.2 i 18a–b (und Par.)</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">18</span>&#160;tinū ˀilama dā tâqîhu</td>
            <td>Liefert den Gott aus, den in Schutz nahm,</td>
            <td>P-O<sub>4</sub>-<i>Attr.Satz</i><sup>O&#8324;</sup>:[P]</td>
            <td>a-b-c-◌</td></tr>
        <tr>
            <td>dā tâqiyanna hamullatu</td>
            <td>den in Schutz nahm die (Götter-)Schar.</td>
            <td><i>Attr.Satz</i><sup>O&#8324;</sup>:[P-S]</td>
            <td>◌-◌-c'-d</td></tr>
    <tr><th colspan="4">KTU 1.3 ii 23–24</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">23</span>&#160;maˀda timtaḫaṣunna wa taˁînu</td>
            <td>Heftig kämpfte und schaute umher,</td>
            <td>A-P ; P</td>
            <td>a-b ; c-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">24</span>&#160;tiḫtaṣabu wa taḥdiyu ˁAnatu</td>
            <td>focht und blickte umher die ˁAnatu.</td>
            <td>P ; P-S</td>
            <td>◌-b' ; c'-d</td></tr>
    <tr><th colspan="4">KTU 1.14 iii 22–23a (und Par.)</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">22</span>&#160;qaḥ kaspa wa yarqa</td>
            <td>Nimm Silber und Gelbgold,</td>
            <td>P-O<sub>4 (Teil 1)</sub>-O<sub>4 (Teil 2)</sub></td>
            <td>a-b-c-◌</td></tr>
        <tr>
            <td>ḫurāṣa <span class="ugaritic-text-line-number">23</span>&#160;yada maqâmihu</td>
            <td>Gold samt seinem Fundort!</td>
            <td>O<sub>4 (Teil 2)</sub>-<i>Präp.Attr.</i></td>
            <td>◌-◌-c'-d</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Die beiden Kola können zurückgeführt werden auf den zusammenhängenden Satz *<i>qaḥ kaspa wa  ḫurāṣa yarqa yada maqâmihu</i> „Nimm Silber und gelbes Gold samt seinem Fundort“ (die Parallelkonstruktion <i>yarqa</i> // <i>ḫurāṣa</i>, i. e. <i>substantiviertes Adjektiv</i> // <i>Substantiv</i>, umschreibt die zusammenhängende Phrase *<i>ḫurāṣa yarqa</i>, i. e. <i>Bezugswort</i> + <i>Adjektivattribut</i>; s. den Kommentar z. St. in der <a href="/edition.html" target="_blank">EUPT-Bearbeitung</a>).</p></td></tr>
    <tr><th colspan="4">KTU 1.19 iii 28b–29</th></tr>
        <tr>
            <td>bi našāˀi ˁênêhu <span class="ugaritic-text-line-number">29</span>&#160;wa yiphênna</td>
            <td>Als er seine Augen hob, da sah er fürwahr</td>
            <td>A-<i>wa</i>-P</td>
            <td>a-b-◌</td></tr>
        <tr>
            <td>yaḥdî ṢML-a ˀumma našarīma</td>
            <td>(da) erspähte er Ṣamlu, die Mutter der Adler.</td>
            <td>P-O<sub>4</sub></td>
            <td>◌-b'-c</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Das Adverbial <i>bi našāˀi ˁênêhu</i> ist im zweiten Kolon ausgelassen. Das Prädikat <i>yiphê</i>(<i>-nna</i>) ist durch das partielle Synonym <i>yaḥdî</i> ersetzt. Im zweiten Teil des zweiten Kolons ist das Akkusativobjekt des Prädikats ergänzt. Vgl. auch KTU 1.4 vii 38b–39 (<i>Anr.</i>-A<sub>Interrogativum</sub>-P // A<sub>Interrogativum</sub>-P-O<sub>4</sub> &#8594; a-b-c-◌ // ◌-b-c-d).</p></td></tr>
    <tr><th colspan="4">KTU 1.23 50</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">50</span>&#160;hanna šap<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tāhumā matuq<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tāmi</td>
            <td>Siehe, ihre Lippen waren süß,</td>
            <td><i>Interj.</i>-S-P<sup>N</sup></td>
            <td>a-b-c-◌</td></tr>
        <tr>
            <td>matuq<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tāmi ka lurmānī[ma]</td>
            <td>süß wie Granatäpfe[l].</td>
            <td>P<sup>N</sup>-A</td>
            <td>◌-◌-c-d</td></tr>
</table>
</details>
<p/>
<details class="exampleGUPF">
<summary>Beispiele B: Der Terrassenparallelismus im Trikolon</summary>
<table class="exampleGUPF-Table verseWithAnalysis">
    <tr><th colspan="4">KTU 1.2 iv 12b–13a</th></tr>
        <tr>
            <td>yugarriš<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> garriš yamma</td>
            <td>Yugarriš(u), vertreibe den Yammu,</td>
            <td><i>Anr.</i>-P-O<sub>4</sub></td>
            <td>a-b-c-◌</td></tr>
        <tr>
            <td>garriš yamma li kussiˀihu</td>
            <td>vertreibe den Yammu von seinem Thron,</td>
            <td>P-O<sub>4</sub>-A</td>
            <td>◌-b-c-d</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">13</span>&#160;nah<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>ra li kaḥṯi<span class="non-ugaritic-text"><sup>?</sup></span> dar<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>k<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tihu</td>
            <td>den Nah(a)ru vom Thron seiner Herrschaft!</td>
            <td>O<sub>4</sub>-A</td>
            <td>◌-◌-c'-d'</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Terrassenparallelismus im 1.–2. Kolon (<i>Anr.</i>-Ellipse / A-Ergänzung).</p></td></tr>
    <tr><th colspan="4">KTU 1.10 ii 21–23</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">21</span>&#160;qarnê dubˀatika batūl<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu ˁanatu</td>
            <td>Die Hörner deiner Kraft, Mädchen, ˁAnatu,</td>
            <td>O<sub>4</sub>-<i>Anr.</i></td>
            <td>a-◌-◌-◌-b</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">22</span>&#160;qarnê dubˀatika baˁlu yimšaḥ</td>
            <td>die Hörner deiner Kraft möge Baˁlu salben,</td>
            <td>O<sub>4</sub>-S-P</td>
            <td>a-c-d-◌-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">23</span>&#160;baˁlu yimšaḥhumā bi ˁûpi<span class="non-ugaritic-text"><sup>?</sup></span></td>
            <td>Baˁlu möge sie im Flug salben!</td>
            <td>S-P<sup>+O</sup>-A</td>
            <td>c-d'<sub>(~d+a')</sub>-e-◌</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Terrassenähnliche Konstruktion im 2.–3. Kolon. Streng genommen ist der Beleg von den anderen hier besprochenen Belegen zu unterscheiden; das erste Element des zweiten Kolons, <i>qarnê dubˀatika</i>, ist im dritten Kolon nämlich nicht ausgelassen, sondern durch das Objektsuffix am verbalen Prädikat, <i>-humā</i>, ersetzt. Das erste und zweite Kolon sind als &#x2197; Stufenparallelismus konstruiert.</p></td></tr>
    <tr><th colspan="4">KTU 1.15 iii 17–19</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">17</span>&#160;tubarrikū ˀilūma taˀtiyū</td>
            <td>Die Götter sprachen den Segen (und) gingen (heim),</td>
            <td>P&#8594;S&#8592;P</td>
            <td>a&#8594;b&#8592;c-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">18</span>&#160;taˀtiyū ˀilūma li ˀahlīhumū</td>
            <td>die Götter gingen (heim) zu ihren Wohnstätten,</td>
            <td>P-S-A</td>
            <td>◌-c-b-d</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">19</span>&#160;dô/āru ˀili li ma<span class="non-ugaritic-text">/</span>iškanātihumū</td>
            <td>die Gemeinschaft des ˀIlu zu ihren Wohnungen.</td>
            <td>S-A</td>
            <td>◌-◌-b'-d'</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Terrassenparallelismus im 1.–2. Kolon (Satz-Ellipse / A-Ergänzung). Das erste Kolon enthält eine &#x2197; Apokoinu-Konstruktion: Das Subjekt <i>ˀilūma</i> bezieht sich sowohl auf das voranstehende Prädikat, <i>tubarrikū</i>, als auch auf das folgende Prädikat, <i>taˀtiyū</i>. Das Prädikat <i>tubarrikū</i> ist im zweiten Kolon ausgelassen. Das Subjekt <i>ˀilūma</i> und das Prädikat <i>taˀtiyū</i> sind im zweiten Kolon wörtlich wiederholt, jedoch in umgekehrter Reihenfolge angeordnet (<i>ˀilūma taˀtiyū</i> // <i>taˀtiyū ˀilūma</i>; die beiden Phrasen sind chiastisch aufgebaut; &#x2197; Chiasmus).</p></td></tr>
    <tr><th colspan="4">KTU 1.16 v 25b–28a</th></tr>
        <tr>
            <td>ˀanāku <span class="ugaritic-text-line-number">26</span>&#160;ˀiḥtar<span class="non-ugaritic-text">(</span>i<span class="non-ugaritic-text">)</span>ša wa ˀašakîna</td>
            <td>Ich selbst will tätig werden und (eine) erschaffen,</td>
            <td>S-P ; <i>wa</i>-P</td>
            <td>a-b ; c-◌</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">27</span>&#160;ˀašakîna yādîta [mV]rṣi</td>
            <td>ich will eine erschaffen, die die [Kran]kheit austreibt,</td>
            <td>P-O<sub>4</sub></td>
            <td>◌-◌ ; c-d</td></tr>
        <tr>
            <td>gāriš(a)ta <span class="ugaritic-text-line-number">28</span>&#160;zabalāni<span class="non-ugaritic-text"><sup>?</sup></span></td>
            <td>die das Siechtum vertreibt!</td>
            <td>O<sub>4</sub></td>
            <td>◌-◌ ; ◌-d'</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Terrassenparallelismus im 1.–2. Kolon (Satz-Ellipse / O<sub>4</sub>-Ergänzung). Vgl. auch KTU 1.19 iv 41b–43a (1.–2. Kolon; Satz-Ellipse / A-Ergänzung: P ; <i>wa</i>-P // P-A // Attr.<sup>A</sup> &#8594; a ; b-◌ // ◌ ; b'-c // -d<sup>c</sup>).</p></td></tr>
    <tr><th colspan="4">KTU 1.16 vi 27–29a</th></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">27</span>&#160;lik<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span> li ˀabīka yaṣṣubu</td>
            <td>Geh zu seinem Vater, Yaṣṣubu,</td>
            <td>P-A-<i>Anr.</i></td>
            <td>a-b-c ; ◌-◌</td></tr>
        <tr>
            <td>lik(a) <span class="ugaritic-text-line-number">28</span>&#160;[li ˀa]bīka wa rugum</td>
            <td>geh [zu] deinem [Va]ter und sag,</td>
            <td>P-A ; <i>wa</i>-P</td>
            <td>a-b-◌ ; d-◌</td></tr>
        <tr>
            <td>ṯiniya <span class="ugaritic-text-line-number">29</span>&#160;li [kirti ˀadānika]</td>
            <td>berichte dem [Kirtu, deinem Herrn]!</td>
            <td>P-A</td>
            <td>◌-◌-◌ ; d'-e<sub>(~a')</sub></td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Terrassenparallelismus im 2.–3. Kolon (Satz-Ellipse / A-Ergänzung). Das erste und zweite Kolon sind als &#x2197; Stufenparallelismus konstruiert.</p></td></tr>
    <tr><th colspan="4">KTU 1.114 2c–4a</th></tr>
        <tr>
            <td>tilḥamūna <span class="ugaritic-text-line-number">3</span>&#160;ˀilūma wa tištûna</td>
            <td>Die Götter essen und trinken,</td>
            <td>P-S ; P</td>
            <td>a-b ; c-◌-◌</td></tr>
        <tr>
            <td>tištûna yê&lt;na&gt; ˁadê šubˁi</td>
            <td>trinken Wein bis zur Sättigung,</td>
            <td>P-O<sub>4</sub>-A</td>
            <td>◌-◌ ; c-d-e</td></tr>
        <tr>
            <td><span class="ugaritic-text-line-number">4</span>&#160;tê/îrāṯa ˁadê šukri</td>
            <td>Most bis zur Trunkenheit.</td>
            <td>O<sub>4</sub>-A</td>
            <td>◌-◌ ; ◌-d'-e'</td></tr>
        <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: Terrassenparallelismus im 1.–2. Kolon (Satz-Ellipse / O<sub>4</sub>-A-Ergänzung).</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary  class="doneEntryGUPFSummary">Tetrakolon</summary>
<p>Das Tetrakolon ist ein &#x2197; Vers, der sich aus vier &#x2197; Kola zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Tetrastichon</summary>
<p>Das Tetrastichon ist eine &#x2197; Strophe, die sich aus vier &#x2197; Versen zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary"><i>totum pro parte</i></summary>
<p>&#x2197; Metonymie.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Trikolon</summary>
<p>Das Trikolon ist ein &#x2197; Vers, der sich aus drei &#x2197; Kola zusammensetzt.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Tristichon</summary>
<p>Das Tristichon ist eine &#x2197; Strophe, die sich aus drei &#x2197; Versen zusammensetzt.</p>
</details>

<h2>V</h2>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Vergleich</summary>
<p>&#x2197; Metapher / Vergleich.</p>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Vers</summary>
<p>Der ugaritische Vers setzt sich aus einem oder mehreren &#x2197; Kola zusammen (s. auch &#x2197; Versgliederung). Das Monokolon enthält ein einziges Kolon, das Bikolon zwei Kola, das Trikolon drei, das Tetrakolon vier, das Pentakolon fünf. Das Bikolon ist die gängigste Versform in der ugaritischen Dichtung; daneben treten immer wieder Monokola und Trikola auf (Tetrakola und Pentakola sind deutlich seltener belegt). Die Kola, die zu einem Vers verbunden sind, stehen oft parallel zueinander (&#x2197; Parallelismus) oder enthalten <i>einen</i> zusammenhängenden Satz (im Enjambement-Vers; &#x2197; Enjambement). Der Vers enthält meist mindestens einen vollständigen Hauptsatz. Daneben sind syntaktisch unvollständige Versformen belegt, in denen der Vers nur einen Teil des übergeordneten Hauptsatzes enthält (und sich mit einem oder mehreren angrenzenden Versen zu einer Enjambement-Strophe verbindet; &#x2197; Enjambement). Die Versgrenzen wurden im Vortrag vermutlich durch Zäsuren kenntlich gemacht (&#x2197; Zäsur).</p>

<details class="exampleGUPF-FirstOrder">
<summary class="exampleGUPF-FirstOrderSummary">Versformen</summary>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Monokolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.14 i 37b–38a</th></tr>
                    <tr>
                        <td>wa yaqrub <span class="ugaritic-text-line-number">38</span>&#160;bi šaˀāli kirti</td>
                        <td>Und er trat heran, um Kirtu zu fragen.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Bikolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.14 i 35b–37a</th></tr>
                    <tr>
                        <td>wa bi ḥilmihu <span class="ugaritic-text-line-number">36</span>&#160;ˀilu yârid</td>
                        <td>Da stieg in seinem Traum ˀIlu herab,</td></tr>
                    <tr>
                        <td>bi ḎHR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tihu <span class="ugaritic-text-line-number">37</span>&#160;ˀabū ˀadami</td>
                        <td>in seiner Vision der Vater der Menschheit.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Trikolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.14 i 28–30</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">28</span>&#160;tinnatikna ˀudmaˁātuhu</td>
                        <td>Seine Tränen flossen</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">29</span>&#160;kama ṯiqalīma ˀarṣah</td>
                        <td>wie Schekel zur Erde,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">30</span>&#160;kama ḫamušāti maṭṭâtah</td>
                        <td>wie Fünftel (eines Schekels) aufs Bett.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Tetrakolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.6 ii 31b–34a</th></tr>
                    <tr>
                        <td>bi ḥarbi <span class="ugaritic-text-line-number">32</span>&#160;tibqaˁ<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ninnu</td>
                        <td>Mit einem Messer schlitzte sie ihn auf,</td></tr>
                    <tr>
                        <td>bi ḪṮR-i tadriyV<span class="ugaritic-text-line-number">33</span>ninnu</td>
                        <td>mit einer Worfgabel<sup>?</sup> worfelte sie ihn,</td></tr>
                    <tr>
                        <td>bi ˀiš<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ti tašrup<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ninnu</td>
                        <td>mit Feuer verbrannte sie ihn,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">34</span>&#160;bi riḥêma tiṭḥanninnu</td>
                        <td>mit (zwei) Mühlsteinen zermahlte sie ihn.</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">Pentakolon</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table">
                <tr><th colspan="2">KTU 1.10 ii 26–30</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">26</span>&#160;wa tiššaˀu ˁênêha batūl<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tu ˁanatu</td>
                        <td>Da hob ihre Augen die Jungfrau, ˁAnatu,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">27</span>&#160;wa tiššaˀu ˁênêha wa taˁîn<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>
                        <td>da hob sie ihre Augen und sah,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">28</span>&#160;wa taˁîn<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> ˀarḫa wa târu bi likti</td>
                        <td>da sah sie eine Kuh und die zog im Gehen herum,</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">29</span>&#160;târu bi likti wa târu bi ḫîli<span class="non-ugaritic-text"><sup>?</sup></span></td>
                        <td>die zog im Gehen herum, ja sie zog im Springen<sup>?</sup> herum</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">30</span>&#160;<span class="non-ugaritic-text">[</span>bi<span class="non-ugaritic-text">]</span> nuˁmima bi YSM-ima ḥabli kôṯarāti</td>
                        <td>[in / samt] der Lieblichkeit, dem Anmut der Schar der Kôṯarātu.</td></tr>
            </table></details>
</details>
</details>
<p/>

<details class="exampleGUPF-FirstOrder">
<summary class="exampleGUPF-FirstOrderSummary">Verskonstruktionen</summary>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a-b-c // a&#8596;b&#8596;c</summary>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-(c) // a'-b'-(c')</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.5 ii 6b–7</th></tr>
                <tr>
                    <td>yara<span class="non-ugaritic-text">(/</span>i<span class="non-ugaritic-text">)</span>ˀaninnu<span class="non-ugaritic-text"><sup>!</sup></span> ˀalˀiyānu baˁlu</td>
                    <td>Der Mächtige, Baˁlu, fürchtete sich vor ihm,</td>
                    <td>P<sup>+O&#8324;</sup>-S</td>
                    <td>a-b</td></tr>
                <tr>
                    <td><span class="ugaritic-text-line-number">7</span>&#160;ṯata<span class="non-ugaritic-text">(/</span>i<span class="non-ugaritic-text">)</span>ˁaninnu rākibu ˁarapāti</td>
                    <td>der Wolkenfahrer hatte Angst vor ihm.</td>
                    <td>P<sup>+O&#8324;</sup>-S</td>
                    <td>a'-b'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-c // (c')-b'-a' (&#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.17 v 10b–11</th></tr>
                    <tr>
                        <td>hVl<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>ka kôṯari <span class="ugaritic-text-line-number">11</span>&#160;kī yaˁin<span class="non-ugaritic-text">(/</span>-înu<span class="non-ugaritic-text">)</span></td>
                        <td>Dass Kôṯaru kam, sah er fürwahr,</td>
                        <td>O<sub>4</sub>-P</td>
                        <td>a-b</td></tr>
                    <tr>
                        <td>wa yaˁin<span class="non-ugaritic-text">(/</span>-înu<span class="non-ugaritic-text">)</span> tadrVqa ḫasīsi</td>
                        <td>ja, er sah, dass Ḫasīsu heranschritt.</td>
                        <td>P-O<sub>4</sub></td>
                        <td>b-a'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-{b-c} // a'-{c'-b'} (anaphorischer partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.3 iii 19–20a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">19</span>&#160;ˁimmaya paˁnāki talsumānna</td>
                        <td>Zu mir mögen deine Füße laufen,</td>
                        <td>A-S-P</td>
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td>ˁimmaya <span class="ugaritic-text-line-number">20</span>&#160;tawattiḥā ˀišdâki</td>
                        <td>zu mir mögen eilen deine Beine!</td>
                        <td>A-P-S</td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.3 v 33b–34b</th></tr>
                    <tr>
                        <td>kullunāyaya qašâhu <span class="ugaritic-text-line-number">34</span>&#160;nâbilanna</td>
                        <td>Wir alle wollen (ihm) seine Schale bringen,</td>
                        <td><i>Pron.</i>-O<sub>4</sub>-P</td>
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td>kullunāyaya nâbila kāsahu</td>
                        <td>wir alle wollen (ihm) bringen seinen Becher.</td>
                        <td><i>Pron.</i>-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.17 i 2b–3a</th></tr>
                    <tr>
                        <td>ˀuzūru<span class="non-ugaritic-text">/</span>a ˀilīma yulaḥḥim<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>
                        <td>Gegürtet gab er den Göttern zu Essen,</td>
                        <td>A-O<sub>4</sub>-P</td>
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">3</span>&#160;<span class="non-ugaritic-text">[</span>ˀuzūru<span class="non-ugaritic-text">/</span>a yušaqqiyu<span class="non-ugaritic-text">]</span> banī qudši</td>
                        <td>[gegürtet gab er zu Trinken] den Söhnen des Heiligen.</td>
                        <td>A-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.6 vi 45b–47</th></tr>
                    <tr>
                        <td>šapšu <span class="ugaritic-text-line-number">46</span>&#160;rāpiˀīma tuḥattikī</td>
                        <td>Šapšu, über die Rāpiˀūma sollst du herrschen,</td>        
                        <td>S-O<sub>4</sub>-P</td>        
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">47</span>&#160;šapšu tuḥattikī ˀilānīyīma</td>
                        <td>Šapšu, du sollst herrschen über die Göttlichen!</td>
                        <td>S-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr>
                <tr><th colspan="4">KTU 1.15 iv 17–18</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">17</span>&#160;ˁalê<span class="non-ugaritic-text">/</span>âhu ṯôrīhu tušaˁrib</td>        
                        <td>Zu ihm brachte sie seine Stiere,</td>        
                        <td>A-O<sub>4</sub>-P</td>        
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">18</span>&#160;ˁalê<span class="non-ugaritic-text">/</span>âhu tušaˁrib ẓabayīhu</td>
                        <td>zu ihm brachte sie seine Gazellen,</td>
                        <td>A-P-O<sub>4</sub></td>
                        <td>a-{c'-b'}</td></tr></table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">{a-b}-c // {b'-a'}-c' (epiphorischer partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iv 44b–45</th></tr>
                    <tr>
                        <td>tašit<span class="non-ugaritic-text">(/</span>-îtu<span class="non-ugaritic-text">)</span> Ḫx<span class="non-ugaritic-text">[x x]</span>-a bi <span class="ugaritic-text-line-number">45</span>&#160;NŠG-iha</td>
                        <td>Sie steckte einen D[olch]<sup>?</sup> in ihre Scheide<sup>?</sup>,</td>
                        <td>P-O<sub>4</sub>-A</td>
                        <td>{a-b}-c</td></tr>
                    <tr>
                        <td>ḥarba tašit<span class="non-ugaritic-text">(/</span>-îtu<span class="non-ugaritic-text">)</span> bi TˁR-<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text">[</span>tiha<span class="non-ugaritic-text">]</span></td>
                        <td>ein Messer steckte sie in [ihre] Scheid[e]<sup>?</sup>.</td>
                        <td>O<sub>4</sub>-P-A</td>
                        <td>{b'-a}-c'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">{a-b}-c // c'-{a'-b'} (anadiplotischer partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iv 8–9a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">8</span>&#160;danīˀilu bêtahu yamġiyu<span class="non-ugaritic-text">/</span>anna</td>
                        <td>Danīˀilu kam zu seinem Haus,</td>        
                        <td>S-A-P</td>        
                        <td>{a-b}-c</td></tr>
                    <tr>
                        <td>yišta<span class="ugaritic-text-line-number">9</span>qVl<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span><span class="non-ugaritic-text"><sup>?</sup></span> danīˀilu li hêkalihu</td>
                        <td>Danīˀilu erreichte seinen Palast.</td>
                        <td>P-S-A</td>
                        <td>c'-{a-b'}</td></tr>
        </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-{b-c} // {b'-c'}-a' (rahmender partieller &#x2197; Chiasmus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iii 8–9a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">8</span>&#160;kanapê našarīma baˁlu yaṯbu<span class="non-ugaritic-text">/</span>ir<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>        
                        <td>Baˁlu zerbrach die Flügel der Adler,</td>        
                        <td>O<sub>4</sub>-S-P</td>        
                        <td>a-{b-c}</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">9</span>&#160;baˁlu ṯabara DˀIY-ê<span class="non-ugaritic-text">/</span>ī humūti</td>
                        <td>Baˁlu zerbrach die Schwingen von jenen.</td>
                        <td>S-P-O<sub>4</sub></td>
                        <td>{b-c}-a'</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a-b-c // ◌&#8596;b'&#8596;c' (&#x2197; Ellipse)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.4 v 61–62</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">61</span>&#160;bal<span class="non-ugaritic-text">(</span>î<span class="non-ugaritic-text">)</span> ˀašit ˀurubbata bi baha<span class="non-ugaritic-text">[</span>tīma<span class="non-ugaritic-text">]</span></td>
                        <td>Soll ich kein Fenster im Ha[us (Pl.)] einsetzen,</td>        
                        <td>P-O<sub>4</sub>-A</td>        
                        <td>a-b-c</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">62</span>&#160;ḥallāna bi qarbi hêkalīma</td>
                        <td>(keine) Fensteröffnung inmitten des Palastes (Pl.)?</td>
                        <td>O<sub>4</sub>-A</td>
                        <td>◌-b'-c'</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a&#8596;b&#8596;◌ // ◌&#8596;b'&#8596;c</summary>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-◌ // ◌-b-c (und Varianten; &#x2197; Terrassenparallelismus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.3 ii 23–24</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">23</span>&#160;maˀda timtaḫaṣa<span class="non-ugaritic-text">/</span>unna wa taˁînu</td>        
                        <td>Heftig kämpfte und schaute umher,</td>
                        <td>A-P ; P</td>
                        <td>a-b ; c-◌</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">24</span>&#160;tiḫtaṣab<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span> wa taḥdiyu ˁanatu</td>
                        <td>focht und und blickte umher die ˁAnatu.</td>
                        <td>P ; P-S</td>
                        <td>◌-b' ; c'-d</td></tr>
                <tr><th colspan="4">KTU 1.114 2c–4a</th></tr>
                    <tr>
                        <td>tilḥamūna <span class="ugaritic-text-line-number">3</span>&#160;ˀilūma wa tištûna</td>        
                        <td>Die Götter essen und trinken,</td>        
                        <td>P-S ; P</td>        
                        <td>a-b ; c-◌-◌</td></tr>
                    <tr>
                        <td>tištûna yê<span class="non-ugaritic-text">&lt;</span>na<span class="non-ugaritic-text">&gt;</span> ˁadê šubˁi</td>
                        <td>trinken Wein bis zur Sättigung,</td>
                        <td>P-O<sub>4</sub>-A</td>
                        <td>◌-◌ ; c-d-e</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">4</span>&#160;tê<span class="non-ugaritic-text">/</span>îrāṯa ˁadê<span class="non-ugaritic-text"><sup>?</sup></span> šukri</td>
                        <td>Most bis zur Trunkenheit.</td>
                        <td>O<sub>4</sub>-A</td>
                        <td>◌-◌ ; ◌-d'-e'</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">a-b-◌ // a-◌-c (und Varianten; &#x2197; Stufenparallelismus)</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.2 iv 8c–9</th></tr>
                    <tr>
                        <td>hitta ˀêbaka <span class="ugaritic-text-line-number">9</span>&#160;baˁluma</td>        
                        <td>Nun, deinen Gegner, Baˁlu,</td>
                        <td>A-O<sub>4</sub>-<i>Anr.</i></td>
                        <td>a-b-◌-c</td></tr>
                    <tr>
                        <td>hitta ˀêbaka timḫaṣ</td>
                        <td>nun schlag deinen Gegner nieder,</td>
                        <td>A-O<sub>4</sub>-P</td>
                        <td>a-b-d-◌</td></tr>
                    <tr>
                        <td>hitta tuṣammit ṣarrataka</td>
                        <td>nun vernichte deinen Feind!</td>
                        <td>A-P-O<sub>4</sub></td>
                        <td>a-d'-b'-◌</td></tr>
                <tr><th colspan="4">KTU 1.17 i 13b–15a</th></tr>
                    <tr>
                        <td>yâdî ṣûtahu<span class="non-ugaritic-text"><sup>?</sup></span> <span class="ugaritic-text-line-number">14</span>&#160;<span class="non-ugaritic-text">[</span>danī<span class="non-ugaritic-text">]</span>ˀilu</td>        
                        <td>Er legte ab sein Gewand, er, [Danī]ˀilu,</td>        
                        <td>P-O<sub>4</sub>-S</td>
                        <td>a-b-c ; ◌ ; ◌</td></tr>
                    <tr>
                        <td>yâdî ṣûtahu<span class="non-ugaritic-text"><sup>?</sup></span> yaˁlû wa yaški<span class="non-ugaritic-text">/</span>ub<span class="non-ugaritic-text">(</span>u<span class="non-ugaritic-text">)</span></td>    
                        <td>er legte ab sein Gewand, stieg hinauf und legte sich hin,</td>
                        <td>P-O<sub>4</sub> ; P ; <i>wa</i>-P</td>
                        <td>a-b-◌ ; d ; e</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">15</span>&#160;<span class="non-ugaritic-text">[</span>yâdî<span class="non-ugaritic-text">]</span> ma<span class="non-ugaritic-text">/</span>iˀzartêhu pa yalin<span class="non-ugaritic-text">(/</span>-înu<span class="non-ugaritic-text">)</span></td>
                        <td>[er legte ab] seinen Mantel und bettete sich.</td>        
                        <td>P-O<sub>4</sub> ; ◌ ; <i>p</i>-P</td>
                        <td>a-b'-◌ ; ◌ ; e</td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary">Weitere Formen</summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.18 iv 23b–24b</th></tr>
                    <tr>
                        <td>šupuk<span class="non-ugaritic-text">(/</span> šipik<span class="non-ugaritic-text">)</span> kama šVˀ<span class="non-ugaritic-text">(</span>V<span class="non-ugaritic-text">)</span>yi <span class="ugaritic-text-line-number">24</span>&#160;dama</td>        
                        <td>Vergieß wie ein Mörder (sein) Blut,</td>
                        <td>P-A<sub>1</sub>-O<sub>4</sub></td>  
                        <td>a-b-◌-d</td></tr>
                    <tr>
                        <td>kama šāḫiṭi<span class="non-ugaritic-text"><sup>?</sup></span> li birkêhu</td>
                        <td>wie ein Schlächter bis zu seinen Knien!</td>
                        <td>A<sub>1</sub>-A<sub>2</sub></td>
                        <td>◌-b'-c-◌</td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary"><u>a</u> ; <u>b</u> // <u>a</u>' ; <u>◌</u> (und Varianten)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.17 ii 12b–14a</th></tr>
                    <tr>
                        <td>ˀâṯibanna ˀanāku <span class="ugaritic-text-line-number">13</span>&#160;wa ˀanûḫanna</td>
                        <td>Ich will mich setzen, (dass) ich zur Ruhe komme,</td>
                        <td>P-S ; P</td>
                        <td><u>a</u> ; <u>b</u><sub>a</sub></td></tr>
                    <tr>
                        <td>wa tanuḫ bi ˀir<span class="non-ugaritic-text">(</span>a<span class="non-ugaritic-text">)</span>tiya <span class="ugaritic-text-line-number">14</span>&#160;napšu</td>
                        <td>ja, (dass) die Seele in meiner Brust zur Ruhe komme.</td>
                        <td>P-A-S</td>
                        <td><u>◌</u> ; <u>b</u><sub>a'(x-y-z)</sub></td></tr>
                <tr><th colspan="4">KTU 1.14 ii 32–34</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">32</span>&#160;ˁadānu nugiba wa yâṣiˀ</td>        
                        <td>Die Armee sei mit Proviant versorgt, dann ziehe sie aus,</td>
                        <td>S-P ; <i>wa</i>-P</td>
                        <td><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c</sub></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">33</span>&#160;ṣabaˀu ṣabaˀi nugiba</td>
                        <td>das Heer des Heeres sei mit Proviant ausgerüstet,</td>
                        <td>S-P</td>
                        <td><u>a</u><sub>a'-b</sub> ; <u>◌</u></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">34</span>&#160;wa yâṣiˀ ˁadānu maˁˁu</td>
                        <td>dann ziehe die gewaltige Armee aus!</td>
                        <td><i>wa</i>-P-S</td>
                        <td><u>◌</u> ; <u>b</u><sub>c-a'</sub></td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary"><u>a</u> ; <u>b</u> // <u>a</u>' ; <u>b</u>' (&#x2197; Alternation)</summary>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary"><u>a</u><sub>a-(b)</sub> ; <u>b</u><sub>c-(d)</sub> // <u>a</u>'<sub>a'-(b')</sub> ; <u>b</u>'<sub>c'-(d')</sub></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.19 iv 32–33</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">32</span>&#160;lV tabarriknī ˀa<span class="non-ugaritic-text">/</span>âlika barī<span class="non-ugaritic-text">/</span>ūkatu<span class="non-ugaritic-text">/</span>ama</td>
                        <td>Segne mich doch, (dass) ich gesegnet (hinfort)gehe,</td>        
                        <td>P<sup>+O&#8324;</sup> ; P-präd.Attr.(/ A)</td>
                        <td><u>a</u><sub>a</sub> ; <u>b</u><sub>c-d</sub></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">33</span>&#160;tamurrānī ˀa<span class="non-ugaritic-text">/</span>âlika namarratu<span class="non-ugaritic-text">/</span>ama</td>
                        <td>segne mich, (dass) ich gesegnet (hinfort)gehe!</td>
                        <td>P<sup>+O&#8324;</sup> ; P-präd.Attr.(/ A)</td>
                        <td><u>a</u>'<sub>a'</sub> ; <u>b</u>'<sub>c-d'</sub></td></tr>
            </table></details>
    <details class="exampleGUPF-ThirdOrder">
        <summary class="exampleGUPF-ThirdOrderSummary"><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c-(d)</sub> // <u>a</u>'<sub>◌-b'</sub> ; <u>b</u>'<sub>c'-(d')</sub></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.14 i 33–35a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">33</span>&#160;šinatu talˀûninnu<span class="non-ugaritic-text"><sup>!</sup></span> <span class="ugaritic-text-line-number">34</span>&#160;wa yiškab</td>
                        <td>Schlaf überwältigte ihn, da legte er sich nieder,</td>
                        <td>S-P<sup>+O&#8324;</sup> ; <i>wa</i>-P</td>        
                        <td><u>a</u><sub>a-b</sub> ; <u>b</u><sub>c</sub></td></tr>
                    <tr>
                        <td>nahamâmatu <span class="ugaritic-text-line-number">35</span>&#160;wa yaqmiṣ</td>
                        <td>Schlummer, da sank er nieder.</td>
                        <td>S ; <i>wa</i>-P</td>
                        <td><u>a</u>'<sub>a'-◌</sub> ; <u>b</u>'<sub>c'</sub></td></tr>
                <tr><th colspan="4">KTU 1.17 vi 26b–28a</th></tr>
                    <tr>
                        <td>ˀiriš ḥayyīma lV ˀaqhatu ġāziru</td>      
                        <td>Wünsch (dir) Leben, o ˀAqhatu, Held,</td>
                        <td>P-O<sub>4</sub>-<i>Anr.</i></td>
                        <td><u>a</u><sub>a-b-c</sub> ; <u>◌</u></td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">27</span>&#160;ˀiriš ḥayyīma wa ˀâtinaka</td>            
                        <td>wünsch (dir) Leben, und ich will es dir geben,</td>
                        <td>P-O<sub>4</sub> ; <i>wa</i>-P<sup>+O</sup></td>
                        <td><u>a</u>'<sub>a-b-◌</sub> ; <u>b</u>'<sub>d</sub></td></tr>
                    <tr>
                        <td>balî<span class="non-ugaritic-text">(-)</span>môta <span class="ugaritic-text-line-number">28</span>&#160;wa ˀašalliḥaka</td>
                        <td>Unsterblichkeit, und ich will sie dir überreichen!</td>
                        <td>O<sub>4</sub> ; <i>wa</i>-P<sup>+O</sup></td>
                        <td><u>a</u>''<sub>◌-b'-◌</sub> ; <u>b</u>''<sub>d</sub></td></tr>
                    <tr><td colspan="4" style="white-space: normal"><p class="exampleGUPF-Anm">Anm.: 2.–3. Kolon; zum 1.–2. Kolon &#x2197; Stufenparallelismus.</p></td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary"><u>a</u> // <u>b</u> (&#x2197; Konnexion)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.4 vii 23–25a</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">23</span>&#160;lV ragamtu laka lV ˀalˀi<span class="ugaritic-text-line-number">24</span>yānu baˁlu</td>
                        <td>Hab ich dir doch gesagt, o Mächtiger, Baˁlu,</td>
                        <td>P<sub>1</sub>-A<sub>1</sub>-<i>Anr.</i></td>
                        <td><u>a</u><sub>a-b-c</sub></td></tr>
                    <tr>
                        <td>taṯûbunna baˁlu <span class="ugaritic-text-line-number">25</span>&#160;li hawâtiya</td>
                        <td>(dass) du, Baˁlu, auf mein Wort zurückkommen wirst!</td>
                        <td>P<sub>2</sub>-<i>Anr.</i>-A<sub>2</sub></td>
                        <td><u>b</u><sub>d-c-e</sub></td></tr>
            </table></details>
</details>

<details class="exampleGUPF-SecondOrder">
<summary class="exampleGUPF-SecondOrderSummary">a- // b (&#x2197; Enjambement)</summary>
    <details class="exampleGUPF-ThirdOrder" open>
        <summary class="exampleGUPF-ThirdOrderSummary" style="display: none;"></summary>
            <table class="exampleGUPF-Table verseWithAnalysis">
                <tr><th colspan="4">KTU 1.6 i 56–57</th></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">56</span>&#160;ˀappūnaka ˁaṯtaru ˁarīẓu<span class="non-ugaritic-text"><sup>?</sup></span></td>        
                        <td>Sodann (ist) ˁAṯtaru, der Starke,</td>
                        <td>A<sub>1</sub>-S-</td>
                        <td>a-b-</td></tr>
                    <tr>
                        <td><span class="ugaritic-text-line-number">57</span>&#160;yaˁlû<span class="non-ugaritic-text">/</span>î bi ṢRR-āti ṣapāni</td>
                        <td>hinaufgestiegen auf die Höhen<sup>?</sup> des Zaphon.</td>
                        <td>P-A<sub>2</sub></td>
                        <td>c-d</td></tr>
            </table></details>
</details>
</details>
<p/>
</details>

<details class="doneEntryGUPF">
<summary class="doneEntryGUPFSummary">Versgliederung</summary>
<p>Die ugaritischen poetischen Texte gliedern sich in verschieden große Verseinheiten. Dazu zählen das &#x2197; Kolon, der &#x2197; Vers und die &#x2197; Strophe (daneben lassen sich größere strukturelle Einheiten identifizieren, die sich aus mehreren Strophen zusammensetzen). Verseinheiten, die sich aus mindestens zwei kleineren Verseinheiten zusammensetzen, werden zusammenfassend als Versgefüge bezeichnet.</p>
<p>Da die ugaritischen Schreiber die Grenzen zwischen Verseinheiten nicht immer graphisch kenntlich machten (etwa durch Zeilensprünge oder horizontale Trennlinien), erweist sich die Rekonstruktion der Versgliederung zuweilen als schwierig. Wichtige Indizien liefern:</p>
<ol type="a">
    <li>das Text- und Zeilenlayout einzelner Manuskripte (die Tafelzeile entspricht zuweilen einem Kolon; horizontale Trennlinien [sofern sie nicht das Ende der Kolumne / die untere Grenze des beschriebenen Bereichs der Tafelseite anzeigen] korrelieren meist mit Strophengrenzen);</li>
    <li style="margin-top: 12px;">bestimmte Lexeme und morphosyntaktische Spezifika (so treten beispielsweise die Lexeme <i>APNK</i> „danach, daraufhin“, <i>MK</i> „dann, schließlich“ und <i>DM</i> „denn; fürwahr“ vorrangig am Anfang des ersten Kolons des Verses auf, die vokativische Anrede oft am Ende des ersten Kolons des Verses und die Konjunktion / Partikel <i>W</i> „und; ja!“ immer wieder am Anfang des zweiten Kolons des Verses; Satzgrenzen stimmen oft mit Kolongrenzen überein, fast immer mit Versgrenzen und immer mit Strophengrenzen);</li>
    <li style="margin-top: 12px;">die Analyse paralleler Strukturen (aufeinanderfolgende Phrasen, Kola oder Verse, die parallel zueinander stehen, gehören i. d. R. zum selben übergeordneten Versgefüge);</li>
    <li style="margin-top: 12px;">die Zählung der Silben im Kolon (aufeinanderfolgende Kola und vor allem Kola, die zum selben Vers gehören, sind oft gleich oder ähnlich lang);</li>
    <li style="margin-top: 12px;">textlogische Überlegungen (Verseinheiten, in denen zwei Sachverhalte beschrieben sind, von denen der eine den anderen bedingt oder unmittelbar aus ihm hervorgeht, sind immer wieder <i>einem</i> übergeordneten Versgefüge zuzuordnen).</li>
</ol>
<p>Die Texte richtig zu phrasieren und die Versgliederung auf diese Weise „hörbar“ (i. e. auditiv erfahrbar) zu machen, war Aufgabe des Vortragenden, der die Texte rezitierte oder sang (in welchen Kontexten die Texte vorgetragen wurden, ist weitgehend unbekannt; es ist nicht auszuschließen, dass unterschiedliche Vortragende die Texte zuweilen unterschiedlich phrasierten; die rekonstruierte Versgliederung spiegelt also im besten Fall <i>eine</i> Möglichkeit wider, die Texte zu gliedern). Die prosodische Gestaltung der Texte (Rhythmus, Sprechtempo, unterschiedlich ausgeprägte Zäsuren etc.) zeigte dem Publikum, welche Phrasen zu einer strukturellen, grammatischen und inhaltlichen Einheit zu verbinden sind und welche voneinander abzugrenzen sind (die Versgliederung ist folglich entscheidend für das Textverständnis).</p>
<details class="exampleGUPF">
<summary>Beispiel</summary>
<table class="exampleGUPF-Table">
<tr><th colspan="2">KTU 1.14 iii 14b–19a</th></tr>
    <tr>
        <td>wa hanna šapšuma <span class="ugaritic-text-line-number">15</span>&#160;bi šābiˁi</td>
        <td>Und siehe, bei Sonnenuntergang, am siebten (Tag),</td></tr>
    <tr>
        <td class="further-colon-of-verse">wa lā yîšanu pabilu <span class="ugaritic-text-line-number">16</span>&#160;malku</td>
        <td class="further-colon-of-verse">da wird König Pabilu nicht schlafen können</td></tr>
    <tr>
        <td>li QR-i ṯaˀgati ˀibbīrīhu</td>
        <td>wegen des Lärms des Gebrülls seiner Stiere,</td></tr>
    <tr>
        <td class="further-colon-of-verse"><span class="ugaritic-text-line-number">17</span>&#160;li qâli nahaqati ḥimārīhu</td>
        <td class="further-colon-of-verse">wegen des Dröhnens des Geschreis seiner Esel,</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">18</span>&#160;li gaˁâti ˀalapī ḥarṯi</td>
        <td>wegen des Gebrülls der Pflugrinder,</td></tr>
    <tr>
        <td class="further-colon-of-verse">zaġâti <span class="ugaritic-text-line-number">19</span>&#160;kalabī ṢPR-i</td>
        <td class="further-colon-of-verse">(wegen) des Gebells der Wachhunde<sup>?</sup></td></tr>
    <tr><td colspan="2" style="white-space: normal">
        <p class="exampleGUPF-Anm">Anm.: Die Strophe setzt sich aus drei Bikola zusammen (i. e. aus drei Versen zu je zwei Kola). Die ersten beiden Kola formen einen Enjambement-Vers (&#x2197; Enjambement; der im ersten Kolon eingeleitete Satz ist im zweiten Kolon fortgesetzt; SG-Str.: <i>Interj.</i>-A<sub>1</sub>- // P-S).</p>
        <p class="exampleGUPF-Anm">Die Kola, die sich im zweiten und dritten Vers gegenüberstehen, sind jeweils semantisch und grammatisch parallel gestaltet (die parallel gestellten Kola sind jeweils grammatisch identisch aufgebaut; nur im zweiten Kolon des dritten Verses ist die Präposition <i>L</i> am Kolonanfang ausgelassen; &#x2197; Grammatische Varianz, und &#x2197; Ellipse):</p>
        <p class="exampleGUPF-Anm">
            Str. Vers 2: a<sub>„Lärm“</sub>-b<sub>„Gebrüll“</sub>-c<sub>„ein Tier“</sub> // a'-b'-c'<br/>
            Str. Vers 3: a<sub>„Gebrüll“</sub>-b<sub>„ein Tier“</sub>-c<sub>„Einsatzgebiet des Tieres“</sub> //  a'-b'-c'.</p>
        <p class="exampleGUPF-Anm">Gleichzeitig stehen der zweite und der dritte Vers - jeweils als Ganzes betrachtet - parallel zueinander. Die einzelnen Kola enthalten jeweils eine mit <i>L</i> eingeleitete Präpositionalphrase (Ausnahme: zweites Kolon des dritten Verses; s. o.), auf die zwei Genitivattribute folgen:</p>
        <p class="exampleGUPF-Anm">Str. Vers 2–3:<br/>
            a<sub>1-„Lärm des Gebrülls“</sub>-b<sub>1-„ein Tier“</sub> // a<sub>1</sub>'-b<sub>1</sub>'<br/>
            a<sub>2-„Gebrüll“</sub>-b<sub>2-„ein Tier in best. Einsatzgebiet“</sub> // a<sub>2</sub>'-b<sub>2</sub>'</p>
        <p class="exampleGUPF-Anm">Außerdem reimen sich die vier Kola des zweiten und dritten Verses; nicht zuletzt die Kola, die sich jeweils innerhalb eines Verses gegenüberstehen, sind sich klanglich ganz ähnlich (Vers 2: <b><i>li</i></b> // <b><i>li</i></b> - <i>ṯ</i><b><i>aˀgati</i></b> // <i>n</i><b><i>ah</i></b><i>a</i><b><i>qati</i></b> - <i>ˀ</i><b><i>i</i></b><i>bbī</i><b><i>rīhu</i></b> // <i>ḥ</i><b><i>i</i></b><i>mār</i><b><i>īhu</i></b>; Vers 3: <i>g</i><b><i>aˁâti</i></b> // <i>z</i><b><i>aġâti</i></b> - <i>ˀ</i><b><i>alapī</i></b> // <i>k</i><b><i>alabī</i></b>; Vers 2–3: <i>ṯ</i><b><i>aˀ</i></b><i>g</i><b><i>ati</i></b> // <i>n</i><b><i>ah</i></b><i>aq</i><b><i>ati</i></b> | <i>g</i><b><i>aˁâti</i></b> // <i>z</i><b><i>aġâti</i></b>; beachte außerdem <i>ṯ</i><b><i>aˀgati</i></b> | <b><i>gaˁâti</i></b>).</p>
        <p class="exampleGUPF-Anm">Der zweite und der dritte Vers sind durch Enjambement mit dem ersten Vers verknüpft (der zweite und der dritte Vers enthalten das Kausaladverbial des übergeordneten Satzes, der im ersten Vers beginnt; SG-Str. Strophe: <i>Interj.</i>-A<sub>1</sub>- // P-S- | A<sub>2</sub> // A<sub>2</sub> | A<sub>2</sub> // A<sub>2</sub>).</p></td></tr>
</table>
</details>
<p/>
</details>

<details class="undoneEntryGUPF">
<summary>Visuelle Poesie</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Vokabular</summary>
</details>

<h2>W</h2>

<details class="undoneEntryGUPF">
<summary>Wortpaar</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Wortstellung</summary>
</details>


<h2>Z</h2>

<details class="undoneEntryGUPF">
<summary>Zäsur</summary>
</details>

<details class="undoneEntryGUPF">
<summary>Zeugma</summary>
</details>

</div>


<input id="tab3" type="radio" name="tabs">
    <label class="EUPT-lab-article-tabs-label  EUPT-lab-article-tabs-last-label" for="tab3"><p>Bibliographie</p></label>
    <div class="EUPT-lab-article-tabs-content">

<h4>Abkürzungen und Sigel</h4>
    <p style="margin-top: 1em;">Die Abkürzungen und Sigel folgen EUPT. Die bibliographischen Abkürzungen sind <a href="/Abkuerzungen.html" target="_self">hier</a> zusammengestellt. Die editorischen Sigel sowie die Abkürzungen und Sigel zur grammatikalischen und versstrukturellen Analyse sind <a href="/Editorische-Prinzipien.html" target="_self">hier</a> erklärt.</p>

<h4>Literaturverzeichnis <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/tags/EUPT-Lab-1/collection" target="_blank"><img class="Bibliography-Logo-zotero" src="https://www.zotero.org/support/_media/logo/zotero_32x32x32.png"></a></h4>

<p class="GMU-bibliography-entry">Bacci, Tommaso, Tyler J. Harris, Jaeseok Heo, Dennis Pardee, und Alexis Wolf. 2024. „The First Tablet of the Ugaritic <i>Krt</i> Text, Column II, Lines 4–5: Reading and Reconstruction“. <i>Maarav</i> 28: 1–56.</p>

<p class="GMU-bibliography-entry">Braak, Ivo, und Martin Neubauer. 2001. <i>Poetik in Stichworten. Literaturwissenschaftliche Grundbegriffe. Eine Einführung</i>. 8., überarb. und erw. Aufl. Berlin / Stuttgart: Gebrüder Borntraeger.</p>

<p class="GMU-bibliography-entry">Dahood, Mitchell. 1969. „Ugaritic-Hebrew Syntax and Style“. <i>UF</i> 1: 15–36.</p>

<p class="GMU-bibliography-entry">Greenstein, Edward L. 1974. „Two Variations of Grammatical Parallelism in Canaanite Poetry and Their Psycholinguistic Background“. <i>JANES</i> 6: 87–105.</p>

<p class="GMU-bibliography-entry">Greenstein, Edward L. 1977. „One More Step on the Staircase“. <i>UF</i> 9: 77–86.</p>

<p class="GMU-bibliography-entry">Grimm, Gunter E. 2007. „Akrostichon“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 9. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Korpel, Marjo C. A. 1998. „Exegesis in the Work of Ilimilku of Ugarit“. In <i>Intertextuality in Ugarit and Israel</i>, herausgegeben von Johannes C. de Moor, 86–111. Oudtestamentische Studiën 40. Leiden / Boston / Köln: Brill.</p>

<p class="GMU-bibliography-entry">Loewenstamm, Samuel E. 1975. „The Expanded Colon, Reconsidered“. <i>UF</i> 7: 261–264.</p>


<p class="GMU-bibliography-entry">Miller, Cynthia L. 1999. „Patterns of Verbal Ellipsis in Ugaritic Poetry“. <i>UF</i> 31: 333–372.</p>

<p class="GMU-bibliography-entry">Moennighoff, Burkhard. 2007. „Rhetorische Frage“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 653. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">de Moor, Johannes C. 1980. „El, the Creator“. In <i>The Bible World. Essays in Honor of Cyrus H. Gordon</i>, herausgegeben von Gary Rendsburg, Ruth Adler, Milton Arfa und Nathan H. Winter, 171–187. New York: KTAV.</p>

<p class="GMU-bibliography-entry">Müller, Reinhard, und Clemens Steinberger. 2022: „‚Das Haus, das ich betrete, darfst du nicht betreten!‘ Zur Konzeption von Raum in den ugaritischen Beschwörungen“. In <i>Rituale und Magie in Ugarit. Praxis, Kontexte und Bedeutung</i>, herausgegeben von Reinhard Müller, Hans Neumann und Reettakaisa Sofia Salo, 37–129. ORA 47. Tübingen: Mohr Siebeck.</p>

<p class="GMU-bibliography-entry">Schweikle, Günther. 2007. „Periphrase“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 578. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Schweikle, Günther, Dieter Burdorf, und Burkhard Moennighoff. 2007a. „Kyklos“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 417. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Schweikle, Günther, Dieter Burdorf, und Burkhard Moennighoff. 2007b. „Oxymoron“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 563. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Schweikle, Günther, und Christian Schlösser. 2007. „Zeugma“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 842. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Steinberger, Clemens. 2022. „Die A // B | A<sup>(</sup>'<sup>)</sup> // B<sup>(</sup>'<sup>)</sup>-Strophe in der akkadischen und ugaritischen Poesie“. <i>AulaOr.</i> 40: 293–308.</p>

<p class="GMU-bibliography-entry">Steinberger, Clemens. 2024. „Versstruktur und Parallelismus. Untersuchungen zur akkadischen und ugaritischen Poesie der Spätbronzezeit“. Diss., Universität Jena.</p>

<p class="GMU-bibliography-entry">Steinhoff, Hans-Hugo. 2007. „Asyndeton“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 51. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Steinhoff, Hans-Hugo, und Dieter Burdorf. 2007. „Apokoinu“. In <i>Metzler Lexikon Literatur. Begriffe und Definitionen</i>, herausgegeben von Dieter Burdorf, Christoph Fasbender und Burkhard Moennighoff, 37. 3., völlig neu bearbeitete Auflage. Stuttgart / Weimar: J. B. Metzler.</p>

<p class="GMU-bibliography-entry">Watson, Wilfred G. E. 1986a. <i>Classical Hebrew Poetry. A Guide to its Techniques</i>. 2. Aufl. JSOT Suppl. Ser. 26. Sheffield: JSOT Press.</p>

<p class="GMU-bibliography-entry">Watson, Wilfred G. E. 1986b. „Antithesis in Ugaritic Verse“. <i>UF</i> 18: 413–419.</p>

<p class="GMU-bibliography-entry">Watson, Wilfred G. E. 2007. <i>Lexical Studies in Ugaritic</i>. AulaOr. Suppl. 19. Sabadell: Editorial Ausa.</p>

</div>

</div>