---
title: Publikationen der Ugarit-Forschungsstelle Göttingen
lang: de
layout: Layout
---

<h1>Publikationen der Ugarit-Forschungsstelle Göttingen</h1>

<p><b>Dönicke, Tillmann / Steinberger, Clemens / Zeterberg, Max-Ferdinand / Kröll, Noah:</b><br/>
2024: „<a href="https://aclanthology.org/2024.tlt-1.3/" target="_blank">A First Look at the Ugaritic Poetic Text Corpus</a>“. In D. Dakota / S. Jablotschkin / S. Kübler / H. Zinsmeister (Hg.): <i>Proceedings of the 22nd Workshop on Treebanks and Linguistic Theories (TLT 2024)</i>. Hamburg. S. 23–29.</p>

<p><b>Müller, Reinhard:</b><br/>
2023: „Dürre als Zeichen göttlichen Zorns. Ein alttestamentliches Motiv und seine kulturgeschichtlichen Hintergründe“. In A. Höfele / B. Kellner (Hg.): <i>Naturkatastrophen. Deutungsmuster vom Altertum bis in die Neuzeit</i>. Paderborn. S. 83–104.</p>

<p>2017: „The Origins of YHWH in Light of the Earliest Psalms“. In J. van Oorschot / M. Witte (Hg.): <i>The Origins of Yahwism</i>. BZAW 484. Berlin. S. 207–236.</p>

<p>2016: „‚Leben erbat er von dir, du hast es ihm gegeben …‘. Ps 21,5 im Licht einer ugaritischen Parallele“. In L. Hiepel / M.-T. Wacker (Hg.): <i>Zwischen Zion und Zaphon. Studien im Gedenken an den Theologen Oswald Loretz</i>. AOAT 438. Münster. S. 217–230.</p>

<p>2008: <i>Jahwe als Wettergott. Studien zur althebräischen Kultlyrik anhand ausgewählter Psalmen</i>. BZAW 387. Berlin / New York.</p>

<p><b>Müller, Reinhard / Levin, Christoph:</b><br/>
2024: <i>Die Psalmen in ihrer Urgestalt</i>. München.</p>

<p><b>Müller, Reinhard / Neumann, Hans / Salo, Reettakaisa Sofia (Hg.):</b><br/>
2022: <i>Rituale und Magie in Ugarit. Praxis, Kontexte und Bedeutung</i>. ORA 47. Tübingen.</p>

<p><b>Müller, Reinhard / Steinberger, Clemens:</b><br/>
2025 (im Druck): „Human and Divine Creation in the Ugaritic Poetic Texts“. In S. Anthonioz / S. Fink / K. Schnegg (Hg.): <i>Creating the Woman in Antiquity: Gender Aspects of Human Creation. Sixteenth Workshop of the Melammu Project, Innsbruck, 2–3 June 2022</i>. Melammu Workshops and Monographs. Münster.</p>
<!-- 
2024 (in Vorbereitung): „Poetry in Ugaritic Ritual and Cult“. In H. Niehr / O. Dyma / C. Nihan (Hg.): *The Ritual Texts of Ugarit in Their Archival Contexts*. Kasion. Münster.
-->
<p>2022: „‚Das Haus, das ich betrete, darfst du nicht betreten!‘ Zur Konzeption von Raum in den ugaritischen Beschwörungen“. In R. Müller / H. Neumann / R. S. Salo (Hg.): <i>Rituale und Magie in Ugarit. Praxis, Kontexte und Bedeutung</i>. ORA 47. Tübingen. S. 37–129.</p>

<p>2021: „<a href="https://www.academia.edu/45309171/M%C3%BCller_Reinhard_and_Clemens_Steinberger_Ein_himmlischer_Betrugsversuch_und_seine_Entlarvung_Edition_und_narratologische_Untersuchung_von_KTU_1_17_VI_Pages_123_173_in_Literaturkontakte_Ugarits_Edited_by_Ingo_Kottsieper_and_Hans_Neumann_Kasion_5_M%C3%BCnster_Zaphon_2021" target="_blank">Ein himmlischer Betrugsversuch und seine Entlarvung. Edition und narratologische Untersuchung von KTU 1.17 VI</a>“. In I. Kottsieper / H. Neumann (Hg.): <i>Literaturkontakte Ugarits. Wurzeln und Entfaltungen</i>. Kasion 5. Münster. S. 123–173.</p>

<p><b>Müller, Reinhard / Töyräänvuori, Joanna:</b><br/>
2024: „Comparative Approaches on Psalm 29“. In J. Jokiranta / M. Nissinen (Hg.): <i>Changes in Sacred Texts and Traditions. Methodological Encounters and Debates</i>. RBS 106. Atlanta. S. 409–462.</p>

<p><b>Steinberger, Clemens:</b><br/>
2025 (im Druck): „On Some Stylistic Features of <i>Ludlul bēl nēmeqi</i> I–II“. In U. Nõmmik / A. Johandi (Hg.): <i>Biblical Job in the Literary Network of the Ancient Near East</i>. Kasion. Münster.</p>

<p>2025 (im Druck): „Sprechende Namen und Beinamen in der mittelbabylonischen und ugaritischen Poesie“. AABNER.</p>

<p>2024: „Versstruktur und Parallelismus. Untersuchungen zur akkadischen und ugaritischen Poesie der Spätbronzezeit“. Unveröffentlichte Dissertation. Friedrich-Schiller-Universität Jena.</p>

<p>2022: „<a href="https://www.academia.edu/88336202/Das_Versmuster_A_B_A_B_in_der_akkadischen_und_ugaritischen_Poesie_JNSL_48_1" target="_blank">Das Versmuster A-B │ A<sup>(</sup>'<sup>)</sup>-B<sup>(</sup>'<sup>)</sup> in der akkadischen und ugaritischen Poesie</a>“. JNSL 48, 61–82.</p>

<p>2022: „<a href="https://www.academia.edu/91758480/Die_A_B_A_B_Strophe_in_der_akkadischen_und_ugaritischen_Poesie_AulaOr_40_2" target="_blank">Die A // B │ A<sup>(</sup>'<sup>)</sup> // B<sup>(</sup>'<sup>)</sup>-Strophe in der akkadischen und ugaritischen Poesie</a>“. AulaOr. 40, 293–308.</p>

<p>2022: „Zum Sitz im Leben von KTU 1.114. Eine Historiola samt Rezept gegen den Angriff des Yarḫu <i>ḥby</i>“. In R. Müller / H. Neumann / R. S. Salo (Hg.): <i>Rituale und Magie in Ugarit. Praxis, Kontexte und Bedeutung</i>. ORA 47. Tübingen. S. 131–159.</p>

<p>2022: „<a href="https://doi.org/10.25365/phaidra.365" target="_blank">Parallelism in Ugaritic Poetry</a>“. Project <a href="https://www.repac.at/research/" target="_blank">REPAC</a> (ERC Grant no. 803060), 2019–2024 (online).</p>

<p>2019: „Flucht und Flüchtige in Nordsyrien, dem Hethitischen Reich sowie dem Mittanni-Reich in der zweiten Hälfte des 2. Jts. v. Chr.“. In R. Rollinger / H. Stadler (Hg.): <i>7 Millionen Jahre Migrationsgeschichte. Annäherungen zwischen Archäologie, Geschichte und Philologie</i>. Innsbruck. S. 125–162.</p>

<p>2019: „Standard und Standardisierung ugaritischer Beschwörungsliteratur“. In M. Friesen / C. L. Hesse (Hg.): <i>Antike Kanonisierungsprozesse und Identitätsbildung in Zeiten des Umbruchs</i>. Wissenschaftliche Schriften der WWU Münster 10 / 28. Münster. S. 57–83.</p>

<p>2017: „RS 25.460: Ein Marduk-Hymnus zwischen Tradition und Innovation“. UF 48, 431–452.</p>

<p style="margin-top: 3em;"><b>Unveröffentlichte Arbeitsmaterialien, zusammengestellt von Clemens Steinberger (bis Mai 2024 auf dem <a href="https://uni-goettingen.de/de/431176.html" target="_blank"><b>Ugarit-Portal Göttingen</b></a> zugänglich):</b><br/>
2022: „Verbesserungen zu KTU&#179;“. Download: <a href="/assets/images-content/Steinberger_Verbesserungen_zu_KTU3.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_Verbesserungen_zu_KTU3.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.1“. Download: <a href="/assets/images-content/Steinberger_KTU_1.1.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.1.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.3“. Download: <a href="/assets/images-content/Steinberger_KTU_1.3.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.3.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.82“. Download: <a href="/assets/images-content/Steinberger_KTU_1.82.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.82.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.100“. Download: <a href="/assets/images-content/Steinberger_KTU_1.100.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.100.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.107“. Download: <a href="/assets/images-content/Steinberger_KTU_1.107.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.107.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.114“. Download: <a href="/assets/images-content/Steinberger_KTU_1.114.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.114.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>

<p>2022: „Die ugaritischen Texte in Umschrift: KTU 1.169“. Download: <a href="/assets/images-content/Steinberger_KTU_1.169.docx" target="_blank">.doc</a> / <a href="/assets/images-content/Steinberger_KTU_1.169.pdf" target="_blank">.pdf</a> (Stand: 16.02.2022).</p>