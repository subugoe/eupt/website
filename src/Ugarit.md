---
title: Ugarit in der Spätbronzezeit
lang: de
layout: Layout
---

# {{ $frontmatter.title }}

<div class="allTextPagesIncludingFigures">

<figure class="float-right">
    <img src="/assets/images-content/Foto_Ugarit_5.jpg" alt="Ras Shamra."/>
    <figcaption>Ras Shamra / Ugarit (<i>Royal Palace</i>). Foto: Dick Osseman, <a href="https://commons.wikimedia.org/wiki/File:Ugarit_Royal_Palace_reception_hall_with_pool_3915.jpg" target="_blank">Wikimedia Commons</a> (this file is licensed under a <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en" target="_blank">Creative Commons CC BY-SA 4.0 License</a>).</figcaption>
<br/>
    <img src="/assets/images-content/Stele_du_Baal_au_foudre_Ras_Shamra.jpg" alt="Stèle du Baal au foudre."/>
    <figcaption><i>Stèle du Baal au foudre</i>. Darstellung eines Gottes, vermutlich des Wetter- und Vegetationsgottes Baˤlu (Schutzgott Ugarits); 1932 in Ras Shamra entdeckt (RS 4.427; Louvre: AO 15775). Foto: <a href="https://commons.wikimedia.org/wiki/File:Baal_thunderbolt_Louvre_AO15775.jpg" target="_blank">Wikimedia Commons</a> (this file is licensed under a <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.en" target="_blank">Creative Commons CC0 1.0 Universal License</a>).</figcaption>
</figure>

<div class="mainTextPagesIncludingFigures">
<p style="margin-top: 0;">Das Königreich Ugarit, gelegen an der nordsyrischen Mittelmeerküste, erlebte seine größte Blüte in der Spätbronzezeit (14.–13. Jh. v. Chr.), bevor es zu Beginn des 12. Jhs. unterging. Das Herrschaftsgebiet von Ugarit umfasste weite Teile der heutigen syrischen Provinz Latakia. Die Hauptstadt lag auf dem Tell Ras Shamra, der wichtigste Hafen befand sich in Minet el-Beida; in Ras Ibn Hani hatte die ugaritische Königsfamilie eine weitere Residenz. (<a href="https://goo.gl/maps/rjerGacffjv2K5Pr8" target="_blank">Karte</a>)</p>

<p>Ugarit lag im Kraftfeld mehrerer Großmächte, die Nordsyrien in der Spätbronzezeit zu beherrschen suchten: Das ägyptische Neue Reich breitete sich entlang der Levanteküste aus; das hethitische Großreich drängte von Anatolien nach Syrien. Im 15. Jh. hatte außerdem das hurritisch geprägte Mittani-Reich großen Einfluss in Nordsyrien: Die mittanischen Könige kontrollierten große Teile Nordmesopotamiens und Nordsyriens, bevor Mittani zwischen dem mittelassyrischen und dem hethitischen Reich zerrieben wurde. Ob einst auch Ugarit zum Mittani-Reich gehörte, ist unklar. Die hurritische Überlieferung aus Ugarit zeigt jedoch, dass der mittanische Einfluss die ugaritische Kultur nachhaltig geprägt hat.</p>

<p>Nachdem die Pharaonen im 15. Jh. weite Teile der Levante erobert hatten, wurde Ugarit in der ersten Hälfte des 14. Jhs. zum ägyptischen Vasallenkönigtum. Als wenig später die ägyptische Vorherrschaft in Nordsyrien zu bröckeln begann, wechselte der ugaritische König die Seite: Um 1340 v. Chr. ging er einen Vasallenvertrag mit dem hethitischen König ein. Ugarit blieb bis zu seinem Untergang Vasallenstaat des hethitischen Großreiches. Kurz nach 1200 v. Chr. wurde Ugarit von unbekannten Eroberern verwüstet; diese gehörten wahrscheinlich zu den „Seevölkern“, von denen in ägyptischen Quellen die Rede ist. Das Königreich geriet in Vergessenheit. Erst 1928 wurden die Ruinen Ugarits zufällig wiederentdeckt; bereits im folgenden Jahr wurden die archäologischen Grabungen unter der Leitung von Claude F. A. Schaeffer aufgenommen.</p>

<p>Der politische Einfluss Ugarits war begrenzt. Das kleine Königreich war jedoch weithin für seinen Reichtum bekannt. Die wichtigste Einnahmequelle war der Fernhandel: Die Lage am Meer ermöglichte intensiven Schiffsverkehr, der bis nach Ägypten, Kleinasien, Zypern und Kreta bis in die Ägäis führte. Im Inland war Ugarit mit den Handelsstraßen verbunden, die das nördliche Syrien durchquerten. Besonders zu Mesopotamien bestanden enge Beziehungen.</p>

<p>Ugarit war von verschiedenen Kulturen und Ethnien geprägt. Neben einer semitisch-sprachigen Bevölkerungsgruppe gab es eine große hurritisch-sprachige Minderheit. Ferner hielten sich hethitische, zypriotische und ägyptische Händler sowie assyrische Schreiber und Gelehrte in der Hafenstadt auf. Die Melange indigener und auswärtiger Traditionen hat die Kultur des Kleinkönigtums maßgeblich bestimmt. Die ugaritische Überlieferung ist in lokalen, levantinischen Traditionen verwurzelt, zugleich aber in hohem Maß von den Nachbarkulturen beeinflusst.</p>

<p>Davon zeugt auch die ugaritische Schriftkultur (<a href="https://uchicago.maps.arcgis.com/apps/webappviewer/index.html?id=8bc5cbfe1d13492aa8afe9c9bf2aee4c" target="_blank">RSTI Web Map</a>): Die ugaritische Keilalphabetschrift, die im 13. Jh. entwickelt wurde, verbindet die aufkeimende syrisch-palästinische Alphabettradition mit dem prestigeträchtigen Schriftbild der mesopotamischen Keilschrift. Texte verschiedener Gattungen und Genres wurden in Keilalphabetschrift notiert: Wirtschaftstexte, Rechtsurkunden, Briefe, medizinische und rituelle Anleitungen, Opferlisten, Omina, Gebete, Beschwörungen und eine Reihe epischer Gedichte. Auch außerhalb Ugarits wurden einzelne Tafeln in Keilalphabetschrift und ähnlichen Schriftformen entdeckt. (<a href="https://goo.gl/maps/yAQUPkjtmVdPRviR6" target="_blank">Karte</a>)</p>

<p>Die keilalphabetischen Texte in ugaritischer Sprache bilden allerdings nur einen Teil der schriftlichen Überlieferung. Aus Ugarit stammen zahlreiche sumerische und akkadische Texte in syllabischer Keilschrift. Daneben traten mehrere hurritische Texte zutage, die zum Teil in Keilalphabetschrift, zum Teil auch in syllabischer Keilschrift aufgezeichnet wurden. Dazu kommen einige hethitische Texte in syllabischer Keilschrift sowie einzelne Zeugnisse in luwischer und ägyptischer Hieroglyphenschrift. Außerdem wurden in Ugarit Tafeln in kypro-minoischer Schrift gefunden; diese Schrift und die zugrundeliegende Sprache sind bislang allerdings nicht vollständig entschlüsselt.</p>

<p>Die sumerisch-akkadische Überlieferung ist von besonderer Bedeutung: Akkadisch war in der 2. Hälfte des 2. Jts. die <i>lingua franca</i>, die internationale Verkehrssprache. Gleichzeitig erfreute sich die akkadische ebenso wie die sumerische Literatur großer Beliebtheit im Vorderen Orient. Neben sumerischen und akkadischen Weisheitstexten, Beschwörungen und Gebeten entstanden in Ugarit auch Exzerpte der Epen über Gilgameš und Atra-ḫasīs. Die ugaritischen Schreiber beschäftigten sich mit der fremdsprachigen Überlieferung, sie legten mehrsprachige Listen an und übersetzten einzelne Texte. Inhaltliche und strukturelle Parallelen zwischen der mesopotamischen und der ugaritischen Poesie zeigen, dass die anderssprachigen Korpora den Dichtern in Ugarit als Inspirationsquell dienten.</p>

</div><!--End of mainText-->
</div><!--End of allText-->