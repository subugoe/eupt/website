---
title: Team / Kontakt
lang: de
layout: Layout
---

<div id="EUPT-TeamKontakt">
    <aside id="EUPT-TeamKontaktLinkeSpalte"> 
    <h1>Team</h1>
		<p><b>Ugaritistik</b></p>
		<div class="Absatz-hängend"><a href="https://www.uni-goettingen.de/de/prof.+dr.+reinhard+m%C3%BCller/56732.html" target="_blank">Prof. Dr. Reinhard Müller</a> (Projektleiter)</div>
        <a href="https://www.uni-goettingen.de/de/clemens+steinberger+/219789.html" target="_blank">Clemens Steinberger</a> (Koordinator)<br/>
        <a href="https://www.uni-goettingen.de/en/68717.html" target="_blank">Noah Kröll</a> (wiss. Mitarbeiter)<br/>
        Janna Bösenberg (stud. Hilfskraft)<br/>
        Wiebke Martens (stud. Hilfskraft)<br/>
        Christiane Meinhold (stud. Hilfskraft)<br/>
        Marion Possiel (stud. Hilfskraft)<br/>
        Frilla Smilgies (stud. Hilfskraft)
        <p><b>Data Science</b></p>
        <a href="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/tillmann-doenicke/" target="_blank">Tillmann Dönicke</a><br/>
        <a href="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/michelle-weidling/" target="_blank">Michelle Weidling</a><br/>
        <a href="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/max-ferdinand-zeterberg/" target="_blank">Max-Ferdinand Zeterberg</a></aside>
    <aside id="EUPT-TeamKontaktRechteSpalte">
        <h1>Kontakt</h1>
        <p><b>Ugarit-Forschungsstelle Göttingen</b></p>
        Theologische Fakultät<br/>
        Platz der Göttinger Sieben 2<br/>
        D-37073 Göttingen<br/>
        <a href="https://lageplan.uni-goettingen.de/?ident=5276_1_1.OG_1.120" target="_blank">Raum 1.120</a>  
        <p/>
        <div class="Absatz-hängend">Tel: +49 551 39 26282  (N. Kröll / C. Steinberger)</div>
        E-Mail: <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a><br/>
        <a href="https://uni-goettingen.de/de/431176.html" target="_blank">Ugarit-Portal Göttingen</a>
</aside>
</div>