---
title: Texte, Tafeln, Forschung
lang: de
layout: Layout
---

<h1 class="h1-in-tab-layout" id="h1-Das-ugaritische-poetische-Textkorpus"></h1>

<div class="EUPT-RSTI-Konkordanz-tabs">

<!--Tab 1: Texte-->
<input id="tab1" type="radio" name="tabs" checked="checked">
<label class="EUPT-RSTI-Konkordanz-tabs-label" for="tab1"><p>Texte</p></label>
<div class="EUPT-RSTI-Konkordanz-tabs-content"> 

<p>Als Hauptwerke der ugaritischen Poesie gelten heute die Gedichte über Baˁlu, Kirtu und ˀAqhatu, die Ende des 13. Jh. v. Chr. von dem Gelehrten ˀIlimilku aufgezeichnet wurden. Daneben sind mehrere Tafeln erhalten, auf denen kürzere Mythen, Gebete, Rituale, Beschwörungen und Historiolae in poetischer Form überliefert sind.</p>
<p>Die folgende Liste gibt einen Überblick über das ugaritische poetische Korpus. Unter jedem Eintrag in grüner Schrift findet sich eine knappe Einleitung zum Text (die Edition der Texte findet sich <a href="/edition.html">hier</a>). Der Katalog wird fortlaufend ergänzt und verbessert.</p>

<p style="margin-bottom: 2em;"><button class="EUPT-html-button" onclick="document.querySelectorAll('details.doneTexteTextuebersicht').forEach((e) => {(e.hasAttribute('open')) ? e.removeAttribute('open') : e.setAttribute('open',true)})">Alle Einträge aus-/einklappen</button></p>

<details class="doneTexteTextuebersicht">
<summary class="doneTexteTextuebersichtSummary">KTU 1.1–6: Das Baˁlu-Epos</summary>
<p>Das Baˁlu-Epos (KTU 1.1–6), mit sechs Tafeln das längste ugaritische Werk, erzählt von den wechselhaften Schicksalen Baˁlus und seiner bedrohten Stellung im Pantheon: Der Gott muss sein Königtum im Kampf gegen den Meeresgott Yammu behaupten und beim Göttervater ˀIlu gegen manche Widerstände die Erlaubnis zum Bau eines Palasts erwirken. Bei der Einweihung kommt es zum Streit mit dem Todesgott Môtu, der den Wettergott im Zorn verschlingt. Der Göttin ˁAnatu, Baˁlus Schwester und Geliebten, gelingt es aber, Baˁlu aus dem Totenreich zurückzuholen. Dieser tötet schließlich den Gott des Todes.</p>
<p>Die Bearbeitungen der Tafeln des Baˁlu-Epos werden zwischen 2026 und 2029 sukzessive veröffentlicht.</p>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.7</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.9</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.10</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.11</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.12</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.13</summary>
</details>

<details class="doneTexteTextuebersicht">
<summary class="doneTexteTextuebersichtSummary">KTU 1.14–16: Das Kirtu-Epos</summary>
<p><i>König Kirtu ist verzweifelt. Nachdem ihm keine seiner Ehefrauen einen Nachkommen schenkte, steht seine Dynastie vor dem Aus. Als er sich in den Schlaf weint, erscheint ihm ˀIlu, der König der Götter und „Vater der Menschheit“, und führt ihm des Dilemmas Lösung vor Augen: Kirtu solle mit einem Heer zur Stadt ˀUdumu ziehen und Ḥurriya, die Tochter des dortigen Königs, fordern. Geträumt, getan. Kirtu marschiert nach ˀUdumu. Auf dem Weg gelobt er der Göttin ˀAṯiratu Gold und Silber, falls der Feldzug glückt und er Ḥurriya gewinnt. Nach siebentägiger Belagerung übergibt Pabilu, der König von ˀUdumu, seine Tochter an Kirtu. Gemeinsam mit Ḥurriya kehrt Kirtu zurück in sein Königreich. Jahre vergehen, und Kirtu und Ḥurriya bekommen viele Söhne und Töchter. Kirtus Nachfolge ist gesichert, sein Gelübde gegenüber ˀAṯiratu scheint er jedoch vergessen zu <span class="noBreakEUPT">haben ...</span></i></p>
<p>Das Kirtu-Epos (KTU 1.14–16) ist eine der drei längsten literarischen Kompositionen in ugaritischer Sprache und Schrift, von denen wir Kenntnis haben. Das Werk liegt in drei Tafeln vor, die vorder- und rückseitig in je drei Kolumnen beschrieben sind. Die erste Tafel ist nahezu vollständig erhalten, von den zwei weiteren fehlt jeweils knapp die Hälfte. Gleichwohl lässt sich die Handlung des Werks recht klar nachvollziehen. Als Kirtu nach einer Reihe von Schicksalsschlägen ohne Nachkommen dasteht, sucht er, seine Dynastie zu retten. Mit göttlicher Hilfe gelingt es ihm, eine neue Familie zu gründen. Sowohl die Ambivalenz, mit der diese Familie gezeichnet wird, als auch die Schilderung des Kirtu als eines Menschen, der sich im Spannungsfeld zwischen göttlichem Wohlwollen und selbstverschuldeter Strafe abarbeitet, offenbaren ein Werk von weltliterarischem Rang.</p>
<p>Zur Zeit ist die Edition der ersten Tafel (KTU 1.14) auf dem EUPT-Portal zugänglich (zur <a href="/edition.html">Edition</a>). Die Bearbeitungen der übrigen Tafeln (KTU 1.15–16) werden 2025 sukzessive veröffentlicht.</p>

<details class="dramatis-personae">
<summary>Dramatis personae</summary>

<p class="Absatz-hängend"><b><i>Kirtu</i>:</b> König von Bêtu-Ḫabūri. Auf Anraten ˀIlus belagert er die Stadt ˀUdumu und zwingt deren König, ihm die Prinzessin Ḥurriya zur Frau zu geben. Mit ihr gründet er eine Familie, erkrankt dann aber aufgrund eines unerfüllten Gelübdes.</p>
<p class="Absatz-hängend"><b><i>ˀIlu</i>:</b> Höchster Gott des ugaritischen Pantheons und wohlmeinender Beschützer Kirtus. Er erscheint Kirtu im Traum und weist ihm den Weg zu Ḥurriya, segnet die beiden beim Hochzeitsbankett und veranlasst, dass Kirtu von seiner Krankheit geheilt wird.</p>
<p class="Absatz-hängend"><b><i>Pabilu</i>:</b> König von ˀUdumu. Er übergibt seine Tochter Ḥurriya an Kirtu, um diesen zum Abzug zu bewegen.</p>
<p class="Absatz-hängend"><b><i>Pabilus Boten</i>:</b> Während Kirtu vor ˀUdumu lagert, überbringen ihm zwei Boten Pabilus ein Friedensangebot.</p>
<p class="Absatz-hängend"><b><i>Ḥurriya</i>:</b> Tochter des Pabilu und Ehefrau Kirtus. Sie schenkt Kirtu mehrere Töchter und Söhne.</p>
<p class="Absatz-hängend"><b><i>ˀAṯiratu</i>:</b> Gefährtin von ˀIlu. Wird als Göttin von Tyros und Sidon eingeführt. Kirtu gelobt, ihr Silber und Gold darzubringen, wenn er Hurriya als Ehefrau gewinnen kann.</p>
<p class="Absatz-hängend"><b><i>Yaṣṣubu</i>:</b> Sohn des Kirtu und der Ḥurriya. Er wird im Segen ˀIlus am Hochzeitsbankett dem Kirtu verheißen. Am Ende des erhaltenen Texts fordert Yaṣṣubu den Thron und wird daraufhin von Kirtu verflucht.</p>
<p class="Absatz-hängend"><b><i>ˀIluḥaˀu</i>:</b> Sohn des Kirtu und der Ḥurriya. Er beklagt seines Vaters Krankheit und sucht anschließend seine Schwester Ṯitmanatu auf.</p>
<p class="Absatz-hängend"><b><i>Ṯitmanatu</i>:</b> Jüngste Tochter des Kirtu und der Ḥurriya. Sie klagt um ihren Vater, als sie von seiner Krankheit erfährt.</p>
<p class="Absatz-hängend"><b><i>ˀIlšu und seine Frau</i>:</b> Götterboten.</p>
<p class="Absatz-hängend"><b><i>Šaˁtiqatu</i>:</b> Heilgöttin, von ˀIlu aus Lehm geformt, um Kirtu zu heilen.</p>

</details>
<p/>
</details>

<details class="doneTexteTextuebersicht">
<summary class="doneTexteTextuebersichtSummary">KTU 1.17–19: Das ˀAqhatu-Epos</summary>
<p>Das ˀAqhatu-Epos (KTU 1.17–19) ist auf drei Tafeln überliefert und handelt von Danīˀilu und seinem Sohn ˀAqhatu: Danīˀilu ist lange Zeit kinderlos, bevor die Götter ihm einen Sohn schenken: den „Helden“ ˀAqhatu. Das Glück ist nicht von Dauer: Als ˀAqhatu die Göttin ˁAnatu in jugendlichem Übermut beleidigt, lässt die erzürnte Göttin ihn ermorden. Sein Vater Danīˀilu bestattet und betrauert ihn und verflucht die Städte, die bei der Ermordung seines Sohns geholfen hatten. ˀAqhatus Schwester plant, den Bruder zu rächen.</p>
<p>Die Bearbeitungen der Tafeln des ˀAqhatu-Epos werden 2025 und 2026 sukzessive veröffentlicht.</p>
</details>

<details class="doneTexteTextuebersicht">
<summary class="doneTexteTextuebersichtSummary">KTU 1.20–22: Die Rāpiˀūma-Fragmente</summary>
<p>Die Rāpiˀūma-Fragmente sind drei nicht direkt aneinander anschließende Tafelbruchstücke (KTU 1.20–22), die wohl zu einer oder zwei Tafeln gehörten. Geht man von einer Tafel aus, ist vom ursprünglichen Text nur ein gutes Sechstel erhalten. Da auch nicht völlig sicher ist, wie die Fragmente anzuordnen sind, fällt es schwer, eine zusammenhängende Handlung zu erkennen. Am Anfang eines Fragments spricht einer der Akteure. Möglicherweise verheißt er dem, an den er sich wendet, einen Sohn. Zu dem Gespräch scheint es am Rande eines Banketts gekommen zu sein, das am Libanon, wohl im Festhaus des ˀIlu, stattfindet. Die Rāpiˀūma, die vergöttlichten Ahnen, tafeln dort gemeinsam mit den Göttern. Später wird Danīˀlu erwähnt, der aus dem ˀAqhatu-Epos bekannt ist. Er scheint die Rāpiˀūma zu einem Gelage einzuladen.</p>
<p>Die Erwähnung des Danīˀlu und eines Sohnes weisen auf eine inhaltliche Verbindung zwischen den Rāpiˀūma-Fragmenten und dem ˀAqhatu-Epos hin. Die genaue Beziehung zwischen den beiden Texten bleibt jedoch unklar.</p>
<p>Die Bearbeitung der Rāpiˀūma-Fragmente wird 2025 / 2026 veröffentlicht.</p>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.23</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.24</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.25</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.40</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.45</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.55</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.61</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.62</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.63</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.65</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.82</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.83</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.88</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.89</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.92</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.93</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.94</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.95</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.96</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.98</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.100</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.101</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.107</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.108</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.113</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.114</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.117</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.119</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.124</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.129</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.133</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.147</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.151</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.152</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.157</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.158</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.159</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.160</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.161</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.166</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.167</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.169</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.172</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.176</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.178</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.179</summary>
</details>

<details class="undoneTexteTextuebersicht">
<summary>KTU 1.180</summary>
</details>


</div>




<!--Tab 2: Konkordanz-->
<input id="tab2" type="radio" name="tabs">
<label class="EUPT-RSTI-Konkordanz-tabs-label" for="tab2"><p>Tafelkonkordanz</p></label>
<div class="EUPT-RSTI-Konkordanz-tabs-content">

<base target="_blank">
<div class="EUPT-RSTI-Konkordanz">
<table class="EUPT-RSTI-KonkordanzTable">
<!--Ab hier neue Tabelle einfügen (<table> am Anfang und Ende der eingefügten Datei jeweils löschen):-->
<thead>
  <tr>
    <th>Fundnummer</th>
    <th>Museumsnummer</th>
    <th>KTU</th>
    <th>CTA</th>
    <th>Moderner Titel</th>
    <th>RSTI-Link / Fundort</th>
  </tr></thead>
<tbody>
  <tr>
    <td><span class=sort-id>1020003</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>RS 2.[003] +</summary>
        RS 2.[003] + 3.324 + 3.344 + 3.414</details></td>
    <td><span class=sort-id>18218</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M8218</summary> 
        M8218 = A2750 (AO 17.190)</details></td>
    <td><span class=sort-id>1014</span>KTU 1.14</td>
    <td><span class=sort-id>014</span>CTA 14</td>
    <td><i>Kirtu</i> I (Kirtu- / Kirta-Epos I)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/36caa276-991b-4073-a375-d8b7ba2ab4b7">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030343</span>RS 3.343 + 3.345</td>
    <td><span class=sort-id>18227</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M8227</summary> 
        M8227 = A2752 (AO 17.327)</details></td>
    <td><span class=sort-id>1015</span>KTU 1.15</td>
    <td><span class=sort-id>015</span>CTA 15</td>
    <td><i>Kirtu</i> II (Kirtu- / Kirta-Epos II)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/f28bb85b-16c5-4132-84f4-e123ca5746fc">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030325</span>RS 3.325 + 3.342 + 3.408</td>
    <td><span class=sort-id>13392</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3392</summary> 
        M3392 = A2751 (AO 17.326)</details></td>
    <td><span class=sort-id>1016</span>KTU 1.16</td>
    <td><span class=sort-id>016</span>CTA 16</td>
    <td><i>Kirtu</i> III (Kirtu- / Kirta-Epos III)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/03100f8d-b61e-4696-84b5-bbcfcfb6f3ee">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020004</span>RS 2.[004]</td>
    <td><span class=sort-id>317324</span>AO 17.324</td>
    <td><span class=sort-id>1017</span>KTU 1.17</td>
    <td><span class=sort-id>017</span>CTA 17</td>
    <td><i>ˀAqhatu</i> I (Aqhat-Epos I)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/9758a6ad-0557-4404-88d9-9fef4086cac9">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030340</span>RS 3.340</td>
    <td><span class=sort-id>317325</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>AO 17.325</summary> 
        AO 17.325 (En dépôt au British Museum: L84)</details></td>
    <td><span class=sort-id>1018</span>KTU 1.18</td>
    <td><span class=sort-id>018</span>CTA 18</td>
    <td><i>ˀAqhatu</i> II (Aqhat-Epos II)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/f185a47f-1c50-4ab6-8e1b-39b15eaaa976">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030322</span>RS 3.322 + 3.349 + 3.366</td>
    <td><span class=sort-id>317323</span>AO 17.323</td>
    <td><span class=sort-id>1019</span>KTU 1.19</td>
    <td><span class=sort-id>019</span>CTA 19</td>
    <td><i>ˀAqhatu</i> III (Aqhat-Epos III)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/9e0cf3a4-f4d6-4f5f-82fa-7e9e7f9da903">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030348</span>RS 3.348</td>
    <td><span class=sort-id>317321</span>AO 17.321</td>
    <td><span class=sort-id>1020</span>KTU 1.20</td>
    <td><span class=sort-id>020</span>CTA 20</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/df0a9c2b-6c8f-4d9c-8d24-efcec4a297dd">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020019</span>RS 2.[019]</td>
    <td><span class=sort-id>13350</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3350</summary> 
        M3350 = A2735 (AO 16.648)</details></td>
    <td><span class=sort-id>1021</span>KTU 1.21</td>
    <td><span class=sort-id>021</span>CTA 21</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/76a50953-e63d-44e1-b13c-afffe886cbb8">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020024</span>RS 2.[024]</td>
    <td><span class=sort-id>13351</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3351</summary> 
        M3351 = A2736 (AO 16.647)</details></td>
    <td><span class=sort-id>1022</span>KTU 1.22</td>
    <td><span class=sort-id>022</span>CTA 22</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/2efb636f-ef36-44fa-8608-ba16957c54cf">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030361</span>RS 3.361</td>
    <td><span class=sort-id>316643</span>AO 16.643</td>
    <td><span class=sort-id>1001</span>KTU 1.1</td>
    <td><span class=sort-id>001</span>CTA 1</td>
    <td><i>Baˁlu</i> I (Baal-Zyklus I)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/0c613cbe-753b-489b-8d74-58024a787806">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030367</span>RS 3.367 (+) 3.346</td>
    <td><span class=sort-id>316640</span>AO 16.640 (+) 16.640 bis</td>
    <td><span class=sort-id>1002</span>KTU 1.2</td>
    <td><span class=sort-id>002</span>CTA 2</td>
    <td><i>Baˁlu</i> II (Baal-Zyklus II)</td>
    <td><details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>Ras Shamra &gt; Acropolis &gt; House of the High Priest</summary>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/172a3413-6438-4e53-9629-34f86a0e1fd0">RS 3.367</a><br/>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/0e0105bb-37e8-488c-b882-aae0284d8365">RS 3.346</a></details></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020014</span>RS 2.[014] + 3.363 + 3.364</td>
    <td><span class=sort-id>13352</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3352 +</summary>
        M3352 = A2737 + M8217 = A2749 (AO 16.639 + 16.638) + M3353 = A2738 (AO 16.645 = RS 3.364) 
        </details>
    </td>
    <td><span class=sort-id>1003</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>KTU 1.3</summary>
        KTU 1.3 (+ KTU&#178; 1.8 = RS 3.364)</details></td>
    <td><span class=sort-id>003</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>CTA 3 +</summary>
        CTA 3 + 8 (CTA 8 = RS 3.364)</details></td>
    <td><i>Baˁlu</i> III (Baal-Zyklus III)</td>
    <td>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>Ras Shamra &gt; Acropolis &gt; House of the High Priest</summary>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/51bf8125-39e3-485c-810c-ee092e1010a9">RS 2.[014] + 3.363</a><br/>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/3ac14cd1-a428-47e1-9696-bc316daafe8a">RS 3.364</a></details></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020008</span>RS 2.[008] + 3.341 + 3.347</td>
    <td><span class=sort-id>18221</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M8221</summary>
        M8221 = A2777 (AO 16.637)</details></td>
    <td><span class=sort-id>1004</span>KTU 1.4</td>
    <td><span class=sort-id>004</span>CTA 4</td>
    <td><i>Baˁlu</i> IV (Baal-Zyklus IV)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/c26101da-b4a0-4760-8223-85994c5748a5">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020022</span>RS 2.[022] + 3.[565]</td>
    <td><span class=sort-id>316641</span>AO 16.641 + 16.642</td>
    <td><span class=sort-id>1005</span>KTU 1.5</td>
    <td><span class=sort-id>005</span>CTA 5</td>
    <td><i>Baˁlu</i> V (Baal-Zyklus V)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/35cb36a7-5bb8-4601-ab40-d11f46328285">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020009</span>RS 2.[009] + 5.155</td>
    <td><span class=sort-id>316636</span>AO 16.636</td>
    <td><span class=sort-id>1006</span>KTU 1.6</td>
    <td><span class=sort-id>006</span>CTA 6</td>
    <td><i>Baˁlu</i> VI (Baal-Zyklus VI)</td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/01b956e9-2ad0-434a-928e-a4f49c4a6b83">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1050180</span>RS 5.180 + 5.198</td>
    <td><span class=sort-id>18498</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M8498 +</summary> 
        M8498 = A2798 + M3373 = A2799</details></td>
    <td><span class=sort-id>1007</span>KTU 1.7</td>
    <td><span class=sort-id>007</span>CTA 7</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/eb19c8eb-8f3e-42fb-ba7c-15ff8c01ee27">Ras Shamra &gt; Acropolis &gt; Tomb IV</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1050229</span>RS 5.229</td>
    <td><span class=sort-id>317293</span>AO 17.293</td>
    <td><span class=sort-id>1009</span>KTU 1.9</td>
    <td><span class=sort-id>009</span>CTA 9</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/9ed4f25f-1418-493d-94e7-d6a116e8afe5">Ras Shamra &gt; Acropolis &gt; Tomb IV</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030362</span>RS 3.362 + 5.181</td>
    <td><span class=sort-id>13391</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3391</summary>
        M3391 = A2740 (AO 16.644)</details></td>
    <td><span class=sort-id>1010</span>KTU 1.10</td>
    <td><span class=sort-id>010</span>CTA 10</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/2273a7f5-6dec-4173-b7ff-6067f953d43a">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030319</span>RS 3.319</td>
    <td><span class=sort-id>13354</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3354</summary> 
        M3354 = A2739 (AO 16.646)</details></td>
    <td><span class=sort-id>1011</span>KTU 1.11</td>
    <td><span class=sort-id>011</span>CTA 11</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/bb54b248-716d-4387-b343-628364637ee8">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020012</span>RS 2.[012]</td>
    <td><span class=sort-id>317319</span>AO 17.319</td>
    <td><span class=sort-id>1012</span>KTU 1.12</td>
    <td><span class=sort-id>012</span>CTA 12</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/abe4b958-081e-4750-816f-5cd31b5b2862">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1010006</span>RS 1.006</td>
    <td><span class=sort-id>13355</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3355</summary>
        M3355 = A2741 (AO 11.994)</details></td>
    <td><span class=sort-id>1013</span>KTU 1.13</td>
    <td><span class=sort-id>013</span>CTA 13</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/023df9af-7a60-4f65-be5e-2d7b1d02e3cd">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020002</span>RS 2.002</td>
    <td><span class=sort-id>317189</span>AO 17.189</td>
    <td><span class=sort-id>1023</span>KTU 1.23</td>
    <td><span class=sort-id>023</span>CTA 23</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/4ea95ca7-f2b2-404b-8365-305212e97599">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1050194</span>RS 5.194</td>
    <td><span class=sort-id>319995</span>AO 19.995</td>
    <td><span class=sort-id>1024</span>KTU 1.24</td>
    <td><span class=sort-id>024</span>CTA 24</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/779bea53-0a71-492c-a74e-2813ec337c47">Ras Shamra &gt; Acropolis &gt; Tomb IV</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1050259</span>RS 5.259</td>
    <td><span class=sort-id>13336</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3336</summary> 
        M3336 = A2716 (AO 17.297)</details></td>
    <td><span class=sort-id>1025</span>KTU 1.25</td>
    <td><span class=sort-id>025</span>CTA 25</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/099c4d1b-a3ea-4fd6-8eee-9c47d31ec706">Ras Shamra &gt; Acropolis &gt; Tomb IV</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1010002</span>RS 1.002 + 1.002 [A]</td>
    <td><span class=sort-id>311993</span>AO 11.993</td>
    <td><span class=sort-id>1040</span>KTU 1.40</td>
    <td><span class=sort-id>032</span>CTA 32</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/a5267b3a-e5f5-438e-8615-1e1bcea06a23">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1010008</span>RS 1.008 + 1.031</td>
    <td><span class=sort-id>13358</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3358</summary> 
        M3358 = A2747 (AO 12.034 + 12.025)</details></td>
    <td><span class=sort-id>1045</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>KTU 1.45</summary>
        KTU 1.45 (+ KTU 7.43 = RS 1.031)</details></td>
    <td><span class=sort-id>027</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>CTA 27 +</summary>
        CTA 27 + 175 (CTA 175 = RS 1.031)</details></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/41fa68ef-be4d-4bbf-9312-32126b9431b4">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1010037</span>RS 1.037</td>
    <td><span class=sort-id>13325</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3325</summary> 
        M3325 = A2771 (AO 12.014)</details></td>
    <td><span class=sort-id>1055</span>KTU 1.55</td>
    <td><span class=sort-id>196</span>CTA 196</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/610ccb54-c8da-4092-8ad7-e9be5c4cd7ac">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020027</span>RS 2.[027]</td>
    <td><span class=sort-id>319998</span>AO 19.998 G</td>
    <td><span class=sort-id>1061</span>KTU 1.61</td>
    <td><span class=sort-id>197</span>CTA 197</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/a773dae1-7131-46aa-8353-19bed915bc72">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1020021</span>RS 2.[021]</td>
    <td><span class=sort-id>317317</span>AO 17.317</td>
    <td><span class=sort-id>1062</span>KTU 1.62</td>
    <td><span class=sort-id>026</span>CTA 26</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/06ee3b50-21c1-4d23-af6d-80ba12044ff1">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030302</span>RS 3.302</td>
    <td><span class=sort-id>500000</span></td>
    <td><span class=sort-id>1063</span>KTU 1.63</td>
    <td><span class=sort-id>028</span>CTA 28</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/53e94294-bd74-4b7a-86e0-36c894fc3e3b">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1040474</span>RS 4.474</td>
    <td><span class=sort-id>13331</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3331</summary> 
        M3331 = A2710 (AO 17.315)</details></td>
    <td><span class=sort-id>1065</span>KTU 1.65</td>
    <td><span class=sort-id>030</span>CTA 30</td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/21d345db-db8a-4244-84d9-738718e90a6c">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1150134</span>RS 15.134</td>
    <td><span class=sort-id>44001</span>DO 4001</td>
    <td><span class=sort-id>1082</span>KTU 1.82</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/563e0497-99b9-46c8-b860-0a9775288efa">Ras Shamra &gt; Residential Quarter &gt; Other Spots</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1160266</span>RS 16.266</td>
    <td><span class=sort-id>44340</span>DO 4340</td>
    <td><span class=sort-id>1083</span>KTU 1.83</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>Ras Shamra (&#8230;) The Annex Office of Archives</summary>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/4e2ebc81-0ea8-4e59-81ab-f5c74452a65f">Ras Shamra &gt; Royal Palace &gt; The Annex Office of Archives</a></details></td>
  </tr>
  <tr>
    <td><span class=sort-id>1180107</span>RS 18.107</td>
    <td><span class=sort-id>44825</span>DO 4825</td>
    <td><span class=sort-id>1088</span>KTU 1.88</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/b859f5fb-b48e-4a54-8c57-f2aed75dd974">Ras Shamra &gt; Royal Palace &gt; Room 74</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1180508</span>RS 18.[508]</td>
    <td><span class=sort-id>400000</span>Damas s.n.</td>
    <td><span class=sort-id>1089</span>KTU 1.89</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/9011500d-06c7-4451-adf8-75ea3cf51ea2">Ras Shamra &gt; Royal Palace &gt; Room 77</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1190039</span>RS 19.039 + 19.174, [51]</td>
    <td><span class=sort-id>45026</span>DO 5026 (+ Damas s.n.)</td>
    <td><span class=sort-id>1092</span>KTU 1.92</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>Ras Shamra &gt; The Southwest Archives &gt; Room 81</summary>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/78ef5ea5-d386-454c-8aa6-e11b9b3a51ce">RS 19.039</a><br/>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/4bffb629-9679-48b8-8dc8-f4501ea6ec9c">RS 19.174, [51]</a></details></td>
  </tr>
  <tr>
    <td><span class=sort-id>1190054</span>RS 19.054</td>
    <td><span class=sort-id>45041</span>DO 5041</td>
    <td><span class=sort-id>1093</span>KTU 1.93</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/09b0a5a6-e839-4cf5-abab-f48a05ad18bf">Ras Shamra &gt; The Southwest Archives &gt; Room 81</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1190059</span>RS 19.059</td>
    <td><span class=sort-id>45046</span>DO 5046</td>
    <td><span class=sort-id>1094</span>KTU 1.94</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/c35f74f2-f76a-4073-b866-412ad939fde2">Ras Shamra &gt; The Southwest Archives &gt; Room 81</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1190179</span>RS 19.179</td>
    <td><span class=sort-id>45143</span>DO 5143</td>
    <td><span class=sort-id>1095</span>KTU 1.95</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/f7a3ba8a-deb0-4025-a8e5-925ee544d9c0">Ras Shamra &gt; The Southwest Archives &gt; Room 81</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1220225</span>RS 22.225</td>
    <td><span class=sort-id>45796</span>DO 5796</td>
    <td><span class=sort-id>1096</span>KTU 1.96</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/dd818e69-65cd-491e-b991-411fc8e0ac1b">Ras Shamra &gt; City South &gt; Other Spots</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240229</span>RS 24.229</td>
    <td><span class=sort-id>46586</span>DO 6586</td>
    <td><span class=sort-id>1098</span>KTU 1.98</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/ab162dd3-1d68-45d2-a847-412170cb79b1">Ras Shamra &gt; South Acropolis &gt; Without Locus</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240244</span>RS 24.244</td>
    <td><span class=sort-id>46587</span>DO 6587</td>
    <td><span class=sort-id>1100</span>KTU 1.100</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/6c5ebe96-278e-48f4-9284-e3aae5bdeda2">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240245</span>RS 24.245</td>
    <td><span class=sort-id>46588</span>DO 6588</td>
    <td><span class=sort-id>1101</span>KTU 1.101</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/e18131d5-2145-408b-a712-7baeb4d84b67">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240251</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>RS 24.251 +</summary>
        RS 24.251 + 24.262 + 24.265 A + 24.267 + 24.275</details></td>
    <td><span class=sort-id>46593</span>DO 6593</td>
    <td><span class=sort-id>1107</span>KTU 1.107</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/07515ad9-679e-476b-b061-354637568fa2">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240252</span>RS 24.252</td>
    <td><span class=sort-id>46594</span>DO 6594</td>
    <td><span class=sort-id>1108</span>KTU 1.108</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/53998024-0ed0-4f23-8f9d-7f1f99852dc2">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240257</span>RS 24.257</td>
    <td><span class=sort-id>46599</span>DO 6599</td>
    <td><span class=sort-id>1113</span>KTU 1.113</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/34f37f5f-c12e-4898-be0d-98da7e7e08f0">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240258</span>RS 24.258</td>
    <td><span class=sort-id>46600</span>DO 6600</td>
    <td><span class=sort-id>1114</span>KTU 1.114</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/17a3541b-6710-4169-8d63-237ff7cdacbd">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240263</span>RS 24.263</td>
    <td><span class=sort-id>46603</span>DO 6603</td>
    <td><span class=sort-id>1117</span>KTU 1.117</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/a229e709-43e2-4093-8c6a-55f77f716019">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240266</span>RS 24.266</td>
    <td><span class=sort-id>46605</span>DO 6605</td>
    <td><span class=sort-id>1119</span>KTU 1.119</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/c03ae71e-4a4a-4ea0-85da-5797db345488">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240272</span>RS 24.272</td>
    <td><span class=sort-id>46609</span>DO 6609</td>
    <td><span class=sort-id>1124</span>KTU 1.124</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/5729c74e-1e68-4ab3-82e7-b61f667bda1a">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240282</span>RS 24.282</td>
    <td><span class=sort-id>46616</span>DO 6616</td>
    <td><span class=sort-id>1129</span>KTU 1.129</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/f4a85ce4-7dca-4862-8269-2d59241eb10f">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240293</span>RS 24.293</td>
    <td><span class=sort-id>46624</span>DO 6624</td>
    <td><span class=sort-id>1133</span>KTU 1.133</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/b6dc0180-002f-47ce-a5c2-a3bcecf2181a">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240642</span>RS 24.642 A</td>
    <td><span class=sort-id>46663</span>DO 6663</td>
    <td><span class=sort-id>1147</span>KTU 1.147</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/dcd68038-80ad-4a1c-9a58-79a26185929c">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240647</span>RS 24.647</td>
    <td><span class=sort-id>46664</span>DO 6664</td>
    <td><span class=sort-id>1151</span>KTU 1.151</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/5c4bac9d-72ef-4b57-890c-885b0716e285">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1240649</span>RS 24.649 A (+) 24.649 B</td>
    <td><span class=sort-id>46668</span>DO 6668 (+) 6669</td>
    <td><span class=sort-id>1152</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>KTU 1.152</summary>
        KTU 1.152 (+ KTU&#178; 7.144 = RS 24.649 B)</details></td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>Ras Shamra &gt; South Acropolis &gt; Tablet Cella</summary>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/f290c556-f314-430b-9df5-7a6cb34c9891">RS 24.649 A</a><br/>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/324db26a-4061-48e0-89bd-0be9b0122e16">RS 24.649 B</a></details></td>
  </tr>
  <tr>
    <td><span class=sort-id>1280054</span>RS 28.054 A = 24.[662]</td>
    <td><span class=sort-id>46803</span>DO 6803</td>
    <td><span class=sort-id>1157</span>KTU 1.157</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/32d3f88f-9980-4aab-b9b1-79ea1f57b7b9">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1280054</span>RS 28.054 B</td>
    <td><span class=sort-id>400000</span>Damas s.n.</td>
    <td><span class=sort-id>1158</span>KTU 1.158</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/ac452669-e6e9-495b-8a0b-c344672b855c">Ras Shamra &gt; South Acropolis &gt; Tablet Cella</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1280059</span>RS 28.059 A = 22.[460]</td>
    <td><span class=sort-id>400000</span>Damas s.n.</td>
    <td><span class=sort-id>1159</span>KTU 1.159</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/4faeb316-3004-4081-8f89-199dba79de9b">Ras Shamra &gt; Block X &gt; VS House B</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1280059</span>RS 28.059 B</td>
    <td><span class=sort-id>400000</span>Damas s.n.</td>
    <td><span class=sort-id>1160</span>KTU 1.160</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/d1df8fda-6a81-4bb1-858d-ce13ed049659">Ras Shamra &gt; Block X &gt; VS House B</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1340126</span>RS 34.126</td>
    <td><span class=sort-id>10849</span>M 849</td>
    <td><span class=sort-id>1161</span>KTU 1.161</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/0d5fd44c-e692-4dbf-9201-624726ac031e">Ras Shamra &gt; Rubble pile</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>2077008</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>RIH 77/08 A +</summary>
        RIH 77/08 A + 77/13 + 77/21 B</details></td>
    <td><span class=sort-id>48373</span>DO 8373</td>
    <td><span class=sort-id>1166</span>KTU 1.166</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/edf109df-58ae-4fb7-bd56-d0287a9f2c0d">Ras Ibn Hani &gt; RIH: Northern Palace &gt; E86 SW</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>2077010</span>RIH 77/10 A</td>
    <td><span class=sort-id>48364</span>DO 8364</td>
    <td><span class=sort-id>1167</span>KTU 1.167</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/fb061bae-1f30-434a-b1c9-cb4d68957905">Ras Ibn Hani &gt; RIH: Northern Palace &gt; E86 SW</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>2078020</span>RIH 78/20</td>
    <td><span class=sort-id>48507</span>DO 8507</td>
    <td><span class=sort-id>1169</span>KTU 1.169</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/493e08e2-832f-4b7e-9d9b-0f879f50f14e">Ras Ibn Hani &gt; RIH: Northern Palace &gt; E86 SE</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>2078001</span>RIH 78/01 + 80/01</td>
    <td><span class=sort-id>48380</span>DO 8380</td>
    <td><span class=sort-id>1172</span>KTU 1.172</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>Ras Ibn Hani (&#8230;) Without Provenance</summary>
        <a href="https://pi.lib.uchicago.edu/1001/org/ochre/20e6fb06-47da-420d-a4e9-288b18b86866">Ras Ibn Hani &gt; RIH: Northern Palace &gt; Without Provenance</a></details></td>
  </tr>
  <tr>
    <td><span class=sort-id>2078026</span>RIH 78/26</td>
    <td><span class=sort-id>48392</span>DO 8392</td>
    <td><span class=sort-id>1176</span>KTU 1.176</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/541d390b-3303-41fd-8ffb-93fd4b462527">Ras Ibn Hani &gt; RIH: Northern Palace &gt; E86 NE</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1922014</span>RS 92.2014</td>
    <td><span class=sort-id>47791</span>DO 7791</td>
    <td><span class=sort-id>1178</span>KTU 1.178</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/9786488d-a026-4c8c-88d4-b89d9764ee1b">Ras Shamra &gt; Ugarit &gt; South Center</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1922016</span>RS 92.2016</td>
    <td><span class=sort-id>47807</span>DO 7807</td>
    <td><span class=sort-id>1179</span>KTU 1.179</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/fd1de644-6c41-49d9-8f6c-46bfeb18b6c1">Ras Shamra &gt; Ugarit &gt; South Center</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>2098002</span>RIH 98/02</td>
    <td><span class=sort-id>48445</span>DO 8445</td>
    <td><span class=sort-id>1180</span>KTU 1.180</td>
    <td><span class=sort-id>900</span></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/a6e9c851-81ff-49cf-81fb-b3cd8e6afaaf">Ras Ibn Hani &gt; G85 NE &gt; SE</a></td>
  </tr>
  <tr>
    <td><span class=sort-id>1030364</span>RS 3.364 (&#8599; RS 2.[014] +)</td>
    <td><span class=sort-id>13353</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>M3353</summary>
        M3353 = A2738 (AO 16.645) (&#8599; M3352 +)</details></td>
    <td><span class=sort-id>1008</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>KTU 1.8</summary>
        KTU 1.8 <span class="noBreakEUPT">(&#8599; KTU 1.3)</span></details></td>
    <td><span class=sort-id>008</span>
      <details class="EUPT-RSTI-KonkordanzTable-details-in-cell">
        <summary>CTA 8</summary>
        CTA 8 <span class="noBreakEUPT">(&#8599; CTA 3)</span></details></td>
    <td></td>
    <td><a href="https://pi.lib.uchicago.edu/1001/org/ochre/3ac14cd1-a428-47e1-9696-bc316daafe8a">Ras Shamra &gt; Acropolis &gt; House of the High Priest</a></td>
  </tr>
</tbody>
<!--Bis hier neue Tabelle einfügen.-->

</table>

</div>
</base>

</div>




<!--Tab 3: Forschungsgeschichte-->
<input id="tab3" type="radio" name="tabs">
<label class="EUPT-RSTI-Konkordanz-tabs-label EUPT-RSTI-Konkordanz-tabs-last-label" for="tab3"><p>Forschungsgeschichte</p></label>
<div class="EUPT-RSTI-Konkordanz-tabs-content">

<div class="allTextTabsPagesIncludingFigures">

<figure class="float-right">
    <img src="/assets/images-content/Keilschrifttafeln_in_situ_Ras_Shamra.jpg" alt="Keilschrifttafelfund am Tell Ras Shamra."/>
    <figcaption>Keilschrifttafelfund am Tell Ras Shamra. Foto: Claude F.-A. Schaeffer (<a href="https://library.biblicalarchaeology.org/article/claude-frederic-armand-schaeffer-forrer-1898-1982-an-appreciation/" target="_blank">BAR 1983</a>; <a href="https://library.biblicalarchaeology.org/images/bsba090505900ljpg/" target="_blank">Image Details</a>; veröffentlicht in <bibl>Syria 10: 295 Fig. 5<VuepressApiPlayground url="/api/eupt/biblio/625U6FNG" method="get" :data="[]"/></bibl>).</figcaption>
<br/>
    <img src="/assets/images-content/Auszug_Brief_Virolleaud_an_Schaeffer_Oktober_1961.jpg" alt="Erste Seite eines Briefes von Charles Virolleaud an Claude F.-A. Schaeffer vom 26. Oktober 1961 über neue Tafelfunde (im zweiten Absatz bespricht Virolleaud Passagen aus KTU 1.101)"/>
    <figcaption>Erste Seite eines Briefes von Charles Virolleaud an Claude F.-A. Schaeffer vom 26. Oktober 1961 über neue Tafelfunde (im zweiten Absatz bespricht Virolleaud Passagen aus KTU 1.101). Scan: <a href="https://bibnum.explore.psl.eu/s/psl/ark:/18469/25080" target="_blank">Fonds Claude Schaeffer, Collège de France</a> (<a href="https://bibnum.explore.psl.eu/s/psl/ark:/18469/289kq" target="_blank">ark:/18469/289kq</a>; this file is licensed under a <a href="https://creativecommons.org/licenses/by-nc-nd/2.0/deed.de" target="_blank">Creative Commons CC BY-NC-ND 2.0 FR License</a>).</figcaption>
</figure>

<div class="mainTextTabsPagesIncludingFigures">
<p style="margin-top: 0;">Die Tontafeln, auf denen die ugaritischen poetischen Texte festgehalten sind, traten bei den Grabungen der <a href="https://www.mission-ougarit.fr/" target="_blank">Mission archéologique syro-française de Ras Shamra – Ougarit</a> am Tell Ras Shamra und in Ras Ibn Hani zutage. Bereits in der ersten Grabungskampagne 1929 wurden am Tell Ras Shamra die ersten keilalphabetischen Schriftzeugnisse in ugaritischer Sprache gefunden. Nachdem das Keilalphabet schon 1930 entziffert werden konnte, nahm sich Charles Virolleaud der <i>editio princeps</i> der Texte an (vgl. <bibl>Virolleaud, 1929<VuepressApiPlayground url="/api/eupt/biblio/SESPT7I9" method="get" :data="[]"/></bibl>; <bibl>1936a<VuepressApiPlayground url="/api/eupt/biblio/3QH7B7JZ" method="get" :data="[]"/></bibl>; <bibl>1936b<VuepressApiPlayground url="/api/eupt/biblio/EWRXVQWT" method="get" :data="[]"/></bibl>; <bibl>1938<VuepressApiPlayground url="/api/eupt/biblio/SZ2KRESA" method="get" :data="[]"/></bibl>; <bibl>1941a<VuepressApiPlayground url="/api/eupt/biblio/9GMW8BAV" method="get" :data="[]"/></bibl>; <bibl>1941b<VuepressApiPlayground url="/api/eupt/biblio/VEBDCEZP" method="get" :data="[]"/></bibl>; <bibl>1942–1943a<VuepressApiPlayground url="/api/eupt/biblio/4SVTCR66" method="get" :data="[]"/></bibl>; <bibl>1942–1943b<VuepressApiPlayground url="/api/eupt/biblio/EG6FUMMN" method="get" :data="[]"/></bibl>). 1963 veröffentlichte Andrée Herdner eine umfassende Zusammenstellung des bis dahin bekannten Textkorpus (<bibl>CTA<VuepressApiPlayground url="/api/eupt/biblio/IVX74AE6" method="get" :data="[]"/></bibl>); Herdners Autographien und Umschriften sind bis heute eine wichtige Referenzgröße. Auch Cyrus Gordon legte <bibl>1965<VuepressApiPlayground url="/api/eupt/biblio/2Q4MCAMV" method="get" :data="[]"/></bibl> nach umfangreichen Vorarbeiten (<bibl>1940<VuepressApiPlayground url="/api/eupt/biblio/8PVIEA9W" method="get" :data="[]"/></bibl>; <bibl>1947<VuepressApiPlayground url="/api/eupt/biblio/SDQR35PC" method="get" :data="[]"/></bibl>; <bibl>1955<VuepressApiPlayground url="/api/eupt/biblio/WDM6KU22" method="get" :data="[]"/></bibl>) eine Textsammlung vor. Manfried Dietrich, Oswald Loretz und Joaquín Sanmartín veröffentlichten 1976 erstmals eine umfassende Bearbeitung der Schriftzeugnisse: Ihre Arbeit „Die Keilalphabetischen Texte aus Ugarit“ (<bibl>KTU<VuepressApiPlayground url="/api/eupt/biblio/ZR6268P6" method="get" :data="[]"/></bibl>), die 1995 in zweiter (<bibl>KTU&#178; / CAT<VuepressApiPlayground url="/api/eupt/biblio/APRJARXI" method="get" :data="[]"/></bibl>) und 2013 (<bibl>KTU&#179;<VuepressApiPlayground url="/api/eupt/biblio/Z4J97DSD" method="get" :data="[]"/></bibl>) in dritter, jeweils erweiterter und verbesserter Auflage erschien, wurde zur meistgebrauchten Ausgabe der keilalphabetischen Texte. Seit den 1990er Jahren sammelte das spanische „Laboratorio de Hermeneumática“ unter der Leitung von Jesús-Luis Cunchillos alle veröffentlichten Umschriften: In der „Ugaritic Data Bank“ (UDB) wurden die Transliterationen in Form einer Partitur zusammengestellt und unterschiedliche Lesungen knapp kommentiert (<bibl>Cunchillos / Vita / Zamora, 2003<VuepressApiPlayground url="/api/eupt/biblio/C2I8RTAP" method="get" :data="[]"/></bibl>). Zahlreiche Beiträge zur Textgrundlage der ugaritischen Überlieferung erschienen in französischen Organen: In der Reihe „Ras Shamra – Ougarit“ (RSOu.) und in der Zeitschrift „Syria“ wurden fortlaufend neue Texte ediert und wichtige Kollationen publiziert. Allen voran waren es die Studien von André Caquot, Pierre Bordreuil und Dennis Pardee, die die Forschung nachhaltig prägten (vgl. <bibl>Caquot, 1960<VuepressApiPlayground url="/api/eupt/biblio/SMQSMNC6" method="get" :data="[]"/></bibl>; <bibl>1969<VuepressApiPlayground url="/api/eupt/biblio/G9F3V2WB" method="get" :data="[]"/></bibl>; <bibl>Bordreuil / Caquot, 1980<VuepressApiPlayground url="/api/eupt/biblio/F8H9URJ3" method="get" :data="[]"/></bibl>; <bibl>Bordreuil / Pardee, 1982<VuepressApiPlayground url="/api/eupt/biblio/Z39EU67X" method="get" :data="[]"/></bibl>; <bibl>1991<VuepressApiPlayground url="/api/eupt/biblio/SHVBHDD4" method="get" :data="[]"/></bibl>; <bibl>Pardee, 1988<VuepressApiPlayground url="/api/eupt/biblio/JDZC5ZTS" method="get" :data="[]"/></bibl>; <bibl>2000<VuepressApiPlayground url="/api/eupt/biblio/DRZUKP27" method="get" :data="[]"/></bibl>; <bibl>2011<VuepressApiPlayground url="/api/eupt/biblio/V2CFRN5J" method="get" :data="[]"/></bibl>).</p>

<p>Seit den 1970ern erschienen zahlreiche Anthologien, in denen die ugaritischen poetischen Texte in Übersetzung geboten wurden. Hervorzuheben sind die Sammlungen von André Caquot et. al. (<bibl>1974<VuepressApiPlayground url="/api/eupt/biblio/PBGACVKN" method="get" :data="[]"/></bibl>; <bibl>1989<VuepressApiPlayground url="/api/eupt/biblio/FNEJ63WB" method="get" :data="[]"/></bibl>), Johannes C. de Moor (<bibl>1987<VuepressApiPlayground url="/api/eupt/biblio/SBKFHDMD" method="get" :data="[]"/></bibl>), Manfried Dietrich und Oswald Loretz (<bibl>1997<VuepressApiPlayground url="/api/eupt/biblio/SBGG6DIB" method="get" :data="[]"/></bibl>), Simon B. Parker et. al. (<bibl>1997<VuepressApiPlayground url="/api/eupt/biblio/I6CT9IPT" method="get" :data="[]"/></bibl>), Nicolas Wyatt (<bibl>2002<VuepressApiPlayground url="/api/eupt/biblio/J8WF5NVK" method="get" :data="[]"/></bibl>), Herbert Niehr (<bibl>2015<VuepressApiPlayground url="/api/eupt/biblio/T86JNIHU" method="get" :data="[]"/></bibl>) sowie Pierre Bordreuil und Dennis Pardee (<bibl>Pardee, 1997<VuepressApiPlayground url="/api/eupt/biblio/3J3QDJKV" method="get" :data="[]"/></bibl>; <bibl>Bordreuil / Pardee, 2004a<VuepressApiPlayground url="/api/eupt/biblio/IBSVGA2E" method="get" :data="[]"/></bibl>; <bibl>2004b<VuepressApiPlayground url="/api/eupt/biblio/KB9VHABD" method="get" :data="[]"/></bibl>; <bibl>2009<VuepressApiPlayground url="/api/eupt/biblio/HY4FG9D7" method="get" :data="[]"/></bibl>).</p>

<p>In deutlich geringerer Zahl wurden kritische Editionen vorgelegt, die neben der Übersetzung auch eine Umschrift der Texte und epigraphisch-philologische Erläuterungen bieten: <bibl>1989<VuepressApiPlayground url="/api/eupt/biblio/6I93JBUW" method="get" :data="[]"/></bibl> veröffentlichte Baruch Margalit eine Edition des ˀAqhatu-Textes; Shirly Natan-Yulzary legte <bibl>2015<VuepressApiPlayground url="/api/eupt/biblio/TGQ86E82" method="get" :data="[]"/></bibl> eine hebräische Bearbeitung vor. Der Baˁlu-Zyklus wurde von Mark Smith eingehend bearbeitet: In zwei umfangreichen Bänden edierte er die Tafeln KTU 1.1–2 und, gemeinsam mit Wayne Pitard, KTU 1.3–4 (<bibl>Smith, 1994<VuepressApiPlayground url="/api/eupt/biblio/KK77USAN" method="get" :data="[]"/></bibl>; <bibl>Smith / Pitard, 2009<VuepressApiPlayground url="/api/eupt/biblio/K6V5DA4V" method="get" :data="[]"/></bibl>). Das Kirtu-Gedicht wurde nach den Arbeiten von Charles Virolleaud (<bibl>1936b<VuepressApiPlayground url="/api/eupt/biblio/EWRXVQWT" method="get" :data="[]"/></bibl>; <bibl>1941a<VuepressApiPlayground url="/api/eupt/biblio/9GMW8BAV" method="get" :data="[]"/></bibl>; <bibl>1941b<VuepressApiPlayground url="/api/eupt/biblio/VEBDCEZP" method="get" :data="[]"/></bibl>; <bibl>1942–1943a<VuepressApiPlayground url="/api/eupt/biblio/4SVTCR66" method="get" :data="[]"/></bibl>; <bibl>1942–1943b<VuepressApiPlayground url="/api/eupt/biblio/EG6FUMMN" method="get" :data="[]"/></bibl>), Harold Ginsberg (<bibl>1946<VuepressApiPlayground url="/api/eupt/biblio/4JRTWBJ8" method="get" :data="[]"/></bibl>) und John Gray (<bibl>1955<VuepressApiPlayground url="/api/eupt/biblio/WDVCXNB9" method="get" :data="[]"/></bibl>; zweite Auflage <bibl>1964<VuepressApiPlayground url="/api/eupt/biblio/3PZWUQEU" method="get" :data="[]"/></bibl>) nur in Anthologien bearbeitet. Die kürzeren Stücke der ugaritischen Poesie – mythologische Texte, Gebete, Beschwörungen und Ritualtexte – wurden meist in Einzelstudien in Zeitschriften, Jahrbüchern oder Fest- und Gedenkschriften behandelt. Nur selten wurden die Texte monographisch ediert: Hervorzuheben sind die Abhandlungen von Dennis Pardee (<bibl>1988<VuepressApiPlayground url="/api/eupt/biblio/JDZC5ZTS" method="get" :data="[]"/></bibl>), Manfried Dietrich und Oswald Loretz (<bibl>2000<VuepressApiPlayground url="/api/eupt/biblio/9SVWBWDU" method="get" :data="[]"/></bibl>) sowie Gregorio del Olmo Lete (<bibl>2014<VuepressApiPlayground url="/api/eupt/biblio/NFRV94UF" method="get" :data="[]"/></bibl>). KTU 1.24 wurde von Gabriele Theuer in einer Studie über den Mondgott in den Religionen Syrien-Palästinas bearbeitet (<bibl>2000<VuepressApiPlayground url="/api/eupt/biblio/29M9C28H" method="get" :data="[]"/></bibl>). Mark Smith edierte KTU 1.23 in einer kürzeren Monographie (<bibl>2006<VuepressApiPlayground url="/api/eupt/biblio/DC8KRHUB" method="get" :data="[]"/></bibl>).</p>

</div><!--End of mainText-->
</div><!--End of allText-->
</div><!--End of tabs-content-->

</div><!--End of EUPT-RSTI-Konkordanz-tabs-->
