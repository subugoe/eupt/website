---
title: Datenschutzerklärung
lang: de
layout: Layout
---

# {{ $frontmatter.title }}

Wir weisen darauf hin, dass die Datenübertragung im Internet (z. B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.

## Anbieter

Anbieter dieser Internetpräsenz ist im Rechtssinne die Niedersächsische Staats- und Universitätsbibliothek Göttingen, siehe [Impressum](https://www.sub.uni-goettingen.de/impressum/).

## Personenbezogene Daten

Die Nutzung dieser Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf diesen Seiten personenbezogene Daten erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis und im hier dargestellten Umfang. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.

Sämtliche personenbezogenen Daten werden nur solange gespeichert wie dies für den jeweils genannten Zweck erforderlich ist. Hierbei werden bestehende gesetzliche Aufbewahrungsfristen berücksichtigt.

## Datenempfänger

Datenempfänger innerhalb der Universität ist die Niedersächsische Staats- und Universitätsbibliothek.

Weiterer Datenempfänger ist die [Gesellschaft für wissenschaftliche Datenverarbeitung Göttingen](https://www.gwdg.de/)

## Informationen zur Verarbeitung personenbezogener Daten

### Name und Kontaktdaten der Verantwortlichen

**Der Präsident**  
Prof. Dr. Metin Tolan  
Wilhelmsplatz 1  
37073 Göttingen  
<a href="+495513921000">+49 551 39-21000</a> (Tel.)  
+49 551 39-1821000 (Fax)  
<a href="mailto:praesident@uni-goettingen.de">praesident@uni-goettingen.de</a>

### Kontaktdaten des betrieblichen Datenschutzbeauftragen

**Prof. Dr. Andreas Wiebe**  
Lehrstuhl für Bürgerliches Recht  
Wettbewerbs- und Immaterialgüterrecht  
Medien- und Informationsrecht  
Platz der Göttinger Sieben 6  
D-37073 Göttingen  
<a href="+49551397381">+49 551 39-7381</a> (Tel.)  
+49 551 39-4437 (Fax)  
<a href="mailto:datenschutz@uni-goettingen.de">datenschutz@uni-goettingen.de</a>

### Zwecke und Rechtsgrundlagen der Verarbeitung

Ihre Daten werden dafür erhoben, um Ihr Anliegen zu bearbeiten. Ihre Daten werden auf Grundlage von Art. 6 Abs. 1e erhoben.

### Empfänger der personenbezogenen Daten

Um ihre Anfrage zu beantworten, werden Ihre personenbezogenen Daten an die innerhalb der SUB Göttingen zuständige Personengruppe übermittelt.

### Übermittlung von personenbezogenen Daten an ein Drittland

Es ist nicht geplant Ihre personenbezogenen Daten zu übermitteln.

### Dauer der Speicherung der personenbezogenen Daten

Ihre Daten werden gespeichert, um die Dienste zu erbringen, mit denen Sie uns beauftragen. Sobald Sie sich abmelden oder um eine Löschung der Daten bitten, werden ihre Daten nur noch so lange gespeichert, bis alle laufenden Vorgänge abgeschlossen sind und dann gelöscht, es sei denn gesetzliche Vorgaben erfordern eine längere Speicherung.

### Betroffenenrechte

Nach der EU-Datenschutzgrundverordnung stehen Ihnen folgende Rechte zu:

Werden Ihre personenbezogenen Daten verarbeitet, so haben Sie das Recht Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art. 15 DSGVO). Sollten unrichtige personenbezogene Daten verarbeitet werden, steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO). Liegen die gesetzlichen Voraussetzungen vor, so können Sie die Löschung oder Einschränkung der Verarbeitung verlangen sowie Widerspruch gegen die Verarbeitung einlegen (Art. 17, 18 und 21 DSGVO).

Wenn Sie die erforderlichen Daten nicht angeben, kann Ihr Antrag nicht bearbeitet werden.

Wenn Sie in die Datenverarbeitung eingewilligt haben oder ein Vertrag zur Datenverarbeitung besteht und die Datenverarbeitung mithilfe automatisierter Verfahren durchgeführt wird, steht Ihnen gegebenenfalls ein Recht auf Datenübertragbarkeit zu (Art. 20 DSGVO). Sollten Sie von Ihren oben genannten Rechten Gebrauch machen, prüft Niedersächsische Staats- und Universitätsbibliothek Göttingen, ob die gesetzlichen Voraussetzungen hierfür erfüllt sind.

Zur Ausübung Ihrer Rechte wenden Sie sich bitte an den betrieblichen Datenschutzbeauftragten.

Bei datenschutzrechtlichen Beschwerden können Sie sich an die zuständige Aufsichtsbehörde wenden:

**Die Landesbeauftragte für den Datenschutz Niedersachsen**  
Prinzenstraße 5  
30159 Hannover  
<a href="tel:+495511204500">+49 551 120 4500</a> (Tel.)  
+49 551 120 4599 (Fax)  
<a href="mailto:poststelle@lfd.niedersachsen.de">poststelle@lfd.niedersachsen.de</a>  
Widerrufsrecht bei Einwilligung

Wenn Sie in die Verarbeitung durch die Niedersächsische Staats- und Universitätsbibliothek Göttingen durch eine entsprechende Erklärung eingewilligt haben, können Sie die Einwilligung jederzeit für die Zukunft widerrufen. Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Datenverarbeitung wird durch diesen nicht berührt.

### Auskunft, Löschung, Sperrung

Sie haben jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit über die im Impressum angegeben Adresse des Webseitenbetreibers an uns wenden.

## Kontaktformulare und E-Mail-Kontakt

### Beschreibung und Umfang der Datenverarbeitung

Auf unserer Internetseite sind Kontaktdaten vorhanden, die für die elektronische Kontaktaufnahme per E-Mail genutzt werden können. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten der Nutzerin oder des Nutzers gespeichert. Es erfolgt in diesem Zusammenhang keine Weitergabe der Daten an Dritte. Die Daten werden ausschließlich für die Verarbeitung der Konversation verwendet.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die Verarbeitung der Daten ist bei Vorliegen einer Einwilligung der Nutzerin oder des Nutzers Art. 6 Abs. 1 lit. a DSGVO. Rechtsgrundlage für die Verarbeitung der Daten, die im Zuge einer Übersendung einer E-Mail übermittelt werden, ist Art. 6 Abs. 1 lit. f DSGVO.

### Zweck der Datenverarbeitung

Im Falle einer Kontaktaufnahme per E-Mail liegt hieran ebenfalls das erforderliche berechtigte Interesse an der Verarbeitung der Daten.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Für die personenbezogenen Daten, die per E-Mail übersandt wurden, ist dies dann der Fall, wenn die jeweilige Konversation mit der Nutzerin oder dem Nutzer beendet ist. Beendet ist die Konversation dann, wenn sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist. Die während des Absendevorgangs zusätzlich erhobenen personenbezogenen Daten werden spätestens nach einer Frist von sieben Tagen gelöscht.

### Widerspruchs- und Beseitigungsmöglichkeit

Die Nutzerin oder der Nutzer hat jederzeit die Möglichkeit, seine Einwilligung zur Verarbeitung der personenbezogenen Daten zu widerrufen. Nimmt die Nutzerin oder der Nutzer per E-Mail Kontakt mit uns auf, so kann er der Speicherung seiner personenbezogenen Daten jederzeit auf demselben Weg per Mail widersprechen. In einem solchen Fall kann die Konversation nicht fortgeführt werden. Alle personenbezogenen Daten, die im Zuge der Kontaktaufnahme gespeichert wurden, werden in diesem Fall gelöscht.

## Server-Log-Files

Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log Files, die Ihr Browser automatisch an uns übermittelt. Dies sind:

- Browsertyp und Browserversion
- verwendetes Betriebssystem
- Referrer URL
- Hostname des zugreifenden Rechners
- Uhrzeit der Serveranfrage

Diese Daten sind nicht bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen, wenn uns konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt werden.

Source: [e-recht24.de](https://www.e-recht24.de/)
