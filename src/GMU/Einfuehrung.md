---
title: Göttinger Miszellen zur Ugaritistik
lang: de
layout: Layout
---
<a href="#top" id="top-link"></a>

<h1 class="h1-in-tab-layout">Göttinger Miszellen zur Ugaritistik</h1>

<div class="GMU-tabs">

<input id="tab1" type="radio" name="tabs" checked="checked">
<label class="GMU-tabs-label" for="tab1"><p>Über GMU</p></label>
<div class="GMU-tabs-content">

<div style="margin-top: 1em;" class="Absatz-hängend"><b>Zeitschrift (<i>online</i> – <i>open access</i> – <i>peer reviewed</i>)</b></div>
<div class="Absatz-hängend"><b>Herausgegeben von Reinhard Müller, Clemens Steinberger und Noah Kröll</b></div>
<div><b>ISSN: 2944-3458</b></div>

<p>In der Online-Zeitschrift <i>Göttinger Miszellen zur Ugaritistik</i> (GMU) werden kurze Beiträge zu ugaritistischen Fragestellungen frei zugänglich gemacht. Veröffentlicht werden Forschungsbeiträge zur schriftlichen und materiellen Überlieferung aus Ugarit, insbesondere zum keilaphabetischen Korpus (u. a. epigraphische, philologische, linguistische, literaturwissenschaftliche, religionsgeschichtliche, historische und archäologische Untersuchungen), zur ugaritischen Kultur sowie zur Geschichte der Ugaritistik. Die Publikationssprachen sind Englisch, Französisch und Deutsch.</p>
<p>Die GMU bieten Forschenden eine Plattform, um kleinere Ergebnisse ihrer Arbeit rasch mit der internationalen Forschungscommunity zu teilen. Gleichzeitig soll die Online-Zeitschrift die Sichtbarkeit ugaritistischer Forschung im Internet stärken und Wissenschaftlerinnen und Wissenschaftlern – sowohl aus der Ugaritistik als auch aus den benachbarten Disziplinen – den Zugang zu neuesten Forschungsergebnissen erleichtern.</p>
<p>Jeder Beitrag wird vor der Veröffentlichung von den Herausgebern und mindestens zwei externen Gutachtenden (in einem <a href="https://authorservices.wiley.com/Reviewers/journal-reviewers/what-is-peer-review/types-of-peer-review.html" target="_blank">Double-Blind Peer Review</a>) geprüft. Die GMU werden fortlaufend veröffentlicht: Jeder Beitrag wird online zugänglich gemacht, sobald das Publikationsverfahren erfolgreich abgeschlossen worden ist.</p>
<p>Die GMU sind eine <a href="https://www.coalition-s.org/diamond-open-access/" target="_blank">Diamond-Open-Access-Zeitschrift</a>: Sämtliche Beiträge sind nach ihrer Veröffentlichung gebührenfrei online abrufbar. Für Autorinnen und Autoren fallen keine Artikelbearbeitungsgebühren (<i>Article Publication Charges</i>) an. Alle Artikel werden unter einer <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC-BY-Lizenz</a> veröffentlicht. Autorinnen und Autoren behalten die Urheberrechte.</p>
<p>Die GMU wurden 2024 im Rahmen des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> ins Leben gerufen. Die GMU-Website ist seitdem Teil des EUPT-Webportals. Die GMU gehen thematisch jedoch über den Fokus des EUPT-Vorhabens hinaus und sollen über die Projektlaufzeit hinaus bestehen. Die Zeitschrift ist so konzipiert, dass für ihre Publikation keine Kosten anfallen. Die Herausgeber werden derzeit von der Universität Göttingen (Reinhard Müller; Professor für Altes Testament) und von der Deutschen Forschungsgemeinschaft (Clemens Steinberger und Noah Kröll; EUPT-Projektmitarbeiter) finanziert. Von anderer Seite wird die Zeitschrift nicht gesponsert.</p>

<details class="GMU-Einfuehrung-h-details" style="text-align: left;">
<summary><h3>Herausgeber</h3></summary>
    <div class="Absatz-hängend" style="margin-top: 1em;"><b>Prof. Dr. Reinhard Müller</b> <span class="noBreakEUPT">
            <a class="Logo-orcid" href="https://orcid.org/0000-0002-7433-2273" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Logo-academia" href="https://xn--uni-gttingen-8ib.academia.edu/ReinhardM%C3%BCller" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Logo-university" href="https://www.uni-goettingen.de/de/prof.+dr.+reinhard+m%C3%BCller/56732.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span></div>
        <div class="Absatz-hängend">Professor für Altes Testament an der 
            <a href="https://www.uni-goettingen.de/de/die+fakult%c3%a4t/55363.html" target="_blank">Theologischen Fakultät</a> der 
            <span class="noBreakEUPT"><a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend">Leiter der <a href="https://uni-goettingen.de/de/431176.html" target="_blank">Ugarit-Forschungsstelle</a> an der Universität Göttingen</div>
        <div class="Absatz-hängend">Leiter des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> an der Universität Göttingen</div>
        <div class="Absatz-hängend"><a href="mailto:Reinhard.Mueller@theologie.uni-goettingen.de">reinhard.mueller@theologie.uni-goettingen.de</a></div>
    <div class="Absatz-hängend" style="margin-top: 1em;"><b>Clemens Steinberger, BA MA</b> <span class="noBreakEUPT">
            <a class="Logo-orcid" href="https://orcid.org/0009-0003-4268-1596" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Logo-academia" href="https://uni-goettingen.academia.edu/ClemensSteinberger" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Logo-university" href="https://uni-goettingen.de/de/219789.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span></div>
        <div class="Absatz-hängend">Koordinator der <a href="https://uni-goettingen.de/de/431176.html" target="_blank">Ugarit-Forschungsstelle</a> an der <span class="noBreakEUPT">
            <a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend">Wissenschaftlicher Mitarbeiter (Postdoc) und Koordinator des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> an der Universität Göttingen</div>
        <div class="Absatz-hängend"><a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a></div>
    <div class="Absatz-hängend" style="margin-top: 1em;"><b>Noah Kröll, BA MA</b> <span class="noBreakEUPT">
            <a class="Logo-orcid" href="https://orcid.org/0009-0000-9138-9928" target="_blank">
                <img style="border-width:0" src="https://orcid.org/assets/vectors/orcid.logo.icon.svg"></a>
            <a class="Logo-academia" href="https://uibk.academia.edu/NoahKr%C3%B6ll" target="_blank">
                <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
            <a class="Logo-university" href="https://www.uni-goettingen.de/en/68717.html" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span></div>
        <div class="Absatz-hängend">Wissenschaftlicher Mitarbeiter (Doktorand) des DFG-Projekts <a href="/" target="_self">Edition des ugaritischen poetischen Textkorpus (EUPT)</a> an der <span class="noBreakEUPT">
            <a href="https://ror.org/01y9bpm73" target="_blank">Universität Göttingen</a> <a class="Logo-ROR" href="https://ror.org/01y9bpm73" target="_blank">
                    <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span></div>
        <div class="Absatz-hängend" style="margin-bottom: 1em;"><a href="mailto:Noah.Kroell@theologie.uni-goettingen.de">noah.kroell@theologie.uni-goettingen.de</a></div>
    <!-- Ehemalige Herausgeberinnen und Herausgeber: ---></details>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Kontakt</h3></summary>
    <p>Beiträge und Gutachten schicken Sie bitte per E-Mail an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a>. Beachten Sie dabei die im Bereich <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">Richtlinien</button> erläuterten Regeln. Dort finden Sie auch hilfreiche Word-Vorlagen, namentlich eine <a href="/assets/images-content/GMU_manuscript-sample.docx" target="_blank">Manuskriptvorlage für GMU-Beiträge</a>, eine <a href="/assets/images-content/GMU_e-mail-template-for-authors-submission.docx" target="_blank">E-Mail-Vorlage für die Einreichung eines Beitrags (e-mail template for submitting an article)</a> und eine <a href="/assets/images-content/GMU_e-mail-template-for-reviewers.docx" target="_blank">E-Mail-Vorlage für die Einreichung eines Gutachtens (e-mail template for submitting a review)</a>.</p>
    <p>Fragen, Anmerkungen und Verbesserungsvorschläge sind an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a> oder <a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a> zu richten.</p></details>

</div><!--End of content of Tab1: Über GMU-->













<!--Tab2: Richtlinien-->
<input id="tab2" type="radio" name="tabs">
<label class="GMU-tabs-label" for="tab2"><p>Richtlinien</p></label>
<div class="GMU-tabs-content">
<p>Die <i>Göttinger Miszellen zur Ugaritistik</i> sollen Wissenschaftlerinnen und Wissenschaftlern eine Plattform bieten, um Forschungsergebnisse schnell einer möglichst großen Öffentlichkeit zugänglich zu machen. Gleichzeitig ist die Zeitschrift höchsten wissenschaftlichen Standards verpflichtet. Um einen reibungslosen Publikationsprozess zu gewährleisten und die wissenschaftliche Qualität der GMU zu sichern, folgen alle, die an der Publikation der Zeitschrift mitwirken, festgelegten Richtlinien. Im Folgenden sind sämtliche Richtlinien zusammengestellt. Autorinnen und Autoren sowie Gutachterinnen und Gutachter sind gehalten, die Richtlinien aufmerksam zu lesen, bevor sie Beiträge einreichen oder begutachten. Ein Überblick über die Phasen des Publikationsverfahrens findet sich im Bereich <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab3').click(), 10);">Publikationsmodus</button>.</p>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Informationen für Autorinnen und Autoren</h3></summary>
    <p>In den <i>Göttinger Miszellen zur Ugaritistik</i> werden Beiträge veröffentlicht, deren Fokus auf der Erforschung der aus Ugarit überlieferten Quellen und besonders der keilalphabetischen Überlieferung liegt. Akzeptiert werden Beiträge auf Englisch, Französisch und Deutsch. Alle Artikel werden unter einer <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC-BY-Lizenz</a> veröffentlicht. Autorinnen und Autoren behalten die Urheberrechte. Für die Publikation fallen keine Gebühren an.</p>
    <p><b>Von der Einreichung zur Publikation:</b> 
        Um einen Beitrag einzureichen, ist die <a href="/assets/images-content/GMU_e-mail-template-for-authors-submission.docx" target="_blank">hier</a> abrufbare E-Mail-Vorlage zu nutzen (für die Manuskripterstellung sind die <a href="#GMU-Manuskriptrichtlinien">unten</a> zusammengestellten Richtlinien zu beachten). Autorinnen und Autoren sind gehalten, den Text der Vorlage in eine leere E-Mail zu kopieren, alle relevanten Informationen zu ergänzen (s. die Erläuterungen in der Vorlage) und die E-Mail anschließend an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a> zu schicken. Der Beitrag selbst ist der E-Mail als Word- und PDF-Dokument beizufügen. Enthält der Beitrag urheberrechtlich geschütztes Material (z. B. Abbildungen), sollte der E-Mail zusätzlich die schriftliche Nutzungserlaubnis der Urheberrechtsinhaberinnen und -inhaber beifügt werden (spätestens bei Einreichung der überarbeieteten Fassung des Beitrags, i. e. nach Abschluss des Review-Verfahrens, ist die Genehmigung nachzureichen; s. § 17 der ethischen Publikationsrichtlinien). Forschende, die beabsichtigen, eine Miszelle einzureichen, an der mehrere Autorinnen und Autoren mitgewirkt haben, sollten vor der Einreichung entscheiden, wer während des Publikationsverfahrens als korrespondierende Autorin / korrespondierender Autor (<i>corresponding author</i>) auftritt. Die Person, die den Beitrag einreicht, ist gleichzeitig korrespondierende Autorin / korrespondierender Autor. Die Herausgeber schicken sämtliche Mitteilungen über das Publikationsverfahren ausschließlich an die korrespondierende Autorin / den korrespondierenden Autor. Die korrespondierende Autorin / der korrespondierende Autor muss sicherstellen, alle relevanten Entscheidungen mit ihren / seinen Co-Autorinnen und -Autoren abzustimmen.</p>
        <p>Nachdem die Autorinnen und Autoren das Beitragsmanuskript eingereicht haben, erhalten sie von einem der Herausgeber eine Empfangsbestätigung. Anschließend wird der Beitrag von den Herausgebern und zwei externen Gutachtenden geprüft. Im Anschluss an das Peer Review haben die Autorinnen und Autoren Gelegenheit, den Beitrag zu überarbeiten und in verbesserter Version erneut einzureichen (vorausgesetzt, dass das Urteil der Gutachten insgesamt positiv ist und die Herausgeber den Beitrag zur Publikation angenommen haben). Danach erstellen die Herausgeber die vorläufige Endfassung des Beitrags. Nach Rücksprache mit den Autorinnen und Autoren und etwaigen Korrekturen veröffentlichen sie den Artikel. S. dazu auch die Aufstellung der Phasen des Publikationsprozesses im Bereich <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab3').click(), 10);">Publikationsmodus</button>.</p>
        <p>Autorinnen und Autoren können die Einreichung ihres Manuskripts jederzeit widerrufen (solange der Beitrag nicht veröffentlicht ist) und den Publikationsprozess abbrechen (per E-Mail an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a>).</p>
        <p>Autorinnen und Autoren steht es frei, die auf dem Zenodo-Portal und auf der GMU-Website veröffentlichte PDF-Fassung des Beitrags (s. <i>Publikationsmodus</i>) auch auf anderen Portalen zugänglich zu machen (beispielsweise auf ihrer persönlichen Website oder auf <a href="https://www.academia.edu/" target="_blank">https://www.academia.edu/</a>).<sup>a</sup> Ebenso dürfen Autorinnen und Autoren ihren Beitrag nach der Publikation in den GMU (in derselben Form oder in überarbeiteter Fassung) in anderen Medien veröffentlichen (beispielsweise in einem Band gesammelter Schriften). Voraussetzung ist, dass a) die Publikation eines bereits andernorts erschienenen Beitrags in dem gewählten Publikationsorgan zulässig ist und b) auf den ursprünglichen Publikationsort (i. e. GMU) verwiesen wird.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Die Herausgeber empfehlen Autorinnen und Autoren, den Beitrag auf privaten Servern / Universitätsservern zu archivieren und auf allen relevanten online-Portalen hochzuladen. Dies erhöht die Sichtbarkeit des Beitrags und leistet gleichzeitig einen Beitrag dazu, die langfristige Zugänglichkeit des Beitrags zu sichern. Sollten Autorinnen und Autoren jedoch die <i>Metrics</i>-Funktion des Zenodo-Portals bestmöglich nutzen wollen, wird empfohlen, auf anderen Portalen lediglich den Zitationshinweis samt DOI-Link sowie den Abstract des Beitrags zu veröffentlichen (i. e. die Titelseite der veröffentlichten PDF-Fassung des Beitrags). So werden Interessierte auf das Zenodo-Portal weitergeleitet, wo <i>Views</i> und <i>Downloads</i> des Beitrags registriert werden. Voraussetzung für den Upload des veröffentlichten GMU-Beitrags (sei es die vollständige PDF-Fassung des Beitrags oder Teile davon) auf anderen online-Portalen ist, dass der Publikationsort (i. e. GMU) und die DOI-Nummer des Beitrags mitangegeben werden. Die auf <a href="https://zenodo.org/communities/gmu/" target="_blank">https://zenodo.org/communities/gmu/</a> und auf der GMU-Website veröffentlichte PDF-Fassung enthält ein Titelblatt, das alle relevanten Metadaten einschließt. Die Veröffentlichung vorläufiger Fassungen des Beitrags ist zwar nicht verboten, wird jedoch nicht empfohlen. Auf jeden Fall darf der Beitrag (gleich in welcher Fassung) frühestens nach Abschluss des Peer Review-Verfahrens öffentlich zugänglich gemacht werden. Andernfalls ist ein objektives Double-Blind Peer Review nicht möglich.</li></ol>
    <p><b id="GMU-Manuskriptrichtlinien">Manuskriptrichtlinien:</b>
        Neben dem Titel und den Namen der Autorinnen und Autoren sind am Anfang des eingereichten Beitrags aussagekräftige <i>Keywords</i> (i.d.R. 5–6) zu nennen, die den Untersuchungsgegenstand der Miszelle erfassen; außerdem ist ein englischer Abstract anzuführen (ca. 150 Wörter; ist die Miszelle auf deutsch oder französisch verfasst, ist am Anfang des Abstracts eine akkurate englische Übersetzung des Titels des Beitrags anzugeben; zusätzliche Abstracts auf Französisch und / oder Deutsch sind optional). Der Hauptteil der Miszelle sollte nicht mehr als 3500 Wörter umfassen (nur in Ausnahmefällen werden längere Artikel akzeptiert). Am Ende jedes Beitrags sind die gebrauchten Abkürzungen aufzuschlüsseln, und es ist ein vollständiges Literaturverzeichnis der in dem Beitrag zitierten Arbeiten aufzustellen. Hier ist eine <a href="/assets/images-content/GMU_manuscript-sample.docx" target="_blank">Manuskriptvorlage für GMU-Beiträge</a> abrufbar.</p>
        <p><i>Font / Zeichensetzung:</i> Das elektronische Manuskript ist in einem Unicode-Font zu erstellen (empfohlen wird <i>Times New Roman</i> oder <i>Gentium Plus</i>). Es sind die in der jeweiligen Sprache üblichen Satz- und Anführungszeichen zu gebrauchen (Anführungszeichen in englischen Beiträgen: “&#8230;” / ‘&#8230;’; in französischen: «&#x202F;&#8230;&#x202F;» / ‹&#x202F;&#8230;&#x202F;›; in deutschen: „&#8230;“ / ,&#8230;‘). Zahlen in Intervallen sind durch einen Halbgeviertstrich / <i>en dash</i> (–) miteinander zu verbinden (z. B. „7–8“). In Intervallen, die sich aus zwei mehrstelligen Zahlen zusammensetzen, ist sowohl die jeweils erste als auch die jeweils zweite Zahl vollständig anzuführen (z. B. „911–914“; nicht „911–14“; s. auch die Erläuterungen zur Bibliographie in der <a href="/assets/images-content/GMU_manuscript-sample.docx" target="_blank">Manuskriptvorlage für GMU-Beiträge</a>).</p>
        <p><i>Layout:</i> Für die Breite der Seitenränder, die Schriftgröße, die Breite etwaiger Einzüge sowie die Höhe der Zeilen- und Absatzabstände gibt es keine Vorgaben.</p>
        <p><i>Editionen:</i> Bearbeitungen ugaritischer (und akkadischer, sumerischer, hethitischer etc.) Texte, die mehr als eine Zeile umfassen, sind als zweispaltige Tabelle anzulegen. In der linken Zelle der ersten Zeile steht die Stellenangabe (z. B. „KTU 1.1 i 1–2“); die rechte Zelle bleibt leer. In den folgenden Tabellenzeilen steht in der jeweils linken Zelle die Transliteration / Transkription des antiken Texts der Zeile, in der jeweils rechten Zelle die dazugehörige Übersetzung. Zeilenzahlen sind ausschließlich in der Transliteration / Transkription des antiken Texts anzuführen: Am Anfang jeder Tafel- / Textzeile ist die Zeilenzahl als hochgestellte Zahl einzufügen. Sofern der Zeilenanfang nicht in die Mitte eines Worts fällt, ist vor und nach der Zeilenzahl ein Leerzeichen zu setzen. Die editorischen Prinzipien der Textbearbeitung sollten grundsätzlich an die <a href="/Einfuehrung/Editorische_Prinzipien.html" target="_self">EUPT-Konventionen</a> angelehnt sein. Autorinnen und Autoren steht jedoch frei, die drei keilalphabetischen Alephzeichen nicht durch die Vokalbuchstaben {a}, {i} und {u}, sondern durch die Zeichen {a͗}, {i͗} und {u͗} wiederzugeben. In dem Fall ist Aleph durch das Unicode-Zeichen <i>Modifier letter right half ring</i> (ʾ) und Ayin durch das Unicode-Zeichen <i>Modifier letter left half ring</i> (ʿ) wiederzugeben (andernfalls – entsprechend den <a href="/Einfuehrung/Editorische_Prinzipien.html" target="_self">EUPT-Prinzipien</a> – durch das Zeichen <i>Modifier Letter Glottal Stop</i> [ˀ] bzw. <i>Modifier Letter Reversed Glottal Stop</i> [ˁ]).</p>
        <p><i>Abbildungen:</i> Abbildungen sind in die Word- und PDF-Datei des eingereichten Beitrags einzubinden. Zusätzlich ist jede Abbildung als separate Datei (Abbildung in bestmöglicher Qualität) einzureichen (große Dateien können beispielsweise über <a href="https://wetransfer.com/" target="_blank">WeTransfer</a> an die Herausgeber geschickt werden). Im Fall urheberrechtlich geschützter Abbildungen müssen Autorinnen und Autoren eine schriftliche Genehmigung der Urheberrechtsinhaberinnen und -inhaber vorweisen, die Abbildungen in dem Beitrag veröffentlichen zu dürfen (s. § 17 der ethischen Publikationsrichtlinien).</p>
        <p><i>Zitation / Bibliographie:</i> Auf Forschungsliteratur ist im Hauptteil und in den Fußnoten der Miszelle in Form von Kurzzitaten (<i>In-Text Citations</i>) zu verweisen, und zwar entsprechend den Vorgaben des <a href="https://www.chicagomanualofstyle.org/tools_citationguide/citation-guide-2.html" target="_blank"><i>Chicago Manual of Style</i> (<i>Author-Date-Style</i>)</a> (s. die Beispiele in Anm. 1 der <a href="/assets/images-content/GMU_manuscript-sample.docx" target="_blank">Manuskriptvorlage für GMU-Beiträge</a>). Sämtliche in Kurzzitaten genannten Arbeiten sind im Literaturverzeichnis am Ende des Beitrags aufzuschlüsseln: Für jedes Werk ist ein vollständiger bibliographischer Eintrag (<i>Reference List Entry</i>) entsprechend den Richtlinien des <a href="https://www.chicagomanualofstyle.org/tools_citationguide/citation-guide-2.html" target="_blank"><i>Chicago Manual of Style</i> (<i>Author-Date-Style</i>)</a> anzulegen. Ausnahmen von den Vorgaben des <i>Chicago Manual of Style</i> sind den Kommentaren zum beispielhaften Literaturverzeichnis in der <a href="/assets/images-content/GMU_manuscript-sample.docx" target="_blank">Manuskriptvorlage für GMU-Beiträge</a> zu entnehmen.</p>
        <p><i>Abkürzungen:</i> Für Abkürzungen und Sigel sollten sich Autorinnen und Autoren an den von <a href="https://rla.badw.de/reallexikon/abkuerzungslisten.html" target="_blank">RlA</a> und <a href="/Einfuehrung/Abkuerzungen.html" target="_self">EUPT</a> geführten Verzeichnissen orientieren (sowohl im Hauptteil der Miszelle als auch im Literaturverzeichnis). Zusätzliche (und / oder in Ausnahmefällen von RlA / EUPT abweichende) Abkürzungen und Sigel sind vollständig aufzuschlüsseln (im Abschnitt <i>Abkürzungen</i> vor dem Literaturverzeichnis; Schema: „Abkürzung = Vollzitat“ bzw. „Sigel = Erklärung des Sigels“; s. die Zitationsvorgaben). Autorinnen und Autoren können auf den Gebrauch von Abkürzungen / Sigel auch ganz verzichten.</p></details>
<details class="GMU-Einfuehrung-h-details">
<summary><h3>Informationen zum Review-Verfahren</h3></summary>
    <p>Nachdem die Autorinnen und Autoren das Beitragsmanuskript eingereicht haben, prüfen die Herausgeber intern, ob die Veröffentlichung der Miszelle in den GMU grundsätzlich in Betracht kommt. Ist dies der Fall, verpflichten sie zwei unabhängige Forschende, den Beitrag zu begutachten. Das Peer Review-Verfahren wird im Doppelblind-Modus durchgeführt (<a href="https://authorservices.wiley.com/Reviewers/journal-reviewers/what-is-peer-review/types-of-peer-review.html" target="_blank">Double-Blind Peer Review</a>).<sup>a</sup> Das Peer Review sichert die wissenschaftliche Qualität und Integrität der GMU. Der Doppelblind-Modus gewährleistet ein Höchstmaß an Objektivität von Seiten der Gutachtenden, aber auch von Seiten der Autorinnen und Autoren, die nach der Begutachtung gegebenenfalls aufgefordert sind, den Beitrag zu überarbeiten und dabei die Anmerkungen der Gutachtenden gebührend zu berücksichtigen.</p>
    <p>Für das Peer Review benennen die Herausgeber zwei unabhängige Forschende mit einschlägiger Expertise (die Gutachtenden gehören nicht dem Herausgeberteam an). Die Gutachtenden geben am Ende ihres Reviews eine Empfehlung ab, ob der eingereichte Beitrag veröffentlicht werden sollte oder nicht (s.u.). Ist das Votum der beiden Gutachtenden nicht eindeutig, wird ein drittes unabhängiges Gutachten eingeholt. Anschließend entscheiden die Herausgeber final darüber, ob der eingereichte Beitrag zur Publikation in den GMU akzeptiert oder abgelehnt wird. Dabei folgen sie der Empfehlung der Mehrheit der Gutachtenden (der Beitrag wird angenommen, wenn mindestens zwei Gutachten positiv sind; der Beitrag wird abgelehnt, wenn mindestens zwei Gutachten negativ sind). Empfehlen die Gutachtenden den Beitrag nur unter der Voraussetzung zur Publikation, dass die in den Gutachten festgestellten Mängel vor der Veröffentlichung behoben werden, fordern die Herausgeber die Autorinnen und Autoren auf, den Beitrag entsprechend zu überarbeiten. Nachdem die Autorinnen und Autoren die überarbeitete Fassung des Beitrags eingereicht haben, entscheiden die Herausgeber (gegebenenfalls nach Rücksprache mit den Gutachtenden), ob die in den Gutachten genannten Voraussetzungen für die Publikation erfüllt sind.</p>
    <p style="margin-bottom: 0.5em;">Die beauftragten Gutachtenden verpflichten sich zu höchster Sachlichkeit und wissenschaftlicher Professionalität und lehnen die Begutachtung ab, falls ein Interessenkonflikt vorliegt oder sie außerstande sind, den Beitrag zeitnah zu begutachten (s. § 10–12 der ethischen Publikationsrichtlinien). Die Gutachtenden sind aufgefordert, den eingereichten Beiträge objektiv zu begutachten, die Plausibilität der aufgestellten Thesen zu prüfen, den Erkenntnisgehalt des Beitrags einzuschätzen und gegebenenfalls auf Schwächen und ethische Vorbehalte (beispielsweise bei Anzeichen auf ein Plagiat) hinzuweisen (Empfehlungen für gutes Peer Review finden sich bei <bibl>Allen et al. 2019<VuepressApiPlayground url="/api/eupt/biblio/TNZSK8T9" method="get" :data="[]"/></bibl> [<a href="https://doi.org/10.1002/leap.1222" target="_blank">Online-Version</a>]). Die Gutachtenden werden ersucht, am Ende ihres Reviews eine der folgenden Empfehlungen abzugeben:</p>
    <ol type="1" style="margin-top: 0.5em; margin-bottom: 0.5em;">
        <li>Der Beitrag wird zur Publikation empfohlen (ohne Einschränkungen).</li>
        <li>Der Beitrag wird zur Publikation empfohlen (mit kleineren Verbesserungsvorschlägen).</li>
        <li>Der Beitrag wird zur Publikation empfohlen, sofern die im Review aufgezeigten Mängel vorher behoben werden.</li>
        <li>Der Beitrag wird nicht zur Publikation empfohlen.</li></ol>
    <p style="margin-top: 0.5em;">Angesichts der Kürze der in GMU veröffentlichten Beiträge werden die verpflichteten Gutachtenden gebeten, ihr Gutachten innerhalb von zwei Wochen (maximal innerhalb von vier Wochen) per E-Mail an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a> zu senden (das Gutachten kann direkt in die E-Mail gesetzt werden oder als Word- oder PDF-Datei der E-Mail beigefügt werden). Hier findet sich eine <a href="/assets/images-content/GMU_e-mail-template-for-reviewers.docx" target="_blank">E-Mail-Vorlage für die Einreichung eines Gutachtens</a>.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Weder kennen die Gutachtenden die Identität der Autorinnen und Autoren, noch kennen die Autorinnen und Autoren die Identität der Gutachtenden. Außerdem ist den einzelnen Gutachtenden die Identität der anderen Gutachtenden nicht bekannt. Die Identität der Gutachtenden sowie die Gutachten selbst werden auch nach der Publikation des Beitrags nicht offengelegt.</li></ol>
</details>





<!----------------------------------------------------------------->
<!------------{Ethische Publikationsrichtlinien}------------------->
<!----------------------------------------------------------------->

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Ethische Publikationsrichtlinien</h3></summary>
    <p id="GMU-Publikationsrichtlinien">Alle, die an der Veröffentlichung der <i>Göttinger Miszellen zur Ugaritistik</i> (GMU) mitwirken, sind verpflichtet, den folgenden ethischen Publikationsrichtlinien zu folgen. Die Richtlinien orientieren sich an den Empfehlungen des <a href="https://publicationethics.org/" target="_blank">Committee on Publication Ethics (COPE)</a> (s. die <a href="https://publicationethics.org/core-practices" target="_blank">COPE <i>Core Practices</i></a>). Bei Verdacht auf einen Verstoß wenden Sie sich bitte an die Herausgeber (per E-Mail an <a href="mailto:ugarit@uni-goettingen.de">ugarit@uni-goettingen.de</a>). Fälle mutmaßlichen Fehlverhaltens prüfen und bearbeiten die Herausgeber angelehnt an die einschlägigen <a href="https://publicationethics.org/guidance/Flowcharts" target="_blank">COPE <i>Flowcharts</i></a>.</p>

<h1 class="GMU-guidelines-h1">Pflichten der Herausgeber, Gutachter*innen und Autor*innen</h1>
<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-1">§ 1: Sämtliche Informationen über das Publikationsverfahren werden vertraulich behandelt.</summary>
    <p>Herausgeber, Gutachter*innen / Reviewer*innen<sup>a</sup> und Autor*innen behandeln sämtliche während des Publikationsprozesses erlangten Informationen vertraulich. Autor*innen geben die Mitteilungen der Herausgeber sowie die Gutachten der Reviewer*innen nicht an andere Personen weiter und veröffentlichen diese nirgends.<sup>b</sup> Herausgeber und Gutachter*innen geben keinerlei Informationen über den eingereichten Beitrag<sup>c</sup> an unbeteiligte Personen weiter und veröffentlichen nirgends Informationen über das laufende Publikationsverfahren. Herausgeber und Gutachter*innen teilen den eingereichten Beitrag (i. e. das Manuskript als Word- / PDF-Dokument) vor seiner Veröffentlichung nicht mit Unbeteiligten und machen das Beitragsmanuskript nirgends öffentlich zugänglich. Herausgeber und Gutachter*innen verwenden keine Informationen, die sie aus dem eingereichten (noch unveröffentlichten) Beitrag gewinnen, für ihre eigene Forschung, ohne vorher die explizite schriftliche Zustimmung der Autor*innen einzuholen.</p>
    <ol type="a" class="GMU-guidelines-notes">
        <li>Dies betrifft sowohl die Wissenschaftler*innen, die den jeweiligen Beitrag begutachten, als auch die Wissenschaftler*innen, die von den Herausgebern eingeladen werden, den Beitrag zu begutachten, die Begutachtung jedoch ablehnen. Letztere werden als <i>potenzielle Gutachter*innen</i> / <i>Reviewer*innen</i> bezeichnet.</li>
        <li>Autor*innen können die Herausgeber um Erlaubnis bitten, Mitteilungen / Gutachten an andere Personen weiterzugeben. Die Herausgeber werden dies jedoch nur in Ausnahmefällen genehmigen. Wünschen Autor*innen, ein oder mehrere Gutachten an andere Personen weiterzugeben, halten die Herausgeber Rücksprache mit den betreffenden Gutachter*innen und holen deren Zustimmung ein.</li>
        <li>Beispielsweise Informationen über die Autor*innen, über die Gutachter*innen, über die Gutachten (bzw. die Gutachten selbst) oder über die Entscheidung, ob der eingereichte Beitrag zur Publikation angenommen wird oder abgelehnt wird.</li>
    </ol>
</details>

<h1 class="GMU-guidelines-h1">Pflichten der Herausgeber</h1>
<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-2">§ 2: Die Herausgeber tragen die Verantwortung für die Inhalte der Zeitschrift.</summary>
    <p>Die Herausgeber entscheiden, ob und wann der jeweils eingereichte Beitrag veröffentlicht wird (die finale Entscheidung, ob der eingereichte Beitrag veröffentlicht wird, treffen sie auf Grundlage der zuvor eingeholten externen Gutachten [Double-Blind Peer Review; s. § 5]; die Publikation erfolgt in jedem Fall erst, nachdem die Autor*innen der Veröffentlichung zugestimmt haben [s. § 7]). Die Herausgeber sind verantwortlich für den gesamten Inhalt der Zeitschrift.</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-3">§ 3: Die Herausgeber behandeln sämtliche Informationen über den Publikationsprozess vertraulich (Ergänzung zu § 1).</summary>
    <p>Die Herausgeber geben keine Informationen über laufende Publikationsverfahren an irgendjemand anderen als Autor*innen, Gutachter*innen und potenzielle Gutachter*innen weiter. Den Autor*innen leiten sie das Urteil des internen Reviews sowie die Gutachten der Reviewer*innen (ohne namentliche Nennung der Reviewer*innen) weiter. An potenzielle Gutachter*innen (s. § 1 Anm. a) geben sie (im Zuge der redaktionellen Anfrage, ob die Wissenschaftler*innen bereit sind, den eingereichten Beitrag zu begutachten) lediglich den Titel des Beitrags sowie Angaben zur Länge des Beitrags weiter. Gutachter*innen erhalten den zuvor anonymisierten Beitrag als PDF. Auch nach der Publikation des jeweiligen Beitrags veröffentlichen die Herausgeber weder die Namen der Gutachter*innen noch die Gutachten selbst.</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-4">§ 4: Die Herausgeber garantieren ein objektives und transparentes internes Review-Verfahren.</summary>
    <p>Die Herausgeber entscheiden noch vor dem Double-Blind Peer Review, ob der jeweils eingereichte Beitrag grundsätzlich geeignet ist, um in den GMU veröffentlicht zu werden. Sie prüfen, ob der eingereichte Beitrag thematisch zum Fokus der GMU passt, ob er die formalen Kriterien der GMU erfüllt<sup>a</sup> und ob die Autor*innen die Publikationsrichtlinien der Zeitschrift beachtet haben. Gleichzeitig berücksichtigen die Herausgeber die Validität / den akademischen Wert des eingereichten Beitrags und seine Relevanz für die Forschung. Herkunft, Geschlecht, sexuelle Orientierung, Staatsbürgerschaft, Religion, politische Einstellung oder institutionelle Zugehörigkeit der Autor*innen spielen im internen Review-Verfahren keine Rolle.</p>
    <p>Die Herausgeber achten im Rahmen des internen Review-Verfahrens auf mögliche Interessenkonflikte. Herausgeber, die in persönlichem, beruflichem oder kompetitivem Verhältnis zu den Autor*innen des eingereichten Beitrags stehen oder standen und sich aus diesem Grund keine objektive Meinung über den eingereichten Beitrag bilden können, beteiligen sich nicht am internen Review-Verfahren und übertragen die Entscheidung, ob der eingereichte Beitrag grundsätzlich für die Publikation in den GMU in Betracht kommt und an zwei unabhängige Gutachter*innen weitergeleitet wird, an ihre Mitherausgeber.</p>
    <p>Befinden die Herausgeber den eingereichten Beitrag für grundsätzlich geeignet, um in den GMU veröffentlicht zu werden, leiten sie den Beitrag (in anonymisierter Form) an zwei unabhängige wissenschaftliche Gutachter*innen weiter (s. § 5).<sup>b</sup> Befinden die Herausgeber den eingereichten Beitrag für ungeeignet für die Publikation in den GMU, informieren sie die Autor*innen unmittelbar über ihre Entscheidung. Die Entscheidung wird begründet.</p>
    <ol type="a" class="GMU-guidelines-notes">
        <li>Die Herausgeber prüfen beispielsweise die Vollständigkeit der eingereichten Unterlagen; sie berücksichtigen die Länge des eingereichten Artikels und prüfen, ob die Zitationsvorgaben eingehalten sind.</li>
        <li>Gelangen die Herausgeber im internen Review-Verfahren zu keinem eindeutigen Urteil, leiten sie den Beitrag ebenfalls an zwei externe Reviewer*innen weiter.</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-5">§ 5: Die Herausgeber gewährleisten ein objektives und transparentes Double-Blind Peer Review-Verfahren.</summary>
    <p>Die Herausgeber leiten den jeweils eingereichten Beitrag – sofern er im internen Review-Verfahren für grundsätzlich geeignet befunden wurde (s. § 4) – an zwei externe Reviewer*innen zur Begutachtung weiter.<sup>a</sup> Für das Peer Review benennen die Herausgeber zwei Expert*innen in dem Forschungsfeld, das in dem eingereichten Beitrag behandelt wird. Die Herausgeber geben keine Informationen über die Autor*innen des eingereichten Beitrags an die Gutachter*innen weiter. Bevor die Herausgeber den eingereichten Beitrag an die Reviewer*innen weiterleiten, anonymisieren sie ihn: Jede Angabe und jede Aussage im eingereichten Manuskript, die die Autor*innen des Beitrags identifizieren könnte, wird durch einen Platzhalter ersetzt. Umgekehrt geben die Herausgeber keine Informationen über die Reviewer*innen an die Autor*innen weiter. Die Gutachten sowie die Namen der Gutachter*innen werden auch nach Abschluss des Publikationsverfahrens nirgends veröffentlicht.</p>
    <p>Die Herausgeber stützen die finale Entscheidung, ob der eingereichte Beitrag in den GMU veröffentlicht wird, maßgeblich auf die wissenschaftlichen Gutachten der Reviewer*innen. Vorausgesetzt, dass die Herausgeber nicht feststellen, dass die Reviewer*innen gegen die Publikationsrichtlinien der GMU verstoßen haben, folgen sie der Empfehlung der Mehrheit der Gutachter*innen, ob und unter welchen Auflagen der Beitrag veröffentlicht werden soll. Gelangen die beiden Reviewer*innen zu keinem eindeutigen Urteil,<sup>b</sup> wird ein drittes Gutachten eingeholt.</p>
    <p>Die Herausgeber leiten die Gutachten der Reviewer*innen schnellstmöglich an die Autor*innen weiter (ohne die Reviewer*innen zu identifizieren). Ebenso teilen sie den Autor*innen baldmöglichst die finale Entscheidung mit, ob und unter welchen Auflagen der eingereichte Beitrag in den GMU veröffentlicht werden kann. Die Herausgeber begründen ihre Entscheidung.</p>
    <ol type="a" class="GMU-guidelines-notes">
        <li>Die Reviewer*innen sind nicht Mitherausgeber*innen der GMU.</li>
        <li>Namentlich, wenn der / die Erstgutachter*in den Beitrag – gegebenenfalls nach Behebung der im Review festgestellten Mängel – zur Publikation in den GMU empfiehlt, während der / die Zweitgutachter*in gegen die Publikation des Beitrags ausspricht.</li>
    </ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-6">§ 6: Die Herausgeber setzen sich aktiv dafür ein, dass alle am Publikationsprozess Beteiligten die ethischen Publikationsrichtlinien wahren.</summary>
        <p>Vermuten die Herausgeber, dass Autor*innen oder Gutachter*innen gegen die Publikationsrichtlinien der GMU verstoßen haben, oder werden sie von anderen Personen über ethische Vorbehalte informiert, reagieren sie entsprechend. Jeder Fall potenziell unethischen Verhaltens wird untersucht (unabhängig davon, ob der betreffende Beitrag bereits publiziert ist). Ergeben die Ermittlungen, dass im Zuge des Publikationsverfahrens des betreffenden Beitrags gegen die ethischen Richtlinien der GMU verstoßen wurde, setzen die Herausgeber entsprechende Maßnahmen.</p>
        <p>Verstoßen Gutachter*innen gegen die Publikationsrichtlinien der GMU, berufen die Herausgeber die Gutachter*innen ab und erklären bereits vorgelegte Gutachten für ungültig. Sofern der eingereichte Beitrag noch nicht veröffentlicht ist, werden die abberufenen Gutachter*innen durch neu ernannte, zuvor am Publikationsprozess unbeteiligte Gutachter*innen ersetzt.<sup>a</sup> Ist der betreffende Beitrag bereits veröffentlicht, informieren die Herausgeber die Autor*innen über den Verstoß im Rahmen des Review-Verfahrens. Auf Wunsch der Autor*innen veröffentlichen die Herausgeber auf den Publikationsportalen der GMU<sup>b</sup> ein entsprechendes Statement, in dem sie den Verstoß offenlegen.</p>
        <p>Verstoßen Autor*innen gegen die Publikationsrichtlinien der GMU und ist der betreffende Beitrag noch nicht veröffentlicht, fordern die Herausgeber die Autor*innen auf, die festgestellten Mängel zu beheben (kommen die Autor*innen der Aufforderung nicht nach, wird der eingereichte Beitrag abgelehnt); im Fall schwerer Verstöße wird der eingereichte Beitrag sofort abgelehnt. Ist der betreffende Beitrag bereits publiziert, veröffentlichen die Herausgeber auf den Publikationsportalen der GMU (s. § 6 Anm. b) a) ein Statement, das über den festgestellten Verstoß informiert und eine an Leser*innen gerichtete Warnung enthält, und b) eine neue Version des Beitrags samt dem genannten Statement (Punkt a) auf dem Titelblatt. Die Herausgeber fordern die Autor*innen in dem Fall auf, die ältere Version des Beitrags auch auf allen anderen Portalen, auf denen die Autor*innen den Beitrag zugänglich gemacht haben, durch die neue Version des Beitrags (inkl. des Statements der Herausgeber) zu ersetzen. Kommen die Autor*innen der Aufforderung nicht nach, halten die Herausgeber dies in ihrem Statement über den Verstoß fest (i. e. in dem oben [in diesem Absatz] unter Punkt a genannten Statement). Die Herausgeber können zusätzlich eine Sperrfrist festsetzen, in der die betreffenden Autor*innen von Publikationen in den GMU ausgeschlossen sind.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Die Herausgeber leiten sämtliche Gutachten samt den für ungültig erklärten Gutachten an die Autor*innen weiter. Die Herausgeber begründen in der Mitteilung an die Autor*innen, weshalb ein oder mehrere Gutachten für ungültig befunden wurden.</li>
            <li>Derzeit: <a href="https://zenodo.org/communities/gmu/" target="blank">https://<wbr>zenodo.org/<wbr>communities/<wbr>gmu/</a> und <a href="/GMU/Einfuehrung.html" target="blank">https://<wbr>eupt.<wbr>uni-<wbr>goettingen.de/<wbr>GMU/<wbr>Einfuehrung.html</a>.</li>
        </ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-7">§ 7: Die Herausgeber veröffentlichen eingereichte Beiträge erst nach Abschluss des Publikationsverfahrens und mit Zustimmung der Autor*innen.</summary>
        <p>Die Herausgeber veröffentlichen den jeweiligen Beitrag erst, nachdem das interne Review-Verfahren sowie das Double-Blind Peer Review-Verfahren erfolgreich abgeschlossen worden sind, die Autor*innen den eingereichten Beitrag in gegebenenfalls überarbeiteter Fassung erneut eingereicht haben, die Autor*innen die von den Herausgebern erstellte vorläufige Endversion des Beitrags gelesen und kontrolliert haben und die Autor*innen der Publikation der finalen Fassung schriftlich zugestimmt haben.</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-8">§ 8: Die Herausgeber informieren die Autor*innen nach der Publikation des Beitrags, wenn sie auf signifikante Fehler oder Ungenauigkeiten im Beitrag stoßen.</summary>
        <p>Werden die Herausgeber nach der Veröffentlichung des Beitrags auf signifikante Fehler oder Ungenauigkeiten in dem Artikel hingewiesen (i.d.R. von zuvor am Publikationsprozess unbeteiligten Personen), oder stoßen sie selbst auf zuvor im internen Review und im Peer Review übersehene Fehler oder Ungenauigkeiten, informieren sie die Autor*innen des Beitrags unverzüglich darüber. Die Herausgeber entscheiden über etwaige Maßnahmen (nach Rücksprache mit den Autor*innen) und halten die Autor*innen darüber auf dem Laufenden. Zu den möglichen Maßnahmen s. § 20.</p>
</details>

<h1 class="GMU-guidelines-h1">Richtlinien für das Peer Review und Pflichten der Gutachter*innen</h1>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-9">§ 9: Die Reviewer*innen wahren die Publikationsrichtlinien der GMU und folgen dem <i>Best Practice</i> für Peer Reviews.</summary>
        <p>Gutachter*innen (und potenzielle Gutachter*innen; s. § 1 Anm. a) wahren die ethischen Publikationsrichtlinien der GMU.<sup>a</sup> Sie behandeln Dokumente und Informationen, an die sie in ihrer Tätigkeit als Reviewer*innen noch vor der Publikation des betreffenden Beitrags gelangen, vertraulich (s. § 1). Sie geben Dokumente und Informationen über den eingereichten Beitrag nicht an unbeteiligte Personen weiter, machen diese nirgends öffentlich zugänglich und nutzen sie nicht für ihre eigene Forschung. Über den Beitrag und das Gutachten tauschen sich Gutachter*innen ausschließlich mit den Herausgebern aus. Sie garantieren ein faires und objektives Double-Blind Peer Review und unternehmen keine Versuche, die Identität der Autor*innen des begutachteten Beitrags in Erfahrung zu bringen.</p>
        <p>Reviewer*innen folgen bei der Begutachtung den etablierten <i>Best Practice</i>-Standards für Peer Review-Verfahren (vgl. <bibl>Allen et al. 2019<VuepressApiPlayground url="/api/eupt/biblio/TNZSK8T9" method="get" :data="[]"/></bibl> [<a href="https://doi.org/10.1002/leap.1222" target="_blank">Online-Version</a>]; vgl. auch die <a href="https://www.elsevier.com/de-de/about/policies-and-standards/publishing-ethics#3-duties-of-reviewers" target="_blank"><i>Duties of Reviewers</i> im Elsevier-Verlag</a>). Fair, objektiv und sachlich prüfen die Gutachter*innen die Validität der im Beitrag vorgebrachten Thesen und den wissenschaftlichen Wert des Beitrags (s. § 9). Die im Gutachten geäußerte Kritik ist konstruktiv. Die Gutachter*innen weisen in ihren Gutachten auf ethische Vorbehalte gegen den eingereichten Beitrag hin (beispielsweise bei Verdacht auf Plagiat). Nachdem sie der Begutachtung zugestimmt haben, begutachten sie den Beitrag möglichst schnell (s. § 10).</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Bei Verstößen gegen die ethischen Publikationsrichtlinien der GMU ergreifen die Herausgeber entsprechende Maßnahmen (s. § 6).</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-9">§ 10: Die Reviewer*innen begutachten den eingereichten Beitrag objektiv und äußern konstruktive Kritik.</summary>
        <p>Die Gutachter*innen überprüfen die wissenschaftliche Qualität und Integrität des eingereichten Beitrags und tragen so dazu bei, die Qualität und Integrität der GMU zu sichern. Die Gutachter*innen prüfen den wissenschaftlichen Wert des begutachteten Beitrags und die Plausibilität der vorgebrachten Thesen, identifizieren etwaige Schwächen und achten auf mögliche Verstöße gegen die ethischen Publikationsrichtlinien der GMU oder die Standards guter wissenschaftlicher Praxis. Erkennen Gutachter*innen Aussagen in dem begutachteten Beitrag, die bereits in einer früheren Publikation gemacht wurden und in dem vorliegenden Beitrag nicht ordnungsgemäß zitiert sind, oder wesentliche Ähnlichkeiten oder Überschneidungen zwischen dem begutachteten Beitrag und anderen ihnen bekannten Arbeiten (veröffentlicht oder unveröffentlicht), weisen sie in ihrem Gutachten darauf hin.</p>
        <p>Die Gutachten der Reviewer*innen sind fair, objektiv und sachlich. Anmerkungen sind klar formuliert und durch Argumente gestützt, sodass die Autor*innen diese nutzen können, um den Beitrag zu verbessern.</p>
        <p>Die Gutachten der Reviewer*innen bilden die Grundlage für die Entscheidung der Herausgeber, ob der eingereichte Beitrag in den GMU veröffentlicht oder abgelehnt wird. Die Gutachter*innen geben ihre Empfehlung ab, ob der Beitrag veröffentlicht werden sollte (gegebenenfalls nach Korrektur kleinerer Fehler), ob der Beitrag nur publiziert werden sollte, wenn vorher die im Gutachten festgestellten Mängel behoben worden sind, oder ob der Beitrag nicht veröffentlicht werden sollte. Empfehlen die Gutachter*innen die Publikation nur, wenn die Autor*innen vorher die aufgezeigten Mängel beheben, entscheiden die Herausgeber, nachdem die Autor*innen die überarbeitete Fassung des Beitrags eingereicht haben, ob die Autor*innen die von den Gutachter*innen vorgebrachte Kritik gebührend berücksichtigt haben und die festgestellten Mängel behoben haben. Bei Unsicherheiten holen die Herausgeber erneut den Rat der Gutachter*innen ein.</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-11">§ 11: Die Reviewer*innen begutachten den eingereichten Beitrag möglichst schnell.</summary>
        <p>Nachdem sich die angefragten Reviewer*innen bereit erklärt haben, den eingereichten Beitrag zu begutachten, und ihnen die Herausgeber die relevanten Dokumente zugesandt haben, erstellen die Reviewer*innen das Gutachten möglichst schnell (die angestrebte Schnelligkeit soll die gründliche Begutachtung des Beitrags hinsichtlich Qualität und Integrität jedoch nicht beeinträchtigen).<sup>a</sup> Sehen sich eingeladene Gutachter*innen außer Stande, den eingereichten Beitrag in der dafür vorgesehenen Zeit zu begutachten (s. Anm. a), informieren sie die Herausgeber schnellstmöglich darüber und treten gegebenenfalls von der Begutachtung zurück.<sup>b</sup></p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Im Idealfall übermitteln Reviewer*innen das Gutachten innerhalb von zwei Wochen an die Herausgeber (maximal innerhalb von vier Wochen).</li>
            <li>Lehnen die eingeladenen Reviewer*innen die Begutachtung ab, können sie die Herausgeber auf andere potenzielle Reviewer*innen hinweisen.</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-12">§ 12: Die eingeladenen Reviewer*innen informieren die Herausgeber über Interessenkonflikte und möglicherweise mangelnde Expertise und treten gegebenenfalls von der Begutachtung zurück.</summary>
        <p>Eingeladene Reviewer*innen lehnen die Begutachtung von Beiträgen ab, wenn sie Kenntnis darüber haben, von wem der eingereichte und für das Review anonymisierte Beitrag verfasst wurde (beispielsweise, weil sich die Autor*innen vor Einreichung des Beitrags mit den eingeladenen Reviewer*innen über den Beitrag austauschten), und vor allem dann, wenn die Objektivität der Reviewer*innen aufgrund von Interessenkonflikten nicht gewährleistet ist (beispielsweise, wenn Reviewer*innen und Autor*innen in persönlicher, beruflicher oder kompetitiver Beziehung stehen oder standen).</p>
        <p>Sehen sich eingeladene Reviewer*innen aufgrund mangelnder fachlicher Expertise außer Stande, den eingereichten Beitrag zu begutachten, informieren sie die Herausgeber schnellstmöglich darüber und lehnen die Begutachtung ab.</p>
        <p>Wenn eingeladene Reviewer*innen die objektive und fachlich fundierte Begutachtung des eingereichten Beitrags nicht gewährleisten können und folglich von der Begutachtung zurücktreten, benennen die Herausgeber andere Gutachter*innen.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Die zurückgetretenen Reviewer*innen sind eingeladen, die Herausgeber auf andere qualifizierte, potenzielle Reviewer*innen hinzuweisen.</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-13">§ 13: Die Reviewer*innen weisen die Herausgeber auf andere potenzielle Reviewer*innen hin, deren Gutachten ihrer Meinung nach eingeholt werden sollte.</summary>
        <p>Im Fall strittiger Thesen und komplexer Fragestellungen informieren die Reviewer*innen die Herausgeber, wenn ihrer Meinung nach das Gutachten eines bestimmten Wissenschaftlers / einer bestimmten Wissenschaftlerin zu dem eingereichten Beitrag eingeholt werden sollte. Die Herausgeber entscheiden intern darüber, ob sie dem Vorschlag folgen.</p>
</details>

<h1 class="GMU-guidelines-h1">Pflichten der Autor*innen</h1>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-14">§ 14: Die Person, die den Beitrag einreicht, ist korrespondierende Autorin / korrespondierender Autor des Beitrags und garantiert, dass alle Autor*innen ordnungsgemäß genannt sind.</summary>
        <p>Nur die Autorin / der Autor bzw. eine / einer der Autor*innen reicht den Beitrag zur Publikation ein. Autorin / Autor ist, wer maßgeblich zur Konzeption, Durchführung, Datenerhebung oder Analyse der dem eingereichten Beitrag zugrundeliegenden Studie beigetragen hat. Autor*innen haben den eingereichten Beitrag (mit-)verfasst oder inhaltlich wesentlich überarbeitet. Andere Personen, die an dem Beitrag mitgewirkt haben, die Kriterien für Autorschaft jedoch nicht erfüllen, werden im Abschnitt <i>Danksagung</i> am Ende des Beitrags erwähnt (zu den Kriterien von Autorschaft vgl. das <a href="https://doi.org/10.24318/cope.2019.3.3" target="_blank">COPE <i>Discussion Document Authorship</i></a>).</p>
        <p>Die Person, die den Beitrag einreicht, tritt als korrespondierende Autorin / korrespondierender Autor auf. Sie stellt sicher, dass etwaige weitere Autor*innen des Beitrags ordnungsgemäß als solche genannt sind und die Co-Autor*innen zugestimmt haben, dass das Beitragsmanuskript zur Publikation in den GMU eingereicht wird. Die korrespondierende Autorin / der korrespondierende Autor garantiert, dass sie / er die Co-Autor*innen über das Publikationsverfahren auf dem Laufenden hält und jeweils die Zustimmung der Co-Autor*innen einholt, bevor sie / er etwaige überarbeitete Versionen des Beitrags einreicht und bevor sie / er die Publikationsfreigabe erteilt (sie / er stellt sicher, dass die Co-Autor*innen die jeweils eingereichte oder zur Publikation freigegebene Fassung des Beitrags gelesen und akzeptiert haben).</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-15">§ 15: Die Autor*innen folgen den Standards guter wissenschaftlicher Praxis, wahren die Publikationsrichtlinien der GMU und informieren die Herausgeber über mögliche Interessenkonflikte.</summary>
        <p>Autor*innen wahren die allgemeinen Standards guter wissenschaftlicher Praxis (s. dazu den <a href="https://doi.org/10.5281/zenodo.6472827" target="_blank">Leitfaden der Deutschen Forschungsgemeinschaft</a>) sowie die ethischen Publikationsrichtlinien der GMU.<sup>a</sup> Autor*innen legen mögliche Interessenkonflikte, die die Ergebnisse ihrer Forschungsarbeit beeinflusst haben könnten (beispielsweise persönliche, berufliche oder kompetitive Beziehungen zu anderen Wissenschaftler*innen), frühzeitig offen. Sie geben an, von welcher Seite sie während ihrer Arbeit an dem eingereichten Beitrag finanziell unterstützt wurden.</p>
        <p>Ist einer der Autoren gleichzeitig Herausgeber der GMU, überträgt er sämtliche herausgeberischen Pflichten während des Publikationsverfahrens seines Beitrags an die Mitherausgeber. Der Autor beteiligt sich in dem Fall nicht an internen Review-Prozessen und nimmt in seiner Tätigkeit als Herausgeber keinen Einfluss auf das Publikationsverfahren.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Bei Verstößen gegen die ethischen Publikationsrichtlinien der GMU ergreifen die Herausgeber entsprechende Maßnahmen (s. § 6).</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-16">§ 16: Die Autor*innen garantieren, dass der eingereichte Beitrag eigenständig durchgeführte und originelle Forschung widerspiegelt.</summary>
        <p>Die Autor*innen stellen in dem eingereichten Beitrag eigenständig durchgeführte und originelle Forschung vor. Sie beschreiben die durchgeführte Forschungsarbeit und die daraus gewonnenen Ergebnisse präzise und nachvollziehbar und diskutieren die Bedeutung der Forschungsergebnisse objektiv. Betrügerische oder bewusst ungenaue Darstellungen sind unethisch und daher inakzeptabel.</p>
        <p>Autor*innen stellen sicher, dass fremde und bereits von anderen oder ihnen selbst publizierte Arbeiten bzw. Teile davon im eingereichten Beitrag ordnungsgemäß zitiert sind (wörtliche Wiedergaben fremder / bereits publizierter Arbeiten sind in Anführungszeichen gestellt). Sogenanntes <i>Text recycling</i> vermeiden sie (s. dazu die entsprechende <a href="https://publicationethics.org/text-recycling-guidelines" target="_blank">COPE-Handreichung</a>).<sup>a</sup> Privat erlangte Informationen werden nur mit ausdrücklicher schriftlicher Genehmigung der Quelle verwendet.<sup>b</sup> Die sinngemäße Wiedergabe fremder bzw. bereits veröffentlichter Forschungsarbeiten sowie das Kopieren / Paraphrasieren fremder bzw. bereits veröffentlichter Beobachtungen, Argumente oder Ergebnisse, ohne dass die jeweilige Quelle angegeben ist, gelten als Plagiat. Plagiate sind unethisch und inakzeptabel.</p>
        <p>Der eingereichte Beitrag und die darin besprochenen Forschungsergebnisse wurden noch nicht andernorts veröffentlicht.<sup>c</sup> Der Beitrag bildet nicht die substantiell gleiche Forschung ab, die die Autor*innen bereits (in den GMU oder andernorts) veröffentlicht haben (es sei denn, dies ist dadurch gerechtfertigt, dass bereits vorher veröffentlichte Ergebnisse systematischer Prüfung unterzogen werden). Die Autor*innen garantieren bei Einreichung, dass der Beitrag nicht bereits zur Publikation in einem anderen Publikationsmedium vorgesehen ist und nicht gleichzeitig bei einer anderen Zeitschrift oder einem anderen Publikationsmedium eingereicht wurde.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Autor*innen berücksichtigen, dass Reviewer*innen, die den eingereichten Beitrag in anonymisierter Form begutachten, <i>Text recycling</i> als Plagiat einstufen.</li>
            <li>Informationen, die im Zuge vertraulicher Verfahren (beispielsweise im Rahmen von Begutachtungsverfahren / Publikationsverfahren / der Beantragung von Fördermitteln) erlangt wurden, dürfen ohne Genehmigung aller an dem betreffenden Verfahren beteiligten Personen nicht genutzt werden.</li>
            <li>Vor Abschluss des Review-Verfahrens darf der eingereichte Beitrag nirgends öffentlich zugänglich sein (andernfalls ist kein Double-Blind Peer Review möglich und die Heruasgeber*innen sind gezwungen, den Beitrag abzulehnen). Machen die Autor*innen vor der Publikation in den GMU eine Vorab-Fassung des Beitrags öffentlich zugänglich (nach Abschluss des Peer Review-Verfahrens), weisen sie diese als solche aus und verweisen auf den Stand des Publikationsverfahrens in den GMU (Beispiel: „Unveröffentlichtes Autorenmanuskript; zur Veröffentlichung in den <i>Göttinger Miszellen zur Ugaritistik</i> [<a href="/GMU/Einfuehrung.html" target="_blank">https://eupt.uni-goettingen.de/GMU/Einfuehrung.html</a>] angenommen“ / „Unpublished author manuscript; accepted for publication in the <i>Göttinger Miszellen zur Ugaritistik</i> [<a href="/GMU/Einfuehrung.html" target="_blank">https://eupt.uni-goettingen.de/GMU/Einfuehrung.html</a>]“). Vor der Veröffentlichung einer Vorab-Fassung des Beitrags halten die Autor*innen Rücksprache mit den Herausgeber.</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-17">§ 17: Die Autor*innen garantieren, dass es ihnen gestattet ist, fremde und urheberrechlich geschützte Materialien in ihrem Beitrag zu veröffentlichen.</summary>
        <p>Beabsichtigen Autor*innen, in ihrem Beitrag urheberrechtlich geschützte Materialien (beispielsweise Abbildungen / Fotos) zu veröffentlichen, die sie nicht selbst erstellt haben und die nicht unter einer entsprechenden Lizenz öffentlich zugänglich sind, die die Reproduktion ausdrücklich erlaubt, holen die Autor*innen frühzeitig die Erlaubnis der Urheberrechtsinhaber*innen ein.<sup>a</sup> Spätestens bei Einreichung der zu veröffentlichenden Endfassung des Beitrags leiten die Autor*innen die schriftliche Genehmigung der Urheberrechtsinhaber*innen, die betreffenden Materialien verwenden zu dürfen, an die Herausgeber weiter (der Beitrag wird erst veröffentlicht, wenn den Herausgeber alle relevanten Genehmigungen vorliegen). Das Genehmigungsschreiben muss die Erlaubnis enthalten, die urheberrechtlich geschützten Materialien in einer frei zugänglichen online-Publikation verwenden zu dürfen, und zwar auf unbegrenzte Zeit.<sup>b</sup></p>
        <p>Enthält der eingereichte Beitrag urheberrechtlich geschützte Materialien, die unter einer Lizenz (beispielsweise einer <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC-BY-Lizenz</a>) öffentlich zugänglich sind, die die Nutzung / Reproduktion in wissenschaftlichen Publikationen ausdrücklich erlaubt, geben die Autor*innen in ihrem Beitrag die relevanten Quellen / Links an, über die die Herausgeber die Nutzungsbedingungen überprüfen können.</p>
        <p>Autor*innen zitieren urheberrechtlich geschützte Materialien korrekt und geben die Urheberrechtsinhaber*innen an.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Autor*innen bezahlen möglicherweise anfallende Gebühren. Sind die relevanten Materialien unter einer Lizenz (beispielsweise einer <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC-BY-Lizenz</a>) öffentlich zugänglich, die die Nutzung / Reproduktion in wissenschaftlichen Publikationen ausdrücklich erlaubt, sind Autor*innen grundsätzlich nicht verpflichtet, die Genehmigung der Urheberrechtsinhaber*innen einzuholen (in unsicheren Fällen wird Autor*innen jedoch empfohlen, die Genehmigung der Urheberrechtsinhaber*innen dennoch einzuholen). Um fremde, bereits veröffentlichte Texte zu zitieren (im in der Wissenschaft üblichen Rahmen; lange Reproduktionen sind meist verboten), ist gewöhnlich keine Genehmigung durch die Urheberrechtsinhaber*innen notwendig (Autor*innen beachten gegebenenfalls die Urheberrechtsrichtlinien der betreffenden Publikationen / Verlage; vgl. z. B. die <a href="https://press.uchicago.edu/resource/fairuse.html" target="_blank"><i>Guidelines for Fair Use of Our Publications</i> der University of Chicago Press</a>).</li>
            <li>Tipps zum Einholen entsprechender Genehmigungen finden sich auf der <a href="https://press.uchicago.edu/resource/permissions.html" target="_blank">Website der University of Chicago Press</a>.</li></ol>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-18">§ 18: Die Autor*innen kooperieren mit den Herausgebern, bringen sich aktiv in den Publikationsprozess ein und berücksichtigen die Gutachten bei Überarbeitung des eingereichten Beitrags.</summary>
        <p>Autor*innen beantworten Anfragen der Herausgeber schnellstmöglich. Sie nehmen die Anmerkungen und Hinweise der Herausgeber und der Gutachter*innen an und gehen im Zuge der Überarbeitung des eingereichten Beitrags in gebührendem Maß auf die Kritik der Herausgeber und der Gutachter*innen ein (ob etwaige in den Gutachten geäußerte Zweifel an der Integrität des eingereichten Beitrags oder der Validität der darin aufgestellten Thesen in der überarbeiteten Fassung des Artikels ausgeräumt sind, entscheiden die Herausgeber, gegebenenfalls nach Rücksprache mit den Reviewer*innen).</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-19">§ 19: Die Autor*innen weisen die Herausgeber auf ethische Vorbehalte hin.</summary>
        <p>Haben die Autor*innen den Verdacht, dass Personen, die am Publikationsverfahren beteiligt sind, die Publikationsrichtlinien der GMU nicht eingehalten haben, informieren sie die Herausgeber unmittelbar darüber.</p>
        <p>Werfen die Autor*innen einem oder mehreren Reviewer*innen vor, gegen die Publikationsrichtlinien verstoßen zu haben, prüfen die Herausgeber den Fall. Kommen die Herausgeber zum Schluss, dass Gutachter*innen tatsächlich gegen die Publikationsrichtlinien verstoßen haben, werten sie die betreffenden Gutachten als ungültig und holen neue Gutachten ein (s. § 6).</p>
        <p>Werfen die Autor*innen einem oder mehreren Herausgeber vor, gegen die Publikationsrichtlinien der GMU verstoßen zu haben, wird der Fall entweder durch die von den Vorwürfen nicht betroffenen Herausgeber geprüft oder von einem externen Gutachter / einer externen Gutachterin (für die Prüfung des Falls werden die Wissenschaftler*innen angefragt, die als Gutachter*innen im Peer Review-Verfahren tätig sind oder für diese Aufgabe vorgesehen sind). Ergeben die Untersuchungen, dass sich die Herausgeber tatsächlich eines Verstoßes gegen die Publikationsrichtlinien der GMU schuldig gemacht haben, sind die Herausgeber verpflichtet, alle für unethisch befundenen Entscheidungen zu widerrufen und Maßnahmen zu ergreifen, um das Publikationsverfahren entsprechend den ethischen Richtlinien der GMU fortzuführen, und zwar in enger Abstimmung mit dem Wissenschaftler / der Wissenschaftlerin, der / die die ethischen Vorbehalte der Autor*innen geprüft hat.</p>
</details>

<details class="GMU-guidelines-details">
    <summary id="GMU-Publikationsrichtlinien-Par-20">§ 20: Die Autor*innen informieren die Herausgeber nach der Publikation des Beitrags über signifikante Fehler oder Ungenauigkeiten in ihrem Beitrag.</summary>
        <p>Stellen Autor*innen nach der Publikation ihres Beitrags signifikante Fehler oder Ungenauigkeiten im Beitrag fest (oder werden sie von anderen auf signifikante Fehler oder Ungenauigkeiten im Beitrag hingewiesen), informieren sie die Herausgeber unverzüglich darüber. Die Herausgeber informieren die Autor*innen, wenn sie von anderen über signifikante Fehler oder Ungenauigkeiten in dem Beitrag informiert werden. Die Herausgeber entscheiden über etwaige Maßnahmen (nach Rücksprache mit den Autor*innen) und halten die Autor*innen darüber auf dem Laufenden.</p>
        <p>In schwerwiegenden Fällen veröffentlichen die Herausgeber auf den Publikationsportalen der GMU (s. § 6 Anm. b) a) ein entsprechendes Statement, in dem auf die Fehler / Ungenauigkeiten hingewiesen wird, und b) eine neue Version des Beitrags samt dem unter Punkt a genannten Statement auf dem Titelblatt. Die Autor*innen können ein weiteres Statement ergänzen. Die Autor*innen haben vor der Veröffentlichung der neuen Version die Möglichkeit, den Beitrag zu überarbeiten. Entscheiden sich die Autor*innen, den Beitrag für die Veröffentlichung der neuen Version zu überarbeiten, legen sie in ihrem Statement offen, wie und an welchen Stellen sie den Beitrag verändert haben. Die Autor*innen sind nach der Veröffentlichung der neuen Version des Beitrags auf den GMU-Publikationsportalen aufgefordert, die überholte Version des Beitrags auf allen anderen Portalen, auf denen sie den Beitrag zugänglich gemacht haben, durch die neue Version des Artikels zu ersetzen. Kommen die Autor*innen der Aufforderung nicht nach, vermerken die Herausgeber dies im oben (in dem in diesem Absatz) unter Punkt a genannten Statement.</p>
</details>
<p/>
</details>
</div><!--End of content of tab2-->



<!--Tab3: Publikationsmodus-->
<input id="tab3" type="radio" name="tabs">
<label class="GMU-tabs-label" for="tab3"><p id="GMU-Startseite-label-Publikationsmodus"></p></label>
<div class="GMU-tabs-content">
    <p>Die GMU werden wissenschaftsgeleitet publiziert (<a href="https://open-access.network/informieren/publizieren/wissenschaftsgeleitetes-publizieren" target="_blank"><i>Scholar-Led Publishing</i></a>; nicht-kommerziell und ohne Einbindung eines Verlags). Die Beiträge erscheinen kontinuierlich. Jeder Artikel wird veröffentlicht, sobald das Review-Verfahren erfolgreich abgeschlossen worden ist und die Autorinnen und Autoren den Herausgebern die Publikationsfreigabe erteilt haben. Die einzelnen Beiträge werden fortlaufend nummeriert und nicht in Ausgaben zusammengefasst. Dies ermöglicht einen beschleunigten Publikationsprozess.</p>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Publikationsportale</h3></summary>
    <p>Jeder Beitrag wird online auf zwei Portalen veröffentlicht: zum einen auf dem <a href="https://zenodo.org/" target="_blank">Zenodo-Portal</a>, zum anderen auf der GMU-Website (derzeit Teil des EUPT-Portals). Auf dem Zenodo-Portal (unter <a href="https://zenodo.org/communities/gmu/records?q=&l=list&p=1&s=10&sort=newest" target="_blank">https://zenodo.org/communities/gmu/</a>) werden die PDF-Fassung des Beitrags sowie sämtliche relevanten Metadaten zugänglich gemacht.<sup>a</sup> Über das Zenodo-Portal wird dem Beitrag eine DOI-Nummer zugewiesen. Auf der GMU-Website wird neben der PDF und den Metadaten des Beitrags zusätzlich eine HTML-Ansicht des Beitrags für die Online-Lektüre bereitgestellt. Der im Zenodo-Repositorium veröffentlichte Datensatz und die auf der GMU-Website angezeigten Daten werden über Links miteinander identifiziert (die Herausgeber garantieren, dass die auf der GMU-Website bereitgestellten Daten den auf dem Zenodo-Portal veröffentlichten Daten entsprechen). Zitiert wird grundsätzlich die über <a href="https://zenodo.org/communities/gmu/records?q=&l=list&p=1&s=10&sort=newest" target="_blank">https://zenodo.org/communities/gmu/</a> zugängliche Version des Beitrags (der DOI-Link, der bei der Zitation des Beitrags unbedingt anzuführen ist, leitet auf das Zenodo-Portal).</p>
    <p>Die auf <a href="https://zenodo.org/communities/gmu/records?q=&l=list&p=1&s=10&sort=newest" target="_blank">https://zenodo.org/communities/gmu/</a> veröffentlichten Daten werden über Zenodo archiviert (Zenodo erstellt regelmäßige Backups; s. die <a href="https://about.zenodo.org/policies/" target="_blank">General Policies</a> des Zenodo-Diensts). Die auf der GMU-Website veröffentlichten Daten sind in <a href="https://gitlab.gwdg.de/subugoe/eupt" target="https://gitlab.gwdg.de/subugoe/eupt">GitLab</a> gesichert. Zusätzlich werden a) die PDF- und die HTML-Version jedes veröffentlichten Artikels auf den Servern der <a href="https://gwdg.de/services/storage-services/data-archiving/" target="_blank">GWDG</a> archiviert, und b) die Zenodo-Website und die GMU-Website, auf der der jeweilige Beitrag zugänglich ist, nach der Publikation des Beitrags im <a href="https://archive.org/" target="_blank">Internet Archive</a> gespeichert.</p>
        <ol type="a" class="GMU-guidelines-notes">
            <li>Für die GMU wurde auf <a href="https://zenodo.org/" target="_blank">zenodo.org</a> eine sogenannte <i>Community</i> eingerichtet (s. <a href="https://zenodo.org/communities/gmu/records?q=&l=list&p=1&s=10&sort=newest" target="_blank">https://zenodo.org/communities/gmu/</a>).</li></ol>
</details>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Phasen des Publikationsprozesses</h3></summary>
    <p>Der Publikationsprozess gliedert sich in mehrere Phasen, von der Einreichung über die interne Prüfung und das externe Double-Blind Peer Review bis zur Veröffentlichung. Im Folgenden findet sich ein Überblick über die zehn zentralen Schritte auf dem Weg zur Publikation (Autorinnen und Autoren sowie Gutachtende finden weitere Informationen zu den einzelnen Arbeitsschritten im Bereich <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">Richtlinien</button>):</p>

<table class="GMU-intro-publication-process-table" open>
<tr>
    <th>Phase</th>
    <th>Zuständigkeit</th>
    <th>Arbeitsschritt</th>
    <th>Erläuterungen</th></tr>
<tr>
    <td>1</td>
    <td>Autor*innen</td>
    <td>Einreichung</td>
    <td>S. die Hinweise unter <i>Von der Einreichung zur Publikation</i> in den <i>Informationen für Autorinnen und Autoren</i> im Bereich <button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">Richtlinien</button>.</td></tr>
<tr>
    <td>2</td>
    <td>Herausgeber</td>
    <td>Interne Begutachtung</td>
    <td>Die Herausgeber wägen ab, ob der eingereichte Beitrag grundsätzlich geeignet ist, um in den GMU veröffentlicht zu werden. Befinden sie den Beitrag für geeignet, gehen die Herausgeber über zu Schritt 3. Befinden sie den Beitrag für ungeeignet, informieren sie die Autorinnen und Autoren unmittelbar darüber (die Entscheidung wird begründet) und brechen den Publikationsprozess ab.</td></tr>
<tr>
    <td>3</td>
    <td>Herausgeber</td>
    <td>Verpflichtung der Gutachtenden</td>
    <td>Die Herausgeber benennen mindestens zwei externe Gutachtende. Den verpflichteten Reviewerinnen und Reviewern schicken sie die PDF-Fassung des zuvor anonymisierten Beitrags.</td></tr>
<tr>
    <td>4</td>
    <td>Gutachtende</td>
    <td>Peer Review</td>
    <td>Die Reviewerinnen und Reviewer begutachten den Beitrag und geben ihre Empfehlung ab, ob der Beitrag in den GMU veröffentlicht werden soll oder nicht.</td></tr>
<tr>
    <td>5</td>
    <td>Herausgeber</td>
    <td>Auswertung der Gutachten</td>
    <td>Die Herausgeber werten die Gutachten aus. Auf Grundlage der Gutachten entscheiden sie, ob der Beitrag zur Publikation angenommen wird oder nicht. Sie teilen den Autorinnen und Autoren ihre Entscheidung schnellstmöglich mit und leiten ihnen sämtliche Gutachten (in anonymisierter Form) weiter. Nehmen die Herausgeber den Beitrag zur Publikation an, wird das Verfahren mit Schritt 6 fortgeführt. Andernfalls wird der Publikationsprozess abgebrochen.</td></tr>
<tr>
    <td>6</td>
    <td>Autor*innen</td>
    <td>Überarbeitung</td>
    <td>Die Autorinnen und Autoren haben Gelegenheit, den Beitrag zu überarbeiten und in überarbeiteter Fassung erneut einzureichen. Weisen die Reviewerinnen und Reviewer in ihren Gutachten auf Mängel oder Fehler hin, die es vor der Publikation unbedingt zu beheben gilt, sind die Autorinnen und Autoren verpflichtet, den Beitrag entsprechend zu korrigieren und in verbesserter Fassung erneut einzureichen.</td></tr>
<tr>
    <td>7</td>
    <td>Herausgeber</td>
    <td>Interne Prüfung</td>
    <td>Nachdem die korrespondierende Autorin / der korrespondierende Autor die überarbeitete Fassung des Beitrags eingereicht hat, prüfen die Herausgeber, ob die Anmerkungen der Gutachtenden gebührend berücksichtigt wurden und etwaige Mängel oder Fehler behoben werden konnten. In unsicheren Fällen halten die Herausgeber Rücksprache mit den Gutachtenden. Gegebenenfalls fordern sie die Autorinnen und Autoren auf, den Beitrag erneut zu überarbeiten. Weigern sich die Autorinnen und Autoren, den Beitrag entsprechend zu überarbeiten, brechen die Herausgeber das Publikationsverfahren ab.</td></tr>
<tr>
    <td>8</td>
    <td>Herausgeber</td>
    <td>Vorbereitung der Publikationsversion</td>
    <td>Die Herausgeber erstellen die vorläufige Endfassung des Beitrags. Sie schicken die PDF-Fassung des Beitrags sowie Screenshots der vorläufigen Web-Ansicht an die korrespondierende Autorin / den korrespondierenden Autor.</td></tr>
<tr>
    <td>9</td>
    <td>Autor*innen</td>
    <td><i>Proofing</i></td>
    <td>Die Autorinnen und Autoren kontrollieren die vorläufige Endfassung des Beitrags und weisen die Herausgeber gegebenenfalls auf Fehler hin. Nachdem die Herausgeber die geforderten Korrekturen vorgenommen haben, erteilt die korrespondierende Autorin / der korrespondierende Autor den Herausgebern die schriftliche Genehmigung, den Beitrag in den GMU zu publizieren.</td></tr>
<tr>
    <td>10</td>
    <td>Herausgeber</td>
    <td>Publikation</td>
    <td>Der Beitrag wird auf dem Zenodo-Portal angelegt; die dort generierte DOI-Nummer wird an allen relevanten Stellen ergänzt. PDF und Metadaten des Beitrags werden auf zenodo.org veröffentlicht. Die Website-Version des Beitrags wird auf der GMU-Website zugänglich gemacht.</td></tr>
</table>
</details>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Indexing und Abstracting</h3></summary>
    <p>Die in den <i>Göttinger Miszellen zur Ugaritistik</i> veröffentlichten Beiträge sind in der <a href="https://explore.openaire.eu/search/find/research-outcomes" target="_blank">OpenAire EXPLORE</a>-Datenbank indiziert.</p><!--(...) sind derzeit in den folgenden Services indiziert:</p><ul><li>(...)</li></ul>-->
    <p>Die Herausgeber setzen sich dafür ein, dass die GMU schrittweise in weitere Zitationsdatenbanken (<i>Citation Indices</i>) und Abstract-Datenbanken aufgenommen werden (die Zeitschrift ist so konzipiert, dass sie den von verschiedenen Datenbanken gebotenen Standards entspricht). Bei Fragen zur Indexing-Strategie wenden Sie sich bitte per E-Mail an <a href="mailto:clemens.steinberger@theologie.uni-goettingen.de">clemens.steinberger@theologie.uni-goettingen.de</a>. An dieselbe Adresse sind Hinweise auf relevante Zitationsdatenbanken zu richten, in denen die GMU berücksichtigt werden sollten.</p>
    <p>Die in den GMU veröffentlichten Beiträge werden an die Deutsche Nationalbibliothek abgeliefert. Die Zeitschrift wird im <a href="https://portal.dnb.de/opac.htm?method=simpleSearch&cqlMode=true&query=idn%3D1350219843" target="_blank">Katalog der Deutschen Nationalbibliothek</a>, in der <a href="https://zdb-katalog.de/title.xhtml?idn=1344290418&view=full" target="_blank">Zeitschriftendatenbank (ZDB)</a> und auf dem internationalen <a href="https://portal.issn.org/resource/ISSN/2944-3458" target="_blank">ISSN-Portal</a> angezeigt. Die GMU sind außerdem in der <a href="https://ancientworldonline.blogspot.com/2015/12/alphabetical-list-of-open-access.html" target="_blank">AWOL List of Open Access Journals in Ancient Studies</a> und im Online-Katalog der <a href="https://isac-idb.uchicago.edu/id/7f0c2270-39ef-4ec3-8bd6-3a53c641e706" target="_blank">ISAC Collections</a> gelistet.</p>
    <p>Um die in den GMU veröffentlichten Beiträge über gängige Online-Suchmaschinen (z. B. Google Scholar) im Internet auffindbar zu machen, werden der auf der GMU-Website angezeigten HTML-Fassung jedes Beitrags diverse <code>meta</code>-Tags zugewiesen (es werden <i>Highwire Press Tags</i> – z. B. citation_title – verwendet; die Herausgeber folgen damit einer <a href="https://scholar.google.com/intl/en/scholar/inclusion.html#indexing" target="_content">Empfehlung von Google Scholar</a>). Diese Tags, die beispielsweise den Titel, die Keywords und das englische Abstract des Beitrags sowie Informationen über die Autorinnen und Autoren erfassen, können von Suchmaschinen ausgewertet werden.</p></details>

<details class="GMU-Einfuehrung-h-details">
<summary><h3>Statistiken</h3></summary>
    <p style="text-align: left;"><b>2025 (bis 05.03.)</b><br/>
        <span style="margin-left: 1em;">Anzahl eingereichter Beiträge: 1</span><br/>
        <span style="margin-left: 1em;">Anzahl veröffentlichter Beiträge: 1</span><br/>
        <span style="margin-left: 1em;">Anzahl angefragter Reviews: 3</span><br/>
        <span style="margin-left: 1em;">Anzahl angefragter Reviews / Beitrag (⌀): -</span><br/>
        <span style="margin-left: 1em;">Anzahl eingegangener Reviews: 2</span><br/>
        <span style="margin-left: 1em;">Anzahl eingegangener Reviews / Beitrag (⌀): -</span><br/>
        <span style="margin-left: 1em;">Dauer von der Einreichung bis zur Veröffentlichung (⌀): 49 Tage</span><br/>
        <span style="margin-left: 1em;"><i>Approval Rate</i>: -</span></p>
    <p style="text-align: left;"><b>2024</b><br/>
        <span style="margin-left: 1em;">Anzahl eingereichter Beiträge: 2</span><br/>
        <span style="margin-left: 1em;">Anzahl veröffentlichter Beiträge: 1</span><br/>
        <span style="margin-left: 1em;">Anzahl angefragter Reviews: 5</span><br/>
        <span style="margin-left: 1em;">Anzahl angefragter Reviews / Beitrag (⌀): 2,5</span><br/>
        <span style="margin-left: 1em;">Anzahl eingegangener Reviews: 4</span><br/>
        <span style="margin-left: 1em;">Anzahl eingegangener Reviews / Beitrag (⌀): 2</span><br/>
        <span style="margin-left: 1em;">Dauer von der Einreichung bis zur Veröffentlichung (⌀): 66 Tage</span><br/>
        <span style="margin-left: 1em;"><i>Approval Rate</i>: -</span></p>
</details>

</div><!--End of contents of Tab3: Publikationsmodus-->


<!--Tab4: Artikel-->
<input id="tab4" type="radio" name="tabs">
<label class="GMU-tabs-label" for="tab4"><p>Artikel</p></label>
<div class="GMU-tabs-content">

<!--<p>Bislang ist kein Beitrag veröffentlicht.</p>-->

<div class="GMU-Publikationen">
<table class="GMU-PublikationenTable">
<thead>
    <tr>
        <th>Nr.</th>
        <th>Erschienen am</th>
        <th>Autor*innen</th>
        <th>Titel</th>
        <th><i>Keywords</i></th>
        <th>DOI (via Zenodo)</th>
        <th>Webversion (HTML)</th></tr></thead>
<tbody>
    <tr>
        <td><span class="sort-id">0001</span>1</td>
        <td>2024-11-15</td>
        <td><span class="sort-id">Burlingame</span>Andrew Burlingame</td>
        <td>Toponymic Remarks to <span class="noBreakEUPT">RIH 83/47<sup>+</sup></span> <span class="noBreakEUPT">(RIH II, no. 43)</span></td>
        <td>Ugarit, toponymy, topography, Ras Ibn Hani, Late Bronze Age administration</td>
        <td><a href="https://doi.org/10.5281/zenodo.13941016" target="_blank">10.5281/zenodo.13941016</a></td>
        <td><a href="/GMU/1_Burlingame_Toponymic-Remarks-to-RIH-83-47.html">HTML</a></td></tr>
    <tr>
        <td><span class="sort-id">0002</span>2</td>
        <td>2025-03-05</td>
        <td><span class="sort-id">Notarius</span>Tania Notarius</td>
        <td>Ugaritic <i>prln</i> and Hurrian <i>furullinni</i> “haruspex”</td>
        <td>Ugarit, scribal tradition, divination</td>
        <td><a href="https://doi.org/10.5281/zenodo.14677547" target="_blank">10.5281/zenodo.14677547</a></td>
        <td><a href="/GMU/2_Notarius_Ugaritic-prln-and-Hurrian-furullinni.html">HTML</a></td></tr>
</tbody>
</table>
</div><!--End of GMU-Publikationen-->

</div><!--End of contents of Tab4: Publikationen-->
</div><!--End of Tabs-->