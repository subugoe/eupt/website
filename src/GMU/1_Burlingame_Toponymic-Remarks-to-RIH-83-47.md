---
title: GMU 1 - Burlingame - Toponymic Remarks to RIH 83/47+ (RIH II, no. 43)
lang: en
layout: Layout
head:
  - - meta
    - name: citation_author
      content: Andrew Burlingame
  - - meta
    - name: citation_author_orcid
      content: 
  - - meta
    - name: citation_author_email
      content: drew.burlingame@wheaton.edu
  - - meta
    - name: citation_author_institution
      content: Wheaton College
  - - meta
    - name: citation_title
      content: Toponymic Remarks to RIH 83/47+ (RIH II, no. 43)
  - - meta
    - name: citation_publication_date
      content: 2024-11-15
  - - meta
    - name: citation_language
      content: en
  - - meta
    - name: citation_doi
      content: https://doi.org/10.5281/zenodo.13941016
  - - meta
    - name: citation_publisher
      content: Reinhard Müller, Clemens Steinberger, Noah Kröll
  - - meta
    - name: citation_journal_title
      content: Göttinger Miszellen zur Ugaritistik
  - - meta
    - name: citation_journal_abbrev
      content: GMU
  - - meta
    - name: citation_issn
      content: 2944-3458
  - - meta
    - name: citation_issue
      content: 0001
  - - meta
    - name: citation_keywords
      content: Ugarit, toponymy, topography, Ras Ibn Hani, Late Bronze Age administration
  - - meta
    - name: citation_abstract
      content: This article proposes toponymic identifications for several entries appearing in RIH 83/47+, first published in 2019. The new insights resulting from this study include an identification of the first alphabetic attestation of the toponym Paṯaratu (previously attested only in logosyllabic texts and in one alphabetic text as a gentilic), a second attestation of the previously hapax toponym ʿry, a series of arguments in favor of identifying the entries appearing in this text as uniformly toponymic, and a number of topographic observations intended to contribute to the challenging work of locating these settlements within the kingdom of Ugarit.
  - - meta
    - name: citation_fulltext_html_url
      content: https://eupt.uni-goettingen.de/GMU/1_Burlingame_Toponymic-Remarks-to-RIH-83-47.html
  - - meta
    - name: citation_pdf_url
      content: https://doi.org/10.5281/zenodo.13941016
---
<a href="#top" id="top-link"></a>

<div id="GMU_1">

<h1 class="GMU-h1">Toponymic Remarks to <span class="noBreakEUPT">RIH 83/47<sup>+</sup></span> <span class="noBreakEUPT">(RIH II, no. 43)</span></h1>
    <p class="GMU-sub-h1"><b>Andrew Burlingame</b></p>
    <p class="GMU-sub-h1">GMU 1 | 2024 | DOI: <a href="https://doi.org/10.5281/zenodo.13941016" target="_blank">10.5281/zenodo.13941016</a><a class="GMU-PDF-Download-Button" href="https://doi.org/10.5281/zenodo.13941016" target="_blank">PDF</a></p>

<div class="GMU-article-tabs">

<input id="tab1" type="radio" name="tabs" checked="checked">
<label class="GMU-article-tabs-label" for="tab1"><p>Info</p></label>
<div class="GMU-article-tabs-content">

<div class="GMU-metadata">
<table class="GMU-metadata-table">
	<tr>
        <td>Title:</td>
        <td><button class="GMU-tabs-label-link" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab2').click(), 10);">Toponymic Remarks to RIH 83/47<sup>+</sup> (RIH II, no. 43)</button></td></tr>
    <tr>
    	<td>Author:</td>
        <td>Andrew Burlingame <span class="noBreakEUPT">
          <a class="Metadata-Logo-academia" href="https://wheaton.academia.edu/AndrewBurlingame" target="_blank">
            <img style="border-width:0" src="https://a.academia-assets.com/images/academia-logo-2021.svg"></a>
          <a class="Metadata-Logo-university" href="https://www.wheaton.edu/academics/faculty/andrew-burlingame-phd/" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>school-outline</title><path d="M12 3L1 9L5 11.18V17.18L12 21L19 17.18V11.18L21 10.09V17H23V9L12 3M18.82 9L12 12.72L5.18 9L12 5.28L18.82 9M17 16L12 18.72L7 16V12.27L12 15L17 12.27V16Z"/></svg></a></span><br/>
          <span class="noBreakEUPT"><a href="https://ror.org/0581k0452" target="_blank">Wheaton College</a> <a class="Metadata-Logo-ROR" href="https://ror.org/0581k0452" target="_blank">
              <img src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-rgb-transparent.svg"></a></span><br/>
          <a href="mailto:drew.burlingame@wheaton.edu">drew.burlingame@wheaton.edu</a></td></tr>
    <tr>
        <td>Keywords:</td>
        <td>Ugarit, toponymy, topography, Ras Ibn Hani, Late Bronze Age administration</td></tr>
    <tr>
        <td>Abstract:</td>
        <td>This article proposes toponymic identifications for several entries appearing in RIH 83/47<sup>+</sup>, first published in 2019. The new insights resulting from this study include an identification of the first alphabetic attestation of the toponym Paṯaratu (previously attested only in logosyllabic texts and in one alphabetic text as a gentilic), a second attestation of the previously <i>hapax</i> toponym <i>ʿry</i>, a series of arguments in favor of identifying the entries appearing in this text as uniformly toponymic, and a number of topographic observations intended to contribute to the challenging work of locating these settlements within the kingdom of Ugarit.</td></tr>
    <tr>
        <td>Date:</td>
        <td>2024-11-15 (date of publication)</td></tr>
    <tr>
        <td>Issue:</td>
        <td><a href="/GMU/Einfuehrung.html" target="_blank">Göttinger Miszellen zur Ugaritistik (GMU)</a> 1</td></tr>
    <tr>
        <td>DOI:</td>
        <td>
            <a class="GMU-aDOI" href="https://doi.org/10.5281/zenodo.13941016" target="_blank">
                <span class="GMU-DOI" style="background-image: url(https://zenodo.org/badge/DOI/10.5281/zenodo.13941016.svg);">DOI</span>
            </a>
        </td></tr>
    <tr>
        <td>License:</td>
        <td><a rel="license" href="https://creativecommons.org/licenses/by/4.0/" target="_blank"><img class="Metadata-Logo-cc" src="https://licensebuttons.net/l/by/4.0/88x31.png"></a><br/>
        This work is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a>. <span class="noBreakEUPT">© Andrew</span> Burlingame 2024.</td></tr>
    <tr>
        <td>Citation:</td>
        <td>Burlingame, Andrew. 2024. “Toponymic Remarks to RIH 83/47<sup>+</sup> (RIH II, no. 43).” <i>Göttinger Miszellen zur Ugaritistik</i> 1. <a href="https://doi.org/10.5281/zenodo.13941016" target="_blank">https://doi.org/<wbr>10.5281/<wbr>zenodo.<wbr>13941016</a>. Accessed on YYYY-MM-DD. <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/JVQWD2J5/item-details" target="_blank"><img class="Bibliography-Logo-zotero" src="https://www.zotero.org/support/_media/logo/zotero_32x32x32.png"></a></td></tr>
</table>
</div>

</div>

<input id="tab2" type="radio" name="tabs">
<label class="GMU-article-tabs-label" for="tab2"><p>Article</p></label>
<div class="GMU-article-tabs-content">

<div class="GMU-main-text">

<h4>1. Introduction</h4>

<p style="margin-top: 1em;">The text under consideration, RIH 83/47<sup>+</sup>, was discovered during the 1983 season of excavations at the site of Ras Ibn Hani, and was first published in 2019 (<bibl>Bordreuil and Pardee 2019, 115–117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>). As the fragments from which this tablet was reconstituted were found at Locus XXIX of the North Palace at Ras Ibn Hani, the text belongs to Lagarce’s “Groupe 4” and can be dated, along with most of the texts recovered from the North Palace, to the reign of ʿAmmiṯtamru III (see Lagarce <i>apud</i> <bibl>Bordreuil, Pardee, and Roche-Hawley 2019, 14, 18&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6HNWTZM8" method="get" :data="[]"/></bibl>)&#8288;<span class="noBreakEUPT">.<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref01" onclick="document.getElementById('GMU-1-fn01').focus();document.getElementById('GMU-1-sn01').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn01" tabindex="0"></span>Note that Lagarce refers to this king as ʿAmmiṯtamru II, reflecting the traditional enumeration of Ugaritian kings found in works like <bibl>Yon 1997&#8288;<VuepressApiPlayground url="/api/eupt/biblio/EJS397IQ" method="get" :data="[]"/></bibl> / <bibl>2006&#8288;<VuepressApiPlayground url="/api/eupt/biblio/S8HS8DWE" method="get" :data="[]"/></bibl> and <bibl>Singer 1999&#8288;<VuepressApiPlayground url="/api/eupt/biblio/927CGDRX" method="get" :data="[]"/></bibl>. The numbering employed here follows the revised system proposed in <bibl>Arnaud 1999&#8288;<VuepressApiPlayground url="/api/eupt/biblio/PE233XRM" method="get" :data="[]"/></bibl>.</span></span></span></p>

<p>In his “Catalogue raisonné des textes de Ras Ibn Hani 1977-2002,” Pardee classified the text among other “Textes économiques” and, more specifically, among “Relevés de comptes” (<bibl>Pardee 2019, 32&#8288;<VuepressApiPlayground url="/api/eupt/biblio/VS7NFW7U" method="get" :data="[]"/></bibl>). The introductory formula of the text is unusual, and this together with the structure and organization of the text merit further study, but the focus of the present study is strictly toponymic. The editors identify the opening four lines as a summary of silver owed by ʿAmmuyānu (215 shekels). The remainder of the text is treated as a list of “creditors,” i.e., the individual contributors to whom repayment is due.</p>

<p>As the editors observe, most of these creditors are actually the names of settlements within the kingdom, but they correctly note that several of the entries could be interpreted as either toponyms (TNs) or personal names (PNs), a fact introducing a measure of uncertainty into the interpretation of the text. In this brief article, I suggest that all of the entries can now be recognized as toponyms based on two varieties of evidence.</p>

<p>First, some of the names in this text treated as ambiguous in the edition can now be more clearly identified with attested toponyms. Second, in one case of clear ambiguity, an independently observable trend in Ugaritian administrative practice can be invoked to achieve resolution. Before introducing the specific entries at issue, I reproduce the text in its entirety as presented in the <i>editio princeps</i> (<bibl>RIH II&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6HNWTZM8" method="get" :data="[]"/></bibl>, no. 43; a proposed revision is offered at the conclusion of this study).</p>

<div class="GMU-sidenotesInTable-text-container">
    <span class="GMU-sidenotesInTable-text liner_3" tabindex="0"><span id="GMU-1-sn02" tabindex="0"></span><span class="GMU-sidenotes-inserted-number-manually">2&#x2007;</span>The <bibl>edition&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl> has “<i>Maʾadiḫu</i>,” but it is clear from the remark on p. 117 that <i>Maʾaduḫu</i> was intended.</span>
</div>

<table class="GMU-ugaritic-text-table">
    <tr>
        <th colspan="2">RIH 83/47<sup>+</sup> = RIH II, no. 43</th>
    </tr>
    <tr>
        <td><i>Recto</i></td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">1</span> tgmr . k[s]⸢p⸣</td>
        <td lang="fr">Total d’argent</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">2</span> d ʿly . w d [ʿ]⸢l⸣</td>
        <td lang="fr">qui est passé en compte débiteur, à savoir au [débi]t</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">3</span> ʿmyn . ḫm[š]⸢t⸣</td>
        <td lang="fr">de <i>ʿAmmuyānu</i>&nbsp;: deux cent</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">4</span> ʿšrt . k⸢bd⸣ mi͗tm</td>
        <td lang="fr">quinze (sicles).</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">5</span> a͗<sup>!</sup>r . ḫmšm</td>
        <td lang="fr">(De la ville de) <i>ʾAru</i>&nbsp;: cinquante (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">6</span> ḫrlm . ḫmšt</td>
        <td lang="fr">(de) ḪRLM&nbsp;: cinq (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">7</span> kṯtġlm . ḫmšt</td>
        <td lang="fr">(de l’individu) KṮTĠLM&nbsp;: quinze (sicles)&nbsp;;</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">8</span> ʿšrt</td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">9</span> ʿry . ḫmšt</td>
        <td lang="fr">(de) ʿRY&nbsp;: quinze (sicles)&nbsp;;</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">10</span> ʿšrt</td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>	
    <tr>
        <td><span class="ugaritic-text-line-number">11</span> ṯmry . ʿšrt</td>
        <td lang="fr">(de la ville de) <i>Ṯamrāya</i>&nbsp;: dix (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span> pṯrt . ʿšrt</td>
        <td lang="fr">(de) PṮRT&nbsp;: dix (sicles)&nbsp;;</td></tr>
    <tr>
        <td></td>
        <td></td></tr>
    <tr>
        <td lang="fr"><i>Tranche inférieure</i></td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">13</span> ypr . ʿšrt</td>
        <td lang="fr">(de) YPR&nbsp;: dix (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td></td>
        <td></td></tr>
    <tr>
        <td><i>Verso</i></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">14</span> mṣbt . ḫmšt</td>
        <td lang="fr">(de la ville de) <i>Maṣibatu</i>&nbsp;: cinq (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">15</span> ma͗dḫ . ḫmšt</td>
        <td lang="fr">(de la ville de) <span class="noBreakEUPT"><i>Maʾaduḫu</i><span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref02" onclick="document.getElementById('GMU-1-fn02').focus();document.getElementById('GMU-1-sn02').focus();"></button></span></span>&nbsp;: cinq (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>	
    <tr>
        <td><span class="ugaritic-text-line-number">16</span> mri͗l<sup>!</sup> . ḫmšt</td>
        <td lang="fr">(de la ville de) <i>Maraʾilu</i>&nbsp;: cinq (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17</span> ḫlb ṣpn . ḫmšt</td>
        <td lang="fr">(de la ville de) <i>Ḫalbuṣapuni</i>&nbsp;: cinq (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">18</span> šbn . ʿšrt</td>
        <td lang="fr">(de la ville de) <i>Šubbanu</i>&nbsp;: dix (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>	
    <tr>
        <td><span class="ugaritic-text-line-number">19</span> u͗škn . ḫmšm</td>
        <td lang="fr">(de la ville de) <i>ʾUškanu</i>&nbsp;: cinquante (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>	
    <tr>
        <td><span class="ugaritic-text-line-number">20</span> [&#8239;&#8239;&#8239;&#8239;&#8239;.] ⸢ʿ⸣šrm</td>
        <td lang="fr">[(de …)]&nbsp;: [v]ingt (sicles)&nbsp;;</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>	
    <tr>
        <td><span class="ugaritic-text-line-number">21</span> [&#8239;&#8239;&#8239;&#8239;&#8239;. ḫ]mšt</td>
        <td lang="fr">[(de …)]&nbsp;: [c]inq (sicles).</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
</table>

<h4>2. Toponymic and Topographic Discussion</h4>

<p>The editors identified most of these entries as well-known toponyms, as indicated by their introduction of the parenthetical clarification, “(de la ville de).” They <span class="noBreakEUPT">judiciously<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref03" onclick="document.getElementById('GMU-1-fn03').focus();document.getElementById('GMU-1-sn03').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn03" tabindex="0"></span>Cases of personal names that were identical to toponyms were already known, but the editors’ identification of the first clear evidence for the personal name <i>u͗škn</i>, found in RIH 83/17<sup>+</sup>:27' (= <bibl>RIH II&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6HNWTZM8" method="get" :data="[]"/></bibl>, no. 9), demonstrated that even greater caution was required than most would have previously thought necessary (see <bibl>Bordreuil and Pardee 2019, 62, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>). Prior to the discovery of this evidence, no interpreter would have hesitated to identify an instance of {u͗škn} in an alphabetic Ugaritic text with the prominent settlement of that name located in the southern coastal plain of the kingdom, but the new textual evidence from Ras Ibn Hani demonstrated beyond doubt that even such apparently obvious identifications could turn out to be misleading.</span></span></span> observed, however, that several entries were strictly ambiguous and could perhaps be identified as either personal names or toponyms. The entries {ḫrlm} in l. 6, {kṯtġlm} in l. 7, {ʿry} in l. 9, {ṯmry} in l. 11, {pṯrt} in l. 12, {ypr} in l. 13, and {u͗škn} in l. 19 were all regarded as potentially problematic (<bibl>2019, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>). Of these, the editors treated {kṯtġlm} in l. 7 as a personal name (adding “(de l’individu)” in their translation) and provisionally preferred to understand {ṯmry} in l. 11 and {u͗škn} in l. 19 as toponyms (though noting that both could also be personal names). The other four entries were deemed ambiguous and were left without an indication of their status (“ville” vs. “individu”) in the published translation (see above).</p>

<p>The entry {ḫrlm} in l. 6 is (to my knowledge) unique, as the editors <span class="noBreakEUPT">state.<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref04" onclick="document.getElementById('GMU-1-fn04').focus();document.getElementById('GMU-1-sn04').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn04" tabindex="0"></span>Moreover, as this text was first published in 2019, the lexeme <i>ḫrlm</i> does not appear in the dictionary of del Olmo Lete and Sanmartín, the most recent edition of which was published in <bibl>2015&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>.</span></span></span> The remaining entries, however, can now be shown to have parallels elsewhere that may serve to clarify the function of the text as a whole.</p>

<p>The entry {kṯtġlm} in l. 7 can be identified as a toponym discussed in van Soldt’s <i>The Topography of the City-State of Ugarit</i> (<bibl>2005, 27&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>) and is found in two other alphabetic texts (RS 17.392:2 [= KTU<sup>1–2</sup> 4.310; KTU<sup>3</sup> 3.18] and RS 19.105:25–26 [= KTU<sup>1–3</sup> 4.643]&#8288;)&#8288;<span class="noBreakEUPT">.<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref05" onclick="document.getElementById('GMU-1-fn05').focus();document.getElementById('GMU-1-sn05').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn05" tabindex="0"></span>The first of these attestations, appearing in RS 17.392, is potentially ambiguous, as it refers to <i>nsk kṯtġlm</i>, “the founder(s) of <i>kṯtġlm</i>.” The same text mentions <i>nsk a͗rym</i> (ll. 5–6) and <i>nsk a͗rt</i> (l. 8). The latter of these clearly involves a TN (“founder(s) of ʾArutu”), but the former involves a gentilic (“founder(s) of the men of ʾAru”). In other words, the case could be made for interpreting <i>kṯtġlm</i> either as a TN (like <i>a͗rt</i> in l. 8) or as a plural term referring to a group of individuals, comparable to the toponymically defined group <i>a͗rym</i> in l. 6. Virolleaud seems to have had the latter possibility in mind when he suggested in his <i>editio princeps</i> of RS 17.392 (<bibl>1957, 170&#8288;<VuepressApiPlayground url="/api/eupt/biblio/B5IEMJHE" method="get" :data="[]"/></bibl>) that <i>kṯtġlm</i> might represent “un mot composé au pl.” like <i>mḏrġlm</i> and <i>ḫḏġlm</i> (both terms referring to professional groups and incorporating the Hurrian morpheme <i>⸗o⸗ḫ</i>(<i>e</i>)<i>⸗li</i> [<i>-uḫli</i>], used to form Hurrian <i>nomina actoris</i> [see <span class="noBreakEUPT"><bibl>Wegner 2007, 57–58&#8288;<VuepressApiPlayground url="/api/eupt/biblio/T5J8N9V2" method="get" :data="[]"/></bibl>]&#8288;).</span> Such a classification was suggested more explicitly by Friedrich (<bibl>1961, 36&#8288;<VuepressApiPlayground url="/api/eupt/biblio/U5ZRZ6HK" method="get" :data="[]"/></bibl>: “ugar. Berufsbezeichnung”), Aistleitner (<bibl>1963, 160&#8288;<VuepressApiPlayground url="/api/eupt/biblio/SVRCMW4F" method="get" :data="[]"/></bibl>: “Bez. e. Berufs?”), and Dietrich and Loretz (<bibl>1966, 199–200&#8288;<VuepressApiPlayground url="/api/eupt/biblio/U8WMAEFB" method="get" :data="[]"/></bibl>).<br/>The discovery of RS 19.105 (= KTU 4.643) in 1955 (published in 1965) shed new and clearer light on the question, as the form {kṯtġlm} appeared following the preposition <i>b</i> “in” in l. 26 of the text and, by emendation, in the preceding line as well ({kṯ&lt;t&gt;ġlm}), in a context in which the resulting prepositional phrase could only be understood to indicate the town in which the individuals named at the beginning of each of these lines were to be found. Virolleaud (<bibl>1965, 142&#8288;<VuepressApiPlayground url="/api/eupt/biblio/26XADU5S" method="get" :data="[]"/></bibl>) described the text as a “[l]iste de gens habitant diverses villes,” suggesting a toponymic status for <i>kṯtġlm</i>, and he listed this term in the index of “villes et pays” (<bibl>1965, 166&#8288;<VuepressApiPlayground url="/api/eupt/biblio/26XADU5S" method="get" :data="[]"/></bibl>). Gordon (<bibl>1965&#8288;<VuepressApiPlayground url="/api/eupt/biblio/7FHGSMSJ" method="get" :data="[]"/></bibl> / <bibl>1998, 425 [sec. 19.1336]&#8288;<VuepressApiPlayground url="/api/eupt/biblio/ST3JFGZQ" method="get" :data="[]"/></bibl>) drew on this new text and interpretation in producing his glossary entry for <i>kṯtġlm</i>, where it is treated as a place name (though without discussion). This new understanding of the term was quickly accepted by others, who noted that it was in fact preferable for RS 17.392 on internal grounds as well, as both <i>a͗rym</i> and <i>a͗rt</i> pertained to towns of origin (<bibl>Dietrich, Loretz, and Sanmartín 1973, 110&#8288;<VuepressApiPlayground url="/api/eupt/biblio/KVK6KNF8" method="get" :data="[]"/></bibl>; <bibl>Pardee 1974, 280 n. 26&#8288;<VuepressApiPlayground url="/api/eupt/biblio/27NP7XT5" method="get" :data="[]"/></bibl>).<br/>While some continued to hold that a term referring to a professional group (typically “bowmakers” is assumed) provided the etymology of the term (e.g., <bibl>Astour 1977, 129&#8288;<VuepressApiPlayground url="/api/eupt/biblio/FP5PMKHU" method="get" :data="[]"/></bibl>; <bibl>1981b, 14 with n. 16&#8288;<VuepressApiPlayground url="/api/eupt/biblio/SXKK2RU9" method="get" :data="[]"/></bibl>; <bibl>Sanmartín 1995, 180–181&#8288;<VuepressApiPlayground url="/api/eupt/biblio/2R8DAWWZ" method="get" :data="[]"/></bibl>; <bibl>Watson 2001, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/A8MS8RCG" method="get" :data="[]"/></bibl>), its toponymic value was generally accepted (exceptions include <bibl>Laroche 1980, 139&#8288;<VuepressApiPlayground url="/api/eupt/biblio/PV9WHNC2" method="get" :data="[]"/></bibl> [citing <bibl>Dietrich and Loretz 1966&#8288;<VuepressApiPlayground url="/api/eupt/biblio/U8WMAEFB" method="get" :data="[]"/></bibl>, though these authors no longer held to the position outlined there, as noted above] and <bibl>Heltzer 1982, 89 with n. 34&#8288;<VuepressApiPlayground url="/api/eupt/biblio/62ICIFPN" method="get" :data="[]"/></bibl> [rejecting the position outlined by Lipiński in an editorial note appearing in <bibl>Heltzer 1979, 478–488 n. 224&#8288;<VuepressApiPlayground url="/api/eupt/biblio/CDC4D2J3" method="get" :data="[]"/></bibl>; he does not, however, address the clear indications of the toponymic value of the term that Lipiński’s editorial remark had identified]&#8288;) and remains normative today (see <bibl>Belmonte Marín 2001, 165&#8288;<VuepressApiPlayground url="/api/eupt/biblio/AXPM75DV" method="get" :data="[]"/></bibl> and <bibl>del Olmo Lete and Sanmartín 2015, 468&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>).</span></span></span> Unfortunately, neither of these texts allows us to locate this settlement, and van Soldt (<bibl>2005, 112&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>) does not assign it to any of his groups of towns.</p>

<p>The entry {ʿry} (l. <span class="noBreakEUPT">9)<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref06" onclick="document.getElementById('GMU-1-fn06').focus();document.getElementById('GMU-1-sn06').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn06" tabindex="0"></span>Bordreuil may originally have read this entry as {ʿky} (see his remarks <i>apud</i> <bibl>Bordreuil et al. 1984, 427&#8288;<VuepressApiPlayground url="/api/eupt/biblio/2HCRZ8H6" method="get" :data="[]"/></bibl>; <bibl>Bordreuil and Pardee 2019, 116 with n. 130&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>; see further <bibl>Cunchillos 1989, 357 n. 30&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6E8Q2VEC" method="get" :data="[]"/></bibl>). While the second sign of this sequence shows some damage in the published photograph, the editors indicate that the reading {ʿry} is certain (<bibl>Bordreuil and Pardee 2019, 116&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>).</span></span></span> was not included among toponyms gathered and studied in <bibl>van Soldt 2005&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>, as its first published attestation had not yet appeared. But a more recently published text, RS 94.2614 (= <bibl>RSO XVIII&#8288;<VuepressApiPlayground url="/api/eupt/biblio/K72NT4DC" method="get" :data="[]"/></bibl>, no. 2; KTU<sup>3</sup> 4.820), provides an attestation of this graphic sequence in a context that makes its toponymic status virtually certain. The text in question consists of a list of town names followed by numerals, which, based on a note written on the reverse of the tablet in logosyllabic cuneiform, refer to quantities of animal hides. The list of settlements is arranged in two columns. All TNs appearing in the left-hand column are independently attested and belong to van Soldt’s Groups 6–8 (i.e., southern groups). Independently attested toponyms in the right-hand column belong to his Groups 1N, 1S, 1, and 2 (i.e., northern groups) (see <bibl>van Soldt 2005, 111–114&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>; <bibl>Bordreuil and Pardee 2012, 18&#8288;<VuepressApiPlayground url="/api/eupt/biblio/JN52XMIG" method="get" :data="[]"/></bibl>). Among these settlements in the right-hand column appears a new toponym, {ʿry} (RS 94.2614 ii 10). If the organization of these settlement names is consistent, then the town of <i>ʿry</i> is likely to be sought at the northern frontier of the kingdom (a possibility the present text may support, as discussed below).</p>

<p>The entries {pṯrt} in l. 12 and {ypr} in l. 13 can be identified as the towns of Paṯaratu and <span class="noBreakEUPT">Yaparu.<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref07" onclick="document.getElementById('GMU-1-fn07').focus();document.getElementById('GMU-1-sn07').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn07" tabindex="0"></span>For Paṯaratu, see <bibl>van Soldt 2005, 38&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>; for additional bibliography, see <bibl>Belmonte Marín 2001, 219&#8288;<VuepressApiPlayground url="/api/eupt/biblio/AXPM75DV" method="get" :data="[]"/></bibl> and <bibl>del Olmo Lete and Sanmartín 2015, 677&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl> (<i>pṯrty</i>). For Yaparu, see <bibl>van Soldt 2005, 25–26&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>; for additional bibliography, see <bibl>Belmonte Marín 2001, 341&#8288;<VuepressApiPlayground url="/api/eupt/biblio/AXPM75DV" method="get" :data="[]"/></bibl> and <bibl>del Olmo Lete and Sanmartín 2015, 960&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>.</span></span></span> The former of these is significant, comprising the first alphabetic attestation of this TN, as previous attestations were limited to logosyllabic texts and one instance of an alphabetic gentilic <span class="noBreakEUPT">(<i>pṯrty</i>).<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref08" onclick="document.getElementById('GMU-1-fn08').focus();document.getElementById('GMU-1-sn08').focus();"></button><span class="GMU-sidenotes-text liner_2" tabindex="0"><span id="GMU-1-sn08" tabindex="0"></span>See references in preceding footnote for lists of attestations.</span></span></span> Furthermore, the immediate juxtaposition of these two TNs in the present list aligns with their cooccurrence elsewhere and tends to support van Soldt’s placement of Paṯaratu in his Group 4 (see <bibl>van Soldt 2005, 90 n. 93, 113 with n. 188&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>).</p>

<p>If these identifications are correct, then {ḫrlm} is the only unique and uncertain term (unless it represents an error, to be corrected to {ḫr&lt;bġ&gt;lm}, a poorly attested town name [see <bibl>van Soldt 2005, 22&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>, and further below]&#8288;), and in this light, the editors’ preference to treat {ṯmry} and {u͗škn} as toponyms is surely justified. Independently observable trends in Ugaritian administration, however, provide a second reason to prefer the toponymic interpretation of {u͗škn} in this text. Specifically, the fact that the largest quantities of silver registered in the list are associated with ʾAru (50 shekels, l. 5) and ʾUškanu (50 shekels, l. 19) conforms precisely to evidence for the comparable size and importance of these two towns available among the administrative texts from Ras <span class="noBreakEUPT">Shamra.<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref09" onclick="document.getElementById('GMU-1-fn09').focus();document.getElementById('GMU-1-sn09').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn09" tabindex="0"></span>These data are presented and examined in van Soldt (<bibl>2005, ch. 5&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>) and suggest that ʾAru and ʾUškanu represented the towns of greatest administrative significance—and possibly size—in the southern plain (<bibl>van Soldt 2005, 127&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>). Further discussion and bibliography can be found in <bibl>Burlingame forthcoming&#8288;<VuepressApiPlayground url="/api/eupt/biblio/EFQ7IMZX" method="get" :data="[]"/></bibl>.</span></span></span></p>

<p>In light of these observations, RIH 83/47<sup>+</sup> can be understood to record quantities of silver that had been provided by a number of the kingdom’s settlements (aligning—even if only approximately—with Saadé’s [<bibl>2011, 444&#8288;<VuepressApiPlayground url="/api/eupt/biblio/H52QDK82" method="get" :data="[]"/></bibl>] inclusion of this text among “listes toponymiques”). The text does not provide any hint as to the purpose these contributions served, and this point of ambiguity is further complicated by both the unusual expression <i>d ʿly</i> found in the introduction (and remarked by the editors [<bibl>Bordreuil and Pardee 2019, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>]&#8288;) and the fact that the total recorded in the introductory paragraph, 215 shekels, does not agree with the total arrived at by adding up the individual entries, namely, 220 shekels (also noted by the editors [<bibl>2019, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>]&#8288;)&#8288;<span class="noBreakEUPT">.<span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref10" onclick="document.getElementById('GMU-1-fn10').focus();document.getElementById('GMU-1-sn10').focus();"></button><span class="GMU-sidenotes-text" tabindex="0"><span id="GMU-1-sn10" tabindex="0"></span>One of the anonymous peer reviewers makes the clever observation that the final entry in the list may have comprised an unanticipated/last-minute addition to the list. In other words, the total of 215 was written down first, followed by the entries occupying lines 5–20, which do reach the stated total. The final entry of 5 shekels may then have been added after the fact. Though impossible to prove, this is imaginable and would account for the discrepancy.</span></span></span> What appears certain, however, is that the contributors in this record were towns rather than individuals. These are presented here together with their respective contributions as well as the group number assigned to each in van Soldt’s study.</p>

<table class="table-spec-1">
    <tr>
        <th>Line</th>
        <th>Settlement</th>
        <th>Contribution</th>
        <th>Group (<bibl>van Soldt 2005&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>)</th></tr>
    <tr>
        <td>5</td>
        <td>ʾAru</td>
        <td>50</td>
        <td>8</td></tr>
    <tr>
        <td>6</td>
        <td>ḪRLM</td>
        <td>5</td>
        <td>[not treated in <bibl>van Soldt 2005&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>]</td></tr>
    <tr>
        <td>7–8</td>
        <td>KṮTĠLM</td>
        <td>15</td>
        <td>?</td></tr>
    <tr>
        <td>9–10</td>
        <td>ʿRY</td>
        <td>15</td>
        <td>[not treated in <bibl>van Soldt 2005&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>]</td></tr>
    <tr>
        <td>11</td>
        <td>Ṯamrāya</td>
        <td>10</td>
        <td>1N</td></tr>
    <tr>
        <td>12</td>
        <td>Paṯaratu</td>
        <td>10</td>
        <td>4?</td></tr>
    <tr>
        <td>13</td>
        <td>Yaparu</td>
        <td>10</td>
        <td>4</td></tr>
    <tr>
        <td>14</td>
        <td>Maṣibatu</td>
        <td>5</td>
        <td>2a</td></tr>
    <tr>
        <td>15</td>
        <td>Maʾaduḫu</td>
        <td>5</td>
        <td>2a</td></tr>
    <tr>
        <td>16</td>
        <td>Maraʾilu</td>
        <td>5</td>
        <td>2</td></tr>
    <tr>
        <td>17</td>
        <td>Ḫalbu-Ṣapuni</td>
        <td>5</td>
        <td>2</td></tr>
    <tr>
        <td>18</td>
        <td>Šubbanu</td>
        <td>10</td>
        <td>6</td></tr>
    <tr>
        <td>19</td>
        <td>ʾUškanu</td>
        <td>50</td>
        <td>6</td></tr>
    <tr>
        <td>20</td>
        <td>[…]</td>
        <td>20</td>
        <td>[not preserved]</td></tr>
    <tr>
        <td>21</td>
        <td>[…]</td>
        <td>5</td>
        <td>[not preserved]</td></tr>
</table>

<p>This presentation illustrates that, to the extent determinable, TNs appear together with other members of the same group in this list (that is, the groups are not interspersed as they are in some lists). As noted already, the fact that Paṯaratu and Yaparu appear together agrees with their treatment elsewhere and supports van Soldt’s placement of Paṯaratu in Group 4 (<bibl>van Soldt 2005, 90 n. 93, 113 with n. 188&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>).</p>

<p>Regrettably, the three entries of uncertain location (<i>ḫrlm</i>, <i>kṯtġlm</i>, and <i>ʿry</i>) appear together between ʾAru (Group 8) and Ṯamrāya (Group 1N). Since <i>ʿry</i> appears in the right-hand column of RS 94.2614, which otherwise seems to include only northern settlements (as discussed above), its placement here immediately prior to Ṯamrāya may allow us to hypothesize a location among the towns of van Soldt’s Group 1.</p>

<p>This leaves the entries in ll. 6–8, which can only be addressed in the most speculative of terms. As noted already, <i>kṯtġlm</i> cannot presently be placed with any certainty. The etymology of the toponym is unclear (see <button class="GMU-crossreference" onclick="document.getElementById('GMU-1-fn05').focus();document.getElementById('GMU-1-sn05').focus();">n. 5</button>, above)&#8288;, but the final three consonants (<i>-ġlm</i>) have been the subject of debate within studies dedicated to Ugaritic toponymy. Two settlements located in the vicinity of Ugarit’s northern frontier with Mukiš exhibit a final element conventionally normalized as <i>-ḫuliwe</i>, namely, Bītu-ḫuliwe and Ḫarbu-ḫuliwe. The final syllable of the former is spelled with both PI (read by van Soldt, among others, as <i>we</i>) and BI (read <i>bé</i>); the latter TN is attested only with the second of these spellings (see <bibl>van Soldt 2005, 15, 22&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>). Van Soldt (<bibl>2005, 15 n. 88&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>) follows Kühne (<bibl>1974, 166–167&#8288;<VuepressApiPlayground url="/api/eupt/biblio/8HWU6BM5" method="get" :data="[]"/></bibl>) in regarding both TNs as Hurrianized (through the addition of the Hurrian morpheme <i>⸗ve</i>&#8288;/<span class="noBreakEUPT"><i>⸗we</i><span class="GMU-sidenotes"><button class="GMU-sidenotes-inserted-number" id="GMU-1-fnref11" onclick="document.getElementById('GMU-1-fn11').focus();document.getElementById('GMU-1-sn11').focus();"></button><span class="GMU-sidenotes-text liner_3" tabindex="0"><span id="GMU-1-sn11" tabindex="0"></span>For this genitive morpheme and case inflection in Hurrian, see <bibl>Giorgieri 1999&#8288;<VuepressApiPlayground url="/api/eupt/biblio/9GVHN4ZR" method="get" :data="[]"/></bibl>; <bibl>Wegner 2007, 65–66&#8288;<VuepressApiPlayground url="/api/eupt/biblio/T5J8N9V2" method="get" :data="[]"/></bibl>; <bibl>Campbell 2015, 15–16&#8288;<VuepressApiPlayground url="/api/eupt/biblio/7SCU795Q" method="get" :data="[]"/></bibl>.</span></span>).</span> He notes the further, though less certain, possibility that the TN represented in syllabic cuneiform as URU <i>ḫar-ba-ḫu-li-bé</i> (RS 17.062<sup>+</sup>:15' [= <bibl>PRU IV&#8288;<VuepressApiPlayground url="/api/eupt/biblio/RWCEUKZ9" method="get" :data="[]"/></bibl>, p. 66]&#8288;) may be identical to the alphabetically written TN <i>ḫrbġlm</i> (<bibl>van Soldt 2005, 22 with n. 155&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>), a possibility first raised tentatively by Nougayrol (<bibl>1970, 146&#8288;<VuepressApiPlayground url="/api/eupt/biblio/RKFRZIRT" method="get" :data="[]"/></bibl>), then, more explicitly by Dietrich, Loretz, and Sanmartín (<bibl>1973, 110 with n. 1&#8288;<VuepressApiPlayground url="/api/eupt/biblio/KVK6KNF8" method="get" :data="[]"/></bibl>), and subsequently accepted by others (e.g., <bibl>Heltzer 1976, 10&#8288;<VuepressApiPlayground url="/api/eupt/biblio/J8EPQU7B" method="get" :data="[]"/></bibl>; <bibl>1979, 487–488 n. 224&#8288;<VuepressApiPlayground url="/api/eupt/biblio/CDC4D2J3" method="get" :data="[]"/></bibl>; <bibl>Astour 1981a, 9 n. 53&#8288;<VuepressApiPlayground url="/api/eupt/biblio/3NSZZJRI" method="get" :data="[]"/></bibl>; <bibl>1981b, 14&#8288;<VuepressApiPlayground url="/api/eupt/biblio/SXKK2RU9" method="get" :data="[]"/></bibl>; <bibl>1995, 67 n. 86&#8288;<VuepressApiPlayground url="/api/eupt/biblio/H3QGR475" method="get" :data="[]"/></bibl>; cf. <bibl>Kühne 1974, 167 n. 73&#8288;<VuepressApiPlayground url="/api/eupt/biblio/8HWU6BM5" method="get" :data="[]"/></bibl>; for further relevant bibliography, see <bibl>del Olmo Lete and Sanmartín 2015, 398&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>).</p>

<p>If this equivalence is correct, then <i>kṯtġlm</i> may represent a similarly formed TN, and, given the placement of Bītu-ḫuliwe and Ḫarbu-ḫuliwe in Group 1N, perhaps <i>kṯtġlm</i> may be similarly located. This is admittedly speculative, resting entirely on a presumption that the existence of two similarly formed TNs on the northern frontier of the kingdom provides <i>prima facie</i> grounds for situating a third TN of the same sort in the same region, which need not be the case. It would, however, align with the likely northern placement of <i>ʿry</i> and Ṯamrāya, which follow it immediately in the text under discussion.</p>

<p>Turning to <i>ḫrlm</i>, methodological prudence requires that we leave the placement of this TN open for the moment. Nevertheless, I register here (with due caution) that, should this represent a simple error for an intended <i>ḫr&lt;bġ&gt;lm</i>, then this TN would (if it is in fact the alphabetic counterpart to syllabic Ḫarbu-ḫuliwe [see above]&#8288;) represent another entry in this section of RIH 83/47<sup>+</sup> belonging to van Soldt’s Group 1N, located at the northern frontier of the kingdom.</p>

<p>These new toponymic identifications facilitate the uniform treatment of the contributors listed: all are best understood as the names of towns within the territory of the kingdom of Ugarit. Their arrangement in this list, however, is no clearer today than previously. The list begins with ʾAru, located at the southern end of the kingdom (see <bibl>Burlingame forthcoming&#8288;<VuepressApiPlayground url="/api/eupt/biblio/EFQ7IMZX" method="get" :data="[]"/></bibl> for detailed discussion), moves to three towns of uncertain location, but possibly to be situated together with Ṯamrāya on Ugarit’s northern frontier, moves next to towns in Group 4, just north of the city of Ugarit itself, then to Group 2a/2 at the northwest corner of the kingdom, and closes with entries from Group 6, occupying the southern coastal plain east/southeast of Lattakia. It is to be hoped that further work on the recently published administrative documents from Ras Ibn Hani will permit a clearer understanding of this text and its organization, but in the meantime, the entries treated here can be safely added to the growing body of evidence for the toponymy and topography of the kingdom of Ugarit.</p>

<h4>3. A Revised Translation</h4>

<p>To close this study, I offer here a revised presentation of the text with translation. I provisionally follow the editors’ understanding of the opening section appearing in lines 1–4 though, as noted above, the unusual wording found here precludes certainty.</p>

<table class="GMU-ugaritic-text-table">
    <tr>
        <th colspan="2">RIH 83/47<sup>+</sup> = RIH II, no. 43</th>
    </tr>
    <tr>
        <td><i>Obverse</i></td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">1</span> tgmr . k[s]⸢p⸣</td>
        <td>Total amount of silver that has</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">2</span> d ʿly . w d [ʿ]⸢l⸣</td>
        <td>“gone up” (is registered as owed?), and that is to the debit</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">3</span> ʿmyn . ḫm[š]⸢t⸣</td>
        <td>of ʿAmmuyānu: two hundred</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">4</span> ʿšrt . k⸢bd⸣ mi͗tm</td>
        <td>and fifteen (shekels).</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">5</span> a͗<sup>!</sup>r . ḫmšm</td>
        <td>(From the city of) ʾAru: fifty (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">6</span> ḫrlm . ḫmšt</td>
        <td>(From the city of) ḪRLM: five (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">7</span> kṯtġlm . ḫmšt</td>
        <td>(From the city of) KṮTĠLM: fifteen (shekels);</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">8</span> ʿšrt</td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">9</span> ʿry . ḫmšt</td>
        <td>(From the city of) ʿRY: fifteen (shekels);</td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">10</span> ʿšrt</td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">11</span> ṯmry . ʿšrt</td>
        <td>(From the city of) Ṯamrāya: ten (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">12</span> pṯrt . ʿšrt</td>
        <td>(From the city of) Paṯaratu: ten (shekels);</td></tr>
    <tr>
        <td></td>
        <td></td></tr>
    <tr>
        <td><i>Lower edge</i></td>
        <td></td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">13</span> ypr . ʿšrt</td>
        <td>(From the city of) Yaparu: ten (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td></td>
        <td></td></tr>
    <tr>
        <td><i>Reverse</i></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">14</span> mṣbt . ḫmšt</td>
        <td>(From the city of) Maṣibatu: five (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">15</span> ma͗dḫ . ḫmšt</td>
        <td>(From the city of) Maʾaduḫu: five (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">16</span> mri͗l<sup>!</sup> . ḫmšt</td>
        <td>(From the city of) Maraʾilu: five (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">17</span> ḫlb ṣpn . ḫmšt</td>
        <td>(From the city of) Ḫalbuṣapuni: five (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">18</span> šbn . ʿšrt</td>
        <td>(From the city of) Šubbanu: ten (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">19</span> u͗škn . ḫmšm</td>
        <td>(From the city of) ʾUškanu: fifty (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">20</span> [&#8239;&#8239;&#8239;&#8239;&#8239;.] ⸢ʿ⸣šrm</td>
        <td>[(From the city of) …]: [t]wenty (shekels);</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
    <tr>
        <td><span class="ugaritic-text-line-number">21</span> [&#8239;&#8239;&#8239;&#8239;&#8239;. ḫ]mšt</td>
        <td>[(From the city of) …]: [f]ive (shekels).</td></tr>
    <tr>
        <td><hr></td>
        <td></td></tr>
</table>

<h4>Acknowledgments</h4>
<p>I would like to express special appreciation to Dennis Pardee, who read an earlier version of this article and provided a number of very helpful suggestions, as well as to the anonymous peer reviewers, who further contributed to its improvement and clarity. Of course, I bear sole responsibility for any faults that may remain.</p>

<h4>
    <button class="GMU-tabs-label-link GMU-tabs-label-link-with-reficon" onclick="document.getElementById('top-link').click(); setTimeout(() => document.getElementById('tab3').click(), 10);">Bibliography</button>
</h4>

</div>

<div class="GMU-endnotes">
<h4 style="margin: 2em 0 1em !important;">Notes</h4>
<ol>
<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn01" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref01').focus();"></button>Note that Lagarce refers to this king as ʿAmmiṯtamru II, reflecting the traditional enumeration of Ugaritian kings found in works like <bibl>Yon 1997&#8288;<VuepressApiPlayground url="/api/eupt/biblio/EJS397IQ" method="get" :data="[]"/></bibl> / <bibl>2006&#8288;<VuepressApiPlayground url="/api/eupt/biblio/S8HS8DWE" method="get" :data="[]"/></bibl> and <bibl>Singer 1999&#8288;<VuepressApiPlayground url="/api/eupt/biblio/927CGDRX" method="get" :data="[]"/></bibl>. The numbering employed here follows the revised system proposed in <bibl>Arnaud 1999&#8288;<VuepressApiPlayground url="/api/eupt/biblio/PE233XRM" method="get" :data="[]"/></bibl>.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn02" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref02').focus();"></button>The <bibl>edition&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl> has “<i>Maʾadiḫu</i>,” but it is clear from the remark on p. 117 that <i>Maʾaduḫu</i> was intended.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn03" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref03').focus();"></button>Cases of personal names that were identical to toponyms were already known, but the editors’ identification of the first clear evidence for the personal name <i>u͗škn</i>, found in RIH 83/17<sup>+</sup>:27' (= <bibl>RIH II&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6HNWTZM8" method="get" :data="[]"/></bibl>, no. 9), demonstrated that even greater caution was required than most would have previously thought necessary (see <bibl>Bordreuil and Pardee 2019, 62, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>). Prior to the discovery of this evidence, no interpreter would have hesitated to identify an instance of {u͗škn} in an alphabetic Ugaritic text with the prominent settlement of that name located in the southern coastal plain of the kingdom, but the new textual evidence from Ras Ibn Hani demonstrated beyond doubt that even such apparently obvious identifications could turn out to be misleading.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn04" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref04').focus();"></button>Moreover, as this text was first published in 2019, the lexeme <i>ḫrlm</i> does not appear in the dictionary of del Olmo Lete and Sanmartín, the most recent edition of which was published in <bibl>2015&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn05" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref05').focus();"></button>The first of these attestations, appearing in RS 17.392, is potentially ambiguous, as it refers to <i>nsk kṯtġlm</i>, “the founder(s) of <i>kṯtġlm</i>.” The same text mentions <i>nsk a͗rym</i> (ll. 5–6) and <i>nsk a͗rt</i> (l. 8). The latter of these clearly involves a TN (“founder(s) of ʾArutu”), but the former involves a gentilic (“founder(s) of the men of ʾAru”). In other words, the case could be made for interpreting <i>kṯtġlm</i> either as a TN (like <i>a͗rt</i> in l. 8) or as a plural term referring to a group of individuals, comparable to the toponymically defined group <i>a͗rym</i> in l. 6. Virolleaud seems to have had the latter possibility in mind when he suggested in his <i>editio princeps</i> of RS 17.392 (<bibl>1957, 170&#8288;<VuepressApiPlayground url="/api/eupt/biblio/B5IEMJHE" method="get" :data="[]"/></bibl>) that <i>kṯtġlm</i> might represent “un mot composé au pl.” like <i>mḏrġlm</i> and <i>ḫḏġlm</i> (both terms referring to professional groups and incorporating the Hurrian morpheme <i>⸗o⸗ḫ</i>(<i>e</i>)<i>⸗li</i> [<i>-uḫli</i>], used to form Hurrian <i>nomina actoris</i> [see <bibl>Wegner 2007, 57–58&#8288;<VuepressApiPlayground url="/api/eupt/biblio/T5J8N9V2" method="get" :data="[]"/></bibl>]&#8288;). Such a classification was suggested more explicitly by Friedrich (<bibl>1961, 36&#8288;<VuepressApiPlayground url="/api/eupt/biblio/U5ZRZ6HK" method="get" :data="[]"/></bibl>: “<span lang="de">ugar. Berufsbezeichnung</span>”), Aistleitner (<bibl>1963, 160&#8288;<VuepressApiPlayground url="/api/eupt/biblio/SVRCMW4F" method="get" :data="[]"/></bibl>: “Bez. e. Berufs?”), and Dietrich and Loretz (<bibl>1966, 199–200&#8288;<VuepressApiPlayground url="/api/eupt/biblio/U8WMAEFB" method="get" :data="[]"/></bibl>).<br/>The discovery of RS 19.105 (= KTU 4.643) in 1955 (published in 1965) shed new and clearer light on the question, as the form {kṯtġlm} appeared following the preposition <i>b</i> “in” in l. 26 of the text and, by emendation, in the preceding line as well ({kṯ&lt;t&gt;ġlm}), in a context in which the resulting prepositional phrase could only be understood to indicate the town in which the individuals named at the beginning of each of these lines were to be found. Virolleaud (<bibl>1965, 142&#8288;<VuepressApiPlayground url="/api/eupt/biblio/26XADU5S" method="get" :data="[]"/></bibl>) described the text as a “[l]iste de gens habitant diverses villes,” suggesting a toponymic status for <i>kṯtġlm</i>, and he listed this term in the index of “villes et pays” (<bibl>1965, 166&#8288;<VuepressApiPlayground url="/api/eupt/biblio/26XADU5S" method="get" :data="[]"/></bibl>). Gordon (<bibl>1965&#8288;<VuepressApiPlayground url="/api/eupt/biblio/7FHGSMSJ" method="get" :data="[]"/></bibl> / <bibl>1998, 425 [sec. 19.1336]&#8288;<VuepressApiPlayground url="/api/eupt/biblio/ST3JFGZQ" method="get" :data="[]"/></bibl>) drew on this new text and interpretation in producing his glossary entry for <i>kṯtġlm</i>, where it is treated as a place name (though without discussion). This new understanding of the term was quickly accepted by others, who noted that it was in fact preferable for RS 17.392 on internal grounds as well, as both <i>a͗rym</i> and <i>a͗rt</i> pertained to towns of origin (<bibl>Dietrich, Loretz, and Sanmartín 1973, 110&#8288;<VuepressApiPlayground url="/api/eupt/biblio/KVK6KNF8" method="get" :data="[]"/></bibl>; <bibl>Pardee 1974, 280 n. 26&#8288;<VuepressApiPlayground url="/api/eupt/biblio/27NP7XT5" method="get" :data="[]"/></bibl>).<br/>While some continued to hold that a term referring to a professional group (typically “bowmakers” is assumed) provided the etymology of the term (e.g., <bibl>Astour 1977, 129&#8288;<VuepressApiPlayground url="/api/eupt/biblio/FP5PMKHU" method="get" :data="[]"/></bibl>; <bibl>1981b, 14 with n. 16&#8288;<VuepressApiPlayground url="/api/eupt/biblio/SXKK2RU9" method="get" :data="[]"/></bibl>; <bibl>Sanmartín 1995, 180–181&#8288;<VuepressApiPlayground url="/api/eupt/biblio/2R8DAWWZ" method="get" :data="[]"/></bibl>; <bibl>Watson 2001, 117&#8288;<VuepressApiPlayground url="/api/eupt/biblio/A8MS8RCG" method="get" :data="[]"/></bibl>), its toponymic value was generally accepted (exceptions include <bibl>Laroche 1980, 139&#8288;<VuepressApiPlayground url="/api/eupt/biblio/PV9WHNC2" method="get" :data="[]"/></bibl> [citing <bibl>Dietrich and Loretz 1966&#8288;<VuepressApiPlayground url="/api/eupt/biblio/U8WMAEFB" method="get" :data="[]"/></bibl>, though these authors no longer held to the position outlined there, as noted above] and <bibl>Heltzer 1982, 89 with n. 34&#8288;<VuepressApiPlayground url="/api/eupt/biblio/62ICIFPN" method="get" :data="[]"/></bibl> [rejecting the position outlined by Lipiński in an editorial note appearing in <bibl>Heltzer 1979, 478–488 n. 224&#8288;<VuepressApiPlayground url="/api/eupt/biblio/CDC4D2J3" method="get" :data="[]"/></bibl>; he does not, however, address the clear indications of the toponymic value of the term that Lipiński’s editorial remark had identified]&#8288;) and remains normative today (see <bibl>Belmonte Marín 2001, 165&#8288;<VuepressApiPlayground url="/api/eupt/biblio/AXPM75DV" method="get" :data="[]"/></bibl> and <bibl>del Olmo Lete and Sanmartín 2015, 468&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>).</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn06" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref06').focus();"></button>Bordreuil may originally have read this entry as {ʿky} (see his remarks <i>apud</i> <bibl>Bordreuil et al. 1984, 427&#8288;<VuepressApiPlayground url="/api/eupt/biblio/2HCRZ8H6" method="get" :data="[]"/></bibl>; <bibl>Bordreuil and Pardee 2019, 116 with n. 130&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>; see further <bibl>Cunchillos 1989, 357 n. 30&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6E8Q2VEC" method="get" :data="[]"/></bibl>). While the second sign of this sequence shows some damage in the published photograph, the editors indicate that the reading {ʿry} is certain (<bibl>Bordreuil and Pardee 2019, 116&#8288;<VuepressApiPlayground url="/api/eupt/biblio/I577SVEK" method="get" :data="[]"/></bibl>).</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn07" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref07').focus();"></button>For Paṯaratu, see <bibl>van Soldt 2005, 38&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>; for additional bibliography, see <bibl>Belmonte Marín 2001, 219&#8288;<VuepressApiPlayground url="/api/eupt/biblio/AXPM75DV" method="get" :data="[]"/></bibl> and <bibl>del Olmo Lete and Sanmartín 2015, 677&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl> (<i>pṯrty</i>). For Yaparu, see <bibl>van Soldt 2005, 25–26&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>; for additional bibliography, see <bibl>Belmonte Marín 2001, 341&#8288;<VuepressApiPlayground url="/api/eupt/biblio/AXPM75DV" method="get" :data="[]"/></bibl> and <bibl>del Olmo Lete and Sanmartín 2015, 960&#8288;<VuepressApiPlayground url="/api/eupt/biblio/Q8IJJAN2" method="get" :data="[]"/></bibl>.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn08" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref08').focus();"></button>See references in preceding footnote for lists of attestations.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn09" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref09').focus();"></button>These data are presented and examined in van Soldt (<bibl>2005, ch. 5&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>) and suggest that ʾAru and ʾUškanu represented the towns of greatest administrative significance—and possibly size—in the southern plain (<bibl>van Soldt 2005, 127&#8288;<VuepressApiPlayground url="/api/eupt/biblio/MTKV2X2S" method="get" :data="[]"/></bibl>). Further discussion and bibliography can be found in <bibl>Burlingame forthcoming&#8288;<VuepressApiPlayground url="/api/eupt/biblio/EFQ7IMZX" method="get" :data="[]"/></bibl>.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn10" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref10').focus();"></button>One of the anonymous peer reviewers makes the clever observation that the final entry in the list may have comprised an unanticipated/last-minute addition to the list. In other words, the total of 215 was written down first, followed by the entries occupying lines 5–20, which do reach the stated total. The final entry of 5 shekels may then have been added after the fact. Though impossible to prove, this is imaginable and would account for the discrepancy.</li>

<li class="GMU-endnotes-entry" tabindex="0"><span class="GMU-endnotes-entry-Sprungmarke" id="GMU-1-fn11" tabindex="0"></span><button class="GMU-endnotes-backToText-button" onclick="document.getElementById('GMU-1-fnref11').focus();"></button>For this genitive morpheme and case inflection in Hurrian, see <bibl>Giorgieri 1999&#8288;<VuepressApiPlayground url="/api/eupt/biblio/9GVHN4ZR" method="get" :data="[]"/></bibl>; <bibl>Wegner 2007, 65–66&#8288;<VuepressApiPlayground url="/api/eupt/biblio/T5J8N9V2" method="get" :data="[]"/></bibl>; <bibl>Campbell 2015, 15–16&#8288;<VuepressApiPlayground url="/api/eupt/biblio/7SCU795Q" method="get" :data="[]"/></bibl>.</li>

</ol>

</div>

</div>

<input id="tab3" type="radio" name="tabs">
<label class="GMU-article-tabs-label" for="tab3"><p>Bibliography</p></label>
<div class="GMU-article-tabs-content">

<h4>Abbreviations</h4>
    <p style="margin-top: 1em;">The abbreviations generally follow <a href="https://rla.badw.de/reallexikon.html" target="_blank">RlA</a> (Streck, Michael P., et al., eds. 1928–2016. <i>Reallexikon der Assyriologie und Vorderasiatischen Archäologie</i>. Berlin etc.: De Gruyter [Download <a href="https://rla.badw.de/fileadmin/user_upload/Files/RLA/03_Abkverz_Ende_Nov2018.pdf" target="_blank">PDF</a>]&#8288;) and EUPT (<a href="/Abkuerzungen.html" target="_blank">list of abbreviations</a>). Additional and deviating abbreviations:</p>
    <div class="Absatz-hängend">PRU IV = <bibl>Nougayrol 1956&#8288;<VuepressApiPlayground url="/api/eupt/biblio/RWCEUKZ9" method="get" :data="[]"/></bibl>.</div>
    <div class="Absatz-hängend">RIH II = <bibl>Bordreuil, Pardee, and Roche-Hawley 2019&#8288;<VuepressApiPlayground url="/api/eupt/biblio/6HNWTZM8" method="get" :data="[]"/></bibl>.</div>
    <div class="Absatz-hängend">RSO XVIII = <bibl>Bordreuil and Pardee, with Hawley 2012&#8288;<VuepressApiPlayground url="/api/eupt/biblio/K72NT4DC" method="get" :data="[]"/></bibl>.</div>

<h4>References <a href="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/tags/GMU-1/collection" target="_blank"><img class="Bibliography-Logo-zotero" src="https://www.zotero.org/support/_media/logo/zotero_32x32x32.png"></a></h4>
<p class="GMU-bibliography-entry">Aistleitner, Joseph. 1963. <i>Wörterbuch der ugaritischen Sprache</i>. Berichte über die Verhandlungen der sächsischen Akademie der Wissenschaften zu Leipzig: Philologisch-historische Klasse 106.3. Berlin: Akademie-Verlag.</p>

<p class="GMU-bibliography-entry">Arnaud, Daniel. 1999. “Prolégommènes à la rédaction d’une histoire d’Ougarit II: les bordereaux de rois divinisés.” <i>Studi Micenei ed Egeo-Anatolici</i> 41: 153–173.</p>

<p class="GMU-bibliography-entry">Astour, Michael C. 1977. “Continuité et changement dans la toponymie de la Syrie du nord.” In <i>La toponymie antique&nbsp;: actes du colloque de Strasbourg, 12-14 juin 1975</i>, 117–141. Travaux du Centre de Recherche sur le Proche-Orient et la Grèce Antiques 4. Leiden: Brill.</p>

<p class="GMU-bibliography-entry">Astour, Michael C. 1981a. “Les frontières et les districts du royaume d’Ugarit.” <i>Ugarit-Forschungen</i> 13: 1–12.</p>

<p class="GMU-bibliography-entry">Astour, Michael C. 1981b. “Toponymic Parallels between the Nuzi Area and Northern Syria.” In <i>Studies on the Civilization and Culture of Nuzi and the Hurrians in Honor of Ernest R. Lacheman on His Seventy-Fifth Birthday, April 29, 1981</i>, edited by Martha A. Morrison and David I. Owen, 11–26. Winona Lake: Eisenbrauns.</p>

<p class="GMU-bibliography-entry">Astour, Michael C. 1995. “La topographie du royaume d’Ougarit.” In <i>Le pays d’Ougarit autour de 1200 av. J.-C. Histoire et archéologie. Actes du Colloque International Paris 28 juin – 1er juillet 1993</i>, edited by Marguerite Yon, Maurice Sznycer, and Pierre Bordreuil, 56–71. Ras Shamra–Ougarit XI. Paris: Éditions Recherche sur les Civilisations.</p>

<p class="GMU-bibliography-entry">Belmonte Marín, Juan Antonio. 2001. <i>Die Orts- und Gewässernamen der Texte aus Syrien im 2. Jt. v. Chr</i>. Répertoire Géographique des Textes Cunéiformes 12/2. Wiesbaden: Dr. Ludwig Reichert Verlag.</p>

<p class="GMU-bibliography-entry">Bordreuil, Pierre, Jacques Lagarce, Élisabeth Lagarce, Adnan Bounni, and Nassib Saliby. 1984. “Les découvertes archéologiques et épigraphiques de Ras Ibn Hani (Syrie) en 1983&nbsp;: un lot d’archives administratives.” <i>Comptes-rendus des séances de l'Académie des inscriptions et belles-lettres</i> 128/2: 398–438.</p>

<p class="GMU-bibliography-entry">Bordreuil, Pierre, and Dennis Pardee. 2012. “Bordereaux et listes (n<sup>os</sup> 1-55).” In <i>Une bibliothèque au sud de la ville***. Textes 1994-2002 en cunéiforme alphabétique de la Maison d’Ourtenou</i>, by Pierre Bordreuil and Dennis Pardee, with Robert Hawley, 11–133. Ras Shamra–Ougarit XVIII. Lyon: Maison de l’Orient et de la Méditerranée.</p>

<p class="GMU-bibliography-entry">Bordreuil, Pierre, and Dennis Pardee. 2019. “Les textes ougaritiques.” In <i>Ras Ibn Hani II. Les textes en écritures cunéiformes de l’âge du Bronze récent (fouilles 1977 à 2002)</i>, by Pierre Bordreuil, Dennis Pardee, and Carole Roche-Hawley, 39–280. Bibliothèque Archéologique et Historique 214. Beirut: Presses de l’IFPO.</p>

<p class="GMU-bibliography-entry">Bordreuil, Pierre, and Dennis Pardee, with Robert Hawley. 2012. <i>Une bibliothèque au sud de la ville***. Textes 1994-2002 en cunéiforme alphabétique de la maison d’Ourtenou</i>. Ras Shamra–Ougarit XVIII. Lyon: Maison de l’Orient et de la Méditerranée.</p>

<p class="GMU-bibliography-entry">Bordreuil, Pierre, Dennis Pardee, and Carole Roche-Hawley. 2019. <i>Ras Ibn Hani II. Les textes en écritures cunéiformes de l’âge du Bronze récent (fouilles 1977 à 2002)</i>. Bibliothèque Archéologique et Historique 214. Beirut: Presses de l’IFPO.</p>

<p class="GMU-bibliography-entry">Burlingame, Andrew. Forthcoming. “An Update on ʾAru and Arruwa.” To appear in <i>Ugarit and Its World</i>, edited by Shirly Natan-Yulzary, Reinhard Müller, and Clemens Steinberger. Kasion. Münster: Zaphon.</p>

<p class="GMU-bibliography-entry">Campbell, Dennis R. M. 2015. <i>Mood and Modality in Hurrian</i>. Languages of the Ancient Near East 5. Winona Lake: Eisenbrauns.</p>

<p class="GMU-bibliography-entry">Cunchillos, Jesús-Luis. 1989. “Correspondance. Introduction, traduction commentaire.” In <i>Textes ougaritiques II</i>, by André Caquot, Jean-Michel de Tarragon, and Jésus-Luis Cunchillos, 239–421. Littératures Anciennes du Proche-Orient 14. Paris: Cerf.</p>

<p class="GMU-bibliography-entry">Dietrich, Manfried, and Oswald Loretz. 1966. “Die soziale Struktur von Alalaḫ und Ugarit: I. Die Berufsbezeichnungen mit der hurritischen Endung <i>-ḫuli</i>.” <i>Die Welt des Orients</i> 3: 188–205.</p>

<p class="GMU-bibliography-entry">Dietrich, Manfried, Oswald Loretz, and Joaquín Sanmartín. 1973. “Zur ugaritischen Lexikographie (VIII). Lexikographische Einzelbemerkungen.” <i>Ugarit-Forschungen</i> 5: 105–117.</p>

<p class="GMU-bibliography-entry">Friedrich, Johannes. 1961. <i>Hethitisches Wörterbuch. Kurzgefasste kritische Sammlung der Deutungen hethitischer Wörter</i>. 2. Ergänzungsheft. Heidelberg: Carl Winter.</p>

<p class="GMU-bibliography-entry">Giorgieri, Mauro. 1999. “Die hurritischen Kasusendungen.” In <i>Nuzi at Seventy-Five</i>, edited by David I. Owen and Gernot Wilhelm, 223–256. Studies on the Civilization and Culture of Nuzi and the Hurrians 10. Bethesda: CDL Press.</p>

<p class="GMU-bibliography-entry">Gordon, Cyrus H. 1965. <i>Ugaritic Textbook: Grammar, Texts in Transliteration, Cuneiform Selections, Glossary, Indices</i>. Analecta Orientalia 38. Rome: Pontifical Biblical Institute.</p>

<p class="GMU-bibliography-entry">Gordon, Cyrus H. 1998. <i>Ugaritic Textbook: Grammar, Texts in Transliteration, Cuneiform Selections, Glossary, Indices. Revised Reprint</i>. Analecta Orientalia 38. Rome: Pontifical Biblical Institute.</p>

<p class="GMU-bibliography-entry">Heltzer, Michael. 1976. <i>The Rural Community in Ancient Ugarit</i>. Wiesbaden: Dr. Ludwig Reichert Verlag.</p>

<p class="GMU-bibliography-entry">Heltzer, Michael. 1979. “Royal Economy in Ancient Ugarit.” In <i>State and Temple Economy in the Ancient Near East II</i>, edited by Edward Lipiński, 459–496. Orientalia Lovaniensia Analecta 6. Leuven: Departement Oriëntalistiek.</p>

<p class="GMU-bibliography-entry">Heltzer, Michael. 1982. <i>The Internal Organization of the Kingdom of Ugarit (Royal Service-System, Taxes, Royal Economy, Army and Administration)</i>. Wiesbaden: Dr. Ludwig Reichert Verlag.</p>

<p class="GMU-bibliography-entry">Kühne, Cord. 1974. “Mit Glossenkeilen markierte fremde Wörter in akkadischen Ugarittexten.” <i>Ugarit-Forschungen</i> 6: 157–167.</p>

<p class="GMU-bibliography-entry">Laroche, Emmanuel. 1980. <i>Glossaire de la langue hourrite</i>. Paris: Éditions Klincksieck.</p>

<p class="GMU-bibliography-entry">Nougayrol, Jean. 1956. <i>Le Palais royal d’Ugarit IV. Textes accadiens des archives sud (Archives internationales)</i>. Mission de Ras Shamra IX. Paris: Imprimerie Nationale.</p>

<p class="GMU-bibliography-entry">Nougayrol, Jean. 1970. <i>Le Palais royal d’Ugarit VI. Textes en cunéiformes babyloniens des archives du grand palais et du palais sud d’Ugarit</i>. Mission de Ras Shamra XII. Paris: Imprimerie Nationale.</p>

<p class="GMU-bibliography-entry">del Olmo Lete, Gregorio, and Joaquín Sanmartín. 2015. <i>A Dictionary of the Ugaritic Language in the Alphabetic Tradition</i>. 3<sup>rd</sup> rev. ed. Handbuch der Orientalistik 1.112. Leiden: Brill.</p>

<p class="GMU-bibliography-entry">Pardee, Dennis. 1974. “The Ugaritic Text 147(90).” <i>Ugarit-Forschungen</i> 6: 275–282.</p>

<p class="GMU-bibliography-entry">Pardee, Dennis. 2019. “Catalogue raisonné des textes de Ras Ibn Hani 1977-2002.” In <i>Ras Ibn Hani II. Les textes en écritures cunéiformes de l’âge du Bronze récent (fouilles 1977 à 2002)</i>, by Pierre Bordreuil, Dennis Pardee, and Carole Roche-Hawley, 31–36. Bibliothèque Archéologique et Historique 214. Beirut: Presses de l’IFPO.</p>

<p class="GMU-bibliography-entry">Saadé, Gabriel. 2011. <i>Ougarit et son royaume des origines à sa destruction</i>, edited by Marguerite Yon and Leila Badre. Bibliothèque Archéologique et Historique 193. Beirut: Presses de l’IFPO.</p>

<p class="GMU-bibliography-entry">Sanmartín, Joaquín. 1995. “Das Handwerk in Ugarit: Eine lexikalische Studie.” <i>Studi epigrafici e linguistici sul Vicino Oriente antico</i> 12: 169–190.</p>

<p class="GMU-bibliography-entry">Singer, Itamar. 1999. “A Political History of Ugarit.” In <i>Handbook of Ugaritic Studies</i>, edited by Wilfred G. E. Watson and Nicolas Wyatt, 603–733. Handbuch der Orientalistik 1.39. Leiden: Brill.</p>

<p class="GMU-bibliography-entry">van Soldt, Wilfred H. 2005. <i>The Topography of the City-State of Ugarit</i>. Alter Orient und Altes Testament 324. Münster: Ugarit-Verlag.</p>

<p class="GMU-bibliography-entry">Virolleaud, Charles. 1957. <i>Le Palais royal d’Ugarit II. Textes en cunéiformes alphabétiques des archives est, ouest et centrales</i>. Mission de Ras Shamra VII. Paris: Imprimerie Nationale.</p>

<p class="GMU-bibliography-entry">Virolleaud, Charles. 1965. <i>Le Palais royal d’Ugarit V. Textes en cunéiformes alphabétiques des archives sud, sud-ouest et du petit palais</i>. Mission de Ras Shamra XI. Paris: Imprimerie Nationale.</p>

<p class="GMU-bibliography-entry">Watson, Wilfred G. E. 2001. “The Lexical Aspect of Ugaritic Toponyms.” <i>Aula Orientalis</i> 19: 109–123.</p>

<p class="GMU-bibliography-entry">Wegner, Ilse. 2007. <i>Einführung in die hurritische Sprache. 2., überarbeitete Auflage</i>. Wiesbaden: Harrassowitz.</p>

<p class="GMU-bibliography-entry">Yon, Marguerite. 1997. <i>La cité d’Ougarit sur le tell de Ras Shamra</i>. Paris: Éditions Recherche sur les Civilisations.</p>

<p class="GMU-bibliography-entry">Yon, Marguerite. 2006. <i>The City of Ugarit at Tell Ras Shamra</i>. Winona Lake: Eisenbrauns.</p>

</div>

</div>

</div>