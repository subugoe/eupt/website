FROM node:20-alpine AS builder

WORKDIR /app

COPY src src
COPY package.json package.json
COPY .npmrc .npmrc

RUN npm install
RUN npm install vuepress-api-playground --legacy-peer-deps
RUN npm run build

FROM nginx

COPY --from=builder /app/src/.vuepress/dist /usr/share/nginx/html
