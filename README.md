# EUPT Website

The website runs at [https://eupt-dev.sub.uni-goettingen.de/](https://eupt-dev.sub.uni-goettingen.de/).

## Getting started

### Prerequisites

* node 20

```sh
git clone https://gitlab.gwdg.de/subugoe/eupt/website.git
cd website
docker build -t eupt-website .
docker run --rm -p 8080:80 eupt-website
```

## Updating website content

### Adding/editing files or directories

Webpages can be added and edited in the `src` directory. You can either:

* navigate into the directory and then click on `+` > `New file` or `New directory` to add a single file or directory
* navigate to a file and click on `Edit` > `Edit single file` to edit a single file
* anywhere in the repository click on `Edit` > `(Open in) Web IDE` to add/edit multiple files or directories

**Note:** New pages must have the file extension `.md` (Markdown file).

Each webpage should start with the following header (change `[Webpage Title]` and `[Webpage Content]` to your needs):

```yaml
---
title: [Webpage Title]
lang: de
layout: Layout
---

# {{ $frontmatter.title }}

[Webpage Content]
```

#### Previewing a page

You can preview a webpage as follows:

* in single-file editing mode: click on `Preview` (on the top left)
* in Web IDE: click on the `Open Preview` symbol (on the top right)

#### Changing the website navigation

If you want to change the website navigation, you have to update `src/.vuepress/contents.js`.

For example, to add the webpage `src/webpage.md` to the website navigation, add the following to the `navbar` element (`[Webpage Name]` is the text shown in the website navigation, usually identical to `[Webpage Title]` or a short form of it):

```js
    navbar: [
      { text: '[Webpage Name]', link: '/webpage.html' }
    ]
```

**Note:** The link ends with `.html` instead of `.md`.

If you want to add a webpage within a sub-directory, e.g. `src/subdir/webpage.md`, add the following to the `navbar` element:

```js
    navbar: [
      { text: '[Subdir Name]',
        children: [
          { text: '[Webpage Name]', link: '/subdir/webpage.html' }
        ]
      }
    ]
```

(More webpages within the same sub-directory should also be added within the `children` element.)

#### Preventing syntax errors

While editing the `config.js` in Web IDE, you can check whether it contains any errors by clicking on `≡` (on the top left) > `Terminal` > `New Terminal` > `PROBLEMS`.

### Committing changes

After adding/editing one or multiple files, enter a commit message and click on `Commit changes`.  
In the Web IDE you have to click on the `Source Control` symbol (on the left), enter a commit message and click on `Commit to 'main'`.

## Releases

In order to release a new version of the website to the production server, perform the following steps:

1. Merge the `develop` branch into `main`.
2. Take note of the new version created automatically. This version number is stated in the commit message that is created by the GitLab CI Template.
3. Switch to the `website-helm` repository and pull the latest changes of `main`.
4. Create and push a tag pointing to the most recent commit with the following structure: `website-X.Y.Z` where `X.Y.Z.` is the version number created in step 2.
5. Switch to the `argocd-provisioning` repository. Pull the latest changes of `main` and create a new branch for the version update.
6. In `deployments/prod/applications/templates/eupt-website.yaml` change the `targetRevision` value to the tag name you created in step 4. Save, commit, push.
7. Create a merge request in `argocd-provisioning` pointing to `main`.
8. Merge. @mgoebel said it's okay to simply merge branches where only a version update takes place.

ArgoCD will take a while before the changes apply.

:warning: For a full update of the EUPT website, a release must be made both for
the TextAPI and the website.
